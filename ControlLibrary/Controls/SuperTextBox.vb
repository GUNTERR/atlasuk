Public Class SuperTextBox
    Inherits System.Windows.Forms.TextBox

    Public Event QueryableChange()
    Public Event QueryMandatoryChange()
    Public Event FormatChange()
    Public Event TextChange()
    Public Event UpdateableChange()
    Public Event PreviousQueryChange()

    Dim m_Queryable As Boolean
    Dim m_QueryMandatory As Boolean
    Dim m_Format As String
    Dim m_Text As String
    Dim m_Updateable As Boolean
    Dim m_PreviousQuery As String

    Public Property Queryable() As Boolean
        Get
            Queryable = m_Queryable
        End Get
        Set(ByVal Value As Boolean)
            m_Queryable = Value
            RaiseEvent QueryableChange()
        End Set
    End Property

    Public Property QueryMandatory() As Boolean
        Get
            QueryMandatory = m_QueryMandatory
        End Get
        Set(ByVal Value As Boolean)
            m_QueryMandatory = Value
            RaiseEvent QueryMandatoryChange()
        End Set
    End Property

    Public Property Updateable() As Boolean
        Get
            Updateable = m_Updateable
        End Get
        Set(ByVal Value As Boolean)
            m_Updateable = Value
            RaiseEvent UpdateableChange()
        End Set
    End Property

    Public Property PreviousQuery() As String
        Get
            PreviousQuery = m_PreviousQuery
        End Get
        Set(ByVal value As String)
            m_PreviousQuery = value
            'PropertyChanged("PreviousQuery")
            RaiseEvent PreviousQueryChange()
        End Set
    End Property
End Class
