Imports System.ComponentModel

Public Class SuperComboBox
    Inherits System.Windows.Forms.ComboBox

#Region "-- Declarations --"
    Private _ReadOnly As Boolean
    Private _DroppedDown As Boolean
    Private _SelectedIndex As Integer = -1
    Private Declare Auto Function GetWindow Lib "user32.dll" (ByVal hwnd As IntPtr, ByVal wCmd As Int32) As IntPtr
    Private Declare Auto Function SendMessage Lib "user32.dll" (ByVal hwnd As IntPtr, ByVal wMsg As Int32, ByVal wParam As Boolean, ByVal lParam As Int32) As Int32
    Private Const EM_SETREADONLY As Int32 = &HCF
    Private Const EM_EMPTYUNDOBUFFER As Integer = &HCD
    Private Const CB_SHOWDROPDOWN As Integer = &H14F
    Private Const GW_CHILD As Int32 = 5
    Dim m_Queryable As Boolean = False
    Dim m_Updateable As Boolean = True

    Public Event QueryableChange()
    Public Event UpdateableChange()

#End Region     ' -- Declarations --

#Region "-- Constructor --"

    Public Sub New()

        MyBase.New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        Me.ForeColor = Color.DimGray
        Me.SelectedIndex = -1
        Me.SelectedItem = ""
        Me.DropDownStyle = ComboBoxStyle.DropDownList
        Me.AutoCompleteSource = Windows.Forms.AutoCompleteSource.ListItems
        Me.AutoCompleteMode = Windows.Forms.AutoCompleteMode.Suggest
        ' Add any initialization after the InitializeComponent() call.
    End Sub
#End Region

#Region "-- Properties --"
    <Browsable(True), DefaultValue(False)> _
    Public Property [ReadOnly]() As Boolean
        Get
            Return _ReadOnly
        End Get
        Set(ByVal Value As Boolean)
            _ReadOnly = Value
            ' Send the textbox portion of the combo the readonly message. 
            ' It will change the color and behavior.
            'SendMessage(GetWindow(Me.Handle, GW_CHILD), EM_SETREADONLY, Value, 0)
            ' If text was typed or pasted into the textbox, the context menu will 
            ' have the undo activated. When the text box is in the readonly state 
            ' the undo will still be active from the right click context menu 
            ' allowing the user to restore the previous value. This sendmessage 
            ' will clear the undo buffer which will clear the undo.
            SendMessage(GetWindow(Me.Handle, GW_CHILD), EM_EMPTYUNDOBUFFER, Value, 0)
            ' the dropdown may have been dropped before the readonly is set
            _DroppedDown = False
            Me.Refresh()
        End Set
    End Property
    '
    ' Saving and returning a local copy of the selected index keeps a
    ' changed value from being returned when the control is in the
    ' readonly state. The OnSelectedIndexChanged event captures the 
    ' index value that is returned here. Must be shadows or else the
    ' value passed won't cause the OnSelectedIndexChanged method to fire
    ' and the text value to be displayed, won't.
    '
    Public Shadows Property SelectedIndex() As Integer
        Get
            Return _SelectedIndex
        End Get
        Set(ByVal Value As Integer)
            _SelectedIndex = Value
            MyBase.SelectedIndex = Value
        End Set
    End Property

    Public Property Queryable() As Boolean
        Get
            Queryable = m_Queryable
        End Get
        Set(ByVal Value As Boolean)
            m_Queryable = Value
            RaiseEvent QueryableChange()
        End Set
    End Property
    Public Property Updateable() As Boolean
        Get
            Updateable = m_Updateable
        End Get
        Set(ByVal Value As Boolean)
            m_Updateable = Value
            RaiseEvent UpdateableChange()
        End Set
    End Property
#End Region     ' -- Properties --

#Region "-- Overrides --"
    '
    ' Intercepting message 273 when readonly and the listbox is dropped 
    ' keeps the user from selecting an item in the list and having it update
    ' the text value of the combo as well as firing the associated changed events.
    ' Since we intercept a windows message we will have to manually bring up
    ' the listbox.
    ' 
    ' msg 305 (0x131)   =  an item was clicked from the dropdown list
    ' msg 273 (0x111)   = (WM_COMMAND) follows dropdown list click and all other actions?
    ' msg 8465 (0x2111) = (WM_REFLECT + WM_COMMAND) subsequent command after the 273
    '
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        ' Cannot use me.DroppedDown, it causes a System.StackOverflowException.
        ' Asking for it's value must produce windows messages for the combobox 
        ' and thus create a recursive loop.
        If _ReadOnly AndAlso _DroppedDown Then
            If m.Msg = 273 Then
                _DroppedDown = False
                ' bring up the dropdown
                SendMessage(Me.Handle, CB_SHOWDROPDOWN, System.Convert.ToInt32(False), 0)
                Exit Sub
            End If
        End If
        MyBase.WndProc(m)
    End Sub
    '
    ' This event will not fire when msg 273 is intercepted in WndProc. When in the
    ' readonly state clicking on an item in the listbox appears to have no effect. It 
    ' does in fact change the value of MyBase.SelectedIndex. Saving the last good index 
    ' value locally allows the overriden Index property to supply the proper index value.
    '
    Protected Overrides Sub OnSelectedIndexChanged(ByVal e As System.EventArgs)
        _SelectedIndex = MyBase.SelectedIndex
        MyBase.OnSelectedIndexChanged(e)
    End Sub
    '
    ' We must manually track dropped state. Asking the control if it's
    ' dropped from within WndProc will cause a System.StackOverflowException.
    '
    Protected Overrides Sub OnDropDown(ByVal e As System.EventArgs)
        _DroppedDown = True
        MyBase.OnDropDown(e)
    End Sub
    '
    ' The up and down arrow keys cause the combobox to change selection to the next 
    ' or previous in the list. The page up and page down keys change the selection 
    ' by one page at a time as defined by the size of the dropdown list. Setting 
    ' e.Handled to true if any of these keys is pressed stops the selection change 
    ' when readonly. The alt down arrow combination is allowed since it drops the listbox.
    '
    Protected Overrides Sub OnKeyPress(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If _ReadOnly Then e.KeyChar = ChrW(0)
    End Sub

    'Protected Overrides Sub OnKeyDown(ByVal e As System.Windows.Forms.KeyEventArgs)
    '    If _ReadOnly Then
    '        If e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.PageUp OrElse _
    '          e.KeyCode = Keys.PageDown OrElse _
    '         (e.KeyCode = Keys.Down And ((Control.ModifierKeys And Keys.Alt) <> Keys.Alt)) Then
    '            e.Handled = True
    '        End If
    '    End If
    '    MyBase.OnKeyDown(e)
    'End Sub
    '
    ' The combobox default behavior when pressing F4 is to drop the listbox.
    ' If F4 is immediately pressed a second time the OnSelectionChangeCommitted 
    ' event fires regardless of whether a change has been made or not. When 
    ' readonly we don't want a change event to fire. This code will stop it.
    '
    Protected Overrides Sub OnSelectionChangeCommitted(ByVal e As System.EventArgs)
        If _ReadOnly Then
        Else
            MyBase.OnSelectionChangeCommitted(e)
        End If
    End Sub

    Protected Overrides Sub OnDataSourceChanged(ByVal e As System.EventArgs)
        Me.SelectedIndex = -1
        Me.SelectedItem = ""
        MyBase.OnDataSourceChanged(e)
    End Sub

    Protected Overrides Sub OnDisplayMemberChanged(ByVal e As System.EventArgs)
        Me.SelectedIndex = -1
        Me.SelectedItem = ""
        MyBase.OnDisplayMemberChanged(e)
    End Sub

    Protected Overrides Sub OnBindingContextChanged(ByVal e As System.EventArgs)
        Me.SelectedIndex = -1
        Me.SelectedItem = ""
        MyBase.OnBindingContextChanged(e)
    End Sub

    Protected Overrides Sub OnValueMemberChanged(ByVal e As System.EventArgs)
        Me.SelectedIndex = -1
        Me.SelectedItem = ""
        MyBase.OnValueMemberChanged(e)
    End Sub
#End Region     ' -- Overrides --

End Class

