Imports System
Imports System.Windows.Forms
Imports System.Data

Public Class SuperCheckBox

    Inherits CheckBox

    Public Sub BindData(ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String)
        Dim db As Binding = New Binding(propertyName, dataSource, dataMember)

        AddHandler db.Format, AddressOf Me.FormatHandler

        AddHandler db.Parse, AddressOf Me.ParseHandler

        Me.DataBindings.Add(db)
    End Sub

    Private Sub FormatHandler(ByVal sender As Object, ByVal e As ConvertEventArgs)
        If e.Value.ToString = "Y" Then
            e.Value = True
        Else
            e.Value = False
        End If
    End Sub

    Private Sub ParseHandler(ByVal sender As Object, ByVal e As ConvertEventArgs)
        If CType(e.Value, Boolean) = True Then
            e.Value = "Y"
        Else
            e.Value = "N"
        End If
    End Sub

End Class
