﻿Module modTenancyStartup
    Friend strMessage As String
    Dim blnViolated As Boolean
    Public Function CheckConstraint(ByVal frmX As frmTenancyStartup) As String
        blnViolated = False
        strMessage = ""

        If Not blnViolated Then TENANCY_START_DT_1(frmX)
        If Not blnViolated Then TENANCY_START_DT_2(frmX)
        'If Not blnViolated Then CLIENT_NM(frmX)
        'If Not blnViolated Then PSN_GENERATD_NO_PSN(frmX)
        'If Not blnViolated Then LWP_GENERATED_NO_LWP(frmX)

        Return strMessage
    End Function

    Private Sub TENANCY_START_DT_1(ByVal frmX As frmTenancyStartup)
        Try

            If frmX.txtTenancyStartDt.Text.Length = 0 Then
                strMessage = "90897 Tenancy Start Date Cannot Be Empty."
                frmX.txtTenancyStartDt.Focus()

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "modTenancyStartup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub TENANCY_START_DT_2(ByVal frmX As frmTenancyStartup)
        Try
            Dim dteTenancyStartDt As Date
            Dim dteTenancyEndDt As Date

            If Date.TryParse(frmX.txtTenancyStartDt.Text, dteTenancyStartDt) Then
                If Date.TryParse(frmX.txtTenancyEndDt.Text, dteTenancyEndDt) Then
                    If dteTenancyStartDt > dteTenancyEndDt Then
                        blnViolated = True
                        strMessage = "90898 Tenancy Start Date Cannot Be After Tenancy End Date."
                        frmX.txtTenancyStartDt.Focus()
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "modTenancyStartup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub





End Module
