Module modMaintainRentalProperty
    Friend strMessage As String
    Dim blnViolated As Boolean
    Public Function CheckConstraint(ByVal frmX As frmMaintainRentalProperty) As String
        blnViolated = False
        strMessage = ""

        If Not blnViolated Then SEND_TO_EMPLOYEE_IN(frmX)

        Return strMessage
    End Function

    Private Function SEND_TO_EMPLOYEE_IN(ByVal frmX As frmMaintainRentalProperty) As Boolean
        Try
            Dim a As DataGridViewRow
            For Each a In frmX.dgvTenancy.Rows
                If Not a.IsNewRow Then
                    'MsgBox(a.Cells("MONTHLYRENTAMDataGridViewTextBoxColumn").Value.ToString)
                    If CType(a.Cells("MONTHLYRENTAMDataGridViewTextBoxColumn").Value.ToString, Decimal) Then
                        strMessage = "50001 The Actual Monthly Rent exceeds the Tenancy Budget. Please complete the Employee Rent Contribution field."
                        Return True
                    End If
                End If
            Next

            'Dim viewX As DataView = New DataView(frmX.DsTenancySearchResults.V_fsubTenancySearchResults)
            'viewX.RowFilter = "SEND_TO_EMPLOYEE_IN = 'Y'"
            'If viewX.Count > 15 Then
            '    strMessage = "50001 You can only select a maximum of 15 searches to send to the employee. You have currently selected " & viewX.Count.ToString & "."
            '    Return True
            'Else
            '    Return False
            'End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "modTenancySearchResults")
        End Try
    End Function
End Module
