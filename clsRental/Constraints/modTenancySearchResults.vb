Module modTenancySearchResults
    Friend strMessage As String
    Dim blnViolated As Boolean
    Public Function CheckConstraint(ByVal frmX As frmTenancySearchResults) As String
        blnViolated = False
        strMessage = ""

        If Not blnViolated Then SEND_TO_EMPLOYEE_IN(frmX)

        Return strMessage
    End Function

    Private Function SEND_TO_EMPLOYEE_IN(ByVal frmX As frmTenancySearchResults) As Boolean
        Try
            Dim viewX As DataView = New DataView(frmX.DsTenancySearchResults.V_fsubTenancySearchResults)
            viewX.RowFilter = "SEND_TO_EMPLOYEE_IN = 'Y'"
            If viewX.Count > 15 Then
                strMessage = "50001 You can only select a maximum of 15 searches to send to the employee. You have currently selected " & viewX.Count.ToString & "."
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "modTenancySearchResults")
        End Try
    End Function
End Module
