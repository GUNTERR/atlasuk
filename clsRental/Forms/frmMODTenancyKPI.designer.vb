<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMODTenancyKPI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim RELATED_CASE_NOLabel As System.Windows.Forms.Label
        Dim CASE_NOLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMODTenancyKPI))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.txtCaseNo = New ControlLibrary.SuperTextBox()
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsMODTenancyKPI = New clsRental.dsMODTenancyKPI()
        Me.txtCustomerNm = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bsPackage = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsKPI = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dgvPackage = New System.Windows.Forms.DataGridView()
        Me.PackageDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboPackageType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboRentalCaseType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CSE_CASE_NO_CSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGS_CLIENT_PACKAGE_ID_CGS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.pnlKPI = New System.Windows.Forms.Panel()
        Me.pbxActualMoveOutDt = New System.Windows.Forms.PictureBox()
        Me.pbxAnticipatedMoveOutDt = New System.Windows.Forms.PictureBox()
        Me.txtAnticipatedMoveOutDt = New System.Windows.Forms.MaskedTextBox()
        Me.txtActualMoveOutDt = New System.Windows.Forms.MaskedTextBox()
        Me.pbxActualMoveInDt = New System.Windows.Forms.PictureBox()
        Me.pbxPreferredMoveInDt = New System.Windows.Forms.PictureBox()
        Me.txtPreferredMoveInDt = New System.Windows.Forms.MaskedTextBox()
        Me.txtActualMoveInDt = New System.Windows.Forms.MaskedTextBox()
        Me.bnKPI = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbAdd = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem1 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem1 = New System.Windows.Forms.ToolStripButton()
        Me.tssAdd = New System.Windows.Forms.ToolStripSeparator()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.taCase = New clsRental.dsMODTenancyKPITableAdapters.V_frmMODTenancyKPITableAdapter()
        Me.taPackage = New clsRental.dsMODTenancyKPITableAdapters.V_fsubMODTenancyKPIPackageTableAdapter()
        Me.taKPI = New clsRental.dsMODTenancyKPITableAdapters.V_fsubMODTenancyKPITableAdapter()
        RELATED_CASE_NOLabel = New System.Windows.Forms.Label()
        CASE_NOLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMODTenancyKPI, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.bsPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsKPI, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.dgvPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlKPI.SuspendLayout()
        CType(Me.pbxActualMoveOutDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxAnticipatedMoveOutDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxActualMoveInDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxPreferredMoveInDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnKPI, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnKPI.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'RELATED_CASE_NOLabel
        '
        RELATED_CASE_NOLabel.AutoSize = True
        RELATED_CASE_NOLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        RELATED_CASE_NOLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RELATED_CASE_NOLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        RELATED_CASE_NOLabel.Location = New System.Drawing.Point(169, 23)
        RELATED_CASE_NOLabel.Name = "RELATED_CASE_NOLabel"
        RELATED_CASE_NOLabel.Size = New System.Drawing.Size(53, 14)
        RELATED_CASE_NOLabel.TabIndex = 91
        RELATED_CASE_NOLabel.Text = "Customer"
        '
        'CASE_NOLabel
        '
        CASE_NOLabel.AutoSize = True
        CASE_NOLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        CASE_NOLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CASE_NOLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CASE_NOLabel.Location = New System.Drawing.Point(22, 22)
        CASE_NOLabel.Name = "CASE_NOLabel"
        CASE_NOLabel.Size = New System.Drawing.Size(48, 14)
        CASE_NOLabel.TabIndex = 81
        CASE_NOLabel.Text = "Case No"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label1.Location = New System.Drawing.Point(25, 23)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(106, 14)
        Label1.TabIndex = 93
        Label1.Text = "Preferred Move In Dt"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label3.Location = New System.Drawing.Point(246, 23)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(91, 14)
        Label3.TabIndex = 95
        Label3.Text = "Actual Move In Dt"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label6.Location = New System.Drawing.Point(248, 49)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(100, 14)
        Label6.TabIndex = 145
        Label6.Text = "Actual Move Out Dt"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label7.Location = New System.Drawing.Point(25, 49)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(123, 14)
        Label7.TabIndex = 144
        Label7.Text = "Anticipated Move Out Dt"
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(502, 24)
        Me.pnlMainToolStrip.TabIndex = 142
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(459, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 9
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(155, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(337, 25)
        Me.tspMain.TabIndex = 10
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox7)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(8, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(484, 102)
        Me.Panel1.TabIndex = 146
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(449, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 79
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.Panel3.Controls.Add(Me.pbxVIP)
        Me.Panel3.Controls.Add(Me.txtCaseNo)
        Me.Panel3.Controls.Add(RELATED_CASE_NOLabel)
        Me.Panel3.Controls.Add(Me.txtCustomerNm)
        Me.Panel3.Controls.Add(CASE_NOLabel)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(466, 55)
        Me.Panel3.TabIndex = 1
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(135, 14)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.pbxVIP.Visible = False
        '
        'txtCaseNo
        '
        Me.txtCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CASE_NO", True))
        Me.txtCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCaseNo.Location = New System.Drawing.Point(76, 19)
        Me.txtCaseNo.Name = "txtCaseNo"
        Me.txtCaseNo.PreviousQuery = Nothing
        Me.txtCaseNo.Queryable = True
        Me.txtCaseNo.QueryMandatory = False
        Me.txtCaseNo.ReadOnly = True
        Me.txtCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txtCaseNo.TabIndex = 1
        Me.txtCaseNo.Updateable = False
        '
        'bsCase
        '
        Me.bsCase.AllowNew = False
        Me.bsCase.DataMember = "V_frmMODTenancyKPI"
        Me.bsCase.DataSource = Me.DsMODTenancyKPI
        '
        'DsMODTenancyKPI
        '
        Me.DsMODTenancyKPI.DataSetName = "dsMODTenancyKPI"
        Me.DsMODTenancyKPI.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtCustomerNm
        '
        Me.txtCustomerNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtCustomerNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "Customer", True))
        Me.txtCustomerNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomerNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCustomerNm.Location = New System.Drawing.Point(226, 19)
        Me.txtCustomerNm.Name = "txtCustomerNm"
        Me.txtCustomerNm.ReadOnly = True
        Me.txtCustomerNm.Size = New System.Drawing.Size(220, 20)
        Me.txtCustomerNm.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.txtVIP)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(421, 24)
        Me.Panel2.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(199, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Case"
        '
        'bsPackage
        '
        Me.bsPackage.DataMember = "V_frmMODTenancyKPI_V_fsubMODTenancyKPIPackage"
        Me.bsPackage.DataSource = Me.bsCase
        '
        'bsKPI
        '
        Me.bsKPI.AllowNew = True
        Me.bsKPI.DataMember = "V_fsubMODTenancyKPIPackage_V_fsubMODTenancyKPI"
        Me.bsKPI.DataSource = Me.bsPackage
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.PictureBox1)
        Me.Panel7.Controls.Add(Me.PictureBox2)
        Me.Panel7.Controls.Add(Me.Panel8)
        Me.Panel7.Controls.Add(Me.dgvPackage)
        Me.Panel7.Location = New System.Drawing.Point(8, 140)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(484, 124)
        Me.Panel7.TabIndex = 147
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(449, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 80
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 79
        Me.PictureBox2.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Silver
        Me.Panel8.Controls.Add(Me.Label4)
        Me.Panel8.Location = New System.Drawing.Point(33, 8)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(421, 24)
        Me.Panel8.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(0, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 14)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Package"
        '
        'dgvPackage
        '
        Me.dgvPackage.AllowUserToAddRows = False
        Me.dgvPackage.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPackage.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPackage.AutoGenerateColumns = False
        Me.dgvPackage.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPackage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPackage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPackage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPackage.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvPackage.ColumnHeadersHeight = 20
        Me.dgvPackage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PackageDataGridViewTextBoxColumn, Me.cboPackageType, Me.cboRentalCaseType, Me.CSE_CASE_NO_CSE, Me.CGS_CLIENT_PACKAGE_ID_CGS})
        Me.dgvPackage.DataSource = Me.bsPackage
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPackage.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvPackage.EnableHeadersVisualStyles = False
        Me.dgvPackage.Location = New System.Drawing.Point(8, 32)
        Me.dgvPackage.Name = "dgvPackage"
        Me.dgvPackage.ReadOnly = True
        Me.dgvPackage.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPackage.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvPackage.RowHeadersWidth = 18
        Me.dgvPackage.RowTemplate.Height = 20
        Me.dgvPackage.Size = New System.Drawing.Size(466, 80)
        Me.dgvPackage.TabIndex = 76
        '
        'PackageDataGridViewTextBoxColumn
        '
        Me.PackageDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.PackageDataGridViewTextBoxColumn.DataPropertyName = "Package"
        Me.PackageDataGridViewTextBoxColumn.HeaderText = "Package"
        Me.PackageDataGridViewTextBoxColumn.Name = "PackageDataGridViewTextBoxColumn"
        Me.PackageDataGridViewTextBoxColumn.ReadOnly = True
        '
        'cboPackageType
        '
        Me.cboPackageType.DataPropertyName = "CGS_PACKAGE_TYPE_CD_CGS"
        Me.cboPackageType.HeaderText = "Package Type"
        Me.cboPackageType.Name = "cboPackageType"
        Me.cboPackageType.ReadOnly = True
        Me.cboPackageType.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'cboRentalCaseType
        '
        Me.cboRentalCaseType.DataPropertyName = "PROP_OR_PSN_CD"
        Me.cboRentalCaseType.HeaderText = "Rental Case Type"
        Me.cboRentalCaseType.Name = "cboRentalCaseType"
        Me.cboRentalCaseType.ReadOnly = True
        Me.cboRentalCaseType.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cboRentalCaseType.Width = 110
        '
        'CSE_CASE_NO_CSE
        '
        Me.CSE_CASE_NO_CSE.DataPropertyName = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.HeaderText = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.Name = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.ReadOnly = True
        Me.CSE_CASE_NO_CSE.Visible = False
        '
        'CGS_CLIENT_PACKAGE_ID_CGS
        '
        Me.CGS_CLIENT_PACKAGE_ID_CGS.DataPropertyName = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGS_CLIENT_PACKAGE_ID_CGS.HeaderText = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGS_CLIENT_PACKAGE_ID_CGS.Name = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGS_CLIENT_PACKAGE_ID_CGS.ReadOnly = True
        Me.CGS_CLIENT_PACKAGE_ID_CGS.Visible = False
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.PictureBox3)
        Me.Panel4.Controls.Add(Me.PictureBox5)
        Me.Panel4.Controls.Add(Me.pnlKPI)
        Me.Panel4.Controls.Add(Me.Panel6)
        Me.Panel4.Location = New System.Drawing.Point(8, 270)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(484, 170)
        Me.Panel4.TabIndex = 147
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(449, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 79
        Me.PictureBox3.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 78
        Me.PictureBox5.TabStop = False
        '
        'pnlKPI
        '
        Me.pnlKPI.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlKPI.Controls.Add(Me.pbxActualMoveOutDt)
        Me.pnlKPI.Controls.Add(Me.pbxAnticipatedMoveOutDt)
        Me.pnlKPI.Controls.Add(Me.txtAnticipatedMoveOutDt)
        Me.pnlKPI.Controls.Add(Me.txtActualMoveOutDt)
        Me.pnlKPI.Controls.Add(Label6)
        Me.pnlKPI.Controls.Add(Label7)
        Me.pnlKPI.Controls.Add(Me.pbxActualMoveInDt)
        Me.pnlKPI.Controls.Add(Me.pbxPreferredMoveInDt)
        Me.pnlKPI.Controls.Add(Me.txtPreferredMoveInDt)
        Me.pnlKPI.Controls.Add(Me.txtActualMoveInDt)
        Me.pnlKPI.Controls.Add(Me.bnKPI)
        Me.pnlKPI.Controls.Add(Label3)
        Me.pnlKPI.Controls.Add(Label1)
        Me.pnlKPI.Location = New System.Drawing.Point(8, 32)
        Me.pnlKPI.Name = "pnlKPI"
        Me.pnlKPI.Size = New System.Drawing.Size(466, 126)
        Me.pnlKPI.TabIndex = 1
        '
        'pbxActualMoveOutDt
        '
        Me.pbxActualMoveOutDt.Image = CType(resources.GetObject("pbxActualMoveOutDt.Image"), System.Drawing.Image)
        Me.pbxActualMoveOutDt.Location = New System.Drawing.Point(425, 48)
        Me.pbxActualMoveOutDt.Name = "pbxActualMoveOutDt"
        Me.pbxActualMoveOutDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxActualMoveOutDt.TabIndex = 149
        Me.pbxActualMoveOutDt.TabStop = False
        '
        'pbxAnticipatedMoveOutDt
        '
        Me.pbxAnticipatedMoveOutDt.Image = CType(resources.GetObject("pbxAnticipatedMoveOutDt.Image"), System.Drawing.Image)
        Me.pbxAnticipatedMoveOutDt.Location = New System.Drawing.Point(225, 48)
        Me.pbxAnticipatedMoveOutDt.Name = "pbxAnticipatedMoveOutDt"
        Me.pbxAnticipatedMoveOutDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxAnticipatedMoveOutDt.TabIndex = 148
        Me.pbxAnticipatedMoveOutDt.TabStop = False
        '
        'txtAnticipatedMoveOutDt
        '
        Me.txtAnticipatedMoveOutDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsKPI, "ANTICIPATED_MOVE_OUT_DT", True))
        Me.txtAnticipatedMoveOutDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAnticipatedMoveOutDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAnticipatedMoveOutDt.Location = New System.Drawing.Point(154, 46)
        Me.txtAnticipatedMoveOutDt.Mask = "00/00/0000"
        Me.txtAnticipatedMoveOutDt.Name = "txtAnticipatedMoveOutDt"
        Me.txtAnticipatedMoveOutDt.Size = New System.Drawing.Size(65, 20)
        Me.txtAnticipatedMoveOutDt.TabIndex = 147
        Me.txtAnticipatedMoveOutDt.ValidatingType = GetType(Date)
        '
        'txtActualMoveOutDt
        '
        Me.txtActualMoveOutDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsKPI, "ACTUAL_MOVE_OUT_DT", True))
        Me.txtActualMoveOutDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualMoveOutDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtActualMoveOutDt.Location = New System.Drawing.Point(354, 46)
        Me.txtActualMoveOutDt.Mask = "00/00/0000"
        Me.txtActualMoveOutDt.Name = "txtActualMoveOutDt"
        Me.txtActualMoveOutDt.Size = New System.Drawing.Size(65, 20)
        Me.txtActualMoveOutDt.TabIndex = 146
        Me.txtActualMoveOutDt.ValidatingType = GetType(Date)
        '
        'pbxActualMoveInDt
        '
        Me.pbxActualMoveInDt.Image = CType(resources.GetObject("pbxActualMoveInDt.Image"), System.Drawing.Image)
        Me.pbxActualMoveInDt.Location = New System.Drawing.Point(425, 22)
        Me.pbxActualMoveInDt.Name = "pbxActualMoveInDt"
        Me.pbxActualMoveInDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxActualMoveInDt.TabIndex = 143
        Me.pbxActualMoveInDt.TabStop = False
        '
        'pbxPreferredMoveInDt
        '
        Me.pbxPreferredMoveInDt.Image = CType(resources.GetObject("pbxPreferredMoveInDt.Image"), System.Drawing.Image)
        Me.pbxPreferredMoveInDt.Location = New System.Drawing.Point(225, 22)
        Me.pbxPreferredMoveInDt.Name = "pbxPreferredMoveInDt"
        Me.pbxPreferredMoveInDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxPreferredMoveInDt.TabIndex = 142
        Me.pbxPreferredMoveInDt.TabStop = False
        '
        'txtPreferredMoveInDt
        '
        Me.txtPreferredMoveInDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsKPI, "PREFERRED_MOVE_IN_DT", True))
        Me.txtPreferredMoveInDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPreferredMoveInDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtPreferredMoveInDt.Location = New System.Drawing.Point(154, 20)
        Me.txtPreferredMoveInDt.Mask = "00/00/0000"
        Me.txtPreferredMoveInDt.Name = "txtPreferredMoveInDt"
        Me.txtPreferredMoveInDt.Size = New System.Drawing.Size(65, 20)
        Me.txtPreferredMoveInDt.TabIndex = 129
        Me.txtPreferredMoveInDt.ValidatingType = GetType(Date)
        '
        'txtActualMoveInDt
        '
        Me.txtActualMoveInDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsKPI, "ACTUAL_MOVE_IN_DT", True))
        Me.txtActualMoveInDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActualMoveInDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtActualMoveInDt.Location = New System.Drawing.Point(354, 20)
        Me.txtActualMoveInDt.Mask = "00/00/0000"
        Me.txtActualMoveInDt.Name = "txtActualMoveInDt"
        Me.txtActualMoveInDt.Size = New System.Drawing.Size(65, 20)
        Me.txtActualMoveInDt.TabIndex = 128
        Me.txtActualMoveInDt.ValidatingType = GetType(Date)
        '
        'bnKPI
        '
        Me.bnKPI.AddNewItem = Me.tsbAdd
        Me.bnKPI.BindingSource = Me.bsKPI
        Me.bnKPI.CountItem = Me.BindingNavigatorCountItem1
        Me.bnKPI.DeleteItem = Nothing
        Me.bnKPI.Dock = System.Windows.Forms.DockStyle.None
        Me.bnKPI.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnKPI.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem1, Me.BindingNavigatorMovePreviousItem1, Me.BindingNavigatorSeparator3, Me.BindingNavigatorPositionItem1, Me.BindingNavigatorCountItem1, Me.BindingNavigatorSeparator4, Me.BindingNavigatorMoveNextItem1, Me.BindingNavigatorMoveLastItem1, Me.tssAdd, Me.tsbAdd})
        Me.bnKPI.Location = New System.Drawing.Point(14, 88)
        Me.bnKPI.MoveFirstItem = Me.BindingNavigatorMoveFirstItem1
        Me.bnKPI.MoveLastItem = Me.BindingNavigatorMoveLastItem1
        Me.bnKPI.MoveNextItem = Me.BindingNavigatorMoveNextItem1
        Me.bnKPI.MovePreviousItem = Me.BindingNavigatorMovePreviousItem1
        Me.bnKPI.Name = "bnKPI"
        Me.bnKPI.PositionItem = Me.BindingNavigatorPositionItem1
        Me.bnKPI.Size = New System.Drawing.Size(198, 25)
        Me.bnKPI.TabIndex = 114
        Me.bnKPI.Text = "BindingNavigator1"
        '
        'tsbAdd
        '
        Me.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAdd.Image = CType(resources.GetObject("tsbAdd.Image"), System.Drawing.Image)
        Me.tsbAdd.Name = "tsbAdd"
        Me.tsbAdd.RightToLeftAutoMirrorImage = True
        Me.tsbAdd.Size = New System.Drawing.Size(23, 22)
        Me.tsbAdd.Text = "Add new"
        '
        'BindingNavigatorCountItem1
        '
        Me.BindingNavigatorCountItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem1.Name = "BindingNavigatorCountItem1"
        Me.BindingNavigatorCountItem1.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem1.Text = "of {0}"
        Me.BindingNavigatorCountItem1.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem1
        '
        Me.BindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem1.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem1.Name = "BindingNavigatorMoveFirstItem1"
        Me.BindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem1.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem1
        '
        Me.BindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem1.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem1.Name = "BindingNavigatorMovePreviousItem1"
        Me.BindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem1.Text = "Move previous"
        '
        'BindingNavigatorSeparator3
        '
        Me.BindingNavigatorSeparator3.Name = "BindingNavigatorSeparator3"
        Me.BindingNavigatorSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem1
        '
        Me.BindingNavigatorPositionItem1.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem1.AutoSize = False
        Me.BindingNavigatorPositionItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem1.Name = "BindingNavigatorPositionItem1"
        Me.BindingNavigatorPositionItem1.Size = New System.Drawing.Size(25, 21)
        Me.BindingNavigatorPositionItem1.Text = "0"
        Me.BindingNavigatorPositionItem1.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator4
        '
        Me.BindingNavigatorSeparator4.Name = "BindingNavigatorSeparator4"
        Me.BindingNavigatorSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem1
        '
        Me.BindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem1.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem1.Name = "BindingNavigatorMoveNextItem1"
        Me.BindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem1.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem1
        '
        Me.BindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem1.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem1.Name = "BindingNavigatorMoveLastItem1"
        Me.BindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem1.Text = "Move last"
        '
        'tssAdd
        '
        Me.tssAdd.Name = "tssAdd"
        Me.tssAdd.Size = New System.Drawing.Size(6, 25)
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Silver
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Location = New System.Drawing.Point(33, 8)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(421, 24)
        Me.Panel6.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(0, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 14)
        Me.Label5.TabIndex = 73
        Me.Label5.Text = "KPI"
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taPackage
        '
        Me.taPackage.ClearBeforeFill = True
        '
        'taKPI
        '
        Me.taKPI.ClearBeforeFill = True
        '
        'frmMODTenancyKPI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(502, 449)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMODTenancyKPI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MOD Tenancy Management KPI"
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMODTenancyKPI, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.bsPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsKPI, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.dgvPackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlKPI.ResumeLayout(False)
        Me.pnlKPI.PerformLayout()
        CType(Me.pbxActualMoveOutDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxAnticipatedMoveOutDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxActualMoveInDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxPreferredMoveInDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnKPI, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnKPI.ResumeLayout(False)
        Me.bnKPI.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents txtCustomerNm As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DsMODTenancyKPI As clsRental.dsMODTenancyKPI
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents taCase As clsRental.dsMODTenancyKPITableAdapters.V_frmMODTenancyKPITableAdapter
    Friend WithEvents bsPackage As System.Windows.Forms.BindingSource
    Friend WithEvents taPackage As clsRental.dsMODTenancyKPITableAdapters.V_fsubMODTenancyKPIPackageTableAdapter
    Friend WithEvents bsKPI As System.Windows.Forms.BindingSource
    Friend WithEvents taKPI As clsRental.dsMODTenancyKPITableAdapters.V_fsubMODTenancyKPITableAdapter
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvPackage As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlKPI As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents bnKPI As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tssAdd As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents txtPreferredMoveInDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtActualMoveInDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents PackageDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboPackageType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboRentalCaseType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSE_CASE_NO_CSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGS_CLIENT_PACKAGE_ID_CGS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pbxActualMoveInDt As System.Windows.Forms.PictureBox
    Friend WithEvents pbxPreferredMoveInDt As System.Windows.Forms.PictureBox
    Friend WithEvents pbxActualMoveOutDt As System.Windows.Forms.PictureBox
    Friend WithEvents pbxAnticipatedMoveOutDt As System.Windows.Forms.PictureBox
    Friend WithEvents txtAnticipatedMoveOutDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtActualMoveOutDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
