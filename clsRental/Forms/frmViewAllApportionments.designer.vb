<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewAllApportionments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewAllApportionments))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlQueryCase = New System.Windows.Forms.Panel()
        Me.SuperLabel4 = New ControlLibrary.SuperLabel(Me.components)
        Me.txbAddress = New ControlLibrary.SuperTextBox()
        Me.SuperLabel3 = New ControlLibrary.SuperLabel(Me.components)
        Me.txbPackageNm = New ControlLibrary.SuperTextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsViewAllApportionments = New clsRental.dsViewAllApportionments()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvCase = New System.Windows.Forms.DataGridView()
        Me.CSE_CASE_NO_CSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboRentalTypeCd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClientPackageDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlInvoice = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvInvoice = New System.Windows.Forms.DataGridView()
        Me.GENERATEDNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASEORDERNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INV_BUSINESS_NO_ORG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INV_EXTERNAL_INV_NO_INV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INV_INVOICE_DT_INV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICEITEMAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ITEMVATAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPICASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONEDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsInvoice = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlApportionment = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvApportionment = New System.Windows.Forms.DataGridView()
        Me.INIBUSINESSNOORGDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INIINVOICEDTINVDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INIGENERATEDNOINIDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GENERATEDNODataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPCCASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BILLUPLOADDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USER_NM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsApportionment = New System.Windows.Forms.BindingSource(Me.components)
        Me.taCase = New clsRental.dsViewAllApportionmentsTableAdapters.V_frmManualApportionmentsTableAdapter()
        Me.taInvoice = New clsRental.dsViewAllApportionmentsTableAdapters.V_fsubViewAllApptnmntsDetailsTableAdapter()
        Me.taApportionment = New clsRental.dsViewAllApportionmentsTableAdapters.V_fsubManualAppmntApportionTableAdapter()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlQueryCase.SuspendLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsViewAllApportionments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCase.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvCase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInvoice.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlApportionment.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvApportionment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsApportionment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(159, 14)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlQueryCase)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(796, 96)
        Me.Panel8.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(763, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlQueryCase
        '
        Me.pnlQueryCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlQueryCase.Controls.Add(Me.pbxVIP)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel4)
        Me.pnlQueryCase.Controls.Add(Me.txbAddress)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel3)
        Me.pnlQueryCase.Controls.Add(Me.txbPackageNm)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel2)
        Me.pnlQueryCase.Controls.Add(Me.pbxCopy)
        Me.pnlQueryCase.Controls.Add(Me.txbCaseNo)
        Me.pnlQueryCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlQueryCase.Name = "pnlQueryCase"
        Me.pnlQueryCase.Size = New System.Drawing.Size(780, 54)
        Me.pnlQueryCase.TabIndex = 0
        '
        'SuperLabel4
        '
        Me.SuperLabel4.AutoSize = True
        Me.SuperLabel4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel4.Location = New System.Drawing.Point(478, 23)
        Me.SuperLabel4.Name = "SuperLabel4"
        Me.SuperLabel4.Size = New System.Drawing.Size(49, 14)
        Me.SuperLabel4.TabIndex = 118
        Me.SuperLabel4.Text = "Address"
        '
        'txbAddress
        '
        Me.txbAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAddress.Location = New System.Drawing.Point(532, 20)
        Me.txbAddress.Name = "txbAddress"
        Me.txbAddress.PreviousQuery = Nothing
        Me.txbAddress.Queryable = True
        Me.txbAddress.QueryMandatory = False
        Me.txbAddress.ReadOnly = True
        Me.txbAddress.Size = New System.Drawing.Size(231, 20)
        Me.txbAddress.TabIndex = 117
        Me.txbAddress.Updateable = False
        '
        'SuperLabel3
        '
        Me.SuperLabel3.AutoSize = True
        Me.SuperLabel3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel3.Location = New System.Drawing.Point(197, 23)
        Me.SuperLabel3.Name = "SuperLabel3"
        Me.SuperLabel3.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel3.TabIndex = 116
        Me.SuperLabel3.Text = "Package"
        '
        'txbPackageNm
        '
        Me.txbPackageNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPackageNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPackageNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPackageNm.Location = New System.Drawing.Point(251, 20)
        Me.txbPackageNm.Name = "txbPackageNm"
        Me.txbPackageNm.PreviousQuery = Nothing
        Me.txbPackageNm.Queryable = True
        Me.txbPackageNm.QueryMandatory = False
        Me.txbPackageNm.ReadOnly = True
        Me.txbPackageNm.Size = New System.Drawing.Size(221, 20)
        Me.txbPackageNm.TabIndex = 115
        Me.txbPackageNm.Updateable = False
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 23)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(138, 23)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(79, 20)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txbCaseNo.TabIndex = 0
        Me.txbCaseNo.Updateable = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(731, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(354, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmManualApportionments"
        Me.bsCase.DataSource = Me.DsViewAllApportionments
        '
        'DsViewAllApportionments
        '
        Me.DsViewAllApportionments.DataSetName = "dsViewAllApportionments"
        Me.DsViewAllApportionments.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Query Case"
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(194, 19)
        Me.SuperLabel1.TabIndex = 5
        Me.SuperLabel1.Text = "View All Apportionments"
        '
        'pnlCase
        '
        Me.pnlCase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCase.Controls.Add(Me.PictureBox6)
        Me.pnlCase.Controls.Add(Me.PictureBox5)
        Me.pnlCase.Controls.Add(Me.Panel4)
        Me.pnlCase.Controls.Add(Me.dgvCase)
        Me.pnlCase.Location = New System.Drawing.Point(0, 168)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(795, 124)
        Me.pnlCase.TabIndex = 9
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(732, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Case"
        '
        'dgvCase
        '
        Me.dgvCase.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvCase.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCase.AutoGenerateColumns = False
        Me.dgvCase.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvCase.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCase.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvCase.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCase.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCase.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSE_CASE_NO_CSE, Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.cboRentalTypeCd, Me.ClientPackageDataGridViewTextBoxColumn, Me.AddressDataGridViewTextBoxColumn})
        Me.dgvCase.DataSource = Me.bsCase
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCase.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvCase.EnableHeadersVisualStyles = False
        Me.dgvCase.Location = New System.Drawing.Point(8, 32)
        Me.dgvCase.Name = "dgvCase"
        Me.dgvCase.ReadOnly = True
        Me.dgvCase.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCase.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvCase.RowHeadersWidth = 18
        Me.dgvCase.RowTemplate.Height = 20
        Me.dgvCase.Size = New System.Drawing.Size(775, 80)
        Me.dgvCase.TabIndex = 76
        '
        'CSE_CASE_NO_CSE
        '
        Me.CSE_CASE_NO_CSE.DataPropertyName = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.HeaderText = "Case"
        Me.CSE_CASE_NO_CSE.Name = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.ReadOnly = True
        Me.CSE_CASE_NO_CSE.Width = 80
        '
        'CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'cboRentalTypeCd
        '
        Me.cboRentalTypeCd.DataPropertyName = "PROP_OR_PSN_CD"
        Me.cboRentalTypeCd.HeaderText = "Type"
        Me.cboRentalTypeCd.Name = "cboRentalTypeCd"
        Me.cboRentalTypeCd.ReadOnly = True
        Me.cboRentalTypeCd.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'ClientPackageDataGridViewTextBoxColumn
        '
        Me.ClientPackageDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ClientPackageDataGridViewTextBoxColumn.DataPropertyName = "ClientPackage"
        Me.ClientPackageDataGridViewTextBoxColumn.HeaderText = "Client Package"
        Me.ClientPackageDataGridViewTextBoxColumn.Name = "ClientPackageDataGridViewTextBoxColumn"
        Me.ClientPackageDataGridViewTextBoxColumn.ReadOnly = True
        '
        'AddressDataGridViewTextBoxColumn
        '
        Me.AddressDataGridViewTextBoxColumn.DataPropertyName = "Address"
        Me.AddressDataGridViewTextBoxColumn.HeaderText = "Address"
        Me.AddressDataGridViewTextBoxColumn.Name = "AddressDataGridViewTextBoxColumn"
        Me.AddressDataGridViewTextBoxColumn.ReadOnly = True
        Me.AddressDataGridViewTextBoxColumn.Width = 300
        '
        'pnlInvoice
        '
        Me.pnlInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInvoice.Controls.Add(Me.PictureBox3)
        Me.pnlInvoice.Controls.Add(Me.PictureBox4)
        Me.pnlInvoice.Controls.Add(Me.Panel2)
        Me.pnlInvoice.Controls.Add(Me.dgvInvoice)
        Me.pnlInvoice.Location = New System.Drawing.Point(0, 298)
        Me.pnlInvoice.Name = "pnlInvoice"
        Me.pnlInvoice.Size = New System.Drawing.Size(795, 229)
        Me.pnlInvoice.TabIndex = 10
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 80
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 79
        Me.PictureBox4.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(732, 24)
        Me.Panel2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 14)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Invoice"
        '
        'dgvInvoice
        '
        Me.dgvInvoice.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvInvoice.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvInvoice.AutoGenerateColumns = False
        Me.dgvInvoice.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvInvoice.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInvoice.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvInvoice.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoice.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvInvoice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GENERATEDNODataGridViewTextBoxColumn, Me.PURCHASEORDERNODataGridViewTextBoxColumn, Me.INV_BUSINESS_NO_ORG, Me.INV_EXTERNAL_INV_NO_INV, Me.INV_INVOICE_DT_INV, Me.INVOICEITEMAMDataGridViewTextBoxColumn, Me.ITEMVATAMDataGridViewTextBoxColumn, Me.SPICASENOCSEDataGridViewTextBoxColumn, Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.APPORTIONEDDTDataGridViewTextBoxColumn})
        Me.dgvInvoice.DataSource = Me.bsInvoice
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvInvoice.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvInvoice.EnableHeadersVisualStyles = False
        Me.dgvInvoice.Location = New System.Drawing.Point(8, 32)
        Me.dgvInvoice.Name = "dgvInvoice"
        Me.dgvInvoice.ReadOnly = True
        Me.dgvInvoice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoice.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvInvoice.RowHeadersWidth = 18
        Me.dgvInvoice.RowTemplate.Height = 20
        Me.dgvInvoice.Size = New System.Drawing.Size(775, 189)
        Me.dgvInvoice.TabIndex = 76
        '
        'GENERATEDNODataGridViewTextBoxColumn
        '
        Me.GENERATEDNODataGridViewTextBoxColumn.DataPropertyName = "GENERATED_NO"
        Me.GENERATEDNODataGridViewTextBoxColumn.HeaderText = "GENERATED_NO"
        Me.GENERATEDNODataGridViewTextBoxColumn.Name = "GENERATEDNODataGridViewTextBoxColumn"
        Me.GENERATEDNODataGridViewTextBoxColumn.ReadOnly = True
        Me.GENERATEDNODataGridViewTextBoxColumn.Visible = False
        '
        'PURCHASEORDERNODataGridViewTextBoxColumn
        '
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.DataPropertyName = "PURCHASE_ORDER_NO"
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.HeaderText = "Purchase Order No"
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.Name = "PURCHASEORDERNODataGridViewTextBoxColumn"
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.ReadOnly = True
        '
        'INV_BUSINESS_NO_ORG
        '
        Me.INV_BUSINESS_NO_ORG.DataPropertyName = "INV_BUSINESS_NO_ORG"
        Me.INV_BUSINESS_NO_ORG.HeaderText = "Supplier No"
        Me.INV_BUSINESS_NO_ORG.Name = "INV_BUSINESS_NO_ORG"
        Me.INV_BUSINESS_NO_ORG.ReadOnly = True
        Me.INV_BUSINESS_NO_ORG.Width = 80
        '
        'INV_EXTERNAL_INV_NO_INV
        '
        Me.INV_EXTERNAL_INV_NO_INV.DataPropertyName = "INV_EXTERNAL_INV_NO_INV"
        Me.INV_EXTERNAL_INV_NO_INV.HeaderText = "Invoice No"
        Me.INV_EXTERNAL_INV_NO_INV.Name = "INV_EXTERNAL_INV_NO_INV"
        Me.INV_EXTERNAL_INV_NO_INV.ReadOnly = True
        Me.INV_EXTERNAL_INV_NO_INV.Width = 120
        '
        'INV_INVOICE_DT_INV
        '
        Me.INV_INVOICE_DT_INV.DataPropertyName = "INV_INVOICE_DT_INV"
        Me.INV_INVOICE_DT_INV.HeaderText = "Invoice Date"
        Me.INV_INVOICE_DT_INV.Name = "INV_INVOICE_DT_INV"
        Me.INV_INVOICE_DT_INV.ReadOnly = True
        '
        'INVOICEITEMAMDataGridViewTextBoxColumn
        '
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.DataPropertyName = "INVOICE_ITEM_AM"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.HeaderText = "Item Amount"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.Name = "INVOICEITEMAMDataGridViewTextBoxColumn"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.Width = 85
        '
        'ITEMVATAMDataGridViewTextBoxColumn
        '
        Me.ITEMVATAMDataGridViewTextBoxColumn.DataPropertyName = "ITEM_VAT_AM"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.ITEMVATAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.ITEMVATAMDataGridViewTextBoxColumn.HeaderText = "Item VAT"
        Me.ITEMVATAMDataGridViewTextBoxColumn.Name = "ITEMVATAMDataGridViewTextBoxColumn"
        Me.ITEMVATAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.ITEMVATAMDataGridViewTextBoxColumn.Width = 80
        '
        'SPICASENOCSEDataGridViewTextBoxColumn
        '
        Me.SPICASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "SPI_CASE_NO_CSE"
        Me.SPICASENOCSEDataGridViewTextBoxColumn.HeaderText = "SPI_CASE_NO_CSE"
        Me.SPICASENOCSEDataGridViewTextBoxColumn.Name = "SPICASENOCSEDataGridViewTextBoxColumn"
        Me.SPICASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        Me.SPICASENOCSEDataGridViewTextBoxColumn.Visible = False
        '
        'SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "SPI_CLIENT_PACKAGE_ID_CGS"
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "SPI_CLIENT_PACKAGE_ID_CGS"
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'APPORTIONEDDTDataGridViewTextBoxColumn
        '
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.DataPropertyName = "APPORTIONED_DT"
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.HeaderText = "Apportioned Date"
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.Name = "APPORTIONEDDTDataGridViewTextBoxColumn"
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.Width = 110
        '
        'bsInvoice
        '
        Me.bsInvoice.DataMember = "V_frmManualApportionments_V_fsubViewAllApptnmntsDetails"
        Me.bsInvoice.DataSource = Me.bsCase
        '
        'pnlApportionment
        '
        Me.pnlApportionment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlApportionment.Controls.Add(Me.PictureBox7)
        Me.pnlApportionment.Controls.Add(Me.PictureBox8)
        Me.pnlApportionment.Controls.Add(Me.Panel5)
        Me.pnlApportionment.Controls.Add(Me.dgvApportionment)
        Me.pnlApportionment.Location = New System.Drawing.Point(0, 533)
        Me.pnlApportionment.Name = "pnlApportionment"
        Me.pnlApportionment.Size = New System.Drawing.Size(795, 168)
        Me.pnlApportionment.TabIndex = 11
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 80
        Me.PictureBox7.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox8.TabIndex = 79
        Me.PictureBox8.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Silver
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Location = New System.Drawing.Point(33, 8)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(732, 24)
        Me.Panel5.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Apportionment"
        '
        'dgvApportionment
        '
        Me.dgvApportionment.AllowUserToDeleteRows = False
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvApportionment.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvApportionment.AutoGenerateColumns = False
        Me.dgvApportionment.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvApportionment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvApportionment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvApportionment.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvApportionment.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvApportionment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.INIBUSINESSNOORGDataGridViewTextBoxColumn, Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn, Me.INIINVOICEDTINVDataGridViewTextBoxColumn, Me.INIGENERATEDNOINIDataGridViewTextBoxColumn, Me.GENERATEDNODataGridViewTextBoxColumn1, Me.CPCCASENOCSEDataGridViewTextBoxColumn, Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.CLIENTPACKAGENMDataGridViewTextBoxColumn, Me.APPORTIONMENTDTDataGridViewTextBoxColumn, Me.APPORTIONEDNETAMDataGridViewTextBoxColumn, Me.APPORTIONEDVATAMDataGridViewTextBoxColumn, Me.BILLUPLOADDTDataGridViewTextBoxColumn, Me.USER_NM})
        Me.dgvApportionment.DataSource = Me.bsApportionment
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvApportionment.DefaultCellStyle = DataGridViewCellStyle15
        Me.dgvApportionment.EnableHeadersVisualStyles = False
        Me.dgvApportionment.Location = New System.Drawing.Point(8, 32)
        Me.dgvApportionment.Name = "dgvApportionment"
        Me.dgvApportionment.ReadOnly = True
        Me.dgvApportionment.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvApportionment.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvApportionment.RowHeadersWidth = 18
        Me.dgvApportionment.RowTemplate.Height = 20
        Me.dgvApportionment.Size = New System.Drawing.Size(775, 123)
        Me.dgvApportionment.TabIndex = 76
        '
        'INIBUSINESSNOORGDataGridViewTextBoxColumn
        '
        Me.INIBUSINESSNOORGDataGridViewTextBoxColumn.DataPropertyName = "INI_BUSINESS_NO_ORG"
        Me.INIBUSINESSNOORGDataGridViewTextBoxColumn.HeaderText = "INI_BUSINESS_NO_ORG"
        Me.INIBUSINESSNOORGDataGridViewTextBoxColumn.Name = "INIBUSINESSNOORGDataGridViewTextBoxColumn"
        Me.INIBUSINESSNOORGDataGridViewTextBoxColumn.ReadOnly = True
        Me.INIBUSINESSNOORGDataGridViewTextBoxColumn.Visible = False
        '
        'INIEXTERNALINVNOINVDataGridViewTextBoxColumn
        '
        Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn.DataPropertyName = "INI_EXTERNAL_INV_NO_INV"
        Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn.HeaderText = "INI_EXTERNAL_INV_NO_INV"
        Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn.Name = "INIEXTERNALINVNOINVDataGridViewTextBoxColumn"
        Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn.ReadOnly = True
        Me.INIEXTERNALINVNOINVDataGridViewTextBoxColumn.Visible = False
        '
        'INIINVOICEDTINVDataGridViewTextBoxColumn
        '
        Me.INIINVOICEDTINVDataGridViewTextBoxColumn.DataPropertyName = "INI_INVOICE_DT_INV"
        Me.INIINVOICEDTINVDataGridViewTextBoxColumn.HeaderText = "INI_INVOICE_DT_INV"
        Me.INIINVOICEDTINVDataGridViewTextBoxColumn.Name = "INIINVOICEDTINVDataGridViewTextBoxColumn"
        Me.INIINVOICEDTINVDataGridViewTextBoxColumn.ReadOnly = True
        Me.INIINVOICEDTINVDataGridViewTextBoxColumn.Visible = False
        '
        'INIGENERATEDNOINIDataGridViewTextBoxColumn
        '
        Me.INIGENERATEDNOINIDataGridViewTextBoxColumn.DataPropertyName = "INI_GENERATED_NO_INI"
        Me.INIGENERATEDNOINIDataGridViewTextBoxColumn.HeaderText = "INI_GENERATED_NO_INI"
        Me.INIGENERATEDNOINIDataGridViewTextBoxColumn.Name = "INIGENERATEDNOINIDataGridViewTextBoxColumn"
        Me.INIGENERATEDNOINIDataGridViewTextBoxColumn.ReadOnly = True
        Me.INIGENERATEDNOINIDataGridViewTextBoxColumn.Visible = False
        '
        'GENERATEDNODataGridViewTextBoxColumn1
        '
        Me.GENERATEDNODataGridViewTextBoxColumn1.DataPropertyName = "GENERATED_NO"
        Me.GENERATEDNODataGridViewTextBoxColumn1.HeaderText = "GENERATED_NO"
        Me.GENERATEDNODataGridViewTextBoxColumn1.Name = "GENERATEDNODataGridViewTextBoxColumn1"
        Me.GENERATEDNODataGridViewTextBoxColumn1.ReadOnly = True
        Me.GENERATEDNODataGridViewTextBoxColumn1.Visible = False
        '
        'CPCCASENOCSEDataGridViewTextBoxColumn
        '
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "CPC_CASE_NO_CSE"
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.HeaderText = "Case No"
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.Name = "CPCCASENOCSEDataGridViewTextBoxColumn"
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.Width = 60
        '
        'CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "Package ID"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Width = 80
        '
        'CLIENTPACKAGENMDataGridViewTextBoxColumn
        '
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.DataPropertyName = "CLIENT_PACKAGE_NM"
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.HeaderText = "Package"
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.Name = "CLIENTPACKAGENMDataGridViewTextBoxColumn"
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'APPORTIONMENTDTDataGridViewTextBoxColumn
        '
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.DataPropertyName = "APPORTIONMENT_DT"
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.HeaderText = "Apportionment Date"
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.Name = "APPORTIONMENTDTDataGridViewTextBoxColumn"
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.Width = 140
        '
        'APPORTIONEDNETAMDataGridViewTextBoxColumn
        '
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn.DataPropertyName = "APPORTIONED_NET_AM"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle13
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn.HeaderText = "Apportioned Net"
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn.Name = "APPORTIONEDNETAMDataGridViewTextBoxColumn"
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.APPORTIONEDNETAMDataGridViewTextBoxColumn.Width = 120
        '
        'APPORTIONEDVATAMDataGridViewTextBoxColumn
        '
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn.DataPropertyName = "APPORTIONED_VAT_AM"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle14
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn.HeaderText = "Apportioned VAT"
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn.Name = "APPORTIONEDVATAMDataGridViewTextBoxColumn"
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.APPORTIONEDVATAMDataGridViewTextBoxColumn.Width = 120
        '
        'BILLUPLOADDTDataGridViewTextBoxColumn
        '
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.DataPropertyName = "BILL_UPLOAD_DT"
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.HeaderText = "Upload Date"
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.Name = "BILLUPLOADDTDataGridViewTextBoxColumn"
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.Visible = False
        '
        'USER_NM
        '
        Me.USER_NM.DataPropertyName = "USER_NM"
        Me.USER_NM.HeaderText = "Apportioned By"
        Me.USER_NM.Name = "USER_NM"
        Me.USER_NM.ReadOnly = True
        '
        'bsApportionment
        '
        Me.bsApportionment.DataMember = "V_fsubViewAllApptnmntsDetails_V_fsubManualAppmntApportion"
        Me.bsApportionment.DataSource = Me.bsInvoice
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taInvoice
        '
        Me.taInvoice.ClearBeforeFill = True
        '
        'taApportionment
        '
        Me.taApportionment.ClearBeforeFill = True
        '
        'frmViewAllApportionments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.pnlApportionment)
        Me.Controls.Add(Me.pnlInvoice)
        Me.Controls.Add(Me.pnlCase)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.KeyPreview = True
        Me.Name = "frmViewAllApportionments"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "View All Apportionments"
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlQueryCase.ResumeLayout(False)
        Me.pnlQueryCase.PerformLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsViewAllApportionments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCase.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvCase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInvoice.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlApportionment.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvApportionment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsApportionment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlQueryCase As System.Windows.Forms.Panel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents pnlCase As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvCase As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInvoice As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents pnlApportionment As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvApportionment As System.Windows.Forms.DataGridView
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents DsViewAllApportionments As clsRental.dsViewAllApportionments
    Friend WithEvents taCase As clsRental.dsViewAllApportionmentsTableAdapters.V_frmManualApportionmentsTableAdapter
    Friend WithEvents bsInvoice As System.Windows.Forms.BindingSource
    Friend WithEvents taInvoice As clsRental.dsViewAllApportionmentsTableAdapters.V_fsubViewAllApptnmntsDetailsTableAdapter
    Friend WithEvents bsApportionment As System.Windows.Forms.BindingSource
    Friend WithEvents taApportionment As clsRental.dsViewAllApportionmentsTableAdapters.V_fsubManualAppmntApportionTableAdapter
    Friend WithEvents GENERATEDNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASEORDERNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INV_BUSINESS_NO_ORG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INV_EXTERNAL_INV_NO_INV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INV_INVOICE_DT_INV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICEITEMAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ITEMVATAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPICASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONEDDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SuperLabel4 As ControlLibrary.SuperLabel
    Friend WithEvents txbAddress As ControlLibrary.SuperTextBox
    Friend WithEvents SuperLabel3 As ControlLibrary.SuperLabel
    Friend WithEvents txbPackageNm As ControlLibrary.SuperTextBox
    Friend WithEvents INIBUSINESSNOORGDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INIEXTERNALINVNOINVDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INIINVOICEDTINVDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INIGENERATEDNOINIDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GENERATEDNODataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPCCASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLIENTPACKAGENMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONMENTDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONEDNETAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONEDVATAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BILLUPLOADDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USER_NM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSE_CASE_NO_CSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboRentalTypeCd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClientPackageDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
