Public Class frmPersonCases
    Private blnInQueryMode As Boolean
    Private blnReadOnly As Boolean = False
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'blnReadOnly = clsGlobal.SetupForm(Me, True, False)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, True, False)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            HidePrimaryKeyFields()

            If CType(clsGlobal.SubMenuCaseId, String) <> "" And clsGlobal.SubMenuCaseId > 0 Then
                FillForm()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.SubMenuCaseId, String) <> "" And clsGlobal.SubMenuCaseId > 0 Then
                strQueryText = CType(clsGlobal.SubMenuCaseId, String)
            End If

            If txtCaseNo.Text <> "" Then
                If IsNumeric(txtCaseNo.Text) And CType(txtCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txtCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taCase.Connection = clsGlobal.Connection
                Me.taCase.FillCase(Me.DsPersonCases.V_frmPersonCase, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.txtCaseNo.Text <> "" Then
                    Me.taPersonCase.Connection = clsGlobal.Connection
                    Me.taPersonCase.FillPersonCase(Me.DsPersonCases.V_fsubPersonCase, New System.Nullable(Of Integer)(CType(Me.txtCaseNo.Text, Integer)))
                End If
            Else
                MsgBox("No records returned", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Me.Text)
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub
    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspRight_ItemClicked")
        End Try
    End Sub
    Private Sub HidePrimaryKeyFields()
        Me.txtVIP.Hide()
    End Sub
End Class