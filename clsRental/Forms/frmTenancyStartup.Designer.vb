﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmTenancyStartup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label6 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTenancyStartup))
        Me.bnCase = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsTenancyStartup = New clsRental.dsTenancyStartup()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.bsTenancy = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txtCaseNo = New ControlLibrary.SuperTextBox()
        Me.txtPackageNm = New System.Windows.Forms.TextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.txtAddressAstType = New System.Windows.Forms.TextBox()
        Me.txtPackageId = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.pnlTenancy = New System.Windows.Forms.Panel()
        Me.lblCharactersRemainingNotes = New System.Windows.Forms.Label()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.lblCharactersRemaining = New System.Windows.Forms.Label()
        Me.txtTenancyStartDt = New System.Windows.Forms.TextBox()
        Me.txtTenancyEndDt = New System.Windows.Forms.TextBox()
        Me.txtDepositPaidDt = New System.Windows.Forms.TextBox()
        Me.txtDepositReturnedDt = New System.Windows.Forms.TextBox()
        Me.pbxDepositPaidDt = New System.Windows.Forms.PictureBox()
        Me.pbxTenancyEndDt = New System.Windows.Forms.PictureBox()
        Me.pbxTenancyStartDt = New System.Windows.Forms.PictureBox()
        Me.pbxDepositReturnedDt = New System.Windows.Forms.PictureBox()
        Me.txtDepositReturnedAm = New System.Windows.Forms.TextBox()
        Me.txtDepositPaidAm = New System.Windows.Forms.TextBox()
        Me.txtClaimComments = New System.Windows.Forms.TextBox()
        Me.txtFinalAgreedClaimAm = New System.Windows.Forms.TextBox()
        Me.txtOriginalClaimAm = New System.Windows.Forms.TextBox()
        Me.bnTenancy = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbAdd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.tssAdd = New System.Windows.Forms.ToolStripSeparator()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtTSPGeneratedNo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.taCase = New clsRental.dsTenancyStartupTableAdapters.V_frmTenancyStartupTableAdapter()
        Me.TableAdapterManager = New clsRental.dsTenancyStartupTableAdapters.TableAdapterManager()
        Me.taTenancy = New clsRental.dsTenancyStartupTableAdapters.V_fsubTenancyStartupTableAdapter()
        Label6 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        CType(Me.bnCase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnCase.SuspendLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsTenancyStartup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCase.SuspendLayout()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTenancy.SuspendLayout()
        CType(Me.pbxDepositPaidDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxTenancyEndDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxTenancyStartDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxDepositReturnedDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTenancy.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label6.Location = New System.Drawing.Point(487, 23)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(49, 14)
        Label6.TabIndex = 116
        Label6.Text = "Address"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label14.Location = New System.Drawing.Point(191, 23)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(48, 14)
        Label14.TabIndex = 2
        Label14.Text = "Package"
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label23.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label23.Location = New System.Drawing.Point(284, 146)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(103, 14)
        Label23.TabIndex = 22
        Label23.Text = "Deposit Returned Dt"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label22.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label22.Location = New System.Drawing.Point(26, 148)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(129, 14)
        Label22.TabIndex = 20
        Label22.Text = "Deposit Returned Amount"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label21.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label21.Location = New System.Drawing.Point(26, 70)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(105, 14)
        Label21.TabIndex = 18
        Label21.Text = "Deposit Paid Amount"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label17.Location = New System.Drawing.Point(26, 122)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(134, 14)
        Label17.TabIndex = 10
        Label17.Text = "Final Agreed Claim Amount"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label16.Location = New System.Drawing.Point(25, 173)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(85, 14)
        Label16.TabIndex = 8
        Label16.Text = "Claim Comments"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label11.Location = New System.Drawing.Point(26, 96)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(110, 14)
        Label11.TabIndex = 6
        Label11.Text = "Original Claim Amount"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label1.Location = New System.Drawing.Point(26, 43)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(87, 14)
        Label1.TabIndex = 201
        Label1.Text = "Tenancy Start Dt"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label3.Location = New System.Drawing.Point(284, 43)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(82, 14)
        Label3.TabIndex = 204
        Label3.Text = "Tenancy End Dt"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(284, 69)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(79, 14)
        Label4.TabIndex = 207
        Label4.Text = "Deposit Paid Dt"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(191, 49)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(33, 14)
        Label5.TabIndex = 193
        Label5.Text = "Client"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label8.Location = New System.Drawing.Point(25, 269)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(35, 14)
        Label8.TabIndex = 244
        Label8.Text = "Notes"
        '
        'bnCase
        '
        Me.bnCase.AddNewItem = Nothing
        Me.bnCase.BindingSource = Me.bsCase
        Me.bnCase.CountItem = Me.BindingNavigatorCountItem
        Me.bnCase.DeleteItem = Nothing
        Me.bnCase.Dock = System.Windows.Forms.DockStyle.None
        Me.bnCase.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnCase.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.bnCase.Location = New System.Drawing.Point(13, 70)
        Me.bnCase.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.bnCase.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.bnCase.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.bnCase.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.bnCase.Name = "bnCase"
        Me.bnCase.PositionItem = Me.BindingNavigatorPositionItem
        Me.bnCase.Size = New System.Drawing.Size(169, 25)
        Me.bnCase.TabIndex = 0
        Me.bnCase.Text = "BindingNavigator1"
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmTenancyStartup"
        Me.bsCase.DataSource = Me.DsTenancyStartup
        '
        'DsTenancyStartup
        '
        Me.DsTenancyStartup.DataSetName = "dsTenancyStartup"
        Me.DsTenancyStartup.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(25, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'bsTenancy
        '
        Me.bsTenancy.AllowNew = True
        Me.bsTenancy.DataMember = "V_frmTenancyStartup_V_fsubTenancyStartup"
        Me.bsTenancy.DataSource = Me.bsCase
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlCase)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(795, 155)
        Me.Panel8.TabIndex = 5
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlCase
        '
        Me.pnlCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlCase.Controls.Add(Label5)
        Me.pnlCase.Controls.Add(Me.TextBox1)
        Me.pnlCase.Controls.Add(Me.pbxVIP)
        Me.pnlCase.Controls.Add(Label6)
        Me.pnlCase.Controls.Add(Me.txtAddress)
        Me.pnlCase.Controls.Add(Me.SuperLabel2)
        Me.pnlCase.Controls.Add(Me.pbxCopy)
        Me.pnlCase.Controls.Add(Me.bnCase)
        Me.pnlCase.Controls.Add(Me.txtCaseNo)
        Me.pnlCase.Controls.Add(Label14)
        Me.pnlCase.Controls.Add(Me.txtPackageNm)
        Me.pnlCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(778, 108)
        Me.pnlCase.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CLIENT", True))
        Me.TextBox1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox1.Location = New System.Drawing.Point(245, 46)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(236, 20)
        Me.TextBox1.TabIndex = 192
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(153, 8)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.pbxVIP.Visible = False
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "PROPERTY_ADDRESS", True))
        Me.txtAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAddress.Location = New System.Drawing.Point(542, 20)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ReadOnly = True
        Me.txtAddress.Size = New System.Drawing.Size(221, 20)
        Me.txtAddress.TabIndex = 2
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 23)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(133, 23)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txtCaseNo
        '
        Me.txtCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CPC_CASE_NO_CSE", True))
        Me.txtCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCaseNo.Location = New System.Drawing.Point(79, 20)
        Me.txtCaseNo.Name = "txtCaseNo"
        Me.txtCaseNo.PreviousQuery = Nothing
        Me.txtCaseNo.Queryable = True
        Me.txtCaseNo.QueryMandatory = False
        Me.txtCaseNo.ReadOnly = True
        Me.txtCaseNo.Size = New System.Drawing.Size(48, 20)
        Me.txtCaseNo.TabIndex = 0
        Me.txtCaseNo.Updateable = False
        '
        'txtPackageNm
        '
        Me.txtPackageNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtPackageNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CLIENT_PACKAGE", True))
        Me.txtPackageNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackageNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtPackageNm.Location = New System.Drawing.Point(245, 20)
        Me.txtPackageNm.Name = "txtPackageNm"
        Me.txtPackageNm.ReadOnly = True
        Me.txtPackageNm.Size = New System.Drawing.Size(236, 20)
        Me.txtPackageNm.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.txtAddressAstType)
        Me.Panel10.Controls.Add(Me.txtPackageId)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(753, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(317, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'txtAddressAstType
        '
        Me.txtAddressAstType.BackColor = System.Drawing.Color.Yellow
        Me.txtAddressAstType.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CCP_ADDR_AST_TYPE_CD_CCP", True))
        Me.txtAddressAstType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressAstType.ForeColor = System.Drawing.Color.DimGray
        Me.txtAddressAstType.Location = New System.Drawing.Point(394, 2)
        Me.txtAddressAstType.Name = "txtAddressAstType"
        Me.txtAddressAstType.ReadOnly = True
        Me.txtAddressAstType.Size = New System.Drawing.Size(43, 20)
        Me.txtAddressAstType.TabIndex = 148
        '
        'txtPackageId
        '
        Me.txtPackageId.BackColor = System.Drawing.Color.Yellow
        Me.txtPackageId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CPC_CLIENT_PACKAGE_ID_CGS", True))
        Me.txtPackageId.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackageId.ForeColor = System.Drawing.Color.DimGray
        Me.txtPackageId.Location = New System.Drawing.Point(345, 2)
        Me.txtPackageId.Name = "txtPackageId"
        Me.txtPackageId.ReadOnly = True
        Me.txtPackageId.Size = New System.Drawing.Size(43, 20)
        Me.txtPackageId.TabIndex = 147
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Case"
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(134, 19)
        Me.SuperLabel1.TabIndex = 11
        Me.SuperLabel1.Text = "Tenancy Startup"
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 9
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 10
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 8
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.pnlTenancy)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(0, 227)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(796, 472)
        Me.Panel1.TabIndex = 12
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'pnlTenancy
        '
        Me.pnlTenancy.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlTenancy.Controls.Add(Me.lblCharactersRemainingNotes)
        Me.pnlTenancy.Controls.Add(Me.txtNotes)
        Me.pnlTenancy.Controls.Add(Label8)
        Me.pnlTenancy.Controls.Add(Me.lblCharactersRemaining)
        Me.pnlTenancy.Controls.Add(Me.txtTenancyStartDt)
        Me.pnlTenancy.Controls.Add(Me.txtTenancyEndDt)
        Me.pnlTenancy.Controls.Add(Me.txtDepositPaidDt)
        Me.pnlTenancy.Controls.Add(Me.txtDepositReturnedDt)
        Me.pnlTenancy.Controls.Add(Me.pbxDepositPaidDt)
        Me.pnlTenancy.Controls.Add(Label4)
        Me.pnlTenancy.Controls.Add(Me.pbxTenancyEndDt)
        Me.pnlTenancy.Controls.Add(Label3)
        Me.pnlTenancy.Controls.Add(Me.pbxTenancyStartDt)
        Me.pnlTenancy.Controls.Add(Label1)
        Me.pnlTenancy.Controls.Add(Me.pbxDepositReturnedDt)
        Me.pnlTenancy.Controls.Add(Label23)
        Me.pnlTenancy.Controls.Add(Label22)
        Me.pnlTenancy.Controls.Add(Me.txtDepositReturnedAm)
        Me.pnlTenancy.Controls.Add(Label21)
        Me.pnlTenancy.Controls.Add(Me.txtDepositPaidAm)
        Me.pnlTenancy.Controls.Add(Label17)
        Me.pnlTenancy.Controls.Add(Me.txtClaimComments)
        Me.pnlTenancy.Controls.Add(Label16)
        Me.pnlTenancy.Controls.Add(Me.txtFinalAgreedClaimAm)
        Me.pnlTenancy.Controls.Add(Label11)
        Me.pnlTenancy.Controls.Add(Me.txtOriginalClaimAm)
        Me.pnlTenancy.Controls.Add(Me.bnTenancy)
        Me.pnlTenancy.Location = New System.Drawing.Point(8, 32)
        Me.pnlTenancy.Name = "pnlTenancy"
        Me.pnlTenancy.Size = New System.Drawing.Size(778, 427)
        Me.pnlTenancy.TabIndex = 1
        '
        'lblCharactersRemainingNotes
        '
        Me.lblCharactersRemainingNotes.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCharactersRemainingNotes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lblCharactersRemainingNotes.Location = New System.Drawing.Point(354, 343)
        Me.lblCharactersRemainingNotes.Name = "lblCharactersRemainingNotes"
        Me.lblCharactersRemainingNotes.Size = New System.Drawing.Size(151, 16)
        Me.lblCharactersRemainingNotes.TabIndex = 245
        Me.lblCharactersRemainingNotes.Text = "Characters Remaining: "
        Me.lblCharactersRemainingNotes.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtNotes
        '
        Me.txtNotes.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtNotes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "NOTES_TX", True))
        Me.txtNotes.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtNotes.Location = New System.Drawing.Point(174, 266)
        Me.txtNotes.MaxLength = 250
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ReadOnly = True
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNotes.Size = New System.Drawing.Size(331, 74)
        Me.txtNotes.TabIndex = 243
        '
        'lblCharactersRemaining
        '
        Me.lblCharactersRemaining.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCharactersRemaining.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lblCharactersRemaining.Location = New System.Drawing.Point(354, 247)
        Me.lblCharactersRemaining.Name = "lblCharactersRemaining"
        Me.lblCharactersRemaining.Size = New System.Drawing.Size(151, 16)
        Me.lblCharactersRemaining.TabIndex = 242
        Me.lblCharactersRemaining.Text = "Characters Remaining: "
        Me.lblCharactersRemaining.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtTenancyStartDt
        '
        Me.txtTenancyStartDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtTenancyStartDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "TENANCY_START_DT", True))
        Me.txtTenancyStartDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyStartDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyStartDt.Location = New System.Drawing.Point(174, 40)
        Me.txtTenancyStartDt.Name = "txtTenancyStartDt"
        Me.txtTenancyStartDt.ReadOnly = True
        Me.txtTenancyStartDt.Size = New System.Drawing.Size(60, 20)
        Me.txtTenancyStartDt.TabIndex = 212
        Me.txtTenancyStartDt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTenancyEndDt
        '
        Me.txtTenancyEndDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtTenancyEndDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "TENANCY_END_DT", True))
        Me.txtTenancyEndDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyEndDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyEndDt.Location = New System.Drawing.Point(419, 40)
        Me.txtTenancyEndDt.Name = "txtTenancyEndDt"
        Me.txtTenancyEndDt.ReadOnly = True
        Me.txtTenancyEndDt.Size = New System.Drawing.Size(60, 20)
        Me.txtTenancyEndDt.TabIndex = 6
        Me.txtTenancyEndDt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDepositPaidDt
        '
        Me.txtDepositPaidDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtDepositPaidDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "DEPOSIT_PAID_DT", True))
        Me.txtDepositPaidDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepositPaidDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtDepositPaidDt.Location = New System.Drawing.Point(419, 67)
        Me.txtDepositPaidDt.Name = "txtDepositPaidDt"
        Me.txtDepositPaidDt.ReadOnly = True
        Me.txtDepositPaidDt.Size = New System.Drawing.Size(60, 20)
        Me.txtDepositPaidDt.TabIndex = 7
        Me.txtDepositPaidDt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDepositReturnedDt
        '
        Me.txtDepositReturnedDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtDepositReturnedDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "DEPOSIT_RETURNED_DT", True))
        Me.txtDepositReturnedDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepositReturnedDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtDepositReturnedDt.Location = New System.Drawing.Point(419, 144)
        Me.txtDepositReturnedDt.Name = "txtDepositReturnedDt"
        Me.txtDepositReturnedDt.ReadOnly = True
        Me.txtDepositReturnedDt.Size = New System.Drawing.Size(60, 20)
        Me.txtDepositReturnedDt.TabIndex = 8
        Me.txtDepositReturnedDt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pbxDepositPaidDt
        '
        Me.pbxDepositPaidDt.Image = CType(resources.GetObject("pbxDepositPaidDt.Image"), System.Drawing.Image)
        Me.pbxDepositPaidDt.Location = New System.Drawing.Point(491, 69)
        Me.pbxDepositPaidDt.Name = "pbxDepositPaidDt"
        Me.pbxDepositPaidDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxDepositPaidDt.TabIndex = 208
        Me.pbxDepositPaidDt.TabStop = False
        '
        'pbxTenancyEndDt
        '
        Me.pbxTenancyEndDt.Image = CType(resources.GetObject("pbxTenancyEndDt.Image"), System.Drawing.Image)
        Me.pbxTenancyEndDt.Location = New System.Drawing.Point(491, 42)
        Me.pbxTenancyEndDt.Name = "pbxTenancyEndDt"
        Me.pbxTenancyEndDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyEndDt.TabIndex = 205
        Me.pbxTenancyEndDt.TabStop = False
        '
        'pbxTenancyStartDt
        '
        Me.pbxTenancyStartDt.Image = CType(resources.GetObject("pbxTenancyStartDt.Image"), System.Drawing.Image)
        Me.pbxTenancyStartDt.Location = New System.Drawing.Point(246, 42)
        Me.pbxTenancyStartDt.Name = "pbxTenancyStartDt"
        Me.pbxTenancyStartDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyStartDt.TabIndex = 202
        Me.pbxTenancyStartDt.TabStop = False
        '
        'pbxDepositReturnedDt
        '
        Me.pbxDepositReturnedDt.Image = CType(resources.GetObject("pbxDepositReturnedDt.Image"), System.Drawing.Image)
        Me.pbxDepositReturnedDt.Location = New System.Drawing.Point(491, 146)
        Me.pbxDepositReturnedDt.Name = "pbxDepositReturnedDt"
        Me.pbxDepositReturnedDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxDepositReturnedDt.TabIndex = 199
        Me.pbxDepositReturnedDt.TabStop = False
        '
        'txtDepositReturnedAm
        '
        Me.txtDepositReturnedAm.AcceptsReturn = True
        Me.txtDepositReturnedAm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtDepositReturnedAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "DEPOSIT_RETURNED_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtDepositReturnedAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepositReturnedAm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtDepositReturnedAm.Location = New System.Drawing.Point(174, 144)
        Me.txtDepositReturnedAm.Name = "txtDepositReturnedAm"
        Me.txtDepositReturnedAm.ReadOnly = True
        Me.txtDepositReturnedAm.Size = New System.Drawing.Size(60, 20)
        Me.txtDepositReturnedAm.TabIndex = 4
        Me.txtDepositReturnedAm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDepositPaidAm
        '
        Me.txtDepositPaidAm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtDepositPaidAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "DEPOSIT_PAID_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtDepositPaidAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepositPaidAm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtDepositPaidAm.Location = New System.Drawing.Point(174, 66)
        Me.txtDepositPaidAm.Name = "txtDepositPaidAm"
        Me.txtDepositPaidAm.ReadOnly = True
        Me.txtDepositPaidAm.Size = New System.Drawing.Size(60, 20)
        Me.txtDepositPaidAm.TabIndex = 1
        Me.txtDepositPaidAm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtClaimComments
        '
        Me.txtClaimComments.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtClaimComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "DILAP_CLAIM_COMMENTS_TX", True))
        Me.txtClaimComments.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimComments.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtClaimComments.Location = New System.Drawing.Point(174, 170)
        Me.txtClaimComments.MaxLength = 250
        Me.txtClaimComments.Multiline = True
        Me.txtClaimComments.Name = "txtClaimComments"
        Me.txtClaimComments.ReadOnly = True
        Me.txtClaimComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtClaimComments.Size = New System.Drawing.Size(331, 74)
        Me.txtClaimComments.TabIndex = 5
        '
        'txtFinalAgreedClaimAm
        '
        Me.txtFinalAgreedClaimAm.AcceptsReturn = True
        Me.txtFinalAgreedClaimAm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtFinalAgreedClaimAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "FINAL_AGREED_DILAP_CLAIM_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtFinalAgreedClaimAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinalAgreedClaimAm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtFinalAgreedClaimAm.Location = New System.Drawing.Point(174, 118)
        Me.txtFinalAgreedClaimAm.Name = "txtFinalAgreedClaimAm"
        Me.txtFinalAgreedClaimAm.ReadOnly = True
        Me.txtFinalAgreedClaimAm.Size = New System.Drawing.Size(60, 20)
        Me.txtFinalAgreedClaimAm.TabIndex = 3
        Me.txtFinalAgreedClaimAm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOriginalClaimAm
        '
        Me.txtOriginalClaimAm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtOriginalClaimAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "ORIGINAL_DILAP_CLAIM_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtOriginalClaimAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOriginalClaimAm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtOriginalClaimAm.Location = New System.Drawing.Point(174, 92)
        Me.txtOriginalClaimAm.Name = "txtOriginalClaimAm"
        Me.txtOriginalClaimAm.ReadOnly = True
        Me.txtOriginalClaimAm.Size = New System.Drawing.Size(60, 20)
        Me.txtOriginalClaimAm.TabIndex = 2
        Me.txtOriginalClaimAm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'bnTenancy
        '
        Me.bnTenancy.AddNewItem = Me.tsbAdd
        Me.bnTenancy.BindingSource = Me.bsTenancy
        Me.bnTenancy.CountItem = Me.ToolStripLabel1
        Me.bnTenancy.DeleteItem = Nothing
        Me.bnTenancy.Dock = System.Windows.Forms.DockStyle.None
        Me.bnTenancy.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnTenancy.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.ToolStripButton3, Me.ToolStripButton4, Me.tssAdd, Me.tsbAdd})
        Me.bnTenancy.Location = New System.Drawing.Point(13, 385)
        Me.bnTenancy.MoveFirstItem = Me.ToolStripButton1
        Me.bnTenancy.MoveLastItem = Me.ToolStripButton4
        Me.bnTenancy.MoveNextItem = Me.ToolStripButton3
        Me.bnTenancy.MovePreviousItem = Me.ToolStripButton2
        Me.bnTenancy.Name = "bnTenancy"
        Me.bnTenancy.PositionItem = Me.ToolStripTextBox1
        Me.bnTenancy.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.bnTenancy.Size = New System.Drawing.Size(198, 25)
        Me.bnTenancy.TabIndex = 35
        Me.bnTenancy.Text = "BindingNavigator1"
        '
        'tsbAdd
        '
        Me.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAdd.Image = CType(resources.GetObject("tsbAdd.Image"), System.Drawing.Image)
        Me.tsbAdd.Name = "tsbAdd"
        Me.tsbAdd.RightToLeftAutoMirrorImage = True
        Me.tsbAdd.Size = New System.Drawing.Size(23, 22)
        Me.tsbAdd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 22)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Move first"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(25, 21)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Move next"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Move last"
        '
        'tssAdd
        '
        Me.tssAdd.Name = "tssAdd"
        Me.tssAdd.Size = New System.Drawing.Size(6, 25)
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.txtTSPGeneratedNo)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(753, 24)
        Me.Panel2.TabIndex = 0
        '
        'txtTSPGeneratedNo
        '
        Me.txtTSPGeneratedNo.BackColor = System.Drawing.Color.Yellow
        Me.txtTSPGeneratedNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "GENERATED_NO_TSP", True))
        Me.txtTSPGeneratedNo.Location = New System.Drawing.Point(316, 2)
        Me.txtTSPGeneratedNo.Name = "txtTSPGeneratedNo"
        Me.txtTSPGeneratedNo.Size = New System.Drawing.Size(46, 20)
        Me.txtTSPGeneratedNo.TabIndex = 187
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Tenancy Details"
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.UpdateOrder = clsRental.dsTenancyStartupTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.V_fsubTenancyStartupTableAdapter = Nothing
        '
        'taTenancy
        '
        Me.taTenancy.ClearBeforeFill = True
        '
        'frmTenancyStartup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 699)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.Panel8)
        Me.KeyPreview = True
        Me.Name = "frmTenancyStartup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Tenancy Startup"
        CType(Me.bnCase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnCase.ResumeLayout(False)
        Me.bnCase.PerformLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsTenancyStartup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCase.ResumeLayout(False)
        Me.pnlCase.PerformLayout()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTenancy.ResumeLayout(False)
        Me.pnlTenancy.PerformLayout()
        CType(Me.pbxDepositPaidDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxTenancyEndDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxTenancyStartDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxDepositReturnedDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTenancy.ResumeLayout(False)
        Me.bnTenancy.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DsTenancyStartup As dsTenancyStartup
    Friend WithEvents bsCase As BindingSource
    Friend WithEvents taCase As dsTenancyStartupTableAdapters.V_frmTenancyStartupTableAdapter
    Friend WithEvents TableAdapterManager As dsTenancyStartupTableAdapters.TableAdapterManager
    Friend WithEvents bnCase As BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents bsTenancy As BindingSource
    Friend WithEvents taTenancy As dsTenancyStartupTableAdapters.V_fsubTenancyStartupTableAdapter
    Friend WithEvents Panel8 As Panel
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents pnlCase As Panel
    Friend WithEvents pbxVIP As PictureBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As PictureBox
    Friend WithEvents txtCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents txtPackageNm As TextBox
    Friend WithEvents Panel10 As Panel
    Friend WithEvents txtVIP As TextBox
    Friend WithEvents txtAddressAstType As TextBox
    Friend WithEvents txtPackageId As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents pnlFormStrip As Panel
    Friend WithEvents pnlMainToolStrip As Panel
    Friend WithEvents tspRight As ToolStrip
    Friend WithEvents tspMain As ToolStrip
    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents pnlTenancy As Panel
    Friend WithEvents pbxDepositReturnedDt As PictureBox
    Friend WithEvents txtDepositReturnedAm As TextBox
    Friend WithEvents txtDepositPaidAm As TextBox
    Friend WithEvents txtClaimComments As TextBox
    Friend WithEvents txtFinalAgreedClaimAm As TextBox
    Friend WithEvents txtOriginalClaimAm As TextBox
    Friend WithEvents bnTenancy As BindingNavigator
    Friend WithEvents tsbAdd As ToolStripButton
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripButton3 As ToolStripButton
    Friend WithEvents ToolStripButton4 As ToolStripButton
    Friend WithEvents tssAdd As ToolStripSeparator
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents pbxDepositPaidDt As PictureBox
    Friend WithEvents pbxTenancyEndDt As PictureBox
    Friend WithEvents pbxTenancyStartDt As PictureBox
    Friend WithEvents txtTenancyStartDt As TextBox
    Friend WithEvents txtTenancyEndDt As TextBox
    Friend WithEvents txtDepositPaidDt As TextBox
    Friend WithEvents txtDepositReturnedDt As TextBox
    Friend WithEvents txtTSPGeneratedNo As TextBox
    Friend WithEvents lblCharactersRemaining As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents lblCharactersRemainingNotes As Label
    Friend WithEvents txtNotes As TextBox
End Class
