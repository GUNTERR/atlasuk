<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainRentalProperty
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FULL_NMLabel As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label32 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim COMPLETION_DTLabel As System.Windows.Forms.Label
        Dim CANCELLATION_DTLabel As System.Windows.Forms.Label
        Dim CANCELLATION_CDLabel As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim Label35 As System.Windows.Forms.Label
        Dim Label36 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainRentalProperty))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxLetter = New System.Windows.Forms.PictureBox()
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.pnlAsset = New System.Windows.Forms.Panel()
        Me.lnlCompleteTenancy = New System.Windows.Forms.LinkLabel()
        Me.txtTenancyCompleteDt = New System.Windows.Forms.TextBox()
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsMaintainRentalProperty = New clsRental.dsMaintainRentalProperty()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.bsAsset = New System.Windows.Forms.BindingSource(Me.components)
        Me.chkStandingOrder = New ControlLibrary.SuperCheckBox()
        Me.txtEmpRentContribution = New System.Windows.Forms.TextBox()
        Me.txtTenancyBudget = New System.Windows.Forms.TextBox()
        Me.pbxDepositReturnedDt = New System.Windows.Forms.PictureBox()
        Me.txbDepositReturnedDt = New System.Windows.Forms.MaskedTextBox()
        Me.txbAgentContact = New System.Windows.Forms.TextBox()
        Me.bsAgent = New System.Windows.Forms.BindingSource(Me.components)
        Me.txbAgentAddress = New System.Windows.Forms.TextBox()
        Me.txbLettingAgent = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.chkBreakClause = New ControlLibrary.SuperCheckBox()
        Me.chkInterestBearing = New ControlLibrary.SuperCheckBox()
        Me.cboDeposit = New ControlLibrary.SuperComboBox()
        Me.cboLeaseType = New ControlLibrary.SuperComboBox()
        Me.pbxFormReceivedDt = New System.Windows.Forms.PictureBox()
        Me.txbFormReceivedDt = New System.Windows.Forms.MaskedTextBox()
        Me.chkWaterMeter = New ControlLibrary.SuperCheckBox()
        Me.chkChildrenAllowed = New ControlLibrary.SuperCheckBox()
        Me.chkSmokersAllowed = New ControlLibrary.SuperCheckBox()
        Me.chkPetsAllowed = New ControlLibrary.SuperCheckBox()
        Me.chkGarage = New ControlLibrary.SuperCheckBox()
        Me.chkRearGarden = New ControlLibrary.SuperCheckBox()
        Me.chkFrontGarden = New ControlLibrary.SuperCheckBox()
        Me.cboManagementCode = New ControlLibrary.SuperComboBox()
        Me.cboFurnishingCode = New ControlLibrary.SuperComboBox()
        Me.cboPropertyType = New ControlLibrary.SuperComboBox()
        Me.cboPropertyStructure = New ControlLibrary.SuperComboBox()
        Me.txbDepositReturnedAm = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.txtFinalAgreedClaimAm = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbAdd = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.tssAdd = New System.Windows.Forms.ToolStripSeparator()
        Me.txbBedroomNo = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnlTenancy = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lnlTAndP = New System.Windows.Forms.LinkLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.dgvTenancy = New System.Windows.Forms.DataGridView()
        Me.AST_CASE_NO_CSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AST_CLIENT_PACKAGE_ID_CGS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AST_GENERATED_NO_ADD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TENANCY_START_DT = New clsLibraryInstance.MaskedDateColumn()
        Me.TENANCYENDDTDataGridViewTextBoxColumn = New clsLibraryInstance.MaskedDateColumn()
        Me.NOTICEDTDataGridViewTextBoxColumn = New clsLibraryInstance.MaskedDateColumn()
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn = New clsLibraryInstance.MaskedDateColumn()
        Me.TENANCY_DURATION_DAYS_QT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkPeriodic = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.NOTICEPERIODDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUSTXDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOTICEPERIODQTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOTICEPERIODCDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsTenancy = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.txbAddress = New System.Windows.Forms.TextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.txbPackageNm = New System.Windows.Forms.TextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.txtAddressAstType = New System.Windows.Forms.TextBox()
        Me.txtPackageId = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.pnlRent = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvRent = New System.Windows.Forms.DataGridView()
        Me.ASTCASENOCSEDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RENTSTARTDTDataGridViewTextBoxColumn = New clsLibraryInstance.MaskedDateColumn()
        Me.RENTENDDTDataGridViewTextBoxColumn = New clsLibraryInstance.MaskedDateColumn()
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Freq = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsRent = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.taCase = New clsRental.dsMaintainRentalPropertyTableAdapters.V_frmMaintainRentalPropertyTableAdapter()
        Me.taAsset = New clsRental.dsMaintainRentalPropertyTableAdapters.V_fsubMaintainRentalPropDetailTableAdapter()
        Me.taTenancy = New clsRental.dsMaintainRentalPropertyTableAdapters.V_fsubMaintainRentlPropTenancyTableAdapter()
        Me.taRent = New clsRental.dsMaintainRentalPropertyTableAdapters.V_fsubMaintainRentalPropRentTableAdapter()
        Me.taAgent = New clsRental.dsMaintainRentalPropertyTableAdapters.V_vrtLettingAgentTableAdapter()
        FULL_NMLabel = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label32 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        COMPLETION_DTLabel = New System.Windows.Forms.Label()
        CANCELLATION_DTLabel = New System.Windows.Forms.Label()
        CANCELLATION_CDLabel = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Label35 = New System.Windows.Forms.Label()
        Label36 = New System.Windows.Forms.Label()
        CType(Me.pbxLetter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFormStrip.SuspendLayout()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAsset.SuspendLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMaintainRentalProperty, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsAsset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxDepositReturnedDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsAgent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxFormReceivedDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlTenancy.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCase.SuspendLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.pnlRent.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvRent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FULL_NMLabel
        '
        FULL_NMLabel.AutoSize = True
        FULL_NMLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        FULL_NMLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FULL_NMLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        FULL_NMLabel.Location = New System.Drawing.Point(26, 20)
        FULL_NMLabel.Name = "FULL_NMLabel"
        FULL_NMLabel.Size = New System.Drawing.Size(75, 14)
        FULL_NMLabel.TabIndex = 0
        FULL_NMLabel.Text = "No. Bedrooms"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label14.Location = New System.Drawing.Point(192, 16)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(48, 14)
        Label14.TabIndex = 2
        Label14.Text = "Package"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(26, 46)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(96, 14)
        Label4.TabIndex = 2
        Label4.Text = "Property Structure"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label11.Location = New System.Drawing.Point(26, 205)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(110, 14)
        Label11.TabIndex = 6
        Label11.Text = "Original Claim Amount"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label16.Location = New System.Drawing.Point(26, 257)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(85, 14)
        Label16.TabIndex = 8
        Label16.Text = "Claim Comments"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label17.Location = New System.Drawing.Point(26, 231)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(134, 14)
        Label17.TabIndex = 10
        Label17.Text = "Final Agreed Claim Amount"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label18.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label18.Location = New System.Drawing.Point(26, 75)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(74, 14)
        Label18.TabIndex = 12
        Label18.Text = "Property Type"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label19.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label19.Location = New System.Drawing.Point(26, 179)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(85, 14)
        Label19.TabIndex = 14
        Label19.Text = "Furnishing Code"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label20.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label20.Location = New System.Drawing.Point(322, 20)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(96, 14)
        Label20.TabIndex = 16
        Label20.Text = "Management Code"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label21.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label21.Location = New System.Drawing.Point(322, 47)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(126, 14)
        Label21.TabIndex = 18
        Label21.Text = "Tenancy Deposit Amount"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label22.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label22.Location = New System.Drawing.Point(322, 93)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(129, 14)
        Label22.TabIndex = 20
        Label22.Text = "Deposit Returned Amount"
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label23.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label23.Location = New System.Drawing.Point(322, 119)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(115, 14)
        Label23.TabIndex = 22
        Label23.Text = "Date Deposit Returned"
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label27.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label27.Location = New System.Drawing.Point(588, 46)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(63, 14)
        Label27.TabIndex = 32
        Label27.Text = "Lease Type"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label28.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label28.Location = New System.Drawing.Point(322, 180)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(128, 14)
        Label28.TabIndex = 24
        Label28.Text = "Break Clause Description"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label6.Location = New System.Drawing.Point(495, 16)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(49, 14)
        Label6.TabIndex = 116
        Label6.Text = "Address"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label7.Location = New System.Drawing.Point(26, 97)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(77, 14)
        Label7.TabIndex = 128
        Label7.Text = "Front Garden?"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(26, 117)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(75, 14)
        Label5.TabIndex = 130
        Label5.Text = "Rear Garden?"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label8.Location = New System.Drawing.Point(26, 137)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(49, 14)
        Label8.TabIndex = 132
        Label8.Text = "Garage?"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label9.Location = New System.Drawing.Point(26, 157)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(76, 14)
        Label9.TabIndex = 134
        Label9.Text = "Pets Allowed?"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label10.Location = New System.Drawing.Point(191, 117)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(97, 14)
        Label10.TabIndex = 136
        Label10.Text = "Smokers Allowed?"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label12.Location = New System.Drawing.Point(191, 137)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(94, 14)
        Label12.TabIndex = 138
        Label12.Text = "Children Allowed?"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label13.Location = New System.Drawing.Point(191, 157)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(72, 14)
        Label13.TabIndex = 140
        Label13.Text = "Water Meter?"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label29.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label29.Location = New System.Drawing.Point(588, 20)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(84, 14)
        Label29.TabIndex = 181
        Label29.Text = "Date Form Rcvd"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label30.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label30.Location = New System.Drawing.Point(588, 72)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(66, 14)
        Label30.TabIndex = 185
        Label30.Text = "Deposit/LoG"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label31.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label31.Location = New System.Drawing.Point(322, 70)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(89, 14)
        Label31.TabIndex = 187
        Label31.Text = "Interest Bearing?"
        '
        'Label32
        '
        Label32.AutoSize = True
        Label32.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label32.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label32.Location = New System.Drawing.Point(322, 142)
        Label32.Name = "Label32"
        Label32.Size = New System.Drawing.Size(77, 14)
        Label32.TabIndex = 189
        Label32.Text = "Break Clause?"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label24.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label24.Location = New System.Drawing.Point(322, 206)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(103, 14)
        Label24.TabIndex = 190
        Label24.Text = "Features/Comments"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label25.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label25.Location = New System.Drawing.Point(322, 232)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(70, 14)
        Label25.TabIndex = 192
        Label25.Text = "Letting Agent"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label26.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label26.Location = New System.Drawing.Point(322, 258)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(80, 14)
        Label26.TabIndex = 194
        Label26.Text = "Agent Address"
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label33.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label33.Location = New System.Drawing.Point(322, 284)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(76, 14)
        Label33.TabIndex = 196
        Label33.Text = "Agent Contact"
        '
        'COMPLETION_DTLabel
        '
        COMPLETION_DTLabel.AutoSize = True
        COMPLETION_DTLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COMPLETION_DTLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        COMPLETION_DTLabel.Location = New System.Drawing.Point(192, 42)
        COMPLETION_DTLabel.Name = "COMPLETION_DTLabel"
        COMPLETION_DTLabel.Size = New System.Drawing.Size(72, 14)
        COMPLETION_DTLabel.TabIndex = 122
        COMPLETION_DTLabel.Text = "Completion Dt"
        '
        'CANCELLATION_DTLabel
        '
        CANCELLATION_DTLabel.AutoSize = True
        CANCELLATION_DTLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CANCELLATION_DTLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CANCELLATION_DTLabel.Location = New System.Drawing.Point(340, 42)
        CANCELLATION_DTLabel.Name = "CANCELLATION_DTLabel"
        CANCELLATION_DTLabel.Size = New System.Drawing.Size(78, 14)
        CANCELLATION_DTLabel.TabIndex = 124
        CANCELLATION_DTLabel.Text = "Cancellation Dt"
        '
        'CANCELLATION_CDLabel
        '
        CANCELLATION_CDLabel.AutoSize = True
        CANCELLATION_CDLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CANCELLATION_CDLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CANCELLATION_CDLabel.Location = New System.Drawing.Point(495, 42)
        CANCELLATION_CDLabel.Name = "CANCELLATION_CDLabel"
        CANCELLATION_CDLabel.Size = New System.Drawing.Size(66, 14)
        CANCELLATION_CDLabel.TabIndex = 126
        CANCELLATION_CDLabel.Text = "Cancel Type"
        '
        'Label34
        '
        Label34.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label34.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label34.Location = New System.Drawing.Point(588, 146)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(86, 39)
        Label34.TabIndex = 209
        Label34.Text = "Employee Rent Contribution"
        '
        'Label35
        '
        Label35.AutoSize = True
        Label35.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label35.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label35.Location = New System.Drawing.Point(588, 122)
        Label35.Name = "Label35"
        Label35.Size = New System.Drawing.Size(115, 14)
        Label35.TabIndex = 207
        Label35.Text = "Standing Order Set Up"
        '
        'Label36
        '
        Label36.AutoSize = True
        Label36.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label36.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label36.Location = New System.Drawing.Point(588, 101)
        Label36.Name = "Label36"
        Label36.Size = New System.Drawing.Size(74, 14)
        Label36.TabIndex = 206
        Label36.Text = "Rental Budget"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxLetter
        '
        Me.pbxLetter.Image = CType(resources.GetObject("pbxLetter.Image"), System.Drawing.Image)
        Me.pbxLetter.Location = New System.Drawing.Point(477, 2)
        Me.pbxLetter.Name = "pbxLetter"
        Me.pbxLetter.Size = New System.Drawing.Size(40, 29)
        Me.pbxLetter.TabIndex = 2
        Me.pbxLetter.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxLetter, "Generate Letters")
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(154, 1)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Controls.Add(Me.pbxLetter)
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox7)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.pnlAsset)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(0, 180)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(796, 372)
        Me.Panel1.TabIndex = 5
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(762, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 79
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'pnlAsset
        '
        Me.pnlAsset.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlAsset.Controls.Add(Me.lnlCompleteTenancy)
        Me.pnlAsset.Controls.Add(Me.txtTenancyCompleteDt)
        Me.pnlAsset.Controls.Add(Me.TextBox6)
        Me.pnlAsset.Controls.Add(Me.chkStandingOrder)
        Me.pnlAsset.Controls.Add(Label34)
        Me.pnlAsset.Controls.Add(Me.txtEmpRentContribution)
        Me.pnlAsset.Controls.Add(Label35)
        Me.pnlAsset.Controls.Add(Label36)
        Me.pnlAsset.Controls.Add(Me.txtTenancyBudget)
        Me.pnlAsset.Controls.Add(Me.pbxDepositReturnedDt)
        Me.pnlAsset.Controls.Add(Me.txbDepositReturnedDt)
        Me.pnlAsset.Controls.Add(Label33)
        Me.pnlAsset.Controls.Add(Me.txbAgentContact)
        Me.pnlAsset.Controls.Add(Label26)
        Me.pnlAsset.Controls.Add(Me.txbAgentAddress)
        Me.pnlAsset.Controls.Add(Label25)
        Me.pnlAsset.Controls.Add(Me.txbLettingAgent)
        Me.pnlAsset.Controls.Add(Label24)
        Me.pnlAsset.Controls.Add(Me.TextBox2)
        Me.pnlAsset.Controls.Add(Me.chkBreakClause)
        Me.pnlAsset.Controls.Add(Label32)
        Me.pnlAsset.Controls.Add(Me.chkInterestBearing)
        Me.pnlAsset.Controls.Add(Label31)
        Me.pnlAsset.Controls.Add(Label30)
        Me.pnlAsset.Controls.Add(Me.cboDeposit)
        Me.pnlAsset.Controls.Add(Me.cboLeaseType)
        Me.pnlAsset.Controls.Add(Me.pbxFormReceivedDt)
        Me.pnlAsset.Controls.Add(Me.txbFormReceivedDt)
        Me.pnlAsset.Controls.Add(Label29)
        Me.pnlAsset.Controls.Add(Me.chkWaterMeter)
        Me.pnlAsset.Controls.Add(Label13)
        Me.pnlAsset.Controls.Add(Me.chkChildrenAllowed)
        Me.pnlAsset.Controls.Add(Label12)
        Me.pnlAsset.Controls.Add(Me.chkSmokersAllowed)
        Me.pnlAsset.Controls.Add(Label10)
        Me.pnlAsset.Controls.Add(Me.chkPetsAllowed)
        Me.pnlAsset.Controls.Add(Label9)
        Me.pnlAsset.Controls.Add(Me.chkGarage)
        Me.pnlAsset.Controls.Add(Label8)
        Me.pnlAsset.Controls.Add(Me.chkRearGarden)
        Me.pnlAsset.Controls.Add(Label5)
        Me.pnlAsset.Controls.Add(Me.chkFrontGarden)
        Me.pnlAsset.Controls.Add(Label7)
        Me.pnlAsset.Controls.Add(Label28)
        Me.pnlAsset.Controls.Add(Me.cboManagementCode)
        Me.pnlAsset.Controls.Add(Me.cboFurnishingCode)
        Me.pnlAsset.Controls.Add(Me.cboPropertyType)
        Me.pnlAsset.Controls.Add(Me.cboPropertyStructure)
        Me.pnlAsset.Controls.Add(Label27)
        Me.pnlAsset.Controls.Add(Label23)
        Me.pnlAsset.Controls.Add(Label22)
        Me.pnlAsset.Controls.Add(Me.txbDepositReturnedAm)
        Me.pnlAsset.Controls.Add(Label21)
        Me.pnlAsset.Controls.Add(Me.TextBox16)
        Me.pnlAsset.Controls.Add(Label20)
        Me.pnlAsset.Controls.Add(Label19)
        Me.pnlAsset.Controls.Add(Label18)
        Me.pnlAsset.Controls.Add(Label17)
        Me.pnlAsset.Controls.Add(Me.TextBox12)
        Me.pnlAsset.Controls.Add(Label16)
        Me.pnlAsset.Controls.Add(Me.txtFinalAgreedClaimAm)
        Me.pnlAsset.Controls.Add(Label11)
        Me.pnlAsset.Controls.Add(Me.TextBox10)
        Me.pnlAsset.Controls.Add(Label4)
        Me.pnlAsset.Controls.Add(Me.BindingNavigator1)
        Me.pnlAsset.Controls.Add(FULL_NMLabel)
        Me.pnlAsset.Controls.Add(Me.txbBedroomNo)
        Me.pnlAsset.Location = New System.Drawing.Point(8, 32)
        Me.pnlAsset.Name = "pnlAsset"
        Me.pnlAsset.Size = New System.Drawing.Size(778, 328)
        Me.pnlAsset.TabIndex = 1
        '
        'lnlCompleteTenancy
        '
        Me.lnlCompleteTenancy.ActiveLinkColor = System.Drawing.Color.White
        Me.lnlCompleteTenancy.DisabledLinkColor = System.Drawing.Color.Gold
        Me.lnlCompleteTenancy.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnlCompleteTenancy.ForeColor = System.Drawing.Color.Gold
        Me.lnlCompleteTenancy.Image = CType(resources.GetObject("lnlCompleteTenancy.Image"), System.Drawing.Image)
        Me.lnlCompleteTenancy.LinkArea = New System.Windows.Forms.LinkArea(0, 25)
        Me.lnlCompleteTenancy.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnlCompleteTenancy.LinkColor = System.Drawing.Color.Gold
        Me.lnlCompleteTenancy.Location = New System.Drawing.Point(637, 258)
        Me.lnlCompleteTenancy.Name = "lnlCompleteTenancy"
        Me.lnlCompleteTenancy.Size = New System.Drawing.Size(120, 20)
        Me.lnlCompleteTenancy.TabIndex = 212
        Me.lnlCompleteTenancy.TabStop = True
        Me.lnlCompleteTenancy.Text = "Complete Tenancy"
        Me.lnlCompleteTenancy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lnlCompleteTenancy.UseCompatibleTextRendering = True
        Me.lnlCompleteTenancy.VisitedLinkColor = System.Drawing.Color.Gold
        '
        'txtTenancyCompleteDt
        '
        Me.txtTenancyCompleteDt.AcceptsReturn = True
        Me.txtTenancyCompleteDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtTenancyCompleteDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "TENANCY_COMPLETE_DT", True))
        Me.txtTenancyCompleteDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyCompleteDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyCompleteDt.Location = New System.Drawing.Point(664, 281)
        Me.txtTenancyCompleteDt.Name = "txtTenancyCompleteDt"
        Me.txtTenancyCompleteDt.ReadOnly = True
        Me.txtTenancyCompleteDt.Size = New System.Drawing.Size(65, 20)
        Me.txtTenancyCompleteDt.TabIndex = 211
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmMaintainRentalProperty"
        Me.bsCase.DataSource = Me.DsMaintainRentalProperty
        '
        'DsMaintainRentalProperty
        '
        Me.DsMaintainRentalProperty.DataSetName = "dsMaintainRentalProperty"
        Me.DsMaintainRentalProperty.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "BREAK_CLAUSE_DS", True))
        Me.TextBox6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox6.Location = New System.Drawing.Point(457, 177)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(309, 20)
        Me.TextBox6.TabIndex = 20
        '
        'bsAsset
        '
        Me.bsAsset.DataMember = "V_frmMaintainRentalProperty_V_fsubMaintainRentalPropDetail"
        Me.bsAsset.DataSource = Me.bsCase
        '
        'chkStandingOrder
        '
        Me.chkStandingOrder.AutoSize = True
        Me.chkStandingOrder.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkStandingOrder.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkStandingOrder.Location = New System.Drawing.Point(709, 123)
        Me.chkStandingOrder.Name = "chkStandingOrder"
        Me.chkStandingOrder.Size = New System.Drawing.Size(15, 14)
        Me.chkStandingOrder.TabIndex = 210
        Me.chkStandingOrder.UseVisualStyleBackColor = True
        '
        'txtEmpRentContribution
        '
        Me.txtEmpRentContribution.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtEmpRentContribution.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "RENT_EMPLOYEE_CONTRIBUTION_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtEmpRentContribution.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpRentContribution.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtEmpRentContribution.Location = New System.Drawing.Point(679, 143)
        Me.txtEmpRentContribution.Name = "txtEmpRentContribution"
        Me.txtEmpRentContribution.ReadOnly = True
        Me.txtEmpRentContribution.Size = New System.Drawing.Size(55, 20)
        Me.txtEmpRentContribution.TabIndex = 208
        Me.txtEmpRentContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTenancyBudget
        '
        Me.txtTenancyBudget.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtTenancyBudget.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "RENTAL_BUDGET_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtTenancyBudget.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyBudget.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyBudget.Location = New System.Drawing.Point(679, 98)
        Me.txtTenancyBudget.Name = "txtTenancyBudget"
        Me.txtTenancyBudget.ReadOnly = True
        Me.txtTenancyBudget.Size = New System.Drawing.Size(55, 20)
        Me.txtTenancyBudget.TabIndex = 205
        Me.txtTenancyBudget.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pbxDepositReturnedDt
        '
        Me.pbxDepositReturnedDt.Image = CType(resources.GetObject("pbxDepositReturnedDt.Image"), System.Drawing.Image)
        Me.pbxDepositReturnedDt.Location = New System.Drawing.Point(529, 119)
        Me.pbxDepositReturnedDt.Name = "pbxDepositReturnedDt"
        Me.pbxDepositReturnedDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxDepositReturnedDt.TabIndex = 199
        Me.pbxDepositReturnedDt.TabStop = False
        '
        'txbDepositReturnedDt
        '
        Me.txbDepositReturnedDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbDepositReturnedDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "DEPOSIT_RETURNED_DT", True))
        Me.txbDepositReturnedDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDepositReturnedDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbDepositReturnedDt.Location = New System.Drawing.Point(457, 116)
        Me.txbDepositReturnedDt.Mask = "99/99/9999"
        Me.txbDepositReturnedDt.Name = "txbDepositReturnedDt"
        Me.txbDepositReturnedDt.Size = New System.Drawing.Size(65, 20)
        Me.txbDepositReturnedDt.TabIndex = 18
        Me.txbDepositReturnedDt.ValidatingType = GetType(Date)
        '
        'txbAgentContact
        '
        Me.txbAgentContact.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAgentContact.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAgent, "LettingAgentContact", True))
        Me.txbAgentContact.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAgentContact.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAgentContact.Location = New System.Drawing.Point(457, 281)
        Me.txbAgentContact.Name = "txbAgentContact"
        Me.txbAgentContact.ReadOnly = True
        Me.txbAgentContact.Size = New System.Drawing.Size(148, 20)
        Me.txbAgentContact.TabIndex = 24
        '
        'bsAgent
        '
        Me.bsAgent.DataMember = "V_fsubMaintainRentalPropDetail_V_vrtLettingAgent"
        Me.bsAgent.DataSource = Me.bsAsset
        '
        'txbAgentAddress
        '
        Me.txbAgentAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAgentAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAgent, "LettingAgentAddress", True))
        Me.txbAgentAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAgentAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAgentAddress.Location = New System.Drawing.Point(457, 255)
        Me.txbAgentAddress.Name = "txbAgentAddress"
        Me.txbAgentAddress.ReadOnly = True
        Me.txbAgentAddress.Size = New System.Drawing.Size(148, 20)
        Me.txbAgentAddress.TabIndex = 23
        '
        'txbLettingAgent
        '
        Me.txbLettingAgent.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbLettingAgent.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAgent, "LettingAgentName", True))
        Me.txbLettingAgent.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbLettingAgent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbLettingAgent.Location = New System.Drawing.Point(457, 229)
        Me.txbLettingAgent.Name = "txbLettingAgent"
        Me.txbLettingAgent.ReadOnly = True
        Me.txbLettingAgent.Size = New System.Drawing.Size(309, 20)
        Me.txbLettingAgent.TabIndex = 22
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "ADDITIONAL_FEAT_COMMENTS_DS", True))
        Me.TextBox2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox2.Location = New System.Drawing.Point(457, 203)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(309, 20)
        Me.TextBox2.TabIndex = 21
        '
        'chkBreakClause
        '
        Me.chkBreakClause.AutoSize = True
        Me.chkBreakClause.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBreakClause.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkBreakClause.Location = New System.Drawing.Point(457, 142)
        Me.chkBreakClause.Name = "chkBreakClause"
        Me.chkBreakClause.Size = New System.Drawing.Size(15, 14)
        Me.chkBreakClause.TabIndex = 19
        Me.chkBreakClause.UseVisualStyleBackColor = True
        '
        'chkInterestBearing
        '
        Me.chkInterestBearing.AutoSize = True
        Me.chkInterestBearing.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInterestBearing.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkInterestBearing.Location = New System.Drawing.Point(457, 70)
        Me.chkInterestBearing.Name = "chkInterestBearing"
        Me.chkInterestBearing.Size = New System.Drawing.Size(15, 14)
        Me.chkInterestBearing.TabIndex = 16
        Me.chkInterestBearing.UseVisualStyleBackColor = True
        '
        'cboDeposit
        '
        Me.cboDeposit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboDeposit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDeposit.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsAsset, "DEPOSIT_TYPE_CD", True))
        Me.cboDeposit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeposit.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeposit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboDeposit.FormattingEnabled = True
        Me.cboDeposit.Location = New System.Drawing.Point(679, 70)
        Me.cboDeposit.Name = "cboDeposit"
        Me.cboDeposit.Queryable = False
        Me.cboDeposit.Size = New System.Drawing.Size(87, 22)
        Me.cboDeposit.TabIndex = 27
        Me.cboDeposit.Updateable = True
        '
        'cboLeaseType
        '
        Me.cboLeaseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboLeaseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboLeaseType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsAsset, "LEASE_TYPE_CD", True))
        Me.cboLeaseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaseType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaseType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboLeaseType.FormattingEnabled = True
        Me.cboLeaseType.Location = New System.Drawing.Point(679, 43)
        Me.cboLeaseType.Name = "cboLeaseType"
        Me.cboLeaseType.Queryable = False
        Me.cboLeaseType.Size = New System.Drawing.Size(87, 22)
        Me.cboLeaseType.TabIndex = 26
        Me.cboLeaseType.Updateable = True
        '
        'pbxFormReceivedDt
        '
        Me.pbxFormReceivedDt.Image = CType(resources.GetObject("pbxFormReceivedDt.Image"), System.Drawing.Image)
        Me.pbxFormReceivedDt.Location = New System.Drawing.Point(751, 20)
        Me.pbxFormReceivedDt.Name = "pbxFormReceivedDt"
        Me.pbxFormReceivedDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxFormReceivedDt.TabIndex = 182
        Me.pbxFormReceivedDt.TabStop = False
        '
        'txbFormReceivedDt
        '
        Me.txbFormReceivedDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbFormReceivedDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "FORM_RECVD_DT", True))
        Me.txbFormReceivedDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbFormReceivedDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbFormReceivedDt.Location = New System.Drawing.Point(679, 17)
        Me.txbFormReceivedDt.Mask = "00/00/0000"
        Me.txbFormReceivedDt.Name = "txbFormReceivedDt"
        Me.txbFormReceivedDt.Size = New System.Drawing.Size(65, 20)
        Me.txbFormReceivedDt.TabIndex = 25
        Me.txbFormReceivedDt.ValidatingType = GetType(Date)
        '
        'chkWaterMeter
        '
        Me.chkWaterMeter.AutoSize = True
        Me.chkWaterMeter.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWaterMeter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkWaterMeter.Location = New System.Drawing.Point(291, 157)
        Me.chkWaterMeter.Name = "chkWaterMeter"
        Me.chkWaterMeter.Size = New System.Drawing.Size(15, 14)
        Me.chkWaterMeter.TabIndex = 9
        Me.chkWaterMeter.UseVisualStyleBackColor = True
        '
        'chkChildrenAllowed
        '
        Me.chkChildrenAllowed.AutoSize = True
        Me.chkChildrenAllowed.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChildrenAllowed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkChildrenAllowed.Location = New System.Drawing.Point(291, 137)
        Me.chkChildrenAllowed.Name = "chkChildrenAllowed"
        Me.chkChildrenAllowed.Size = New System.Drawing.Size(15, 14)
        Me.chkChildrenAllowed.TabIndex = 7
        Me.chkChildrenAllowed.UseVisualStyleBackColor = True
        '
        'chkSmokersAllowed
        '
        Me.chkSmokersAllowed.AutoSize = True
        Me.chkSmokersAllowed.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSmokersAllowed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkSmokersAllowed.Location = New System.Drawing.Point(291, 117)
        Me.chkSmokersAllowed.Name = "chkSmokersAllowed"
        Me.chkSmokersAllowed.Size = New System.Drawing.Size(15, 14)
        Me.chkSmokersAllowed.TabIndex = 5
        Me.chkSmokersAllowed.UseVisualStyleBackColor = True
        '
        'chkPetsAllowed
        '
        Me.chkPetsAllowed.AutoSize = True
        Me.chkPetsAllowed.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPetsAllowed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkPetsAllowed.Location = New System.Drawing.Point(165, 157)
        Me.chkPetsAllowed.Name = "chkPetsAllowed"
        Me.chkPetsAllowed.Size = New System.Drawing.Size(15, 14)
        Me.chkPetsAllowed.TabIndex = 8
        Me.chkPetsAllowed.UseVisualStyleBackColor = True
        '
        'chkGarage
        '
        Me.chkGarage.AutoSize = True
        Me.chkGarage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGarage.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkGarage.Location = New System.Drawing.Point(165, 137)
        Me.chkGarage.Name = "chkGarage"
        Me.chkGarage.Size = New System.Drawing.Size(15, 14)
        Me.chkGarage.TabIndex = 6
        Me.chkGarage.UseVisualStyleBackColor = True
        '
        'chkRearGarden
        '
        Me.chkRearGarden.AutoSize = True
        Me.chkRearGarden.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRearGarden.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkRearGarden.Location = New System.Drawing.Point(165, 117)
        Me.chkRearGarden.Name = "chkRearGarden"
        Me.chkRearGarden.Size = New System.Drawing.Size(15, 14)
        Me.chkRearGarden.TabIndex = 4
        Me.chkRearGarden.UseVisualStyleBackColor = True
        '
        'chkFrontGarden
        '
        Me.chkFrontGarden.AutoSize = True
        Me.chkFrontGarden.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFrontGarden.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkFrontGarden.Location = New System.Drawing.Point(165, 97)
        Me.chkFrontGarden.Name = "chkFrontGarden"
        Me.chkFrontGarden.Size = New System.Drawing.Size(15, 14)
        Me.chkFrontGarden.TabIndex = 3
        Me.chkFrontGarden.UseVisualStyleBackColor = True
        '
        'cboManagementCode
        '
        Me.cboManagementCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboManagementCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboManagementCode.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsAsset, "MANAGE_CD", True))
        Me.cboManagementCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboManagementCode.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboManagementCode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboManagementCode.FormattingEnabled = True
        Me.cboManagementCode.Location = New System.Drawing.Point(457, 17)
        Me.cboManagementCode.Name = "cboManagementCode"
        Me.cboManagementCode.Queryable = False
        Me.cboManagementCode.Size = New System.Drawing.Size(125, 22)
        Me.cboManagementCode.TabIndex = 14
        Me.cboManagementCode.Updateable = True
        '
        'cboFurnishingCode
        '
        Me.cboFurnishingCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboFurnishingCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFurnishingCode.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsAsset, "FURNISH_CD", True))
        Me.cboFurnishingCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFurnishingCode.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFurnishingCode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboFurnishingCode.FormattingEnabled = True
        Me.cboFurnishingCode.Location = New System.Drawing.Point(165, 175)
        Me.cboFurnishingCode.Name = "cboFurnishingCode"
        Me.cboFurnishingCode.Queryable = False
        Me.cboFurnishingCode.Size = New System.Drawing.Size(140, 22)
        Me.cboFurnishingCode.TabIndex = 10
        Me.cboFurnishingCode.Updateable = True
        '
        'cboPropertyType
        '
        Me.cboPropertyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboPropertyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPropertyType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsAsset, "PROPERTY_TYPE_CD", True))
        Me.cboPropertyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPropertyType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPropertyType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboPropertyType.FormattingEnabled = True
        Me.cboPropertyType.Location = New System.Drawing.Point(165, 70)
        Me.cboPropertyType.Name = "cboPropertyType"
        Me.cboPropertyType.Queryable = False
        Me.cboPropertyType.Size = New System.Drawing.Size(140, 22)
        Me.cboPropertyType.TabIndex = 2
        Me.cboPropertyType.Updateable = True
        '
        'cboPropertyStructure
        '
        Me.cboPropertyStructure.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboPropertyStructure.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPropertyStructure.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsAsset, "PROP_STRUCT_CD", True))
        Me.cboPropertyStructure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPropertyStructure.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPropertyStructure.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboPropertyStructure.FormattingEnabled = True
        Me.cboPropertyStructure.Location = New System.Drawing.Point(165, 43)
        Me.cboPropertyStructure.Name = "cboPropertyStructure"
        Me.cboPropertyStructure.Queryable = False
        Me.cboPropertyStructure.Size = New System.Drawing.Size(140, 22)
        Me.cboPropertyStructure.TabIndex = 1
        Me.cboPropertyStructure.Updateable = True
        '
        'txbDepositReturnedAm
        '
        Me.txbDepositReturnedAm.AcceptsReturn = True
        Me.txbDepositReturnedAm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbDepositReturnedAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "DEPOSIT_RETURNED_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txbDepositReturnedAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDepositReturnedAm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbDepositReturnedAm.Location = New System.Drawing.Point(457, 90)
        Me.txbDepositReturnedAm.Name = "txbDepositReturnedAm"
        Me.txbDepositReturnedAm.ReadOnly = True
        Me.txbDepositReturnedAm.Size = New System.Drawing.Size(65, 20)
        Me.txbDepositReturnedAm.TabIndex = 17
        Me.txbDepositReturnedAm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox16
        '
        Me.TextBox16.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "TENANCY_DEPOSIT_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.TextBox16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox16.Location = New System.Drawing.Point(457, 44)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(65, 20)
        Me.TextBox16.TabIndex = 15
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox12.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "COMMENTS_TX", True))
        Me.TextBox12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox12.Location = New System.Drawing.Point(117, 254)
        Me.TextBox12.Multiline = True
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox12.Size = New System.Drawing.Size(189, 37)
        Me.TextBox12.TabIndex = 13
        '
        'txtFinalAgreedClaimAm
        '
        Me.txtFinalAgreedClaimAm.AcceptsReturn = True
        Me.txtFinalAgreedClaimAm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtFinalAgreedClaimAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "FINAL_AGREED_CLAIM_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtFinalAgreedClaimAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinalAgreedClaimAm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtFinalAgreedClaimAm.Location = New System.Drawing.Point(165, 228)
        Me.txtFinalAgreedClaimAm.Name = "txtFinalAgreedClaimAm"
        Me.txtFinalAgreedClaimAm.ReadOnly = True
        Me.txtFinalAgreedClaimAm.Size = New System.Drawing.Size(65, 20)
        Me.txtFinalAgreedClaimAm.TabIndex = 12
        Me.txtFinalAgreedClaimAm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox10
        '
        Me.TextBox10.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox10.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "ORIGINAL_DILAP_CLAIM_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.TextBox10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox10.Location = New System.Drawing.Point(165, 202)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(65, 20)
        Me.TextBox10.TabIndex = 11
        Me.TextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.tsbAdd
        Me.BindingNavigator1.BindingSource = Me.bsAsset
        Me.BindingNavigator1.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.tssAdd, Me.tsbAdd})
        Me.BindingNavigator1.Location = New System.Drawing.Point(8, 294)
        Me.BindingNavigator1.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator1.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator1.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator1.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator1.Size = New System.Drawing.Size(198, 25)
        Me.BindingNavigator1.TabIndex = 35
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'tsbAdd
        '
        Me.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAdd.Image = CType(resources.GetObject("tsbAdd.Image"), System.Drawing.Image)
        Me.tsbAdd.Name = "tsbAdd"
        Me.tsbAdd.RightToLeftAutoMirrorImage = True
        Me.tsbAdd.Size = New System.Drawing.Size(23, 22)
        Me.tsbAdd.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(25, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'tssAdd
        '
        Me.tssAdd.Name = "tssAdd"
        Me.tssAdd.Size = New System.Drawing.Size(6, 25)
        '
        'txbBedroomNo
        '
        Me.txbBedroomNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbBedroomNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsAsset, "BEDROOM_QT", True))
        Me.txbBedroomNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbBedroomNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbBedroomNo.Location = New System.Drawing.Point(165, 17)
        Me.txbBedroomNo.Name = "txbBedroomNo"
        Me.txbBedroomNo.ReadOnly = True
        Me.txbBedroomNo.Size = New System.Drawing.Size(33, 20)
        Me.txbBedroomNo.TabIndex = 0
        Me.txbBedroomNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(732, 24)
        Me.Panel2.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Asset"
        '
        'pnlTenancy
        '
        Me.pnlTenancy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTenancy.Controls.Add(Me.Panel4)
        Me.pnlTenancy.Controls.Add(Me.PictureBox6)
        Me.pnlTenancy.Controls.Add(Me.PictureBox5)
        Me.pnlTenancy.Controls.Add(Me.dgvTenancy)
        Me.pnlTenancy.Location = New System.Drawing.Point(0, 558)
        Me.pnlTenancy.Name = "pnlTenancy"
        Me.pnlTenancy.Size = New System.Drawing.Size(523, 144)
        Me.pnlTenancy.TabIndex = 6
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.lnlTAndP)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(460, 24)
        Me.Panel4.TabIndex = 1
        '
        'lnlTAndP
        '
        Me.lnlTAndP.ActiveLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlTAndP.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnlTAndP.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlTAndP.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnlTAndP.LinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlTAndP.Location = New System.Drawing.Point(331, 2)
        Me.lnlTAndP.Name = "lnlTAndP"
        Me.lnlTAndP.Size = New System.Drawing.Size(129, 19)
        Me.lnlTAndP.TabIndex = 143
        Me.lnlTAndP.TabStop = True
        Me.lnlTAndP.Text = "Details"
        Me.lnlTAndP.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnlTAndP.VisitedLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Tenancy History"
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(488, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'dgvTenancy
        '
        Me.dgvTenancy.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvTenancy.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTenancy.AutoGenerateColumns = False
        Me.dgvTenancy.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvTenancy.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTenancy.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTenancy.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTenancy.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTenancy.ColumnHeadersHeight = 30
        Me.dgvTenancy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AST_CASE_NO_CSE, Me.AST_CLIENT_PACKAGE_ID_CGS, Me.AST_GENERATED_NO_ADD, Me.TENANCY_START_DT, Me.TENANCYENDDTDataGridViewTextBoxColumn, Me.NOTICEDTDataGridViewTextBoxColumn, Me.AGREEMENTRETDDTDataGridViewTextBoxColumn, Me.TENANCY_DURATION_DAYS_QT, Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn, Me.MONTHLYRENTAMDataGridViewTextBoxColumn, Me.chkPeriodic, Me.NOTICEPERIODDataGridViewTextBoxColumn, Me.STATUSTXDataGridViewTextBoxColumn, Me.NOTICEPERIODQTDataGridViewTextBoxColumn, Me.NOTICEPERIODCDDataGridViewTextBoxColumn})
        Me.dgvTenancy.DataSource = Me.bsTenancy
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTenancy.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvTenancy.EnableHeadersVisualStyles = False
        Me.dgvTenancy.Location = New System.Drawing.Point(8, 32)
        Me.dgvTenancy.Name = "dgvTenancy"
        Me.dgvTenancy.ReadOnly = True
        Me.dgvTenancy.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Arial", 7.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTenancy.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvTenancy.RowHeadersWidth = 18
        Me.dgvTenancy.RowTemplate.Height = 20
        Me.dgvTenancy.Size = New System.Drawing.Size(505, 98)
        Me.dgvTenancy.TabIndex = 76
        '
        'AST_CASE_NO_CSE
        '
        Me.AST_CASE_NO_CSE.DataPropertyName = "AST_CASE_NO_CSE"
        Me.AST_CASE_NO_CSE.HeaderText = "AST_CASE_NO_CSE"
        Me.AST_CASE_NO_CSE.Name = "AST_CASE_NO_CSE"
        Me.AST_CASE_NO_CSE.ReadOnly = True
        Me.AST_CASE_NO_CSE.Visible = False
        '
        'AST_CLIENT_PACKAGE_ID_CGS
        '
        Me.AST_CLIENT_PACKAGE_ID_CGS.DataPropertyName = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.AST_CLIENT_PACKAGE_ID_CGS.HeaderText = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.AST_CLIENT_PACKAGE_ID_CGS.Name = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.AST_CLIENT_PACKAGE_ID_CGS.ReadOnly = True
        Me.AST_CLIENT_PACKAGE_ID_CGS.Visible = False
        '
        'AST_GENERATED_NO_ADD
        '
        Me.AST_GENERATED_NO_ADD.DataPropertyName = "AST_GENERATED_NO_ADD"
        Me.AST_GENERATED_NO_ADD.HeaderText = "AST_GENERATED_NO_ADD"
        Me.AST_GENERATED_NO_ADD.Name = "AST_GENERATED_NO_ADD"
        Me.AST_GENERATED_NO_ADD.ReadOnly = True
        Me.AST_GENERATED_NO_ADD.Visible = False
        '
        'TENANCY_START_DT
        '
        Me.TENANCY_START_DT.DataPropertyName = "TENANCY_START_DT"
        Me.TENANCY_START_DT.HeaderText = "Start Date"
        Me.TENANCY_START_DT.Name = "TENANCY_START_DT"
        Me.TENANCY_START_DT.ReadOnly = True
        Me.TENANCY_START_DT.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TENANCY_START_DT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.TENANCY_START_DT.Width = 65
        '
        'TENANCYENDDTDataGridViewTextBoxColumn
        '
        Me.TENANCYENDDTDataGridViewTextBoxColumn.DataPropertyName = "TENANCY_END_DT"
        Me.TENANCYENDDTDataGridViewTextBoxColumn.HeaderText = "End Date"
        Me.TENANCYENDDTDataGridViewTextBoxColumn.Name = "TENANCYENDDTDataGridViewTextBoxColumn"
        Me.TENANCYENDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.TENANCYENDDTDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TENANCYENDDTDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.TENANCYENDDTDataGridViewTextBoxColumn.Width = 65
        '
        'NOTICEDTDataGridViewTextBoxColumn
        '
        Me.NOTICEDTDataGridViewTextBoxColumn.DataPropertyName = "NOTICE_DT"
        Me.NOTICEDTDataGridViewTextBoxColumn.HeaderText = "Notice Date"
        Me.NOTICEDTDataGridViewTextBoxColumn.Name = "NOTICEDTDataGridViewTextBoxColumn"
        Me.NOTICEDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOTICEDTDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.NOTICEDTDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.NOTICEDTDataGridViewTextBoxColumn.Width = 65
        '
        'AGREEMENTRETDDTDataGridViewTextBoxColumn
        '
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.DataPropertyName = "AGREEMENT_RETD_DT"
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.HeaderText = "Agree Retd"
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.Name = "AGREEMENTRETDDTDataGridViewTextBoxColumn"
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.AGREEMENTRETDDTDataGridViewTextBoxColumn.Width = 65
        '
        'TENANCY_DURATION_DAYS_QT
        '
        Me.TENANCY_DURATION_DAYS_QT.DataPropertyName = "TENANCY_DURATION_DAYS_QT"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.TENANCY_DURATION_DAYS_QT.DefaultCellStyle = DataGridViewCellStyle3
        Me.TENANCY_DURATION_DAYS_QT.HeaderText = "Dur- ation"
        Me.TENANCY_DURATION_DAYS_QT.Name = "TENANCY_DURATION_DAYS_QT"
        Me.TENANCY_DURATION_DAYS_QT.ReadOnly = True
        Me.TENANCY_DURATION_DAYS_QT.Width = 40
        '
        'ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn
        '
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn.DataPropertyName = "ORIGINAL_MONTHLY_RENT_AM"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn.HeaderText = "Orig Mnth Rent"
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn.Name = "ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn"
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn.Width = 67
        '
        'MONTHLYRENTAMDataGridViewTextBoxColumn
        '
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn.DataPropertyName = "MONTHLY_RENT_AM"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn.HeaderText = "Act Mnth Rent"
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn.Name = "MONTHLYRENTAMDataGridViewTextBoxColumn"
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn.Width = 65
        '
        'chkPeriodic
        '
        Me.chkPeriodic.DataPropertyName = "TENANCY_PERIODIC_IN"
        Me.chkPeriodic.FalseValue = "N"
        Me.chkPeriodic.HeaderText = "Periodic"
        Me.chkPeriodic.Name = "chkPeriodic"
        Me.chkPeriodic.ReadOnly = True
        Me.chkPeriodic.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.chkPeriodic.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.chkPeriodic.TrueValue = "Y"
        Me.chkPeriodic.Width = 55
        '
        'NOTICEPERIODDataGridViewTextBoxColumn
        '
        Me.NOTICEPERIODDataGridViewTextBoxColumn.DataPropertyName = "NOTICE_PERIOD"
        Me.NOTICEPERIODDataGridViewTextBoxColumn.HeaderText = "NOTICE_PERIOD"
        Me.NOTICEPERIODDataGridViewTextBoxColumn.Name = "NOTICEPERIODDataGridViewTextBoxColumn"
        Me.NOTICEPERIODDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOTICEPERIODDataGridViewTextBoxColumn.Visible = False
        '
        'STATUSTXDataGridViewTextBoxColumn
        '
        Me.STATUSTXDataGridViewTextBoxColumn.DataPropertyName = "STATUS_TX"
        Me.STATUSTXDataGridViewTextBoxColumn.HeaderText = "STATUS_TX"
        Me.STATUSTXDataGridViewTextBoxColumn.Name = "STATUSTXDataGridViewTextBoxColumn"
        Me.STATUSTXDataGridViewTextBoxColumn.ReadOnly = True
        Me.STATUSTXDataGridViewTextBoxColumn.Visible = False
        '
        'NOTICEPERIODQTDataGridViewTextBoxColumn
        '
        Me.NOTICEPERIODQTDataGridViewTextBoxColumn.DataPropertyName = "NOTICE_PERIOD_QT"
        Me.NOTICEPERIODQTDataGridViewTextBoxColumn.HeaderText = "NOTICE_PERIOD_QT"
        Me.NOTICEPERIODQTDataGridViewTextBoxColumn.Name = "NOTICEPERIODQTDataGridViewTextBoxColumn"
        Me.NOTICEPERIODQTDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOTICEPERIODQTDataGridViewTextBoxColumn.Visible = False
        '
        'NOTICEPERIODCDDataGridViewTextBoxColumn
        '
        Me.NOTICEPERIODCDDataGridViewTextBoxColumn.DataPropertyName = "NOTICE_PERIOD_CD"
        Me.NOTICEPERIODCDDataGridViewTextBoxColumn.HeaderText = "NOTICE_PERIOD_CD"
        Me.NOTICEPERIODCDDataGridViewTextBoxColumn.Name = "NOTICEPERIODCDDataGridViewTextBoxColumn"
        Me.NOTICEPERIODCDDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOTICEPERIODCDDataGridViewTextBoxColumn.Visible = False
        '
        'bsTenancy
        '
        Me.bsTenancy.DataMember = "V_frmMaintainRentalProperty_V_fsubMaintainRentlPropTenancy"
        Me.bsTenancy.DataSource = Me.bsCase
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlCase)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(795, 108)
        Me.Panel8.TabIndex = 4
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(762, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlCase
        '
        Me.pnlCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlCase.Controls.Add(Me.pbxVIP)
        Me.pnlCase.Controls.Add(Me.TextBox1)
        Me.pnlCase.Controls.Add(CANCELLATION_CDLabel)
        Me.pnlCase.Controls.Add(Me.TextBox3)
        Me.pnlCase.Controls.Add(CANCELLATION_DTLabel)
        Me.pnlCase.Controls.Add(Me.TextBox4)
        Me.pnlCase.Controls.Add(COMPLETION_DTLabel)
        Me.pnlCase.Controls.Add(Me.BindingNavigator2)
        Me.pnlCase.Controls.Add(Label6)
        Me.pnlCase.Controls.Add(Me.txbAddress)
        Me.pnlCase.Controls.Add(Me.SuperLabel2)
        Me.pnlCase.Controls.Add(Me.pbxCopy)
        Me.pnlCase.Controls.Add(Me.txbCaseNo)
        Me.pnlCase.Controls.Add(Label14)
        Me.pnlCase.Controls.Add(Me.txbPackageNm)
        Me.pnlCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(778, 68)
        Me.pnlCase.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CANCELLATION_CODE", True))
        Me.TextBox1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox1.Location = New System.Drawing.Point(568, 39)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(158, 20)
        Me.TextBox1.TabIndex = 127
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CANCELLATION_DT", True))
        Me.TextBox3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox3.Location = New System.Drawing.Point(424, 39)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(65, 20)
        Me.TextBox3.TabIndex = 125
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "COMPLETION_DT", True))
        Me.TextBox4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox4.Location = New System.Drawing.Point(270, 39)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(65, 20)
        Me.TextBox4.TabIndex = 123
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.BindingSource = Me.bsCase
        Me.BindingNavigator2.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.ToolStripButton3, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.ToolStripButton4, Me.ToolStripButton5})
        Me.BindingNavigator2.Location = New System.Drawing.Point(8, 36)
        Me.BindingNavigator2.MoveFirstItem = Me.ToolStripButton2
        Me.BindingNavigator2.MoveLastItem = Me.ToolStripButton5
        Me.BindingNavigator2.MoveNextItem = Me.ToolStripButton4
        Me.BindingNavigator2.MovePreviousItem = Me.ToolStripButton3
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator2.Size = New System.Drawing.Size(169, 25)
        Me.BindingNavigator2.TabIndex = 117
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 22)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Move first"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(25, 21)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Move next"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Move last"
        '
        'txbAddress
        '
        Me.txbAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "PropertyAddress", True))
        Me.txbAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAddress.Location = New System.Drawing.Point(568, 13)
        Me.txbAddress.Name = "txbAddress"
        Me.txbAddress.ReadOnly = True
        Me.txbAddress.Size = New System.Drawing.Size(194, 20)
        Me.txbAddress.TabIndex = 2
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 16)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(133, 16)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CPC_CASE_NO_CSE", True))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(79, 13)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(48, 20)
        Me.txbCaseNo.TabIndex = 0
        Me.txbCaseNo.Updateable = False
        '
        'txbPackageNm
        '
        Me.txbPackageNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPackageNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CLIENT_PACKAGE_NM", True))
        Me.txbPackageNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPackageNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPackageNm.Location = New System.Drawing.Point(270, 13)
        Me.txbPackageNm.Name = "txbPackageNm"
        Me.txbPackageNm.ReadOnly = True
        Me.txbPackageNm.Size = New System.Drawing.Size(219, 20)
        Me.txbPackageNm.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.txtAddressAstType)
        Me.Panel10.Controls.Add(Me.txtPackageId)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(732, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(317, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'txtAddressAstType
        '
        Me.txtAddressAstType.BackColor = System.Drawing.Color.Yellow
        Me.txtAddressAstType.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CCP_ADDR_AST_TYPE_CD_CCP", True))
        Me.txtAddressAstType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressAstType.ForeColor = System.Drawing.Color.DimGray
        Me.txtAddressAstType.Location = New System.Drawing.Point(394, 2)
        Me.txtAddressAstType.Name = "txtAddressAstType"
        Me.txtAddressAstType.ReadOnly = True
        Me.txtAddressAstType.Size = New System.Drawing.Size(43, 20)
        Me.txtAddressAstType.TabIndex = 148
        '
        'txtPackageId
        '
        Me.txtPackageId.BackColor = System.Drawing.Color.Yellow
        Me.txtPackageId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CPC_CLIENT_PACKAGE_ID_CGS", True))
        Me.txtPackageId.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackageId.ForeColor = System.Drawing.Color.DimGray
        Me.txtPackageId.Location = New System.Drawing.Point(345, 2)
        Me.txtPackageId.Name = "txtPackageId"
        Me.txtPackageId.ReadOnly = True
        Me.txtPackageId.Size = New System.Drawing.Size(43, 20)
        Me.txtPackageId.TabIndex = 147
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Case"
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(196, 19)
        Me.SuperLabel1.TabIndex = 7
        Me.SuperLabel1.Text = "Maintain Rental Property"
        '
        'pnlRent
        '
        Me.pnlRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlRent.Controls.Add(Me.PictureBox3)
        Me.pnlRent.Controls.Add(Me.PictureBox8)
        Me.pnlRent.Controls.Add(Me.Panel5)
        Me.pnlRent.Controls.Add(Me.dgvRent)
        Me.pnlRent.Location = New System.Drawing.Point(529, 558)
        Me.pnlRent.Name = "pnlRent"
        Me.pnlRent.Size = New System.Drawing.Size(266, 144)
        Me.pnlRent.TabIndex = 8
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(232, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 80
        Me.PictureBox3.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox8.TabIndex = 79
        Me.PictureBox8.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Silver
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Location = New System.Drawing.Point(33, 8)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(212, 24)
        Me.Panel5.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Rent History"
        '
        'dgvRent
        '
        Me.dgvRent.AllowUserToDeleteRows = False
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvRent.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvRent.AutoGenerateColumns = False
        Me.dgvRent.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvRent.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvRent.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvRent.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRent.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvRent.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ASTCASENOCSEDataGridViewTextBoxColumn1, Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1, Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1, Me.RENTSTARTDTDataGridViewTextBoxColumn, Me.RENTENDDTDataGridViewTextBoxColumn, Me.MONTHLYRENTAMDataGridViewTextBoxColumn1, Me.Freq})
        Me.dgvRent.DataSource = Me.bsRent
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRent.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvRent.EnableHeadersVisualStyles = False
        Me.dgvRent.Location = New System.Drawing.Point(8, 32)
        Me.dgvRent.Name = "dgvRent"
        Me.dgvRent.ReadOnly = True
        Me.dgvRent.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRent.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvRent.RowHeadersWidth = 18
        Me.dgvRent.RowTemplate.Height = 20
        Me.dgvRent.Size = New System.Drawing.Size(249, 98)
        Me.dgvRent.TabIndex = 76
        '
        'ASTCASENOCSEDataGridViewTextBoxColumn1
        '
        Me.ASTCASENOCSEDataGridViewTextBoxColumn1.DataPropertyName = "AST_CASE_NO_CSE"
        Me.ASTCASENOCSEDataGridViewTextBoxColumn1.HeaderText = "AST_CASE_NO_CSE"
        Me.ASTCASENOCSEDataGridViewTextBoxColumn1.Name = "ASTCASENOCSEDataGridViewTextBoxColumn1"
        Me.ASTCASENOCSEDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ASTCASENOCSEDataGridViewTextBoxColumn1.Visible = False
        '
        'ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1
        '
        Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.DataPropertyName = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.HeaderText = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.Name = "ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1"
        Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.Visible = False
        '
        'ASTGENERATEDNOADDDataGridViewTextBoxColumn1
        '
        Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1.DataPropertyName = "AST_GENERATED_NO_ADD"
        Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1.HeaderText = "AST_GENERATED_NO_ADD"
        Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1.Name = "ASTGENERATEDNOADDDataGridViewTextBoxColumn1"
        Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ASTGENERATEDNOADDDataGridViewTextBoxColumn1.Visible = False
        '
        'RENTSTARTDTDataGridViewTextBoxColumn
        '
        Me.RENTSTARTDTDataGridViewTextBoxColumn.DataPropertyName = "RENT_START_DT"
        Me.RENTSTARTDTDataGridViewTextBoxColumn.HeaderText = "Start"
        Me.RENTSTARTDTDataGridViewTextBoxColumn.Name = "RENTSTARTDTDataGridViewTextBoxColumn"
        Me.RENTSTARTDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.RENTSTARTDTDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RENTSTARTDTDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.RENTSTARTDTDataGridViewTextBoxColumn.Width = 65
        '
        'RENTENDDTDataGridViewTextBoxColumn
        '
        Me.RENTENDDTDataGridViewTextBoxColumn.DataPropertyName = "RENT_END_DT"
        Me.RENTENDDTDataGridViewTextBoxColumn.HeaderText = "End"
        Me.RENTENDDTDataGridViewTextBoxColumn.Name = "RENTENDDTDataGridViewTextBoxColumn"
        Me.RENTENDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.RENTENDDTDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RENTENDDTDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.RENTENDDTDataGridViewTextBoxColumn.Width = 65
        '
        'MONTHLYRENTAMDataGridViewTextBoxColumn1
        '
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1.DataPropertyName = "MONTHLY_RENT_AM"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle10
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1.HeaderText = "Amt"
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1.Name = "MONTHLYRENTAMDataGridViewTextBoxColumn1"
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1.ReadOnly = True
        Me.MONTHLYRENTAMDataGridViewTextBoxColumn1.Width = 55
        '
        'Freq
        '
        Me.Freq.DataPropertyName = "Freq"
        Me.Freq.HeaderText = "Frq"
        Me.Freq.Name = "Freq"
        Me.Freq.ReadOnly = True
        Me.Freq.Width = 30
        '
        'bsRent
        '
        Me.bsRent.DataMember = "V_frmMaintainRentalProperty_V_fsubMaintainRentalPropRent"
        Me.bsRent.DataSource = Me.bsCase
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "AST_CASE_NO_CSE"
        Me.DataGridViewTextBoxColumn1.HeaderText = "AST_CASE_NO_CSE"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.DataGridViewTextBoxColumn2.HeaderText = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "AST_GENERATED_NO_ADD"
        Me.DataGridViewTextBoxColumn3.HeaderText = "AST_GENERATED_NO_ADD"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "TENANCY_START_DT"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Start"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "TENANCY_END_DT"
        Me.DataGridViewTextBoxColumn5.HeaderText = "End"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "NOTICE_DT"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Notice Dt"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 70
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "AGREEMENT_RETD_DT"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Agree Retd"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "TENANCY_DURATION_DAYS_QT"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn8.HeaderText = "Dur- ation"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "ORIGINAL_MONTHLY_RENT_AM"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn9.HeaderText = "Orig Mnth Rent"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "MONTHLY_RENT_AM"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "N2"
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn10.HeaderText = "Act Mnth Rent"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "NOTICE_PERIOD"
        Me.DataGridViewTextBoxColumn11.HeaderText = "NOTICE_PERIOD"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "STATUS_TX"
        Me.DataGridViewTextBoxColumn12.HeaderText = "STATUS_TX"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "NOTICE_PERIOD_QT"
        Me.DataGridViewTextBoxColumn13.HeaderText = "NOTICE_PERIOD_QT"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "NOTICE_PERIOD_CD"
        Me.DataGridViewTextBoxColumn14.HeaderText = "NOTICE_PERIOD_CD"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "AST_CASE_NO_CSE"
        Me.DataGridViewTextBoxColumn15.HeaderText = "AST_CASE_NO_CSE"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.DataGridViewTextBoxColumn16.HeaderText = "AST_CLIENT_PACKAGE_ID_CGS"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "AST_GENERATED_NO_ADD"
        Me.DataGridViewTextBoxColumn17.HeaderText = "AST_GENERATED_NO_ADD"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "RENT_START_DT"
        Me.DataGridViewTextBoxColumn18.HeaderText = "Start"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Width = 70
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "RENT_END_DT"
        Me.DataGridViewTextBoxColumn19.HeaderText = "End"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Width = 70
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "MONTHLY_RENT_AM"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N2"
        Me.DataGridViewTextBoxColumn20.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn20.HeaderText = "Amt"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Width = 55
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "Freq"
        Me.DataGridViewTextBoxColumn21.HeaderText = "Frq"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.Width = 25
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taAsset
        '
        Me.taAsset.ClearBeforeFill = True
        '
        'taTenancy
        '
        Me.taTenancy.ClearBeforeFill = True
        '
        'taRent
        '
        Me.taRent.ClearBeforeFill = True
        '
        'taAgent
        '
        Me.taAgent.ClearBeforeFill = True
        '
        'frmMaintainRentalProperty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.pnlRent)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlTenancy)
        Me.KeyPreview = True
        Me.Name = "frmMaintainRentalProperty"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Maintain Rental Property"
        CType(Me.pbxLetter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFormStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAsset.ResumeLayout(False)
        Me.pnlAsset.PerformLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMaintainRentalProperty, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsAsset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxDepositReturnedDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsAgent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxFormReceivedDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlTenancy.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCase.ResumeLayout(False)
        Me.pnlCase.PerformLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.pnlRent.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvRent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlAsset As System.Windows.Forms.Panel
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tssAdd As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents txbBedroomNo As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pnlTenancy As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvTenancy As System.Windows.Forms.DataGridView
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlCase As System.Windows.Forms.Panel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents txbPackageNm As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents txtFinalAgreedClaimAm As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents txbDepositReturnedAm As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents cboManagementCode As ControlLibrary.SuperComboBox
    Friend WithEvents cboFurnishingCode As ControlLibrary.SuperComboBox
    Friend WithEvents cboPropertyType As ControlLibrary.SuperComboBox
    Friend WithEvents cboPropertyStructure As ControlLibrary.SuperComboBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents pnlRent As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvRent As System.Windows.Forms.DataGridView
    Friend WithEvents txbAddress As System.Windows.Forms.TextBox
    Friend WithEvents chkFrontGarden As ControlLibrary.SuperCheckBox
    Friend WithEvents chkWaterMeter As ControlLibrary.SuperCheckBox
    Friend WithEvents chkChildrenAllowed As ControlLibrary.SuperCheckBox
    Friend WithEvents chkSmokersAllowed As ControlLibrary.SuperCheckBox
    Friend WithEvents chkPetsAllowed As ControlLibrary.SuperCheckBox
    Friend WithEvents chkGarage As ControlLibrary.SuperCheckBox
    Friend WithEvents chkRearGarden As ControlLibrary.SuperCheckBox
    Friend WithEvents pbxFormReceivedDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbFormReceivedDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents chkBreakClause As ControlLibrary.SuperCheckBox
    Friend WithEvents chkInterestBearing As ControlLibrary.SuperCheckBox
    Friend WithEvents cboDeposit As ControlLibrary.SuperComboBox
    Friend WithEvents cboLeaseType As ControlLibrary.SuperComboBox
    Friend WithEvents txbAgentContact As System.Windows.Forms.TextBox
    Friend WithEvents txbAgentAddress As System.Windows.Forms.TextBox
    Friend WithEvents txbLettingAgent As System.Windows.Forms.TextBox
    Friend WithEvents pbxDepositReturnedDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbDepositReturnedDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents DsMaintainRentalProperty As clsRental.dsMaintainRentalProperty
    Friend WithEvents taCase As clsRental.dsMaintainRentalPropertyTableAdapters.V_frmMaintainRentalPropertyTableAdapter
    Friend WithEvents bsAsset As System.Windows.Forms.BindingSource
    Friend WithEvents taAsset As clsRental.dsMaintainRentalPropertyTableAdapters.V_fsubMaintainRentalPropDetailTableAdapter
    Friend WithEvents bsTenancy As System.Windows.Forms.BindingSource
    Friend WithEvents taTenancy As clsRental.dsMaintainRentalPropertyTableAdapters.V_fsubMaintainRentlPropTenancyTableAdapter
    Friend WithEvents bsRent As System.Windows.Forms.BindingSource
    Friend WithEvents taRent As clsRental.dsMaintainRentalPropertyTableAdapters.V_fsubMaintainRentalPropRentTableAdapter
    Friend WithEvents bsAgent As System.Windows.Forms.BindingSource
    Friend WithEvents taAgent As clsRental.dsMaintainRentalPropertyTableAdapters.V_vrtLettingAgentTableAdapter
    Friend WithEvents lnlTAndP As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ASTCASENOCSEDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ASTCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ASTGENERATEDNOADDDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RENTSTARTDTDataGridViewTextBoxColumn As clsLibraryInstance.MaskedDateColumn
    Friend WithEvents RENTENDDTDataGridViewTextBoxColumn As clsLibraryInstance.MaskedDateColumn
    Friend WithEvents MONTHLYRENTAMDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Freq As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AST_CASE_NO_CSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AST_CLIENT_PACKAGE_ID_CGS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AST_GENERATED_NO_ADD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TENANCY_START_DT As clsLibraryInstance.MaskedDateColumn
    Friend WithEvents TENANCYENDDTDataGridViewTextBoxColumn As clsLibraryInstance.MaskedDateColumn
    Friend WithEvents NOTICEDTDataGridViewTextBoxColumn As clsLibraryInstance.MaskedDateColumn
    Friend WithEvents AGREEMENTRETDDTDataGridViewTextBoxColumn As clsLibraryInstance.MaskedDateColumn
    Friend WithEvents TENANCY_DURATION_DAYS_QT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ORIGINALMONTHLYRENTAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MONTHLYRENTAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkPeriodic As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NOTICEPERIODDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUSTXDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOTICEPERIODQTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOTICEPERIODCDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents chkStandingOrder As ControlLibrary.SuperCheckBox
    Friend WithEvents txtEmpRentContribution As System.Windows.Forms.TextBox
    Friend WithEvents txtTenancyBudget As System.Windows.Forms.TextBox
    Friend WithEvents txtTenancyCompleteDt As System.Windows.Forms.TextBox
    Friend WithEvents lnlCompleteTenancy As System.Windows.Forms.LinkLabel
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtPackageId As System.Windows.Forms.TextBox
    Friend WithEvents pbxLetter As System.Windows.Forms.PictureBox
    Friend WithEvents txtAddressAstType As System.Windows.Forms.TextBox
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
