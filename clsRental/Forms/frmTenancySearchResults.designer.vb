<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTenancySearchResults
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label35 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label36 As System.Windows.Forms.Label
        Dim Label37 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTenancySearchResults))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxLetter = New System.Windows.Forms.PictureBox()
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.cboTenancySearchStatus = New ControlLibrary.SuperComboBox()
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsTenancySearchResults = New clsRental.dsTenancySearchResults()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.txtCustomerNm = New System.Windows.Forms.TextBox()
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txtPackage = New System.Windows.Forms.TextBox()
        Me.bnCase = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.txtCaseNo = New ControlLibrary.SuperTextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.txtClientId = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPackageId = New System.Windows.Forms.TextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.bsTenancy = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtTimestamp = New System.Windows.Forms.TextBox()
        Me.chkSendToEmployee = New ControlLibrary.SuperCheckBox()
        Me.txtGeneratedNo = New System.Windows.Forms.TextBox()
        Me.txtTenancyCaseNo = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTenancyPackageId = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlTenancy = New System.Windows.Forms.Panel()
        Me.lblCharactersRemaining = New System.Windows.Forms.Label()
        Me.cboRentFrequency = New ControlLibrary.SuperComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtPostOutCode = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCounty = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtAddressLine3 = New System.Windows.Forms.TextBox()
        Me.txtPostInCode = New System.Windows.Forms.TextBox()
        Me.txtAddressLine2 = New System.Windows.Forms.TextBox()
        Me.cboParkingDetails = New ControlLibrary.SuperComboBox()
        Me.txtAvailable = New System.Windows.Forms.MaskedTextBox()
        Me.txtTown = New System.Windows.Forms.TextBox()
        Me.txtAddressLine1 = New System.Windows.Forms.TextBox()
        Me.chkChildrenAllowed = New ControlLibrary.SuperCheckBox()
        Me.chkSmokersAllowed = New ControlLibrary.SuperCheckBox()
        Me.chkPetsAllowed = New ControlLibrary.SuperCheckBox()
        Me.chkGarage = New ControlLibrary.SuperCheckBox()
        Me.cboFurnishingCode = New ControlLibrary.SuperComboBox()
        Me.cboPropertyType = New ControlLibrary.SuperComboBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.txtPropertyLink = New System.Windows.Forms.TextBox()
        Me.txtBedrooms = New System.Windows.Forms.TextBox()
        Me.txtBathrooms = New System.Windows.Forms.TextBox()
        Me.txtReceptionRooms = New System.Windows.Forms.TextBox()
        Me.txtAgentNm = New System.Windows.Forms.TextBox()
        Me.pbxAvailable = New System.Windows.Forms.PictureBox()
        Me.bnTenancy = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbAdd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.chkGarden = New ControlLibrary.SuperCheckBox()
        Me.txtRent = New System.Windows.Forms.TextBox()
        Me.txtAgentTelNo = New System.Windows.Forms.TextBox()
        Me.taCase = New clsRental.dsTenancySearchResultsTableAdapters.V_frmTenancySearchResultsTableAdapter()
        Me.taTenancy = New clsRental.dsTenancySearchResultsTableAdapters.V_fsubTenancySearchResultsTableAdapter()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label35 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label36 = New System.Windows.Forms.Label()
        Label37 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        CType(Me.pbxLetter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFormStrip.SuspendLayout()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCase.SuspendLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsTenancySearchResults, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnCase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnCase.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTenancy.SuspendLayout()
        CType(Me.pbxAvailable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTenancy.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(480, 23)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(53, 14)
        Label4.TabIndex = 119
        Label4.Text = "Customer"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(194, 22)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(48, 14)
        Label5.TabIndex = 120
        Label5.Text = "Package"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label7.Location = New System.Drawing.Point(319, 127)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(49, 14)
        Label7.TabIndex = 108
        Label7.Text = "Garden?"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label8.Location = New System.Drawing.Point(480, 58)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(29, 14)
        Label8.TabIndex = 91
        Label8.Text = "Rent"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label9.Location = New System.Drawing.Point(39, 214)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(74, 14)
        Label9.TabIndex = 83
        Label9.Text = "Agent Tel. No."
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label10.Location = New System.Drawing.Point(480, 32)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(75, 14)
        Label10.TabIndex = 93
        Label10.Text = "Date Available"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label28.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label28.Location = New System.Drawing.Point(480, 167)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(77, 14)
        Label28.TabIndex = 208
        Label28.Text = "Parking Details"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label31.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label31.Location = New System.Drawing.Point(39, 293)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(64, 28)
        Label31.TabIndex = 205
        Label31.Text = "Description/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Comments"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label13.Location = New System.Drawing.Point(319, 84)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(75, 14)
        Label13.TabIndex = 203
        Label13.Text = "No. Bedrooms"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label15.Location = New System.Drawing.Point(319, 58)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(78, 14)
        Label15.TabIndex = 201
        Label15.Text = "No. Bathrooms"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label22.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label22.Location = New System.Drawing.Point(39, 188)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(66, 14)
        Label22.TabIndex = 200
        Label22.Text = "Agent Name"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label24.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label24.Location = New System.Drawing.Point(319, 32)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(81, 14)
        Label24.TabIndex = 197
        Label24.Text = "No. Recpt. Rms"
        '
        'Label35
        '
        Label35.AutoSize = True
        Label35.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label35.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label35.Location = New System.Drawing.Point(480, 113)
        Label35.Name = "Label35"
        Label35.Size = New System.Drawing.Size(74, 14)
        Label35.TabIndex = 188
        Label35.Text = "Property Type"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label14.Location = New System.Drawing.Point(480, 140)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(85, 14)
        Label14.TabIndex = 212
        Label14.Text = "Furnishing Code"
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label23.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label23.Location = New System.Drawing.Point(319, 187)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(94, 14)
        Label23.TabIndex = 220
        Label23.Text = "Children Allowed?"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label25.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label25.Location = New System.Drawing.Point(319, 147)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(97, 14)
        Label25.TabIndex = 219
        Label25.Text = "Smokers Allowed?"
        '
        'Label36
        '
        Label36.AutoSize = True
        Label36.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label36.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label36.Location = New System.Drawing.Point(319, 167)
        Label36.Name = "Label36"
        Label36.Size = New System.Drawing.Size(76, 14)
        Label36.TabIndex = 218
        Label36.Text = "Pets Allowed?"
        '
        'Label37
        '
        Label37.AutoSize = True
        Label37.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label37.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label37.Location = New System.Drawing.Point(319, 107)
        Label37.Name = "Label37"
        Label37.Size = New System.Drawing.Size(49, 14)
        Label37.TabIndex = 217
        Label37.Text = "Garage?"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label3.Location = New System.Drawing.Point(39, 110)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(34, 14)
        Label3.TabIndex = 224
        Label3.Text = "Town"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.Silver
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.White
        Label6.Location = New System.Drawing.Point(595, 5)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(108, 14)
        Label6.TabIndex = 120
        Label6.Text = "Send To Employee"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label11.Location = New System.Drawing.Point(39, 240)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(71, 14)
        Label11.TabIndex = 227
        Label11.Text = "Property URL"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label1.Location = New System.Drawing.Point(480, 84)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(84, 14)
        Label1.TabIndex = 244
        Label1.Text = "Rent Frequency"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label18.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label18.Location = New System.Drawing.Point(480, 48)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(120, 14)
        Label18.TabIndex = 246
        Label18.Text = "Tenancy Search Status"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxLetter
        '
        Me.pbxLetter.Image = CType(resources.GetObject("pbxLetter.Image"), System.Drawing.Image)
        Me.pbxLetter.Location = New System.Drawing.Point(477, 2)
        Me.pbxLetter.Name = "pbxLetter"
        Me.pbxLetter.Size = New System.Drawing.Size(40, 29)
        Me.pbxLetter.TabIndex = 1
        Me.pbxLetter.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxLetter, "Generate Letters")
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(156, 14)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 247
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 129
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Controls.Add(Me.pbxLetter)
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 128
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 127
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(747, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 10
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox7)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.pnlCase)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(0, 64)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(795, 138)
        Me.Panel1.TabIndex = 124
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 79
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'pnlCase
        '
        Me.pnlCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlCase.Controls.Add(Me.pbxVIP)
        Me.pnlCase.Controls.Add(Label18)
        Me.pnlCase.Controls.Add(Me.cboTenancySearchStatus)
        Me.pnlCase.Controls.Add(Me.SuperLabel1)
        Me.pnlCase.Controls.Add(Label4)
        Me.pnlCase.Controls.Add(Me.txtCustomerNm)
        Me.pnlCase.Controls.Add(Label5)
        Me.pnlCase.Controls.Add(Me.pbxCopy)
        Me.pnlCase.Controls.Add(Me.txtPackage)
        Me.pnlCase.Controls.Add(Me.bnCase)
        Me.pnlCase.Controls.Add(Me.txtCaseNo)
        Me.pnlCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(775, 92)
        Me.pnlCase.TabIndex = 1
        '
        'cboTenancySearchStatus
        '
        Me.cboTenancySearchStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboTenancySearchStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTenancySearchStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "TENANCY_SEARCH_STATUS_CD", True))
        Me.cboTenancySearchStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTenancySearchStatus.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTenancySearchStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboTenancySearchStatus.FormattingEnabled = True
        Me.cboTenancySearchStatus.Location = New System.Drawing.Point(657, 45)
        Me.cboTenancySearchStatus.Name = "cboTenancySearchStatus"
        Me.cboTenancySearchStatus.Queryable = False
        Me.cboTenancySearchStatus.Size = New System.Drawing.Size(83, 22)
        Me.cboTenancySearchStatus.TabIndex = 245
        Me.cboTenancySearchStatus.Updateable = True
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmTenancySearchResults"
        Me.bsCase.DataSource = Me.DsTenancySearchResults
        '
        'DsTenancySearchResults
        '
        Me.DsTenancySearchResults.DataSetName = "dsTenancySearchResults"
        Me.DsTenancySearchResults.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(22, 22)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel1.TabIndex = 122
        Me.SuperLabel1.Text = "Case No"
        '
        'txtCustomerNm
        '
        Me.txtCustomerNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtCustomerNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CUSTOMER_NM", True))
        Me.txtCustomerNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomerNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCustomerNm.Location = New System.Drawing.Point(543, 19)
        Me.txtCustomerNm.Name = "txtCustomerNm"
        Me.txtCustomerNm.ReadOnly = True
        Me.txtCustomerNm.Size = New System.Drawing.Size(197, 20)
        Me.txtCustomerNm.TabIndex = 117
        Me.txtCustomerNm.TabStop = False
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(135, 22)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txtPackage
        '
        Me.txtPackage.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtPackage.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CLIENT_PACKAGE_NM", True))
        Me.txtPackage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackage.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtPackage.Location = New System.Drawing.Point(248, 19)
        Me.txtPackage.Name = "txtPackage"
        Me.txtPackage.ReadOnly = True
        Me.txtPackage.Size = New System.Drawing.Size(217, 20)
        Me.txtPackage.TabIndex = 121
        Me.txtPackage.TabStop = False
        '
        'bnCase
        '
        Me.bnCase.AddNewItem = Nothing
        Me.bnCase.BindingSource = Me.bsCase
        Me.bnCase.CountItem = Me.BindingNavigatorCountItem
        Me.bnCase.DeleteItem = Nothing
        Me.bnCase.Dock = System.Windows.Forms.DockStyle.None
        Me.bnCase.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnCase.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.bnCase.Location = New System.Drawing.Point(20, 53)
        Me.bnCase.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.bnCase.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.bnCase.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.bnCase.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.bnCase.Name = "bnCase"
        Me.bnCase.PositionItem = Me.BindingNavigatorPositionItem
        Me.bnCase.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.bnCase.Size = New System.Drawing.Size(169, 25)
        Me.bnCase.TabIndex = 111
        Me.bnCase.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(25, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'txtCaseNo
        '
        Me.txtCaseNo.AcceptsReturn = True
        Me.txtCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CSE_CASE_NO_CSE", True))
        Me.txtCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCaseNo.Location = New System.Drawing.Point(76, 19)
        Me.txtCaseNo.Name = "txtCaseNo"
        Me.txtCaseNo.PreviousQuery = Nothing
        Me.txtCaseNo.Queryable = True
        Me.txtCaseNo.QueryMandatory = False
        Me.txtCaseNo.ReadOnly = True
        Me.txtCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txtCaseNo.TabIndex = 1
        Me.txtCaseNo.TabStop = False
        Me.txtCaseNo.Updateable = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.txtVIP)
        Me.Panel2.Controls.Add(Me.txtClientId)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtPackageId)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(725, 24)
        Me.Panel2.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(269, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'txtClientId
        '
        Me.txtClientId.BackColor = System.Drawing.Color.Yellow
        Me.txtClientId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CLT_BUSINESS_NO_ORG", True))
        Me.txtClientId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClientId.ForeColor = System.Drawing.Color.DimGray
        Me.txtClientId.Location = New System.Drawing.Point(344, 2)
        Me.txtClientId.Name = "txtClientId"
        Me.txtClientId.Size = New System.Drawing.Size(44, 20)
        Me.txtClientId.TabIndex = 117
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Case"
        '
        'txtPackageId
        '
        Me.txtPackageId.BackColor = System.Drawing.Color.Yellow
        Me.txtPackageId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CGS_CLIENT_PACKAGE_ID_CGS", True))
        Me.txtPackageId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackageId.ForeColor = System.Drawing.Color.DimGray
        Me.txtPackageId.Location = New System.Drawing.Point(306, 2)
        Me.txtPackageId.Name = "txtPackageId"
        Me.txtPackageId.Size = New System.Drawing.Size(36, 20)
        Me.txtPackageId.TabIndex = 116
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(194, 19)
        Me.SuperLabel2.TabIndex = 123
        Me.SuperLabel2.Text = "Tenancy Search Results"
        '
        'bsTenancy
        '
        Me.bsTenancy.AllowNew = True
        Me.bsTenancy.DataMember = "V_frmTenancySearchResults_V_fsubTenancySearchResults"
        Me.bsTenancy.DataSource = Me.bsCase
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Controls.Add(Me.PictureBox2)
        Me.Panel3.Controls.Add(Me.pnlTenancy)
        Me.Panel3.Location = New System.Drawing.Point(0, 208)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(795, 492)
        Me.Panel3.TabIndex = 125
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Silver
        Me.Panel5.Controls.Add(Me.txtTimestamp)
        Me.Panel5.Controls.Add(Me.chkSendToEmployee)
        Me.Panel5.Controls.Add(Label6)
        Me.Panel5.Controls.Add(Me.txtGeneratedNo)
        Me.Panel5.Controls.Add(Me.txtTenancyCaseNo)
        Me.Panel5.Controls.Add(Me.Label12)
        Me.Panel5.Controls.Add(Me.txtTenancyPackageId)
        Me.Panel5.Location = New System.Drawing.Point(33, 8)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(738, 24)
        Me.Panel5.TabIndex = 0
        '
        'txtTimestamp
        '
        Me.txtTimestamp.BackColor = System.Drawing.Color.Yellow
        Me.txtTimestamp.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "INT_TIMESTAMP", True))
        Me.txtTimestamp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTimestamp.ForeColor = System.Drawing.Color.DimGray
        Me.txtTimestamp.Location = New System.Drawing.Point(419, 2)
        Me.txtTimestamp.Name = "txtTimestamp"
        Me.txtTimestamp.Size = New System.Drawing.Size(87, 20)
        Me.txtTimestamp.TabIndex = 121
        '
        'chkSendToEmployee
        '
        Me.chkSendToEmployee.AutoSize = True
        Me.chkSendToEmployee.Location = New System.Drawing.Point(714, 5)
        Me.chkSendToEmployee.Name = "chkSendToEmployee"
        Me.chkSendToEmployee.Size = New System.Drawing.Size(15, 14)
        Me.chkSendToEmployee.TabIndex = 0
        Me.chkSendToEmployee.UseVisualStyleBackColor = True
        '
        'txtGeneratedNo
        '
        Me.txtGeneratedNo.BackColor = System.Drawing.Color.Yellow
        Me.txtGeneratedNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "GENERATED_NO", True))
        Me.txtGeneratedNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneratedNo.ForeColor = System.Drawing.Color.DimGray
        Me.txtGeneratedNo.Location = New System.Drawing.Point(285, 2)
        Me.txtGeneratedNo.Name = "txtGeneratedNo"
        Me.txtGeneratedNo.Size = New System.Drawing.Size(36, 20)
        Me.txtGeneratedNo.TabIndex = 118
        '
        'txtTenancyCaseNo
        '
        Me.txtTenancyCaseNo.BackColor = System.Drawing.Color.Yellow
        Me.txtTenancyCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "CPC_CASE_NO_CSE", True))
        Me.txtTenancyCaseNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyCaseNo.ForeColor = System.Drawing.Color.DimGray
        Me.txtTenancyCaseNo.Location = New System.Drawing.Point(335, 2)
        Me.txtTenancyCaseNo.Name = "txtTenancyCaseNo"
        Me.txtTenancyCaseNo.Size = New System.Drawing.Size(36, 20)
        Me.txtTenancyCaseNo.TabIndex = 117
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(0, 5)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 14)
        Me.Label12.TabIndex = 73
        Me.Label12.Text = "Rental Searches"
        '
        'txtTenancyPackageId
        '
        Me.txtTenancyPackageId.BackColor = System.Drawing.Color.Yellow
        Me.txtTenancyPackageId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "CPC_CLIENT_PACKAGE_ID_CGS", True))
        Me.txtTenancyPackageId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyPackageId.ForeColor = System.Drawing.Color.DimGray
        Me.txtTenancyPackageId.Location = New System.Drawing.Point(377, 2)
        Me.txtTenancyPackageId.Name = "txtTenancyPackageId"
        Me.txtTenancyPackageId.Size = New System.Drawing.Size(36, 20)
        Me.txtTenancyPackageId.TabIndex = 116
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlTenancy
        '
        Me.pnlTenancy.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlTenancy.Controls.Add(Me.lblCharactersRemaining)
        Me.pnlTenancy.Controls.Add(Label1)
        Me.pnlTenancy.Controls.Add(Me.cboRentFrequency)
        Me.pnlTenancy.Controls.Add(Me.Label16)
        Me.pnlTenancy.Controls.Add(Me.txtPostOutCode)
        Me.pnlTenancy.Controls.Add(Me.Label17)
        Me.pnlTenancy.Controls.Add(Me.txtCounty)
        Me.pnlTenancy.Controls.Add(Me.Label19)
        Me.pnlTenancy.Controls.Add(Me.Label20)
        Me.pnlTenancy.Controls.Add(Me.Label21)
        Me.pnlTenancy.Controls.Add(Me.txtAddressLine3)
        Me.pnlTenancy.Controls.Add(Me.txtPostInCode)
        Me.pnlTenancy.Controls.Add(Me.txtAddressLine2)
        Me.pnlTenancy.Controls.Add(Label11)
        Me.pnlTenancy.Controls.Add(Me.cboParkingDetails)
        Me.pnlTenancy.Controls.Add(Me.txtAvailable)
        Me.pnlTenancy.Controls.Add(Label3)
        Me.pnlTenancy.Controls.Add(Me.txtTown)
        Me.pnlTenancy.Controls.Add(Me.txtAddressLine1)
        Me.pnlTenancy.Controls.Add(Me.chkChildrenAllowed)
        Me.pnlTenancy.Controls.Add(Label23)
        Me.pnlTenancy.Controls.Add(Me.chkSmokersAllowed)
        Me.pnlTenancy.Controls.Add(Label25)
        Me.pnlTenancy.Controls.Add(Me.chkPetsAllowed)
        Me.pnlTenancy.Controls.Add(Label36)
        Me.pnlTenancy.Controls.Add(Me.chkGarage)
        Me.pnlTenancy.Controls.Add(Label37)
        Me.pnlTenancy.Controls.Add(Me.cboFurnishingCode)
        Me.pnlTenancy.Controls.Add(Label14)
        Me.pnlTenancy.Controls.Add(Label28)
        Me.pnlTenancy.Controls.Add(Label31)
        Me.pnlTenancy.Controls.Add(Label13)
        Me.pnlTenancy.Controls.Add(Label15)
        Me.pnlTenancy.Controls.Add(Label22)
        Me.pnlTenancy.Controls.Add(Label24)
        Me.pnlTenancy.Controls.Add(Me.cboPropertyType)
        Me.pnlTenancy.Controls.Add(Me.txtDescription)
        Me.pnlTenancy.Controls.Add(Me.txtPropertyLink)
        Me.pnlTenancy.Controls.Add(Me.txtBedrooms)
        Me.pnlTenancy.Controls.Add(Me.txtBathrooms)
        Me.pnlTenancy.Controls.Add(Me.txtReceptionRooms)
        Me.pnlTenancy.Controls.Add(Me.txtAgentNm)
        Me.pnlTenancy.Controls.Add(Label35)
        Me.pnlTenancy.Controls.Add(Me.pbxAvailable)
        Me.pnlTenancy.Controls.Add(Me.bnTenancy)
        Me.pnlTenancy.Controls.Add(Me.chkGarden)
        Me.pnlTenancy.Controls.Add(Label7)
        Me.pnlTenancy.Controls.Add(Label8)
        Me.pnlTenancy.Controls.Add(Label9)
        Me.pnlTenancy.Controls.Add(Me.txtRent)
        Me.pnlTenancy.Controls.Add(Me.txtAgentTelNo)
        Me.pnlTenancy.Controls.Add(Label10)
        Me.pnlTenancy.Location = New System.Drawing.Point(8, 32)
        Me.pnlTenancy.Name = "pnlTenancy"
        Me.pnlTenancy.Size = New System.Drawing.Size(775, 449)
        Me.pnlTenancy.TabIndex = 1
        '
        'lblCharactersRemaining
        '
        Me.lblCharactersRemaining.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCharactersRemaining.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lblCharactersRemaining.Location = New System.Drawing.Point(582, 403)
        Me.lblCharactersRemaining.Name = "lblCharactersRemaining"
        Me.lblCharactersRemaining.Size = New System.Drawing.Size(158, 16)
        Me.lblCharactersRemaining.TabIndex = 245
        Me.lblCharactersRemaining.Text = "Characters Remaining: "
        Me.lblCharactersRemaining.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboRentFrequency
        '
        Me.cboRentFrequency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboRentFrequency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRentFrequency.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "PAYMENT_FREQUENCY_CD", True))
        Me.cboRentFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRentFrequency.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRentFrequency.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboRentFrequency.FormattingEnabled = True
        Me.cboRentFrequency.Location = New System.Drawing.Point(600, 81)
        Me.cboRentFrequency.Name = "cboRentFrequency"
        Me.cboRentFrequency.Queryable = False
        Me.cboRentFrequency.ReadOnly = True
        Me.cboRentFrequency.Size = New System.Drawing.Size(104, 22)
        Me.cboRentFrequency.TabIndex = 243
        Me.cboRentFrequency.Updateable = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(39, 31)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(81, 14)
        Me.Label16.TabIndex = 234
        Me.Label16.Text = "Address Line 1"
        '
        'txtPostOutCode
        '
        Me.txtPostOutCode.AcceptsTab = True
        Me.txtPostOutCode.BackColor = System.Drawing.Color.White
        Me.txtPostOutCode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "POSTCODE_OUTCODE", True))
        Me.txtPostOutCode.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostOutCode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtPostOutCode.Location = New System.Drawing.Point(123, 159)
        Me.txtPostOutCode.Name = "txtPostOutCode"
        Me.txtPostOutCode.Size = New System.Drawing.Size(38, 20)
        Me.txtPostOutCode.TabIndex = 5
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(39, 58)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(81, 14)
        Me.Label17.TabIndex = 242
        Me.Label17.Text = "Address Line 2"
        '
        'txtCounty
        '
        Me.txtCounty.BackColor = System.Drawing.Color.White
        Me.txtCounty.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "COUNTY", True))
        Me.txtCounty.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCounty.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCounty.Location = New System.Drawing.Point(123, 133)
        Me.txtCounty.Name = "txtCounty"
        Me.txtCounty.Size = New System.Drawing.Size(154, 20)
        Me.txtCounty.TabIndex = 4
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(40, 136)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(41, 14)
        Me.Label19.TabIndex = 232
        Me.Label19.Text = "County"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(39, 84)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(81, 14)
        Me.Label20.TabIndex = 240
        Me.Label20.Text = "Address Line 3"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(40, 162)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(56, 14)
        Me.Label21.TabIndex = 233
        Me.Label21.Text = "Post Code"
        '
        'txtAddressLine3
        '
        Me.txtAddressLine3.AcceptsReturn = True
        Me.txtAddressLine3.BackColor = System.Drawing.Color.White
        Me.txtAddressLine3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "ADDRESS_LINE3", True))
        Me.txtAddressLine3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAddressLine3.Location = New System.Drawing.Point(123, 81)
        Me.txtAddressLine3.Name = "txtAddressLine3"
        Me.txtAddressLine3.Size = New System.Drawing.Size(154, 20)
        Me.txtAddressLine3.TabIndex = 2
        '
        'txtPostInCode
        '
        Me.txtPostInCode.BackColor = System.Drawing.Color.White
        Me.txtPostInCode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "POSTCODE_INCODE", True))
        Me.txtPostInCode.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostInCode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtPostInCode.Location = New System.Drawing.Point(162, 159)
        Me.txtPostInCode.Name = "txtPostInCode"
        Me.txtPostInCode.Size = New System.Drawing.Size(38, 20)
        Me.txtPostInCode.TabIndex = 6
        '
        'txtAddressLine2
        '
        Me.txtAddressLine2.AcceptsReturn = True
        Me.txtAddressLine2.BackColor = System.Drawing.Color.White
        Me.txtAddressLine2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "ADDRESS_LINE2", True))
        Me.txtAddressLine2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAddressLine2.Location = New System.Drawing.Point(123, 55)
        Me.txtAddressLine2.Name = "txtAddressLine2"
        Me.txtAddressLine2.Size = New System.Drawing.Size(154, 20)
        Me.txtAddressLine2.TabIndex = 1
        '
        'cboParkingDetails
        '
        Me.cboParkingDetails.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboParkingDetails.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboParkingDetails.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "PARKING_DETAILS_CD", True))
        Me.cboParkingDetails.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboParkingDetails.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboParkingDetails.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboParkingDetails.FormattingEnabled = True
        Me.cboParkingDetails.Location = New System.Drawing.Point(600, 164)
        Me.cboParkingDetails.Name = "cboParkingDetails"
        Me.cboParkingDetails.Queryable = False
        Me.cboParkingDetails.Size = New System.Drawing.Size(140, 22)
        Me.cboParkingDetails.TabIndex = 23
        Me.cboParkingDetails.Updateable = True
        '
        'txtAvailable
        '
        Me.txtAvailable.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "AVAILABLE_DT", True))
        Me.txtAvailable.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAvailable.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAvailable.Location = New System.Drawing.Point(600, 29)
        Me.txtAvailable.Mask = "00/00/0000"
        Me.txtAvailable.Name = "txtAvailable"
        Me.txtAvailable.Size = New System.Drawing.Size(65, 20)
        Me.txtAvailable.TabIndex = 19
        Me.txtAvailable.ValidatingType = GetType(Date)
        '
        'txtTown
        '
        Me.txtTown.BackColor = System.Drawing.Color.White
        Me.txtTown.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "POST_TOWN", True))
        Me.txtTown.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTown.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTown.Location = New System.Drawing.Point(123, 107)
        Me.txtTown.Name = "txtTown"
        Me.txtTown.Size = New System.Drawing.Size(154, 20)
        Me.txtTown.TabIndex = 3
        '
        'txtAddressLine1
        '
        Me.txtAddressLine1.AcceptsReturn = True
        Me.txtAddressLine1.BackColor = System.Drawing.Color.White
        Me.txtAddressLine1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "ADDRESS_LINE1", True))
        Me.txtAddressLine1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAddressLine1.Location = New System.Drawing.Point(123, 29)
        Me.txtAddressLine1.Name = "txtAddressLine1"
        Me.txtAddressLine1.Size = New System.Drawing.Size(154, 20)
        Me.txtAddressLine1.TabIndex = 0
        '
        'chkChildrenAllowed
        '
        Me.chkChildrenAllowed.AutoSize = True
        Me.chkChildrenAllowed.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChildrenAllowed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkChildrenAllowed.Location = New System.Drawing.Point(419, 187)
        Me.chkChildrenAllowed.Name = "chkChildrenAllowed"
        Me.chkChildrenAllowed.Size = New System.Drawing.Size(15, 14)
        Me.chkChildrenAllowed.TabIndex = 16
        Me.chkChildrenAllowed.UseVisualStyleBackColor = True
        '
        'chkSmokersAllowed
        '
        Me.chkSmokersAllowed.AutoSize = True
        Me.chkSmokersAllowed.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSmokersAllowed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkSmokersAllowed.Location = New System.Drawing.Point(419, 147)
        Me.chkSmokersAllowed.Name = "chkSmokersAllowed"
        Me.chkSmokersAllowed.Size = New System.Drawing.Size(15, 14)
        Me.chkSmokersAllowed.TabIndex = 14
        Me.chkSmokersAllowed.UseVisualStyleBackColor = True
        '
        'chkPetsAllowed
        '
        Me.chkPetsAllowed.AutoSize = True
        Me.chkPetsAllowed.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPetsAllowed.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkPetsAllowed.Location = New System.Drawing.Point(419, 167)
        Me.chkPetsAllowed.Name = "chkPetsAllowed"
        Me.chkPetsAllowed.Size = New System.Drawing.Size(15, 14)
        Me.chkPetsAllowed.TabIndex = 15
        Me.chkPetsAllowed.UseVisualStyleBackColor = True
        '
        'chkGarage
        '
        Me.chkGarage.AutoSize = True
        Me.chkGarage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGarage.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkGarage.Location = New System.Drawing.Point(419, 106)
        Me.chkGarage.Name = "chkGarage"
        Me.chkGarage.Size = New System.Drawing.Size(15, 14)
        Me.chkGarage.TabIndex = 12
        Me.chkGarage.UseVisualStyleBackColor = True
        '
        'cboFurnishingCode
        '
        Me.cboFurnishingCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboFurnishingCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFurnishingCode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "FURNISH_CD", True))
        Me.cboFurnishingCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFurnishingCode.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFurnishingCode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboFurnishingCode.FormattingEnabled = True
        Me.cboFurnishingCode.Location = New System.Drawing.Point(600, 137)
        Me.cboFurnishingCode.Name = "cboFurnishingCode"
        Me.cboFurnishingCode.Queryable = False
        Me.cboFurnishingCode.Size = New System.Drawing.Size(140, 22)
        Me.cboFurnishingCode.TabIndex = 22
        Me.cboFurnishingCode.Updateable = True
        '
        'cboPropertyType
        '
        Me.cboPropertyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboPropertyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPropertyType.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "PROPERTY_TYPE_CD", True))
        Me.cboPropertyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPropertyType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPropertyType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboPropertyType.FormattingEnabled = True
        Me.cboPropertyType.Location = New System.Drawing.Point(600, 110)
        Me.cboPropertyType.Name = "cboPropertyType"
        Me.cboPropertyType.Queryable = False
        Me.cboPropertyType.Size = New System.Drawing.Size(104, 22)
        Me.cboPropertyType.TabIndex = 21
        Me.cboPropertyType.Updateable = True
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.Color.White
        Me.txtDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "DESCRIPTION_TX", True))
        Me.txtDescription.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtDescription.Location = New System.Drawing.Point(123, 290)
        Me.txtDescription.MaxLength = 2000
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(617, 110)
        Me.txtDescription.TabIndex = 18
        '
        'txtPropertyLink
        '
        Me.txtPropertyLink.AcceptsReturn = True
        Me.txtPropertyLink.BackColor = System.Drawing.Color.White
        Me.txtPropertyLink.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "PROPERTY_LINK_TX", True))
        Me.txtPropertyLink.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPropertyLink.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtPropertyLink.Location = New System.Drawing.Point(123, 237)
        Me.txtPropertyLink.MaxLength = 2000
        Me.txtPropertyLink.Multiline = True
        Me.txtPropertyLink.Name = "txtPropertyLink"
        Me.txtPropertyLink.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPropertyLink.Size = New System.Drawing.Size(617, 47)
        Me.txtPropertyLink.TabIndex = 17
        '
        'txtBedrooms
        '
        Me.txtBedrooms.BackColor = System.Drawing.Color.White
        Me.txtBedrooms.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "BEDROOM_QT", True))
        Me.txtBedrooms.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBedrooms.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtBedrooms.Location = New System.Drawing.Point(419, 81)
        Me.txtBedrooms.Name = "txtBedrooms"
        Me.txtBedrooms.Size = New System.Drawing.Size(25, 20)
        Me.txtBedrooms.TabIndex = 11
        '
        'txtBathrooms
        '
        Me.txtBathrooms.BackColor = System.Drawing.Color.White
        Me.txtBathrooms.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "BATHROOM_QT", True))
        Me.txtBathrooms.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBathrooms.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtBathrooms.Location = New System.Drawing.Point(419, 55)
        Me.txtBathrooms.Name = "txtBathrooms"
        Me.txtBathrooms.Size = New System.Drawing.Size(25, 20)
        Me.txtBathrooms.TabIndex = 10
        '
        'txtReceptionRooms
        '
        Me.txtReceptionRooms.BackColor = System.Drawing.Color.White
        Me.txtReceptionRooms.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "RECEPT_ROOM_QT", True))
        Me.txtReceptionRooms.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReceptionRooms.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtReceptionRooms.Location = New System.Drawing.Point(419, 29)
        Me.txtReceptionRooms.Name = "txtReceptionRooms"
        Me.txtReceptionRooms.Size = New System.Drawing.Size(25, 20)
        Me.txtReceptionRooms.TabIndex = 9
        '
        'txtAgentNm
        '
        Me.txtAgentNm.BackColor = System.Drawing.Color.White
        Me.txtAgentNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "LETTING_AGENT_NM", True))
        Me.txtAgentNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgentNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAgentNm.Location = New System.Drawing.Point(123, 185)
        Me.txtAgentNm.Name = "txtAgentNm"
        Me.txtAgentNm.Size = New System.Drawing.Size(154, 20)
        Me.txtAgentNm.TabIndex = 7
        '
        'pbxAvailable
        '
        Me.pbxAvailable.Image = CType(resources.GetObject("pbxAvailable.Image"), System.Drawing.Image)
        Me.pbxAvailable.Location = New System.Drawing.Point(674, 29)
        Me.pbxAvailable.Name = "pbxAvailable"
        Me.pbxAvailable.Size = New System.Drawing.Size(15, 15)
        Me.pbxAvailable.TabIndex = 117
        Me.pbxAvailable.TabStop = False
        '
        'bnTenancy
        '
        Me.bnTenancy.AddNewItem = Me.tsbAdd
        Me.bnTenancy.BindingSource = Me.bsTenancy
        Me.bnTenancy.CountItem = Me.ToolStripLabel1
        Me.bnTenancy.DeleteItem = Nothing
        Me.bnTenancy.Dock = System.Windows.Forms.DockStyle.None
        Me.bnTenancy.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnTenancy.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.ToolStripButton3, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.ToolStripButton4, Me.ToolStripButton5, Me.ToolStripSeparator3, Me.tsbAdd})
        Me.bnTenancy.Location = New System.Drawing.Point(20, 407)
        Me.bnTenancy.MoveFirstItem = Me.ToolStripButton2
        Me.bnTenancy.MoveLastItem = Me.ToolStripButton5
        Me.bnTenancy.MoveNextItem = Me.ToolStripButton4
        Me.bnTenancy.MovePreviousItem = Me.ToolStripButton3
        Me.bnTenancy.Name = "bnTenancy"
        Me.bnTenancy.PositionItem = Me.ToolStripTextBox1
        Me.bnTenancy.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.bnTenancy.Size = New System.Drawing.Size(198, 25)
        Me.bnTenancy.TabIndex = 111
        Me.bnTenancy.Text = "BindingNavigator2"
        '
        'tsbAdd
        '
        Me.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAdd.Image = CType(resources.GetObject("tsbAdd.Image"), System.Drawing.Image)
        Me.tsbAdd.Name = "tsbAdd"
        Me.tsbAdd.RightToLeftAutoMirrorImage = True
        Me.tsbAdd.Size = New System.Drawing.Size(23, 22)
        Me.tsbAdd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 22)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Move first"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(25, 21)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Move next"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'chkGarden
        '
        Me.chkGarden.AutoSize = True
        Me.chkGarden.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGarden.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkGarden.Location = New System.Drawing.Point(419, 127)
        Me.chkGarden.Name = "chkGarden"
        Me.chkGarden.Size = New System.Drawing.Size(15, 14)
        Me.chkGarden.TabIndex = 13
        Me.chkGarden.UseVisualStyleBackColor = True
        '
        'txtRent
        '
        Me.txtRent.BackColor = System.Drawing.Color.White
        Me.txtRent.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "RENT_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtRent.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtRent.Location = New System.Drawing.Point(600, 55)
        Me.txtRent.Name = "txtRent"
        Me.txtRent.Size = New System.Drawing.Size(66, 20)
        Me.txtRent.TabIndex = 20
        Me.txtRent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAgentTelNo
        '
        Me.txtAgentTelNo.BackColor = System.Drawing.Color.White
        Me.txtAgentTelNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "LETTING_AGENT_TEL_NO", True))
        Me.txtAgentTelNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgentTelNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtAgentTelNo.Location = New System.Drawing.Point(123, 211)
        Me.txtAgentTelNo.Name = "txtAgentTelNo"
        Me.txtAgentTelNo.Size = New System.Drawing.Size(104, 20)
        Me.txtAgentTelNo.TabIndex = 8
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taTenancy
        '
        Me.taTenancy.ClearBeforeFill = True
        '
        'frmTenancySearchResults
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 700)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.SuperLabel2)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "frmTenancySearchResults"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Tenancy Search Results"
        CType(Me.pbxLetter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFormStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCase.ResumeLayout(False)
        Me.pnlCase.PerformLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsTenancySearchResults, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnCase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnCase.ResumeLayout(False)
        Me.bnCase.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTenancy.ResumeLayout(False)
        Me.pnlTenancy.PerformLayout()
        CType(Me.pbxAvailable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTenancy.ResumeLayout(False)
        Me.bnTenancy.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlCase As System.Windows.Forms.Panel
    Friend WithEvents bnCase As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents pbxLetter As System.Windows.Forms.PictureBox
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents txtPackageId As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomerNm As System.Windows.Forms.TextBox
    Friend WithEvents txtPackage As System.Windows.Forms.TextBox
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents DsTenancySearchResults As clsRental.dsTenancySearchResults
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents taCase As clsRental.dsTenancySearchResultsTableAdapters.V_frmTenancySearchResultsTableAdapter
    Friend WithEvents bsTenancy As System.Windows.Forms.BindingSource
    Friend WithEvents taTenancy As clsRental.dsTenancySearchResultsTableAdapters.V_fsubTenancySearchResultsTableAdapter
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlTenancy As System.Windows.Forms.Panel
    Friend WithEvents pbxAvailable As System.Windows.Forms.PictureBox
    Friend WithEvents txtTenancyPackageId As System.Windows.Forms.TextBox
    Friend WithEvents bnTenancy As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents chkGarden As ControlLibrary.SuperCheckBox
    Friend WithEvents txtRent As System.Windows.Forms.TextBox
    Friend WithEvents txtAgentTelNo As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboPropertyType As ControlLibrary.SuperComboBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtPropertyLink As System.Windows.Forms.TextBox
    Friend WithEvents txtBedrooms As System.Windows.Forms.TextBox
    Friend WithEvents txtBathrooms As System.Windows.Forms.TextBox
    Friend WithEvents txtReceptionRooms As System.Windows.Forms.TextBox
    Friend WithEvents txtAgentNm As System.Windows.Forms.TextBox
    Friend WithEvents cboFurnishingCode As ControlLibrary.SuperComboBox
    Friend WithEvents chkChildrenAllowed As ControlLibrary.SuperCheckBox
    Friend WithEvents chkSmokersAllowed As ControlLibrary.SuperCheckBox
    Friend WithEvents chkPetsAllowed As ControlLibrary.SuperCheckBox
    Friend WithEvents chkGarage As ControlLibrary.SuperCheckBox
    Friend WithEvents txtTown As System.Windows.Forms.TextBox
    Friend WithEvents txtAddressLine1 As System.Windows.Forms.TextBox
    Friend WithEvents txtGeneratedNo As System.Windows.Forms.TextBox
    Friend WithEvents txtTenancyCaseNo As System.Windows.Forms.TextBox
    Friend WithEvents txtAvailable As System.Windows.Forms.MaskedTextBox
    Friend WithEvents chkSendToEmployee As ControlLibrary.SuperCheckBox
    Friend WithEvents cboParkingDetails As ControlLibrary.SuperComboBox
    Friend WithEvents txtClientId As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtPostOutCode As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCounty As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtAddressLine3 As System.Windows.Forms.TextBox
    Friend WithEvents txtPostInCode As System.Windows.Forms.TextBox
    Friend WithEvents txtAddressLine2 As System.Windows.Forms.TextBox
    Friend WithEvents cboRentFrequency As ControlLibrary.SuperComboBox
    Friend WithEvents txtTimestamp As System.Windows.Forms.TextBox
    Friend WithEvents cboTenancySearchStatus As ControlLibrary.SuperComboBox
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
    Friend WithEvents lblCharactersRemaining As System.Windows.Forms.Label
End Class
