'Imports System.IO
Public Class fdlgCompleteTenancy
    Dim mtxbX As TextBox
    Public Sub SetOpeningParameters(ByVal txbX As TextBox)
        mtxbX = txbX
    End Sub
    Private Sub fdlgCompleteTenancy_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.txtCompletionDt.Text = Date.Today

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Try
            If Not IsDate(Me.txtCompletionDt.Text) Then
                MsgBox("Date is not in correct format.", MsgBoxStyle.Exclamation, "Date Invalid")
                'ElseIf CType(Me.txtCompletionDt.Text, Date) > Date.Today Then
                '    MsgBox("Date cannot be after today.", MsgBoxStyle.Exclamation, "Date Invalid")
            Else
                mtxbX.Text = Me.txtCompletionDt.Text
                '"'" & Format(CType(Me.txtCompletionDt.Text, Date), "yyyyMMdd") & "'"
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "OK_Button_Click")
        End Try

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub pbxBackdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxBackdate.Click
        Dim frmX As New frmDatePicker
        frmX.CallingForm(Me.txtCompletionDt)
        frmX.ShowDialog()
    End Sub

    Private Sub pbxBackdate_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxBackdate.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCompletionDt)
    End Sub

    Private Sub pbxBackdate_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxBackdate.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub
End Class