<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmManualApportionments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label24 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmManualApportionments))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlQueryCase = New System.Windows.Forms.Panel()
        Me.SuperLabel4 = New ControlLibrary.SuperLabel(Me.components)
        Me.txbAddress = New ControlLibrary.SuperTextBox()
        Me.SuperLabel3 = New ControlLibrary.SuperLabel(Me.components)
        Me.txbPackageNm = New ControlLibrary.SuperTextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsManualApportionments = New clsRental.dsManualApportionments()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvCase = New System.Windows.Forms.DataGridView()
        Me.CSE_CASE_NO_CSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboRentalTypeCd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClientPackageDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlInvoice = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lnkDeleteApportionment = New System.Windows.Forms.LinkLabel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.dgvInvoice = New System.Windows.Forms.DataGridView()
        Me.PURCHASE_ORDER_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INV_BUSINESS_NO_ORG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INV_EXTERNAL_INV_NO_INV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INV_INVOICE_DT_INV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICE_ITEM_AM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ITEM_VAT_AM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONED_DT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GENERATED_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPICASENOCSEDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsInvoice = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlApportionment = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.pnlCaseCapability = New System.Windows.Forms.Panel()
        Me.lnlPackage = New System.Windows.Forms.LinkLabel()
        Me.chkDelayApportionment = New ControlLibrary.SuperCheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvApportionment = New System.Windows.Forms.DataGridView()
        Me.INI_BUSINESS_NO_ORG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INI_EXTERNAL_INV_NO_INV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INI_INVOICE_DT_INV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INI_GENERATED_NO_INI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GENERATED_NO_IAP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPC_CASE_NO_CSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPC_CLIENT_PACKAGE_ID_CGS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLIENT_PACKAGE_NM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONED_NET_AM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONED_VAT_AM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATE_RANGE_DS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BILLUPLOADDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsApportionment = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsCaseCapability = New System.Windows.Forms.BindingSource(Me.components)
        Me.taCase = New clsRental.dsManualApportionmentsTableAdapters.V_frmManualApportionmentsTableAdapter()
        Me.taInvoice = New clsRental.dsManualApportionmentsTableAdapters.V_fsubManualAppmntPurchaseTableAdapter()
        Me.taApportionment = New clsRental.dsManualApportionmentsTableAdapters.V_fsubManualAppmntApportionTableAdapter()
        Me.taCaseCapability = New clsRental.dsManualApportionmentsTableAdapters.V_fsubMaintainRentalCseCapDetlTableAdapter()
        Me.bsClientPackage = New System.Windows.Forms.BindingSource(Me.components)
        Me.taClientPackage = New clsRental.dsManualApportionmentsTableAdapters.CLIENT_PACKAGETableAdapter()
        Label24 = New System.Windows.Forms.Label()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlQueryCase.SuspendLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsManualApportionments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCase.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvCase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInvoice.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlApportionment.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCaseCapability.SuspendLayout()
        CType(Me.dgvApportionment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsApportionment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCaseCapability, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsClientPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.BackColor = System.Drawing.Color.Silver
        Label24.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.White
        Label24.Location = New System.Drawing.Point(514, 5)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(179, 14)
        Label24.TabIndex = 128
        Label24.Text = "Case Delay Apportionment Flag"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(159, 14)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlQueryCase)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(796, 96)
        Me.Panel8.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(763, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlQueryCase
        '
        Me.pnlQueryCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlQueryCase.Controls.Add(Me.pbxVIP)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel4)
        Me.pnlQueryCase.Controls.Add(Me.txbAddress)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel3)
        Me.pnlQueryCase.Controls.Add(Me.txbPackageNm)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel2)
        Me.pnlQueryCase.Controls.Add(Me.pbxCopy)
        Me.pnlQueryCase.Controls.Add(Me.txbCaseNo)
        Me.pnlQueryCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlQueryCase.Name = "pnlQueryCase"
        Me.pnlQueryCase.Size = New System.Drawing.Size(780, 54)
        Me.pnlQueryCase.TabIndex = 0
        '
        'SuperLabel4
        '
        Me.SuperLabel4.AutoSize = True
        Me.SuperLabel4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel4.Location = New System.Drawing.Point(478, 23)
        Me.SuperLabel4.Name = "SuperLabel4"
        Me.SuperLabel4.Size = New System.Drawing.Size(49, 14)
        Me.SuperLabel4.TabIndex = 118
        Me.SuperLabel4.Text = "Address"
        '
        'txbAddress
        '
        Me.txbAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAddress.Location = New System.Drawing.Point(532, 20)
        Me.txbAddress.Name = "txbAddress"
        Me.txbAddress.PreviousQuery = Nothing
        Me.txbAddress.Queryable = True
        Me.txbAddress.QueryMandatory = False
        Me.txbAddress.ReadOnly = True
        Me.txbAddress.Size = New System.Drawing.Size(231, 20)
        Me.txbAddress.TabIndex = 117
        Me.txbAddress.Updateable = False
        '
        'SuperLabel3
        '
        Me.SuperLabel3.AutoSize = True
        Me.SuperLabel3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel3.Location = New System.Drawing.Point(197, 23)
        Me.SuperLabel3.Name = "SuperLabel3"
        Me.SuperLabel3.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel3.TabIndex = 116
        Me.SuperLabel3.Text = "Package"
        '
        'txbPackageNm
        '
        Me.txbPackageNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPackageNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPackageNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPackageNm.Location = New System.Drawing.Point(251, 20)
        Me.txbPackageNm.Name = "txbPackageNm"
        Me.txbPackageNm.PreviousQuery = Nothing
        Me.txbPackageNm.Queryable = True
        Me.txbPackageNm.QueryMandatory = False
        Me.txbPackageNm.ReadOnly = True
        Me.txbPackageNm.Size = New System.Drawing.Size(221, 20)
        Me.txbPackageNm.TabIndex = 115
        Me.txbPackageNm.Updateable = False
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 23)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(138, 23)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(79, 20)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txbCaseNo.TabIndex = 0
        Me.txbCaseNo.Updateable = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(731, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(354, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmManualApportionments"
        Me.bsCase.DataSource = Me.DsManualApportionments
        '
        'DsManualApportionments
        '
        Me.DsManualApportionments.DataSetName = "dsManualApportionments"
        Me.DsManualApportionments.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Query Case"
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(190, 19)
        Me.SuperLabel1.TabIndex = 5
        Me.SuperLabel1.Text = "Manual Apportionments"
        '
        'pnlCase
        '
        Me.pnlCase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCase.Controls.Add(Me.PictureBox6)
        Me.pnlCase.Controls.Add(Me.PictureBox5)
        Me.pnlCase.Controls.Add(Me.Panel4)
        Me.pnlCase.Controls.Add(Me.dgvCase)
        Me.pnlCase.Location = New System.Drawing.Point(0, 168)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(795, 124)
        Me.pnlCase.TabIndex = 9
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(732, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Case"
        '
        'dgvCase
        '
        Me.dgvCase.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvCase.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCase.AutoGenerateColumns = False
        Me.dgvCase.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvCase.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCase.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvCase.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCase.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCase.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSE_CASE_NO_CSE, Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.cboRentalTypeCd, Me.ClientPackageDataGridViewTextBoxColumn, Me.AddressDataGridViewTextBoxColumn})
        Me.dgvCase.DataSource = Me.bsCase
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCase.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvCase.EnableHeadersVisualStyles = False
        Me.dgvCase.Location = New System.Drawing.Point(8, 32)
        Me.dgvCase.Name = "dgvCase"
        Me.dgvCase.ReadOnly = True
        Me.dgvCase.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCase.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvCase.RowHeadersWidth = 18
        Me.dgvCase.RowTemplate.Height = 20
        Me.dgvCase.Size = New System.Drawing.Size(775, 80)
        Me.dgvCase.TabIndex = 76
        '
        'CSE_CASE_NO_CSE
        '
        Me.CSE_CASE_NO_CSE.DataPropertyName = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.HeaderText = "Case"
        Me.CSE_CASE_NO_CSE.Name = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.ReadOnly = True
        Me.CSE_CASE_NO_CSE.Width = 80
        '
        'CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'cboRentalTypeCd
        '
        Me.cboRentalTypeCd.DataPropertyName = "PROP_OR_PSN_CD"
        Me.cboRentalTypeCd.HeaderText = "Type"
        Me.cboRentalTypeCd.Name = "cboRentalTypeCd"
        Me.cboRentalTypeCd.ReadOnly = True
        Me.cboRentalTypeCd.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'ClientPackageDataGridViewTextBoxColumn
        '
        Me.ClientPackageDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ClientPackageDataGridViewTextBoxColumn.DataPropertyName = "ClientPackage"
        Me.ClientPackageDataGridViewTextBoxColumn.HeaderText = "Client Package"
        Me.ClientPackageDataGridViewTextBoxColumn.Name = "ClientPackageDataGridViewTextBoxColumn"
        Me.ClientPackageDataGridViewTextBoxColumn.ReadOnly = True
        '
        'AddressDataGridViewTextBoxColumn
        '
        Me.AddressDataGridViewTextBoxColumn.DataPropertyName = "Address"
        Me.AddressDataGridViewTextBoxColumn.HeaderText = "Address"
        Me.AddressDataGridViewTextBoxColumn.Name = "AddressDataGridViewTextBoxColumn"
        Me.AddressDataGridViewTextBoxColumn.ReadOnly = True
        Me.AddressDataGridViewTextBoxColumn.Width = 300
        '
        'pnlInvoice
        '
        Me.pnlInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInvoice.Controls.Add(Me.Panel2)
        Me.pnlInvoice.Controls.Add(Me.PictureBox3)
        Me.pnlInvoice.Controls.Add(Me.PictureBox4)
        Me.pnlInvoice.Controls.Add(Me.dgvInvoice)
        Me.pnlInvoice.Location = New System.Drawing.Point(0, 298)
        Me.pnlInvoice.Name = "pnlInvoice"
        Me.pnlInvoice.Size = New System.Drawing.Size(795, 229)
        Me.pnlInvoice.TabIndex = 10
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.lnkDeleteApportionment)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(738, 24)
        Me.Panel2.TabIndex = 1
        '
        'lnkDeleteApportionment
        '
        Me.lnkDeleteApportionment.ActiveLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkDeleteApportionment.AutoSize = True
        Me.lnkDeleteApportionment.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkDeleteApportionment.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkDeleteApportionment.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkDeleteApportionment.LinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkDeleteApportionment.Location = New System.Drawing.Point(535, 5)
        Me.lnkDeleteApportionment.Name = "lnkDeleteApportionment"
        Me.lnkDeleteApportionment.Size = New System.Drawing.Size(197, 14)
        Me.lnkDeleteApportionment.TabIndex = 144
        Me.lnkDeleteApportionment.TabStop = True
        Me.lnkDeleteApportionment.Text = "Delete Apportionments for Invoice"
        Me.lnkDeleteApportionment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkDeleteApportionment.VisitedLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 14)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Invoice"
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 80
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 79
        Me.PictureBox4.TabStop = False
        '
        'dgvInvoice
        '
        Me.dgvInvoice.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvInvoice.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvInvoice.AutoGenerateColumns = False
        Me.dgvInvoice.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvInvoice.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInvoice.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvInvoice.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoice.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvInvoice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PURCHASE_ORDER_NO, Me.INV_BUSINESS_NO_ORG, Me.INV_EXTERNAL_INV_NO_INV, Me.INV_INVOICE_DT_INV, Me.INVOICE_ITEM_AM, Me.ITEM_VAT_AM, Me.APPORTIONED_DT, Me.GENERATED_NO, Me.SPICASENOCSEDataGridViewTextBoxColumn1, Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1})
        Me.dgvInvoice.DataSource = Me.bsInvoice
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvInvoice.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvInvoice.EnableHeadersVisualStyles = False
        Me.dgvInvoice.Location = New System.Drawing.Point(8, 32)
        Me.dgvInvoice.Name = "dgvInvoice"
        Me.dgvInvoice.ReadOnly = True
        Me.dgvInvoice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoice.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvInvoice.RowHeadersWidth = 18
        Me.dgvInvoice.RowTemplate.Height = 20
        Me.dgvInvoice.Size = New System.Drawing.Size(775, 189)
        Me.dgvInvoice.TabIndex = 76
        '
        'PURCHASE_ORDER_NO
        '
        Me.PURCHASE_ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.PURCHASE_ORDER_NO.DataPropertyName = "PURCHASE_ORDER_NO"
        Me.PURCHASE_ORDER_NO.HeaderText = "Purchase Order No"
        Me.PURCHASE_ORDER_NO.Name = "PURCHASE_ORDER_NO"
        Me.PURCHASE_ORDER_NO.ReadOnly = True
        '
        'INV_BUSINESS_NO_ORG
        '
        Me.INV_BUSINESS_NO_ORG.DataPropertyName = "INV_BUSINESS_NO_ORG"
        Me.INV_BUSINESS_NO_ORG.HeaderText = "Supplier No"
        Me.INV_BUSINESS_NO_ORG.Name = "INV_BUSINESS_NO_ORG"
        Me.INV_BUSINESS_NO_ORG.ReadOnly = True
        Me.INV_BUSINESS_NO_ORG.Width = 80
        '
        'INV_EXTERNAL_INV_NO_INV
        '
        Me.INV_EXTERNAL_INV_NO_INV.DataPropertyName = "INV_EXTERNAL_INV_NO_INV"
        Me.INV_EXTERNAL_INV_NO_INV.HeaderText = "Invoice No"
        Me.INV_EXTERNAL_INV_NO_INV.Name = "INV_EXTERNAL_INV_NO_INV"
        Me.INV_EXTERNAL_INV_NO_INV.ReadOnly = True
        Me.INV_EXTERNAL_INV_NO_INV.Width = 120
        '
        'INV_INVOICE_DT_INV
        '
        Me.INV_INVOICE_DT_INV.DataPropertyName = "INV_INVOICE_DT_INV"
        Me.INV_INVOICE_DT_INV.HeaderText = "Invoice Date"
        Me.INV_INVOICE_DT_INV.Name = "INV_INVOICE_DT_INV"
        Me.INV_INVOICE_DT_INV.ReadOnly = True
        '
        'INVOICE_ITEM_AM
        '
        Me.INVOICE_ITEM_AM.DataPropertyName = "INVOICE_ITEM_AM"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.INVOICE_ITEM_AM.DefaultCellStyle = DataGridViewCellStyle7
        Me.INVOICE_ITEM_AM.HeaderText = "Item Amount"
        Me.INVOICE_ITEM_AM.Name = "INVOICE_ITEM_AM"
        Me.INVOICE_ITEM_AM.ReadOnly = True
        Me.INVOICE_ITEM_AM.Width = 85
        '
        'ITEM_VAT_AM
        '
        Me.ITEM_VAT_AM.DataPropertyName = "ITEM_VAT_AM"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.ITEM_VAT_AM.DefaultCellStyle = DataGridViewCellStyle8
        Me.ITEM_VAT_AM.HeaderText = "Item VAT"
        Me.ITEM_VAT_AM.Name = "ITEM_VAT_AM"
        Me.ITEM_VAT_AM.ReadOnly = True
        Me.ITEM_VAT_AM.Width = 80
        '
        'APPORTIONED_DT
        '
        Me.APPORTIONED_DT.DataPropertyName = "APPORTIONED_DT"
        Me.APPORTIONED_DT.HeaderText = "Apportioned Date"
        Me.APPORTIONED_DT.Name = "APPORTIONED_DT"
        Me.APPORTIONED_DT.ReadOnly = True
        Me.APPORTIONED_DT.Width = 110
        '
        'GENERATED_NO
        '
        Me.GENERATED_NO.DataPropertyName = "GENERATED_NO"
        Me.GENERATED_NO.HeaderText = "GENERATED_NO"
        Me.GENERATED_NO.Name = "GENERATED_NO"
        Me.GENERATED_NO.ReadOnly = True
        Me.GENERATED_NO.Visible = False
        '
        'SPICASENOCSEDataGridViewTextBoxColumn1
        '
        Me.SPICASENOCSEDataGridViewTextBoxColumn1.DataPropertyName = "SPI_CASE_NO_CSE"
        Me.SPICASENOCSEDataGridViewTextBoxColumn1.HeaderText = "SPI_CASE_NO_CSE"
        Me.SPICASENOCSEDataGridViewTextBoxColumn1.Name = "SPICASENOCSEDataGridViewTextBoxColumn1"
        Me.SPICASENOCSEDataGridViewTextBoxColumn1.ReadOnly = True
        Me.SPICASENOCSEDataGridViewTextBoxColumn1.Visible = False
        '
        'SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1
        '
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.DataPropertyName = "SPI_CLIENT_PACKAGE_ID_CGS"
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.HeaderText = "SPI_CLIENT_PACKAGE_ID_CGS"
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.Name = "SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1"
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.ReadOnly = True
        Me.SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.Visible = False
        '
        'bsInvoice
        '
        Me.bsInvoice.DataMember = "V_frmManualApportionments_V_fsubManualAppmntPurchase"
        Me.bsInvoice.DataSource = Me.bsCase
        '
        'pnlApportionment
        '
        Me.pnlApportionment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlApportionment.Controls.Add(Me.PictureBox7)
        Me.pnlApportionment.Controls.Add(Me.PictureBox8)
        Me.pnlApportionment.Controls.Add(Me.pnlCaseCapability)
        Me.pnlApportionment.Controls.Add(Me.dgvApportionment)
        Me.pnlApportionment.Location = New System.Drawing.Point(0, 533)
        Me.pnlApportionment.Name = "pnlApportionment"
        Me.pnlApportionment.Size = New System.Drawing.Size(795, 168)
        Me.pnlApportionment.TabIndex = 11
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 80
        Me.PictureBox7.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox8.TabIndex = 79
        Me.PictureBox8.TabStop = False
        '
        'pnlCaseCapability
        '
        Me.pnlCaseCapability.BackColor = System.Drawing.Color.Silver
        Me.pnlCaseCapability.Controls.Add(Me.lnlPackage)
        Me.pnlCaseCapability.Controls.Add(Me.chkDelayApportionment)
        Me.pnlCaseCapability.Controls.Add(Label24)
        Me.pnlCaseCapability.Controls.Add(Me.Label3)
        Me.pnlCaseCapability.Location = New System.Drawing.Point(33, 8)
        Me.pnlCaseCapability.Name = "pnlCaseCapability"
        Me.pnlCaseCapability.Size = New System.Drawing.Size(732, 24)
        Me.pnlCaseCapability.TabIndex = 1
        '
        'lnlPackage
        '
        Me.lnlPackage.ActiveLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlPackage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnlPackage.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlPackage.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnlPackage.LinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlPackage.Location = New System.Drawing.Point(435, 3)
        Me.lnlPackage.Name = "lnlPackage"
        Me.lnlPackage.Size = New System.Drawing.Size(67, 19)
        Me.lnlPackage.TabIndex = 143
        Me.lnlPackage.TabStop = True
        Me.lnlPackage.Text = "Package"
        Me.lnlPackage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnlPackage.VisitedLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        '
        'chkDelayApportionment
        '
        Me.chkDelayApportionment.AutoSize = True
        Me.chkDelayApportionment.Location = New System.Drawing.Point(704, 5)
        Me.chkDelayApportionment.Name = "chkDelayApportionment"
        Me.chkDelayApportionment.Size = New System.Drawing.Size(15, 14)
        Me.chkDelayApportionment.TabIndex = 127
        Me.chkDelayApportionment.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Apportionment"
        '
        'dgvApportionment
        '
        Me.dgvApportionment.AllowUserToDeleteRows = False
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvApportionment.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvApportionment.AutoGenerateColumns = False
        Me.dgvApportionment.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvApportionment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvApportionment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvApportionment.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvApportionment.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvApportionment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.INI_BUSINESS_NO_ORG, Me.INI_EXTERNAL_INV_NO_INV, Me.INI_INVOICE_DT_INV, Me.INI_GENERATED_NO_INI, Me.GENERATED_NO_IAP, Me.CPC_CASE_NO_CSE, Me.CPC_CLIENT_PACKAGE_ID_CGS, Me.CLIENT_PACKAGE_NM, Me.APPORTIONMENTDTDataGridViewTextBoxColumn, Me.APPORTIONED_NET_AM, Me.APPORTIONED_VAT_AM, Me.DATE_RANGE_DS, Me.BILLUPLOADDTDataGridViewTextBoxColumn})
        Me.dgvApportionment.DataSource = Me.bsApportionment
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvApportionment.DefaultCellStyle = DataGridViewCellStyle15
        Me.dgvApportionment.EnableHeadersVisualStyles = False
        Me.dgvApportionment.Location = New System.Drawing.Point(8, 32)
        Me.dgvApportionment.Name = "dgvApportionment"
        Me.dgvApportionment.ReadOnly = True
        Me.dgvApportionment.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvApportionment.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvApportionment.RowHeadersWidth = 18
        Me.dgvApportionment.RowTemplate.Height = 20
        Me.dgvApportionment.Size = New System.Drawing.Size(775, 123)
        Me.dgvApportionment.TabIndex = 76
        '
        'INI_BUSINESS_NO_ORG
        '
        Me.INI_BUSINESS_NO_ORG.DataPropertyName = "INI_BUSINESS_NO_ORG"
        Me.INI_BUSINESS_NO_ORG.HeaderText = "INI_BUSINESS_NO_ORG"
        Me.INI_BUSINESS_NO_ORG.Name = "INI_BUSINESS_NO_ORG"
        Me.INI_BUSINESS_NO_ORG.ReadOnly = True
        Me.INI_BUSINESS_NO_ORG.Visible = False
        '
        'INI_EXTERNAL_INV_NO_INV
        '
        Me.INI_EXTERNAL_INV_NO_INV.DataPropertyName = "INI_EXTERNAL_INV_NO_INV"
        Me.INI_EXTERNAL_INV_NO_INV.HeaderText = "INI_EXTERNAL_INV_NO_INV"
        Me.INI_EXTERNAL_INV_NO_INV.Name = "INI_EXTERNAL_INV_NO_INV"
        Me.INI_EXTERNAL_INV_NO_INV.ReadOnly = True
        Me.INI_EXTERNAL_INV_NO_INV.Visible = False
        '
        'INI_INVOICE_DT_INV
        '
        Me.INI_INVOICE_DT_INV.DataPropertyName = "INI_INVOICE_DT_INV"
        Me.INI_INVOICE_DT_INV.HeaderText = "INI_INVOICE_DT_INV"
        Me.INI_INVOICE_DT_INV.Name = "INI_INVOICE_DT_INV"
        Me.INI_INVOICE_DT_INV.ReadOnly = True
        Me.INI_INVOICE_DT_INV.Visible = False
        '
        'INI_GENERATED_NO_INI
        '
        Me.INI_GENERATED_NO_INI.DataPropertyName = "INI_GENERATED_NO_INI"
        Me.INI_GENERATED_NO_INI.HeaderText = "INI_GENERATED_NO_INI"
        Me.INI_GENERATED_NO_INI.Name = "INI_GENERATED_NO_INI"
        Me.INI_GENERATED_NO_INI.ReadOnly = True
        Me.INI_GENERATED_NO_INI.Visible = False
        '
        'GENERATED_NO_IAP
        '
        Me.GENERATED_NO_IAP.DataPropertyName = "GENERATED_NO"
        Me.GENERATED_NO_IAP.HeaderText = "GENERATED_NO"
        Me.GENERATED_NO_IAP.Name = "GENERATED_NO_IAP"
        Me.GENERATED_NO_IAP.ReadOnly = True
        Me.GENERATED_NO_IAP.Visible = False
        '
        'CPC_CASE_NO_CSE
        '
        Me.CPC_CASE_NO_CSE.DataPropertyName = "CPC_CASE_NO_CSE"
        Me.CPC_CASE_NO_CSE.HeaderText = "Case No"
        Me.CPC_CASE_NO_CSE.Name = "CPC_CASE_NO_CSE"
        Me.CPC_CASE_NO_CSE.ReadOnly = True
        Me.CPC_CASE_NO_CSE.Width = 60
        '
        'CPC_CLIENT_PACKAGE_ID_CGS
        '
        Me.CPC_CLIENT_PACKAGE_ID_CGS.DataPropertyName = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPC_CLIENT_PACKAGE_ID_CGS.HeaderText = "Package ID"
        Me.CPC_CLIENT_PACKAGE_ID_CGS.Name = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPC_CLIENT_PACKAGE_ID_CGS.ReadOnly = True
        Me.CPC_CLIENT_PACKAGE_ID_CGS.Width = 80
        '
        'CLIENT_PACKAGE_NM
        '
        Me.CLIENT_PACKAGE_NM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CLIENT_PACKAGE_NM.DataPropertyName = "CLIENT_PACKAGE_NM"
        Me.CLIENT_PACKAGE_NM.HeaderText = "Package"
        Me.CLIENT_PACKAGE_NM.Name = "CLIENT_PACKAGE_NM"
        Me.CLIENT_PACKAGE_NM.ReadOnly = True
        '
        'APPORTIONMENTDTDataGridViewTextBoxColumn
        '
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.DataPropertyName = "APPORTIONMENT_DT"
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.HeaderText = "Apportionment Dt"
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.Name = "APPORTIONMENTDTDataGridViewTextBoxColumn"
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.APPORTIONMENTDTDataGridViewTextBoxColumn.Width = 120
        '
        'APPORTIONED_NET_AM
        '
        Me.APPORTIONED_NET_AM.DataPropertyName = "APPORTIONED_NET_AM"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        Me.APPORTIONED_NET_AM.DefaultCellStyle = DataGridViewCellStyle13
        Me.APPORTIONED_NET_AM.HeaderText = "Net"
        Me.APPORTIONED_NET_AM.Name = "APPORTIONED_NET_AM"
        Me.APPORTIONED_NET_AM.ReadOnly = True
        Me.APPORTIONED_NET_AM.Width = 60
        '
        'APPORTIONED_VAT_AM
        '
        Me.APPORTIONED_VAT_AM.DataPropertyName = "APPORTIONED_VAT_AM"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        Me.APPORTIONED_VAT_AM.DefaultCellStyle = DataGridViewCellStyle14
        Me.APPORTIONED_VAT_AM.HeaderText = "VAT"
        Me.APPORTIONED_VAT_AM.Name = "APPORTIONED_VAT_AM"
        Me.APPORTIONED_VAT_AM.ReadOnly = True
        Me.APPORTIONED_VAT_AM.Width = 60
        '
        'DATE_RANGE_DS
        '
        Me.DATE_RANGE_DS.DataPropertyName = "DATE_RANGE_DS"
        Me.DATE_RANGE_DS.HeaderText = "Date Range"
        Me.DATE_RANGE_DS.Name = "DATE_RANGE_DS"
        Me.DATE_RANGE_DS.ReadOnly = True
        Me.DATE_RANGE_DS.Width = 160
        '
        'BILLUPLOADDTDataGridViewTextBoxColumn
        '
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.DataPropertyName = "BILL_UPLOAD_DT"
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.HeaderText = "BILL_UPLOAD_DT"
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.Name = "BILLUPLOADDTDataGridViewTextBoxColumn"
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.BILLUPLOADDTDataGridViewTextBoxColumn.Visible = False
        '
        'bsApportionment
        '
        Me.bsApportionment.DataMember = "V_fsubManualAppmntPurchase_V_fsubManualAppmntApportion"
        Me.bsApportionment.DataSource = Me.bsInvoice
        '
        'bsCaseCapability
        '
        Me.bsCaseCapability.DataMember = "V_frmManualApportionments_V_fsubMaintainRentalCseCapDetl"
        Me.bsCaseCapability.DataSource = Me.bsCase
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taInvoice
        '
        Me.taInvoice.ClearBeforeFill = True
        '
        'taApportionment
        '
        Me.taApportionment.ClearBeforeFill = True
        '
        'taCaseCapability
        '
        Me.taCaseCapability.ClearBeforeFill = True
        '
        'bsClientPackage
        '
        Me.bsClientPackage.DataMember = "CLIENT_PACKAGE"
        Me.bsClientPackage.DataSource = Me.DsManualApportionments
        '
        'taClientPackage
        '
        Me.taClientPackage.ClearBeforeFill = True
        '
        'frmManualApportionments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.pnlApportionment)
        Me.Controls.Add(Me.pnlInvoice)
        Me.Controls.Add(Me.pnlCase)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.KeyPreview = True
        Me.Name = "frmManualApportionments"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Manual Apportionments"
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlQueryCase.ResumeLayout(False)
        Me.pnlQueryCase.PerformLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsManualApportionments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCase.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvCase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInvoice.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlApportionment.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCaseCapability.ResumeLayout(False)
        Me.pnlCaseCapability.PerformLayout()
        CType(Me.dgvApportionment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsApportionment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCaseCapability, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsClientPackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlQueryCase As System.Windows.Forms.Panel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents pnlCase As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvCase As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInvoice As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents pnlApportionment As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlCaseCapability As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvApportionment As System.Windows.Forms.DataGridView
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents bsInvoice As System.Windows.Forms.BindingSource
    Friend WithEvents bsApportionment As System.Windows.Forms.BindingSource
    Friend WithEvents SuperLabel4 As ControlLibrary.SuperLabel
    Friend WithEvents txbAddress As ControlLibrary.SuperTextBox
    Friend WithEvents SuperLabel3 As ControlLibrary.SuperLabel
    Friend WithEvents txbPackageNm As ControlLibrary.SuperTextBox
    Friend WithEvents DsManualApportionments As clsRental.dsManualApportionments
    Friend WithEvents taCase As clsRental.dsManualApportionmentsTableAdapters.V_frmManualApportionmentsTableAdapter
    Friend WithEvents taInvoice As clsRental.dsManualApportionmentsTableAdapters.V_fsubManualAppmntPurchaseTableAdapter
    Friend WithEvents taApportionment As clsRental.dsManualApportionmentsTableAdapters.V_fsubManualAppmntApportionTableAdapter
    Friend WithEvents chkDelayApportionment As ControlLibrary.SuperCheckBox
    Friend WithEvents lnlPackage As System.Windows.Forms.LinkLabel
    Friend WithEvents bsCaseCapability As System.Windows.Forms.BindingSource
    Friend WithEvents taCaseCapability As clsRental.dsManualApportionmentsTableAdapters.V_fsubMaintainRentalCseCapDetlTableAdapter
    Friend WithEvents PURCHASE_ORDER_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INV_BUSINESS_NO_ORG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INV_EXTERNAL_INV_NO_INV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INV_INVOICE_DT_INV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICE_ITEM_AM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ITEM_VAT_AM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONED_DT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GENERATED_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPICASENOCSEDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPICLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bsClientPackage As System.Windows.Forms.BindingSource
    Friend WithEvents taClientPackage As clsRental.dsManualApportionmentsTableAdapters.CLIENT_PACKAGETableAdapter
    Private WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents lnkDeleteApportionment As System.Windows.Forms.LinkLabel
    Friend WithEvents INI_BUSINESS_NO_ORG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INI_EXTERNAL_INV_NO_INV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INI_INVOICE_DT_INV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INI_GENERATED_NO_INI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GENERATED_NO_IAP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPC_CASE_NO_CSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPC_CLIENT_PACKAGE_ID_CGS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLIENT_PACKAGE_NM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONMENTDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONED_NET_AM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONED_VAT_AM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATE_RANGE_DS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BILLUPLOADDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSE_CASE_NO_CSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboRentalTypeCd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClientPackageDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
