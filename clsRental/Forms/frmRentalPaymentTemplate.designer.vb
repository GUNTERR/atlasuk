<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRentalPaymentTemplate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label14 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRentalPaymentTemplate))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxPasteAddress = New System.Windows.Forms.PictureBox()
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.bsInstruction = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsRentalPaymentTemplate = New clsRental.dsRentalPaymentTemplate()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.lnlTAndP = New System.Windows.Forms.LinkLabel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlInstruction = New System.Windows.Forms.Panel()
        Me.txbBudgetAm = New System.Windows.Forms.TextBox()
        Me.cboRequestStatus = New ControlLibrary.SuperComboBox()
        Me.cboRentalCaseType = New ControlLibrary.SuperComboBox()
        Me.chkOngoing = New ControlLibrary.SuperCheckBox()
        Me.txbSupplierNo = New System.Windows.Forms.TextBox()
        Me.pbxDismissedDt = New System.Windows.Forms.PictureBox()
        Me.txbDismissedDt = New System.Windows.Forms.MaskedTextBox()
        Me.txbSupplierAddress = New System.Windows.Forms.TextBox()
        Me.txbDismissedDesc = New System.Windows.Forms.TextBox()
        Me.txbInstructionDt = New System.Windows.Forms.TextBox()
        Me.txbAddress = New System.Windows.Forms.TextBox()
        Me.txbSupplierNm = New System.Windows.Forms.TextBox()
        Me.cboDismissalReasonCd = New ControlLibrary.SuperComboBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.txbPackageNm = New System.Windows.Forms.TextBox()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.bsInstructionDetails = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.pnlRental = New System.Windows.Forms.Panel()
        Me.pbxRentEndDt = New System.Windows.Forms.PictureBox()
        Me.txbRentEndDt = New System.Windows.Forms.MaskedTextBox()
        Me.bsRental = New System.Windows.Forms.BindingSource(Me.components)
        Me.pbxRentStartDt = New System.Windows.Forms.PictureBox()
        Me.txbRentStartDt = New System.Windows.Forms.MaskedTextBox()
        Me.txbNoDaysCovered = New System.Windows.Forms.TextBox()
        Me.txbPayments = New System.Windows.Forms.TextBox()
        Me.txbPaymentsRemaining = New System.Windows.Forms.TextBox()
        Me.txbComments = New System.Windows.Forms.TextBox()
        Me.cboPaymentFrequency = New ControlLibrary.SuperComboBox()
        Me.cboRentalInstructionType = New ControlLibrary.SuperComboBox()
        Me.pbxFirstPaymentDt = New System.Windows.Forms.PictureBox()
        Me.txbFirstPaymentDt = New System.Windows.Forms.MaskedTextBox()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbAdd = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem1 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem1 = New System.Windows.Forms.ToolStripButton()
        Me.txbAgentRef = New System.Windows.Forms.TextBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.pnlInstructionDetails = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lnkCancel = New System.Windows.Forms.LinkLabel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.dgvInstructionDetail = New System.Windows.Forms.DataGridView()
        Me.SWRCASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INSTRUCTIONNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSV_SERVICE_CD_SVC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASE_ORDER_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtQuoteAm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WORKQUOTEAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WORKS_DESC_DS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WORKSRSNDSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CANCEL_DT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CASECOUNTQTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAXABLEAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAXABLEQAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EXEMPTAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EXPENSESTATUSCDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLAIMEDAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REJECTIONRSNDSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.taInstruction = New clsRental.dsRentalPaymentTemplateTableAdapters.V_frmRentalPaymentTemplateTableAdapter()
        Me.taInstructionDetails = New clsRental.dsRentalPaymentTemplateTableAdapters.V_fsubRentalPaymentTemplateSPITableAdapter()
        Me.taRental = New clsRental.dsRentalPaymentTemplateTableAdapters.V_fsubRentalPaymentTemplateRNITableAdapter()
        Label14 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        CType(Me.pbxPasteAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainToolStrip.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.bsInstruction, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsRentalPaymentTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInstruction.SuspendLayout()
        CType(Me.pbxDismissedDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsInstructionDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlRental.SuspendLayout()
        CType(Me.pbxRentEndDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRental, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxRentStartDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxFirstPaymentDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.pnlInstructionDetails.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvInstructionDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label14.Location = New System.Drawing.Point(25, 49)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(77, 14)
        Label14.TabIndex = 2
        Label14.Text = "Client Package"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label16.Location = New System.Drawing.Point(25, 122)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(46, 14)
        Label16.TabIndex = 118
        Label16.Text = "Supplier"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label18.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label18.Location = New System.Drawing.Point(548, 75)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(87, 14)
        Label18.TabIndex = 122
        Label18.Text = "Dismiss. Reason"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label25.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label25.Location = New System.Drawing.Point(318, 102)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(87, 14)
        Label25.TabIndex = 127
        Label25.Text = "Dismissed Desc."
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label26.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label26.Location = New System.Drawing.Point(564, 49)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(71, 14)
        Label26.TabIndex = 128
        Label26.Text = "Dismissed on"
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label27.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label27.Location = New System.Drawing.Point(318, 49)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(70, 14)
        Label27.TabIndex = 129
        Label27.Text = "Instructed on"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label28.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label28.Location = New System.Drawing.Point(318, 23)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(49, 14)
        Label28.TabIndex = 130
        Label28.Text = "Address"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label29.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label29.Location = New System.Drawing.Point(318, 148)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(90, 14)
        Label29.TabIndex = 137
        Label29.Text = "Supplier Address"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label9.Location = New System.Drawing.Point(25, 99)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(99, 14)
        Label9.TabIndex = 163
        Label9.Text = "Ongoing Required?"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label31.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label31.Location = New System.Drawing.Point(318, 12)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(97, 14)
        Label31.TabIndex = 43
        Label31.Text = "First Payment Date"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label30.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label30.Location = New System.Drawing.Point(25, 12)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(106, 14)
        Label30.TabIndex = 41
        Label30.Text = "Agent Ref / Address"
        Label30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label20.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label20.Location = New System.Drawing.Point(25, 75)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(91, 14)
        Label20.TabIndex = 182
        Label20.Text = "Rental Case Type"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label2.Location = New System.Drawing.Point(318, 75)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(81, 14)
        Label2.TabIndex = 184
        Label2.Text = "Request Status"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label3.Location = New System.Drawing.Point(25, 38)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(116, 14)
        Label3.TabIndex = 184
        Label3.Text = "Rental Instruction Type"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(25, 65)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(103, 14)
        Label4.TabIndex = 186
        Label4.Text = "Payment Frequency"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(25, 92)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(57, 14)
        Label5.TabIndex = 188
        Label5.Text = "Comments"
        Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label6.Location = New System.Drawing.Point(318, 65)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(106, 14)
        Label6.TabIndex = 190
        Label6.Text = "Payments Remaining"
        Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label7.Location = New System.Drawing.Point(318, 38)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(54, 14)
        Label7.TabIndex = 192
        Label7.Text = "Payments"
        Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label8.Location = New System.Drawing.Point(542, 65)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(129, 14)
        Label8.TabIndex = 194
        Label8.Text = "Number of Days Covered"
        Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label10.Location = New System.Drawing.Point(542, 12)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(113, 14)
        Label10.TabIndex = 196
        Label10.Text = "Rent Period Start Date"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label12.Location = New System.Drawing.Point(542, 38)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(108, 14)
        Label12.TabIndex = 199
        Label12.Text = "Rent Period End Date"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxPasteAddress
        '
        Me.pbxPasteAddress.Image = CType(resources.GetObject("pbxPasteAddress.Image"), System.Drawing.Image)
        Me.pbxPasteAddress.Location = New System.Drawing.Point(287, 12)
        Me.pbxPasteAddress.Name = "pbxPasteAddress"
        Me.pbxPasteAddress.Size = New System.Drawing.Size(15, 15)
        Me.pbxPasteAddress.TabIndex = 201
        Me.pbxPasteAddress.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxPasteAddress, "Paste Address")
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(215, 8)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.BindingSource = Me.bsInstruction
        Me.BindingNavigator1.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.BindingNavigator1.Location = New System.Drawing.Point(28, 153)
        Me.BindingNavigator1.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator1.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator1.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator1.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator1.Size = New System.Drawing.Size(169, 25)
        Me.BindingNavigator1.TabIndex = 35
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'bsInstruction
        '
        Me.bsInstruction.DataMember = "V_frmRentalPaymentTemplate"
        Me.bsInstruction.DataSource = Me.DsRentalPaymentTemplate
        '
        'DsRentalPaymentTemplate
        '
        Me.DsRentalPaymentTemplate.DataSetName = "dsRentalPaymentTemplate"
        Me.DsRentalPaymentTemplate.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(25, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlInstruction)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(796, 231)
        Me.Panel8.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.lnlTAndP)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(738, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(341, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'lnlTAndP
        '
        Me.lnlTAndP.ActiveLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlTAndP.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnlTAndP.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlTAndP.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnlTAndP.LinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnlTAndP.Location = New System.Drawing.Point(609, 3)
        Me.lnlTAndP.Name = "lnlTAndP"
        Me.lnlTAndP.Size = New System.Drawing.Size(129, 19)
        Me.lnlTAndP.TabIndex = 142
        Me.lnlTAndP.TabStop = True
        Me.lnlTAndP.Text = "Trouble and Praise"
        Me.lnlTAndP.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnlTAndP.VisitedLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(67, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Instruction"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(763, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlInstruction
        '
        Me.pnlInstruction.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlInstruction.Controls.Add(Me.pbxVIP)
        Me.pnlInstruction.Controls.Add(Me.txbBudgetAm)
        Me.pnlInstruction.Controls.Add(Label2)
        Me.pnlInstruction.Controls.Add(Me.cboRequestStatus)
        Me.pnlInstruction.Controls.Add(Label20)
        Me.pnlInstruction.Controls.Add(Me.cboRentalCaseType)
        Me.pnlInstruction.Controls.Add(Me.chkOngoing)
        Me.pnlInstruction.Controls.Add(Label9)
        Me.pnlInstruction.Controls.Add(Me.txbSupplierNo)
        Me.pnlInstruction.Controls.Add(Me.pbxDismissedDt)
        Me.pnlInstruction.Controls.Add(Me.txbDismissedDt)
        Me.pnlInstruction.Controls.Add(Label29)
        Me.pnlInstruction.Controls.Add(Me.txbSupplierAddress)
        Me.pnlInstruction.Controls.Add(Me.txbDismissedDesc)
        Me.pnlInstruction.Controls.Add(Me.txbInstructionDt)
        Me.pnlInstruction.Controls.Add(Me.txbAddress)
        Me.pnlInstruction.Controls.Add(Label28)
        Me.pnlInstruction.Controls.Add(Label27)
        Me.pnlInstruction.Controls.Add(Label26)
        Me.pnlInstruction.Controls.Add(Label25)
        Me.pnlInstruction.Controls.Add(Label18)
        Me.pnlInstruction.Controls.Add(Label16)
        Me.pnlInstruction.Controls.Add(Me.txbSupplierNm)
        Me.pnlInstruction.Controls.Add(Me.cboDismissalReasonCd)
        Me.pnlInstruction.Controls.Add(Me.SuperLabel2)
        Me.pnlInstruction.Controls.Add(Me.BindingNavigator1)
        Me.pnlInstruction.Controls.Add(Me.pbxCopy)
        Me.pnlInstruction.Controls.Add(Me.txbCaseNo)
        Me.pnlInstruction.Controls.Add(Label14)
        Me.pnlInstruction.Controls.Add(Me.txbPackageNm)
        Me.pnlInstruction.Location = New System.Drawing.Point(8, 32)
        Me.pnlInstruction.Name = "pnlInstruction"
        Me.pnlInstruction.Size = New System.Drawing.Size(780, 187)
        Me.pnlInstruction.TabIndex = 0
        '
        'txbBudgetAm
        '
        Me.txbBudgetAm.BackColor = System.Drawing.Color.Yellow
        Me.txbBudgetAm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "BUDGET_AM", True))
        Me.txbBudgetAm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbBudgetAm.ForeColor = System.Drawing.Color.DimGray
        Me.txbBudgetAm.Location = New System.Drawing.Point(190, 96)
        Me.txbBudgetAm.Name = "txbBudgetAm"
        Me.txbBudgetAm.ReadOnly = True
        Me.txbBudgetAm.Size = New System.Drawing.Size(50, 20)
        Me.txbBudgetAm.TabIndex = 185
        '
        'cboRequestStatus
        '
        Me.cboRequestStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboRequestStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRequestStatus.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsInstruction, "REQUEST_STATUS_CD", True))
        Me.cboRequestStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequestStatus.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRequestStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboRequestStatus.FormattingEnabled = True
        Me.cboRequestStatus.Location = New System.Drawing.Point(416, 72)
        Me.cboRequestStatus.Name = "cboRequestStatus"
        Me.cboRequestStatus.Queryable = False
        Me.cboRequestStatus.ReadOnly = True
        Me.cboRequestStatus.Size = New System.Drawing.Size(127, 22)
        Me.cboRequestStatus.TabIndex = 8
        Me.cboRequestStatus.Updateable = True
        '
        'cboRentalCaseType
        '
        Me.cboRentalCaseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboRentalCaseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRentalCaseType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsInstruction, "CaseType", True))
        Me.cboRentalCaseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRentalCaseType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRentalCaseType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboRentalCaseType.FormattingEnabled = True
        Me.cboRentalCaseType.Location = New System.Drawing.Point(135, 72)
        Me.cboRentalCaseType.Name = "cboRentalCaseType"
        Me.cboRentalCaseType.Queryable = False
        Me.cboRentalCaseType.ReadOnly = True
        Me.cboRentalCaseType.Size = New System.Drawing.Size(177, 22)
        Me.cboRentalCaseType.TabIndex = 2
        Me.cboRentalCaseType.Updateable = True
        '
        'chkOngoing
        '
        Me.chkOngoing.AutoSize = True
        Me.chkOngoing.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOngoing.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkOngoing.Location = New System.Drawing.Point(135, 99)
        Me.chkOngoing.Name = "chkOngoing"
        Me.chkOngoing.Size = New System.Drawing.Size(15, 14)
        Me.chkOngoing.TabIndex = 3
        Me.chkOngoing.UseVisualStyleBackColor = True
        '
        'txbSupplierNo
        '
        Me.txbSupplierNo.BackColor = System.Drawing.Color.Yellow
        Me.txbSupplierNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "SUP_BUSINESS_NO_ORG", True))
        Me.txbSupplierNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbSupplierNo.ForeColor = System.Drawing.Color.DimGray
        Me.txbSupplierNo.Location = New System.Drawing.Point(246, 96)
        Me.txbSupplierNo.Name = "txbSupplierNo"
        Me.txbSupplierNo.ReadOnly = True
        Me.txbSupplierNo.Size = New System.Drawing.Size(43, 20)
        Me.txbSupplierNo.TabIndex = 145
        '
        'pbxDismissedDt
        '
        Me.pbxDismissedDt.Image = CType(resources.GetObject("pbxDismissedDt.Image"), System.Drawing.Image)
        Me.pbxDismissedDt.Location = New System.Drawing.Point(711, 49)
        Me.pbxDismissedDt.Name = "pbxDismissedDt"
        Me.pbxDismissedDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxDismissedDt.TabIndex = 138
        Me.pbxDismissedDt.TabStop = False
        '
        'txbDismissedDt
        '
        Me.txbDismissedDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbDismissedDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "DISMISSED_DT", True))
        Me.txbDismissedDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDismissedDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbDismissedDt.Location = New System.Drawing.Point(639, 46)
        Me.txbDismissedDt.Mask = "00/00/0000"
        Me.txbDismissedDt.Name = "txbDismissedDt"
        Me.txbDismissedDt.Size = New System.Drawing.Size(65, 20)
        Me.txbDismissedDt.TabIndex = 7
        Me.txbDismissedDt.ValidatingType = GetType(Date)
        '
        'txbSupplierAddress
        '
        Me.txbSupplierAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbSupplierAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "SupplierAddress", True))
        Me.txbSupplierAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbSupplierAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbSupplierAddress.Location = New System.Drawing.Point(416, 145)
        Me.txbSupplierAddress.Name = "txbSupplierAddress"
        Me.txbSupplierAddress.ReadOnly = True
        Me.txbSupplierAddress.Size = New System.Drawing.Size(347, 20)
        Me.txbSupplierAddress.TabIndex = 11
        '
        'txbDismissedDesc
        '
        Me.txbDismissedDesc.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbDismissedDesc.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "DISMISSED_RSN_DS", True))
        Me.txbDismissedDesc.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDismissedDesc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbDismissedDesc.Location = New System.Drawing.Point(416, 99)
        Me.txbDismissedDesc.MaxLength = 100
        Me.txbDismissedDesc.Multiline = True
        Me.txbDismissedDesc.Name = "txbDismissedDesc"
        Me.txbDismissedDesc.ReadOnly = True
        Me.txbDismissedDesc.Size = New System.Drawing.Size(347, 40)
        Me.txbDismissedDesc.TabIndex = 10
        '
        'txbInstructionDt
        '
        Me.txbInstructionDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbInstructionDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "INSTRUCTION_DT", True))
        Me.txbInstructionDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbInstructionDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbInstructionDt.Location = New System.Drawing.Point(416, 46)
        Me.txbInstructionDt.Name = "txbInstructionDt"
        Me.txbInstructionDt.ReadOnly = True
        Me.txbInstructionDt.Size = New System.Drawing.Size(65, 20)
        Me.txbInstructionDt.TabIndex = 6
        '
        'txbAddress
        '
        Me.txbAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "Address", True))
        Me.txbAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAddress.Location = New System.Drawing.Point(416, 20)
        Me.txbAddress.Name = "txbAddress"
        Me.txbAddress.ReadOnly = True
        Me.txbAddress.Size = New System.Drawing.Size(347, 20)
        Me.txbAddress.TabIndex = 5
        '
        'txbSupplierNm
        '
        Me.txbSupplierNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbSupplierNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "Supplier", True))
        Me.txbSupplierNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbSupplierNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbSupplierNm.Location = New System.Drawing.Point(135, 119)
        Me.txbSupplierNm.Name = "txbSupplierNm"
        Me.txbSupplierNm.ReadOnly = True
        Me.txbSupplierNm.Size = New System.Drawing.Size(177, 20)
        Me.txbSupplierNm.TabIndex = 4
        '
        'cboDismissalReasonCd
        '
        Me.cboDismissalReasonCd.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboDismissalReasonCd.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDismissalReasonCd.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsInstruction, "DISMISSED_RSN_CD", True))
        Me.cboDismissalReasonCd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDismissalReasonCd.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDismissalReasonCd.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboDismissalReasonCd.FormattingEnabled = True
        Me.cboDismissalReasonCd.Location = New System.Drawing.Point(639, 72)
        Me.cboDismissalReasonCd.Name = "cboDismissalReasonCd"
        Me.cboDismissalReasonCd.Queryable = False
        Me.cboDismissalReasonCd.ReadOnly = True
        Me.cboDismissalReasonCd.Size = New System.Drawing.Size(124, 22)
        Me.cboDismissalReasonCd.TabIndex = 9
        Me.cboDismissalReasonCd.Updateable = True
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 23)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(194, 23)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "CSC_CASE_NO_CSE", True))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(135, 20)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txbCaseNo.TabIndex = 0
        Me.txbCaseNo.Updateable = False
        '
        'txbPackageNm
        '
        Me.txbPackageNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPackageNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInstruction, "CLIENT_PACKAGE_NM", True))
        Me.txbPackageNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPackageNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPackageNm.Location = New System.Drawing.Point(135, 46)
        Me.txbPackageNm.Name = "txbPackageNm"
        Me.txbPackageNm.ReadOnly = True
        Me.txbPackageNm.Size = New System.Drawing.Size(177, 20)
        Me.txbPackageNm.TabIndex = 1
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(129, 19)
        Me.SuperLabel1.TabIndex = 5
        Me.SuperLabel1.Text = "Rental Payment"
        '
        'bsInstructionDetails
        '
        Me.bsInstructionDetails.DataMember = "V_frmRentalPaymentTemplate_V_fsubRentalPaymentTemplateSPI"
        Me.bsInstructionDetails.DataSource = Me.bsInstruction
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.PictureBox3)
        Me.Panel3.Controls.Add(Me.PictureBox8)
        Me.Panel3.Controls.Add(Me.pnlRental)
        Me.Panel3.Controls.Add(Me.Panel7)
        Me.Panel3.Location = New System.Drawing.Point(0, 509)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(796, 192)
        Me.Panel3.TabIndex = 7
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(763, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 79
        Me.PictureBox3.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox8.TabIndex = 78
        Me.PictureBox8.TabStop = False
        '
        'pnlRental
        '
        Me.pnlRental.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlRental.Controls.Add(Me.pbxPasteAddress)
        Me.pnlRental.Controls.Add(Me.pbxRentEndDt)
        Me.pnlRental.Controls.Add(Me.txbRentEndDt)
        Me.pnlRental.Controls.Add(Label12)
        Me.pnlRental.Controls.Add(Me.pbxRentStartDt)
        Me.pnlRental.Controls.Add(Me.txbRentStartDt)
        Me.pnlRental.Controls.Add(Label10)
        Me.pnlRental.Controls.Add(Label8)
        Me.pnlRental.Controls.Add(Me.txbNoDaysCovered)
        Me.pnlRental.Controls.Add(Label7)
        Me.pnlRental.Controls.Add(Me.txbPayments)
        Me.pnlRental.Controls.Add(Label6)
        Me.pnlRental.Controls.Add(Me.txbPaymentsRemaining)
        Me.pnlRental.Controls.Add(Label5)
        Me.pnlRental.Controls.Add(Me.txbComments)
        Me.pnlRental.Controls.Add(Label4)
        Me.pnlRental.Controls.Add(Me.cboPaymentFrequency)
        Me.pnlRental.Controls.Add(Label3)
        Me.pnlRental.Controls.Add(Me.cboRentalInstructionType)
        Me.pnlRental.Controls.Add(Me.pbxFirstPaymentDt)
        Me.pnlRental.Controls.Add(Me.txbFirstPaymentDt)
        Me.pnlRental.Controls.Add(Me.BindingNavigator2)
        Me.pnlRental.Controls.Add(Label31)
        Me.pnlRental.Controls.Add(Label30)
        Me.pnlRental.Controls.Add(Me.txbAgentRef)
        Me.pnlRental.Location = New System.Drawing.Point(8, 32)
        Me.pnlRental.Name = "pnlRental"
        Me.pnlRental.Size = New System.Drawing.Size(780, 147)
        Me.pnlRental.TabIndex = 1
        '
        'pbxRentEndDt
        '
        Me.pbxRentEndDt.Image = CType(resources.GetObject("pbxRentEndDt.Image"), System.Drawing.Image)
        Me.pbxRentEndDt.Location = New System.Drawing.Point(745, 38)
        Me.pbxRentEndDt.Name = "pbxRentEndDt"
        Me.pbxRentEndDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxRentEndDt.TabIndex = 200
        Me.pbxRentEndDt.TabStop = False
        '
        'txbRentEndDt
        '
        Me.txbRentEndDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbRentEndDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "RENT_PERIOD_END_DT", True))
        Me.txbRentEndDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbRentEndDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbRentEndDt.Location = New System.Drawing.Point(674, 35)
        Me.txbRentEndDt.Mask = "00/00/0000"
        Me.txbRentEndDt.Name = "txbRentEndDt"
        Me.txbRentEndDt.Size = New System.Drawing.Size(65, 20)
        Me.txbRentEndDt.TabIndex = 8
        Me.txbRentEndDt.ValidatingType = GetType(Date)
        '
        'bsRental
        '
        Me.bsRental.DataMember = "V_fsubRentalPaymentTemplateSPI_V_fsubRentalPaymentTemplateRNI"
        Me.bsRental.DataSource = Me.bsInstructionDetails
        '
        'pbxRentStartDt
        '
        Me.pbxRentStartDt.Image = CType(resources.GetObject("pbxRentStartDt.Image"), System.Drawing.Image)
        Me.pbxRentStartDt.Location = New System.Drawing.Point(745, 12)
        Me.pbxRentStartDt.Name = "pbxRentStartDt"
        Me.pbxRentStartDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxRentStartDt.TabIndex = 197
        Me.pbxRentStartDt.TabStop = False
        '
        'txbRentStartDt
        '
        Me.txbRentStartDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbRentStartDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "RENT_PERIOD_START_DT", True))
        Me.txbRentStartDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbRentStartDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbRentStartDt.Location = New System.Drawing.Point(674, 9)
        Me.txbRentStartDt.Mask = "00/00/0000"
        Me.txbRentStartDt.Name = "txbRentStartDt"
        Me.txbRentStartDt.Size = New System.Drawing.Size(65, 20)
        Me.txbRentStartDt.TabIndex = 7
        Me.txbRentStartDt.ValidatingType = GetType(Date)
        '
        'txbNoDaysCovered
        '
        Me.txbNoDaysCovered.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbNoDaysCovered.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "RENT_PERIOD_DYS_QT", True))
        Me.txbNoDaysCovered.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbNoDaysCovered.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbNoDaysCovered.Location = New System.Drawing.Point(674, 62)
        Me.txbNoDaysCovered.Name = "txbNoDaysCovered"
        Me.txbNoDaysCovered.ReadOnly = True
        Me.txbNoDaysCovered.Size = New System.Drawing.Size(39, 20)
        Me.txbNoDaysCovered.TabIndex = 9
        '
        'txbPayments
        '
        Me.txbPayments.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPayments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "PAYMENTS_QT", True))
        Me.txbPayments.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPayments.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPayments.Location = New System.Drawing.Point(430, 35)
        Me.txbPayments.Name = "txbPayments"
        Me.txbPayments.ReadOnly = True
        Me.txbPayments.Size = New System.Drawing.Size(39, 20)
        Me.txbPayments.TabIndex = 5
        '
        'txbPaymentsRemaining
        '
        Me.txbPaymentsRemaining.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPaymentsRemaining.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "PAYMENTS_REMAINING_QT", True))
        Me.txbPaymentsRemaining.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPaymentsRemaining.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPaymentsRemaining.Location = New System.Drawing.Point(430, 62)
        Me.txbPaymentsRemaining.Name = "txbPaymentsRemaining"
        Me.txbPaymentsRemaining.ReadOnly = True
        Me.txbPaymentsRemaining.Size = New System.Drawing.Size(39, 20)
        Me.txbPaymentsRemaining.TabIndex = 6
        '
        'txbComments
        '
        Me.txbComments.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "COMMENT_DS", True))
        Me.txbComments.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbComments.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbComments.Location = New System.Drawing.Point(148, 89)
        Me.txbComments.Name = "txbComments"
        Me.txbComments.ReadOnly = True
        Me.txbComments.Size = New System.Drawing.Size(615, 20)
        Me.txbComments.TabIndex = 3
        '
        'cboPaymentFrequency
        '
        Me.cboPaymentFrequency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboPaymentFrequency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPaymentFrequency.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsRental, "PAYMENT_FREQUENCY_CD", True))
        Me.cboPaymentFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentFrequency.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentFrequency.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboPaymentFrequency.FormattingEnabled = True
        Me.cboPaymentFrequency.Location = New System.Drawing.Point(148, 62)
        Me.cboPaymentFrequency.Name = "cboPaymentFrequency"
        Me.cboPaymentFrequency.Queryable = False
        Me.cboPaymentFrequency.ReadOnly = True
        Me.cboPaymentFrequency.Size = New System.Drawing.Size(154, 22)
        Me.cboPaymentFrequency.TabIndex = 2
        Me.cboPaymentFrequency.Updateable = True
        '
        'cboRentalInstructionType
        '
        Me.cboRentalInstructionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboRentalInstructionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRentalInstructionType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsRental, "ONEOFF_TYPE_CD", True))
        Me.cboRentalInstructionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRentalInstructionType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRentalInstructionType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboRentalInstructionType.FormattingEnabled = True
        Me.cboRentalInstructionType.Location = New System.Drawing.Point(148, 35)
        Me.cboRentalInstructionType.Name = "cboRentalInstructionType"
        Me.cboRentalInstructionType.Queryable = False
        Me.cboRentalInstructionType.ReadOnly = True
        Me.cboRentalInstructionType.Size = New System.Drawing.Size(154, 22)
        Me.cboRentalInstructionType.TabIndex = 1
        Me.cboRentalInstructionType.Updateable = True
        '
        'pbxFirstPaymentDt
        '
        Me.pbxFirstPaymentDt.Image = CType(resources.GetObject("pbxFirstPaymentDt.Image"), System.Drawing.Image)
        Me.pbxFirstPaymentDt.Location = New System.Drawing.Point(501, 12)
        Me.pbxFirstPaymentDt.Name = "pbxFirstPaymentDt"
        Me.pbxFirstPaymentDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxFirstPaymentDt.TabIndex = 139
        Me.pbxFirstPaymentDt.TabStop = False
        '
        'txbFirstPaymentDt
        '
        Me.txbFirstPaymentDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbFirstPaymentDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "FIRST_PAYMENT_DT", True))
        Me.txbFirstPaymentDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbFirstPaymentDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbFirstPaymentDt.Location = New System.Drawing.Point(430, 9)
        Me.txbFirstPaymentDt.Mask = "00/00/0000"
        Me.txbFirstPaymentDt.Name = "txbFirstPaymentDt"
        Me.txbFirstPaymentDt.Size = New System.Drawing.Size(65, 20)
        Me.txbFirstPaymentDt.TabIndex = 4
        Me.txbFirstPaymentDt.ValidatingType = GetType(Date)
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Me.tsbAdd
        Me.BindingNavigator2.BindingSource = Me.bsRental
        Me.BindingNavigator2.CountItem = Me.BindingNavigatorCountItem1
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem1, Me.BindingNavigatorMovePreviousItem1, Me.BindingNavigatorSeparator3, Me.BindingNavigatorPositionItem1, Me.BindingNavigatorCountItem1, Me.BindingNavigatorSeparator4, Me.BindingNavigatorMoveNextItem1, Me.BindingNavigatorMoveLastItem1, Me.tsbAdd})
        Me.BindingNavigator2.Location = New System.Drawing.Point(11, 112)
        Me.BindingNavigator2.MoveFirstItem = Me.BindingNavigatorMoveFirstItem1
        Me.BindingNavigator2.MoveLastItem = Me.BindingNavigatorMoveLastItem1
        Me.BindingNavigator2.MoveNextItem = Me.BindingNavigatorMoveNextItem1
        Me.BindingNavigator2.MovePreviousItem = Me.BindingNavigatorMovePreviousItem1
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Me.BindingNavigatorPositionItem1
        Me.BindingNavigator2.Size = New System.Drawing.Size(192, 25)
        Me.BindingNavigator2.TabIndex = 50
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'tsbAdd
        '
        Me.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAdd.Image = CType(resources.GetObject("tsbAdd.Image"), System.Drawing.Image)
        Me.tsbAdd.Name = "tsbAdd"
        Me.tsbAdd.RightToLeftAutoMirrorImage = True
        Me.tsbAdd.Size = New System.Drawing.Size(23, 22)
        Me.tsbAdd.Text = "Add new"
        '
        'BindingNavigatorCountItem1
        '
        Me.BindingNavigatorCountItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem1.Name = "BindingNavigatorCountItem1"
        Me.BindingNavigatorCountItem1.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem1.Text = "of {0}"
        Me.BindingNavigatorCountItem1.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem1
        '
        Me.BindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem1.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem1.Name = "BindingNavigatorMoveFirstItem1"
        Me.BindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem1.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem1
        '
        Me.BindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem1.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem1.Name = "BindingNavigatorMovePreviousItem1"
        Me.BindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem1.Text = "Move previous"
        '
        'BindingNavigatorSeparator3
        '
        Me.BindingNavigatorSeparator3.Name = "BindingNavigatorSeparator3"
        Me.BindingNavigatorSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem1
        '
        Me.BindingNavigatorPositionItem1.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem1.AutoSize = False
        Me.BindingNavigatorPositionItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem1.Name = "BindingNavigatorPositionItem1"
        Me.BindingNavigatorPositionItem1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigatorPositionItem1.Size = New System.Drawing.Size(25, 21)
        Me.BindingNavigatorPositionItem1.Text = "0"
        Me.BindingNavigatorPositionItem1.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator4
        '
        Me.BindingNavigatorSeparator4.Name = "BindingNavigatorSeparator4"
        Me.BindingNavigatorSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem1
        '
        Me.BindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem1.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem1.Name = "BindingNavigatorMoveNextItem1"
        Me.BindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem1.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem1
        '
        Me.BindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem1.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem1.Name = "BindingNavigatorMoveLastItem1"
        Me.BindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem1.Text = "Move last"
        '
        'txbAgentRef
        '
        Me.txbAgentRef.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbAgentRef.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsRental, "EXTERNAL_INVOICE_NO", True))
        Me.txbAgentRef.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbAgentRef.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbAgentRef.Location = New System.Drawing.Point(148, 9)
        Me.txbAgentRef.Name = "txbAgentRef"
        Me.txbAgentRef.ReadOnly = True
        Me.txbAgentRef.Size = New System.Drawing.Size(133, 20)
        Me.txbAgentRef.TabIndex = 0
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Silver
        Me.Panel7.Controls.Add(Me.Label11)
        Me.Panel7.Location = New System.Drawing.Point(33, 8)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(734, 24)
        Me.Panel7.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(0, 5)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 14)
        Me.Label11.TabIndex = 73
        Me.Label11.Text = "Rental Parameters"
        '
        'pnlInstructionDetails
        '
        Me.pnlInstructionDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInstructionDetails.Controls.Add(Me.Panel4)
        Me.pnlInstructionDetails.Controls.Add(Me.PictureBox6)
        Me.pnlInstructionDetails.Controls.Add(Me.PictureBox5)
        Me.pnlInstructionDetails.Controls.Add(Me.dgvInstructionDetail)
        Me.pnlInstructionDetails.Location = New System.Drawing.Point(0, 303)
        Me.pnlInstructionDetails.Name = "pnlInstructionDetails"
        Me.pnlInstructionDetails.Size = New System.Drawing.Size(795, 200)
        Me.pnlInstructionDetails.TabIndex = 9
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.lnkCancel)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(738, 24)
        Me.Panel4.TabIndex = 1
        '
        'lnkCancel
        '
        Me.lnkCancel.ActiveLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkCancel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkCancel.LinkArea = New System.Windows.Forms.LinkArea(0, 23)
        Me.lnkCancel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkCancel.LinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkCancel.Location = New System.Drawing.Point(578, 3)
        Me.lnkCancel.Name = "lnkCancel"
        Me.lnkCancel.Size = New System.Drawing.Size(156, 19)
        Me.lnkCancel.TabIndex = 143
        Me.lnkCancel.TabStop = True
        Me.lnkCancel.Text = "Cancel Instruction Line"
        Me.lnkCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkCancel.VisitedLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Instruction Details"
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'dgvInstructionDetail
        '
        Me.dgvInstructionDetail.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvInstructionDetail.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvInstructionDetail.AutoGenerateColumns = False
        Me.dgvInstructionDetail.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvInstructionDetail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInstructionDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvInstructionDetail.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInstructionDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvInstructionDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SWRCASENOCSEDataGridViewTextBoxColumn, Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn, Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn, Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn, Me.INSTRUCTIONNODataGridViewTextBoxColumn, Me.SSV_SERVICE_CD_SVC, Me.PURCHASE_ORDER_NO, Me.txtQuoteAm, Me.WORKQUOTEAMDataGridViewTextBoxColumn, Me.WORKS_DESC_DS, Me.WORKSRSNDSDataGridViewTextBoxColumn, Me.CANCEL_DT, Me.CASECOUNTQTDataGridViewTextBoxColumn, Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn, Me.TAXABLEAMDataGridViewTextBoxColumn, Me.TAXABLEQAMDataGridViewTextBoxColumn, Me.EXEMPTAMDataGridViewTextBoxColumn, Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn, Me.EXPENSESTATUSCDDataGridViewTextBoxColumn, Me.CLAIMEDAMDataGridViewTextBoxColumn, Me.REJECTIONRSNDSDataGridViewTextBoxColumn})
        Me.dgvInstructionDetail.DataSource = Me.bsInstructionDetails
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvInstructionDetail.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvInstructionDetail.EnableHeadersVisualStyles = False
        Me.dgvInstructionDetail.Location = New System.Drawing.Point(8, 32)
        Me.dgvInstructionDetail.Name = "dgvInstructionDetail"
        Me.dgvInstructionDetail.ReadOnly = True
        Me.dgvInstructionDetail.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInstructionDetail.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvInstructionDetail.RowHeadersWidth = 18
        Me.dgvInstructionDetail.RowTemplate.Height = 20
        Me.dgvInstructionDetail.Size = New System.Drawing.Size(775, 157)
        Me.dgvInstructionDetail.TabIndex = 76
        '
        'SWRCASENOCSEDataGridViewTextBoxColumn
        '
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "SWR_CASE_NO_CSE"
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.HeaderText = "SWR_CASE_NO_CSE"
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.Name = "SWRCASENOCSEDataGridViewTextBoxColumn"
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.Visible = False
        '
        'SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "SWR_CLIENT_PACKAGE_ID_CGS"
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "SWR_CLIENT_PACKAGE_ID_CGS"
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn
        '
        Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn.DataPropertyName = "SWR_ADDR_AST_TYPE_CD_CCP"
        Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn.HeaderText = "SWR_ADDR_AST_TYPE_CD_CCP"
        Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn.Name = "SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn"
        Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn.Visible = False
        '
        'SWRGENERATEDNOCCPDataGridViewTextBoxColumn
        '
        Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn.DataPropertyName = "SWR_GENERATED_NO_CCP"
        Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn.HeaderText = "SWR_GENERATED_NO_CCP"
        Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn.Name = "SWRGENERATEDNOCCPDataGridViewTextBoxColumn"
        Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRGENERATEDNOCCPDataGridViewTextBoxColumn.Visible = False
        '
        'SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn
        '
        Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn.DataPropertyName = "SWR_WORK_REQUEST_NO_SWR"
        Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn.HeaderText = "SWR_WORK_REQUEST_NO_SWR"
        Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn.Name = "SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn"
        Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn.Visible = False
        '
        'INSTRUCTIONNODataGridViewTextBoxColumn
        '
        Me.INSTRUCTIONNODataGridViewTextBoxColumn.DataPropertyName = "INSTRUCTION_NO"
        Me.INSTRUCTIONNODataGridViewTextBoxColumn.HeaderText = "INSTRUCTION_NO"
        Me.INSTRUCTIONNODataGridViewTextBoxColumn.Name = "INSTRUCTIONNODataGridViewTextBoxColumn"
        Me.INSTRUCTIONNODataGridViewTextBoxColumn.ReadOnly = True
        Me.INSTRUCTIONNODataGridViewTextBoxColumn.Visible = False
        '
        'SSV_SERVICE_CD_SVC
        '
        Me.SSV_SERVICE_CD_SVC.DataPropertyName = "SSV_SERVICE_CD_SVC"
        Me.SSV_SERVICE_CD_SVC.HeaderText = "Service"
        Me.SSV_SERVICE_CD_SVC.Name = "SSV_SERVICE_CD_SVC"
        Me.SSV_SERVICE_CD_SVC.ReadOnly = True
        Me.SSV_SERVICE_CD_SVC.Width = 60
        '
        'PURCHASE_ORDER_NO
        '
        Me.PURCHASE_ORDER_NO.DataPropertyName = "PURCHASE_ORDER_NO"
        Me.PURCHASE_ORDER_NO.HeaderText = "PO Number"
        Me.PURCHASE_ORDER_NO.Name = "PURCHASE_ORDER_NO"
        Me.PURCHASE_ORDER_NO.ReadOnly = True
        Me.PURCHASE_ORDER_NO.Width = 160
        '
        'txtQuoteAm
        '
        Me.txtQuoteAm.DataPropertyName = "QUOTE_AM"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.txtQuoteAm.DefaultCellStyle = DataGridViewCellStyle3
        Me.txtQuoteAm.HeaderText = "PO Amount"
        Me.txtQuoteAm.Name = "txtQuoteAm"
        Me.txtQuoteAm.ReadOnly = True
        Me.txtQuoteAm.Width = 80
        '
        'WORKQUOTEAMDataGridViewTextBoxColumn
        '
        Me.WORKQUOTEAMDataGridViewTextBoxColumn.DataPropertyName = "WORK_QUOTE_AM"
        Me.WORKQUOTEAMDataGridViewTextBoxColumn.HeaderText = "WORK_QUOTE_AM"
        Me.WORKQUOTEAMDataGridViewTextBoxColumn.Name = "WORKQUOTEAMDataGridViewTextBoxColumn"
        Me.WORKQUOTEAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.WORKQUOTEAMDataGridViewTextBoxColumn.Visible = False
        '
        'WORKS_DESC_DS
        '
        Me.WORKS_DESC_DS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.WORKS_DESC_DS.DataPropertyName = "WORKS_DESC_DS"
        Me.WORKS_DESC_DS.HeaderText = "Description of Works"
        Me.WORKS_DESC_DS.Name = "WORKS_DESC_DS"
        Me.WORKS_DESC_DS.ReadOnly = True
        '
        'WORKSRSNDSDataGridViewTextBoxColumn
        '
        Me.WORKSRSNDSDataGridViewTextBoxColumn.DataPropertyName = "WORKS_RSN_DS"
        Me.WORKSRSNDSDataGridViewTextBoxColumn.HeaderText = "WORKS_RSN_DS"
        Me.WORKSRSNDSDataGridViewTextBoxColumn.Name = "WORKSRSNDSDataGridViewTextBoxColumn"
        Me.WORKSRSNDSDataGridViewTextBoxColumn.ReadOnly = True
        Me.WORKSRSNDSDataGridViewTextBoxColumn.Visible = False
        '
        'CANCEL_DT
        '
        Me.CANCEL_DT.DataPropertyName = "CANCEL_DT"
        Me.CANCEL_DT.HeaderText = "Cancel Date"
        Me.CANCEL_DT.Name = "CANCEL_DT"
        Me.CANCEL_DT.ReadOnly = True
        Me.CANCEL_DT.Width = 90
        '
        'CASECOUNTQTDataGridViewTextBoxColumn
        '
        Me.CASECOUNTQTDataGridViewTextBoxColumn.DataPropertyName = "CASE_COUNT_QT"
        Me.CASECOUNTQTDataGridViewTextBoxColumn.HeaderText = "CASE_COUNT_QT"
        Me.CASECOUNTQTDataGridViewTextBoxColumn.Name = "CASECOUNTQTDataGridViewTextBoxColumn"
        Me.CASECOUNTQTDataGridViewTextBoxColumn.ReadOnly = True
        Me.CASECOUNTQTDataGridViewTextBoxColumn.Visible = False
        '
        'INVLATESTSTATUSCDINVDataGridViewTextBoxColumn
        '
        Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn.DataPropertyName = "INV_LATEST_STATUS_CD_INV"
        Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn.HeaderText = "INV_LATEST_STATUS_CD_INV"
        Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn.Name = "INVLATESTSTATUSCDINVDataGridViewTextBoxColumn"
        Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn.ReadOnly = True
        Me.INVLATESTSTATUSCDINVDataGridViewTextBoxColumn.Visible = False
        '
        'TAXABLEAMDataGridViewTextBoxColumn
        '
        Me.TAXABLEAMDataGridViewTextBoxColumn.DataPropertyName = "TAXABLE_AM"
        Me.TAXABLEAMDataGridViewTextBoxColumn.HeaderText = "TAXABLE_AM"
        Me.TAXABLEAMDataGridViewTextBoxColumn.Name = "TAXABLEAMDataGridViewTextBoxColumn"
        Me.TAXABLEAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.TAXABLEAMDataGridViewTextBoxColumn.Visible = False
        '
        'TAXABLEQAMDataGridViewTextBoxColumn
        '
        Me.TAXABLEQAMDataGridViewTextBoxColumn.DataPropertyName = "TAXABLE_Q_AM"
        Me.TAXABLEQAMDataGridViewTextBoxColumn.HeaderText = "TAXABLE_Q_AM"
        Me.TAXABLEQAMDataGridViewTextBoxColumn.Name = "TAXABLEQAMDataGridViewTextBoxColumn"
        Me.TAXABLEQAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.TAXABLEQAMDataGridViewTextBoxColumn.Visible = False
        '
        'EXEMPTAMDataGridViewTextBoxColumn
        '
        Me.EXEMPTAMDataGridViewTextBoxColumn.DataPropertyName = "EXEMPT_AM"
        Me.EXEMPTAMDataGridViewTextBoxColumn.HeaderText = "EXEMPT_AM"
        Me.EXEMPTAMDataGridViewTextBoxColumn.Name = "EXEMPTAMDataGridViewTextBoxColumn"
        Me.EXEMPTAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.EXEMPTAMDataGridViewTextBoxColumn.Visible = False
        '
        'EXPENSEINCURREDDTDataGridViewTextBoxColumn
        '
        Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn.DataPropertyName = "EXPENSE_INCURRED_DT"
        Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn.HeaderText = "EXPENSE_INCURRED_DT"
        Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn.Name = "EXPENSEINCURREDDTDataGridViewTextBoxColumn"
        Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.EXPENSEINCURREDDTDataGridViewTextBoxColumn.Visible = False
        '
        'EXPENSESTATUSCDDataGridViewTextBoxColumn
        '
        Me.EXPENSESTATUSCDDataGridViewTextBoxColumn.DataPropertyName = "EXPENSE_STATUS_CD"
        Me.EXPENSESTATUSCDDataGridViewTextBoxColumn.HeaderText = "EXPENSE_STATUS_CD"
        Me.EXPENSESTATUSCDDataGridViewTextBoxColumn.Name = "EXPENSESTATUSCDDataGridViewTextBoxColumn"
        Me.EXPENSESTATUSCDDataGridViewTextBoxColumn.ReadOnly = True
        Me.EXPENSESTATUSCDDataGridViewTextBoxColumn.Visible = False
        '
        'CLAIMEDAMDataGridViewTextBoxColumn
        '
        Me.CLAIMEDAMDataGridViewTextBoxColumn.DataPropertyName = "CLAIMED_AM"
        Me.CLAIMEDAMDataGridViewTextBoxColumn.HeaderText = "CLAIMED_AM"
        Me.CLAIMEDAMDataGridViewTextBoxColumn.Name = "CLAIMEDAMDataGridViewTextBoxColumn"
        Me.CLAIMEDAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.CLAIMEDAMDataGridViewTextBoxColumn.Visible = False
        '
        'REJECTIONRSNDSDataGridViewTextBoxColumn
        '
        Me.REJECTIONRSNDSDataGridViewTextBoxColumn.DataPropertyName = "REJECTION_RSN_DS"
        Me.REJECTIONRSNDSDataGridViewTextBoxColumn.HeaderText = "REJECTION_RSN_DS"
        Me.REJECTIONRSNDSDataGridViewTextBoxColumn.Name = "REJECTIONRSNDSDataGridViewTextBoxColumn"
        Me.REJECTIONRSNDSDataGridViewTextBoxColumn.ReadOnly = True
        Me.REJECTIONRSNDSDataGridViewTextBoxColumn.Visible = False
        '
        'taInstruction
        '
        Me.taInstruction.ClearBeforeFill = True
        '
        'taInstructionDetails
        '
        Me.taInstructionDetails.ClearBeforeFill = True
        '
        'taRental
        '
        Me.taRental.ClearBeforeFill = True
        '
        'frmRentalPaymentTemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.pnlInstructionDetails)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.KeyPreview = True
        Me.Name = "frmRentalPaymentTemplate"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Rental Payment"
        CType(Me.pbxPasteAddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.bsInstruction, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsRentalPaymentTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInstruction.ResumeLayout(False)
        Me.pnlInstruction.PerformLayout()
        CType(Me.pbxDismissedDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsInstructionDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlRental.ResumeLayout(False)
        Me.pnlRental.PerformLayout()
        CType(Me.pbxRentEndDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRental, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxRentStartDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxFirstPaymentDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.pnlInstructionDetails.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvInstructionDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlInstruction As System.Windows.Forms.Panel
    Friend WithEvents cboDismissalReasonCd As ControlLibrary.SuperComboBox
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents txbPackageNm As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents bsInstruction As System.Windows.Forms.BindingSource
    Friend WithEvents bsInstructionDetails As System.Windows.Forms.BindingSource
    Friend WithEvents txbSupplierNm As System.Windows.Forms.TextBox
    Friend WithEvents txbDismissedDesc As System.Windows.Forms.TextBox
    Friend WithEvents txbInstructionDt As System.Windows.Forms.TextBox
    Friend WithEvents txbAddress As System.Windows.Forms.TextBox
    Friend WithEvents txbSupplierAddress As System.Windows.Forms.TextBox
    Friend WithEvents txbDismissedDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxDismissedDt As System.Windows.Forms.PictureBox
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents txbSupplierNo As System.Windows.Forms.TextBox
    Friend WithEvents lnlTAndP As System.Windows.Forms.LinkLabel
    Friend WithEvents chkOngoing As ControlLibrary.SuperCheckBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlRental As System.Windows.Forms.Panel
    Friend WithEvents pbxFirstPaymentDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbFirstPaymentDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents txbAgentRef As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboRentalCaseType As ControlLibrary.SuperComboBox
    Friend WithEvents bsRental As System.Windows.Forms.BindingSource
    Friend WithEvents cboRequestStatus As ControlLibrary.SuperComboBox
    Friend WithEvents pnlInstructionDetails As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvInstructionDetail As System.Windows.Forms.DataGridView
    Friend WithEvents cboPaymentFrequency As ControlLibrary.SuperComboBox
    Friend WithEvents cboRentalInstructionType As ControlLibrary.SuperComboBox
    Friend WithEvents txbComments As System.Windows.Forms.TextBox
    Friend WithEvents txbPaymentsRemaining As System.Windows.Forms.TextBox
    Friend WithEvents pbxRentStartDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbRentStartDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txbNoDaysCovered As System.Windows.Forms.TextBox
    Friend WithEvents txbPayments As System.Windows.Forms.TextBox
    Friend WithEvents pbxRentEndDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbRentEndDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxPasteAddress As System.Windows.Forms.PictureBox
    Friend WithEvents DsRentalPaymentTemplate As clsRental.dsRentalPaymentTemplate
    Friend WithEvents taInstruction As clsRental.dsRentalPaymentTemplateTableAdapters.V_frmRentalPaymentTemplateTableAdapter
    Friend WithEvents taInstructionDetails As clsRental.dsRentalPaymentTemplateTableAdapters.V_fsubRentalPaymentTemplateSPITableAdapter
    Friend WithEvents taRental As clsRental.dsRentalPaymentTemplateTableAdapters.V_fsubRentalPaymentTemplateRNITableAdapter
    Friend WithEvents lnkCancel As System.Windows.Forms.LinkLabel
    Friend WithEvents SWRCASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SWRADDRASTTYPECDCCPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SWRGENERATEDNOCCPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SWRWORKREQUESTNOSWRDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INSTRUCTIONNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSV_SERVICE_CD_SVC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASE_ORDER_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtQuoteAm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WORKQUOTEAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WORKS_DESC_DS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WORKSRSNDSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CANCEL_DT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CASECOUNTQTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVLATESTSTATUSCDINVDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TAXABLEAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TAXABLEQAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EXEMPTAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EXPENSEINCURREDDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EXPENSESTATUSCDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLAIMEDAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REJECTIONRSNDSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txbBudgetAm As System.Windows.Forms.TextBox
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
