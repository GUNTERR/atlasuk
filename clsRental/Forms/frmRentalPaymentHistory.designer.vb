<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRentalPaymentHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRentalPaymentHistory))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.pnlPaymentHistory = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvPaymentHistory = New System.Windows.Forms.DataGridView()
        Me.SWRCASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASEORDERNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICEITEMAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ITEMVATAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSVSERVICECDSVCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MANUALLYPAIDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APPORTIONEDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATERANGEDSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsPaymentHistory = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsRentalPaymentHistory = New clsRental.dsRentalPaymentHistory()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlQueryCase = New System.Windows.Forms.Panel()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvCase = New System.Windows.Forms.DataGridView()
        Me.CPCCASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClientPackageDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STARTDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ENDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CAPABILITYCODEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CREDITAUTHINDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WKIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.taCase = New clsRental.dsRentalPaymentHistoryTableAdapters.V_frmRentalPaymentHistoryTableAdapter()
        Me.taPaymentHistory = New clsRental.dsRentalPaymentHistoryTableAdapters.V_fsubRentalPaymentHistoryTableAdapter()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.pnlPaymentHistory.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvPaymentHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPaymentHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsRentalPaymentHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlQueryCase.SuspendLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.pnlCase.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvCase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(154, 7)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'pnlPaymentHistory
        '
        Me.pnlPaymentHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPaymentHistory.Controls.Add(Me.PictureBox6)
        Me.pnlPaymentHistory.Controls.Add(Me.PictureBox5)
        Me.pnlPaymentHistory.Controls.Add(Me.Panel4)
        Me.pnlPaymentHistory.Controls.Add(Me.dgvPaymentHistory)
        Me.pnlPaymentHistory.Location = New System.Drawing.Point(0, 304)
        Me.pnlPaymentHistory.Name = "pnlPaymentHistory"
        Me.pnlPaymentHistory.Size = New System.Drawing.Size(795, 397)
        Me.pnlPaymentHistory.TabIndex = 6
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(732, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Payment History"
        '
        'dgvPaymentHistory
        '
        Me.dgvPaymentHistory.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPaymentHistory.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPaymentHistory.AutoGenerateColumns = False
        Me.dgvPaymentHistory.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPaymentHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPaymentHistory.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPaymentHistory.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPaymentHistory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvPaymentHistory.ColumnHeadersHeight = 40
        Me.dgvPaymentHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SWRCASENOCSEDataGridViewTextBoxColumn, Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.INVINVOICEDTINVDataGridViewTextBoxColumn, Me.PURCHASEORDERNODataGridViewTextBoxColumn, Me.INVOICEITEMAMDataGridViewTextBoxColumn, Me.ITEMVATAMDataGridViewTextBoxColumn, Me.SSVSERVICECDSVCDataGridViewTextBoxColumn, Me.MANUALLYPAIDDTDataGridViewTextBoxColumn, Me.APPORTIONEDDTDataGridViewTextBoxColumn, Me.DATERANGEDSDataGridViewTextBoxColumn})
        Me.dgvPaymentHistory.DataSource = Me.bsPaymentHistory
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPaymentHistory.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvPaymentHistory.EnableHeadersVisualStyles = False
        Me.dgvPaymentHistory.Location = New System.Drawing.Point(8, 32)
        Me.dgvPaymentHistory.Name = "dgvPaymentHistory"
        Me.dgvPaymentHistory.ReadOnly = True
        Me.dgvPaymentHistory.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPaymentHistory.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvPaymentHistory.RowHeadersWidth = 18
        Me.dgvPaymentHistory.RowTemplate.Height = 20
        Me.dgvPaymentHistory.Size = New System.Drawing.Size(775, 352)
        Me.dgvPaymentHistory.TabIndex = 76
        '
        'SWRCASENOCSEDataGridViewTextBoxColumn
        '
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "SWR_CASE_NO_CSE"
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.HeaderText = "SWR_CASE_NO_CSE"
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.Name = "SWRCASENOCSEDataGridViewTextBoxColumn"
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRCASENOCSEDataGridViewTextBoxColumn.Visible = False
        '
        'SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "SWR_CLIENT_PACKAGE_ID_CGS"
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "SWR_CLIENT_PACKAGE_ID_CGS"
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'INVINVOICEDTINVDataGridViewTextBoxColumn
        '
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.DataPropertyName = "INV_INVOICE_DT_INV"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.HeaderText = "Invoice Date"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.Name = "INVINVOICEDTINVDataGridViewTextBoxColumn"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.ReadOnly = True
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.Width = 80
        '
        'PURCHASEORDERNODataGridViewTextBoxColumn
        '
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.DataPropertyName = "PURCHASE_ORDER_NO"
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.HeaderText = "Purchase Order No"
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.Name = "PURCHASEORDERNODataGridViewTextBoxColumn"
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.ReadOnly = True
        Me.PURCHASEORDERNODataGridViewTextBoxColumn.Width = 150
        '
        'INVOICEITEMAMDataGridViewTextBoxColumn
        '
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.DataPropertyName = "INVOICE_ITEM_AM"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.HeaderText = "Item Amount"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.Name = "INVOICEITEMAMDataGridViewTextBoxColumn"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.Width = 80
        '
        'ITEMVATAMDataGridViewTextBoxColumn
        '
        Me.ITEMVATAMDataGridViewTextBoxColumn.DataPropertyName = "ITEM_VAT_AM"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.ITEMVATAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.ITEMVATAMDataGridViewTextBoxColumn.HeaderText = "Item VAT"
        Me.ITEMVATAMDataGridViewTextBoxColumn.Name = "ITEMVATAMDataGridViewTextBoxColumn"
        Me.ITEMVATAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.ITEMVATAMDataGridViewTextBoxColumn.Width = 70
        '
        'SSVSERVICECDSVCDataGridViewTextBoxColumn
        '
        Me.SSVSERVICECDSVCDataGridViewTextBoxColumn.DataPropertyName = "SSV_SERVICE_CD_SVC"
        Me.SSVSERVICECDSVCDataGridViewTextBoxColumn.HeaderText = "SSV_SERVICE_CD_SVC"
        Me.SSVSERVICECDSVCDataGridViewTextBoxColumn.Name = "SSVSERVICECDSVCDataGridViewTextBoxColumn"
        Me.SSVSERVICECDSVCDataGridViewTextBoxColumn.ReadOnly = True
        Me.SSVSERVICECDSVCDataGridViewTextBoxColumn.Visible = False
        '
        'MANUALLYPAIDDTDataGridViewTextBoxColumn
        '
        Me.MANUALLYPAIDDTDataGridViewTextBoxColumn.DataPropertyName = "MANUALLY_PAID_DT"
        Me.MANUALLYPAIDDTDataGridViewTextBoxColumn.HeaderText = "Manually Paid Date"
        Me.MANUALLYPAIDDTDataGridViewTextBoxColumn.Name = "MANUALLYPAIDDTDataGridViewTextBoxColumn"
        Me.MANUALLYPAIDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.MANUALLYPAIDDTDataGridViewTextBoxColumn.Width = 120
        '
        'APPORTIONEDDTDataGridViewTextBoxColumn
        '
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.DataPropertyName = "APPORTIONED_DT"
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.HeaderText = "Apportioned Date"
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.Name = "APPORTIONEDDTDataGridViewTextBoxColumn"
        Me.APPORTIONEDDTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DATERANGEDSDataGridViewTextBoxColumn
        '
        Me.DATERANGEDSDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DATERANGEDSDataGridViewTextBoxColumn.DataPropertyName = "DATE_RANGE_DS"
        Me.DATERANGEDSDataGridViewTextBoxColumn.HeaderText = "Period"
        Me.DATERANGEDSDataGridViewTextBoxColumn.Name = "DATERANGEDSDataGridViewTextBoxColumn"
        Me.DATERANGEDSDataGridViewTextBoxColumn.ReadOnly = True
        '
        'bsPaymentHistory
        '
        Me.bsPaymentHistory.DataMember = "V_frmRentalPaymentHistory_V_fsubRentalPaymentHistory"
        Me.bsPaymentHistory.DataSource = Me.bsCase
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmRentalPaymentHistory"
        Me.bsCase.DataSource = Me.DsRentalPaymentHistory
        '
        'DsRentalPaymentHistory
        '
        Me.DsRentalPaymentHistory.DataSetName = "dsRentalPaymentHistory"
        Me.DsRentalPaymentHistory.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlQueryCase)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(795, 93)
        Me.Panel8.TabIndex = 4
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlQueryCase
        '
        Me.pnlQueryCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlQueryCase.Controls.Add(Me.pbxVIP)
        Me.pnlQueryCase.Controls.Add(Me.SuperLabel2)
        Me.pnlQueryCase.Controls.Add(Me.pbxCopy)
        Me.pnlQueryCase.Controls.Add(Me.txbCaseNo)
        Me.pnlQueryCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlQueryCase.Name = "pnlQueryCase"
        Me.pnlQueryCase.Size = New System.Drawing.Size(775, 48)
        Me.pnlQueryCase.TabIndex = 1
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 16)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(133, 16)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(79, 13)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(48, 20)
        Me.txbCaseNo.TabIndex = 1
        Me.txbCaseNo.Updateable = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(732, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(355, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Query Case"
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(188, 19)
        Me.SuperLabel1.TabIndex = 7
        Me.SuperLabel1.Text = "Rental Payment History"
        Me.SuperLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlCase
        '
        Me.pnlCase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCase.Controls.Add(Me.PictureBox3)
        Me.pnlCase.Controls.Add(Me.PictureBox4)
        Me.pnlCase.Controls.Add(Me.Panel2)
        Me.pnlCase.Controls.Add(Me.dgvCase)
        Me.pnlCase.Location = New System.Drawing.Point(0, 165)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(795, 133)
        Me.pnlCase.TabIndex = 8
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 80
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 79
        Me.PictureBox4.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(732, 24)
        Me.Panel2.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Case"
        '
        'dgvCase
        '
        Me.dgvCase.AllowUserToDeleteRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvCase.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvCase.AutoGenerateColumns = False
        Me.dgvCase.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvCase.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCase.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvCase.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCase.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvCase.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CPCCASENOCSEDataGridViewTextBoxColumn, Me.TypeCodeDataGridViewTextBoxColumn, Me.ClientPackageDataGridViewTextBoxColumn, Me.AddressDataGridViewTextBoxColumn, Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn, Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn, Me.STARTDTDataGridViewTextBoxColumn, Me.ENDDTDataGridViewTextBoxColumn, Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn, Me.CAPABILITYCODEDataGridViewTextBoxColumn, Me.CREDITAUTHINDataGridViewTextBoxColumn, Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn, Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn, Me.WKIDDataGridViewTextBoxColumn})
        Me.dgvCase.DataSource = Me.bsCase
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCase.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvCase.EnableHeadersVisualStyles = False
        Me.dgvCase.Location = New System.Drawing.Point(8, 32)
        Me.dgvCase.Name = "dgvCase"
        Me.dgvCase.ReadOnly = True
        Me.dgvCase.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCase.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvCase.RowHeadersWidth = 18
        Me.dgvCase.RowTemplate.Height = 20
        Me.dgvCase.Size = New System.Drawing.Size(775, 89)
        Me.dgvCase.TabIndex = 76
        '
        'CPCCASENOCSEDataGridViewTextBoxColumn
        '
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "CPC_CASE_NO_CSE"
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.HeaderText = "Case"
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.Name = "CPCCASENOCSEDataGridViewTextBoxColumn"
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CPCCASENOCSEDataGridViewTextBoxColumn.Width = 60
        '
        'TypeCodeDataGridViewTextBoxColumn
        '
        Me.TypeCodeDataGridViewTextBoxColumn.DataPropertyName = "TypeCode"
        Me.TypeCodeDataGridViewTextBoxColumn.HeaderText = "Rental Case Type"
        Me.TypeCodeDataGridViewTextBoxColumn.Name = "TypeCodeDataGridViewTextBoxColumn"
        Me.TypeCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.TypeCodeDataGridViewTextBoxColumn.Width = 130
        '
        'ClientPackageDataGridViewTextBoxColumn
        '
        Me.ClientPackageDataGridViewTextBoxColumn.DataPropertyName = "ClientPackage"
        Me.ClientPackageDataGridViewTextBoxColumn.HeaderText = "Client Package"
        Me.ClientPackageDataGridViewTextBoxColumn.Name = "ClientPackageDataGridViewTextBoxColumn"
        Me.ClientPackageDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClientPackageDataGridViewTextBoxColumn.Width = 230
        '
        'AddressDataGridViewTextBoxColumn
        '
        Me.AddressDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.AddressDataGridViewTextBoxColumn.DataPropertyName = "Address"
        Me.AddressDataGridViewTextBoxColumn.HeaderText = "Address"
        Me.AddressDataGridViewTextBoxColumn.Name = "AddressDataGridViewTextBoxColumn"
        Me.AddressDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'CCPGENERATEDNOCCPDataGridViewTextBoxColumn
        '
        Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn.DataPropertyName = "CCP_GENERATED_NO_CCP"
        Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn.HeaderText = "CCP_GENERATED_NO_CCP"
        Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn.Name = "CCPGENERATEDNOCCPDataGridViewTextBoxColumn"
        Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn.ReadOnly = True
        Me.CCPGENERATEDNOCCPDataGridViewTextBoxColumn.Visible = False
        '
        'CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn
        '
        Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn.DataPropertyName = "CCP_ADDR_AST_TYPE_CD_CCP"
        Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn.HeaderText = "CCP_ADDR_AST_TYPE_CD_CCP"
        Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn.Name = "CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn"
        Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn.ReadOnly = True
        Me.CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn.Visible = False
        '
        'STARTDTDataGridViewTextBoxColumn
        '
        Me.STARTDTDataGridViewTextBoxColumn.DataPropertyName = "START_DT"
        Me.STARTDTDataGridViewTextBoxColumn.HeaderText = "START_DT"
        Me.STARTDTDataGridViewTextBoxColumn.Name = "STARTDTDataGridViewTextBoxColumn"
        Me.STARTDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.STARTDTDataGridViewTextBoxColumn.Visible = False
        '
        'ENDDTDataGridViewTextBoxColumn
        '
        Me.ENDDTDataGridViewTextBoxColumn.DataPropertyName = "END_DT"
        Me.ENDDTDataGridViewTextBoxColumn.HeaderText = "END_DT"
        Me.ENDDTDataGridViewTextBoxColumn.Name = "ENDDTDataGridViewTextBoxColumn"
        Me.ENDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.ENDDTDataGridViewTextBoxColumn.Visible = False
        '
        'SPECIALPROVISIONSTXDataGridViewTextBoxColumn
        '
        Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn.DataPropertyName = "SPECIAL_PROVISIONS_TX"
        Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn.HeaderText = "SPECIAL_PROVISIONS_TX"
        Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn.Name = "SPECIALPROVISIONSTXDataGridViewTextBoxColumn"
        Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn.ReadOnly = True
        Me.SPECIALPROVISIONSTXDataGridViewTextBoxColumn.Visible = False
        '
        'CAPABILITYCODEDataGridViewTextBoxColumn
        '
        Me.CAPABILITYCODEDataGridViewTextBoxColumn.DataPropertyName = "CAPABILITY_CODE"
        Me.CAPABILITYCODEDataGridViewTextBoxColumn.HeaderText = "CAPABILITY_CODE"
        Me.CAPABILITYCODEDataGridViewTextBoxColumn.Name = "CAPABILITYCODEDataGridViewTextBoxColumn"
        Me.CAPABILITYCODEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CAPABILITYCODEDataGridViewTextBoxColumn.Visible = False
        '
        'CREDITAUTHINDataGridViewTextBoxColumn
        '
        Me.CREDITAUTHINDataGridViewTextBoxColumn.DataPropertyName = "CREDIT_AUTH_IN"
        Me.CREDITAUTHINDataGridViewTextBoxColumn.HeaderText = "CREDIT_AUTH_IN"
        Me.CREDITAUTHINDataGridViewTextBoxColumn.Name = "CREDITAUTHINDataGridViewTextBoxColumn"
        Me.CREDITAUTHINDataGridViewTextBoxColumn.ReadOnly = True
        Me.CREDITAUTHINDataGridViewTextBoxColumn.Visible = False
        '
        'OVERRIDEVALIDATIONINDataGridViewTextBoxColumn
        '
        Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn.DataPropertyName = "OVERRIDE_VALIDATION_IN"
        Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn.HeaderText = "OVERRIDE_VALIDATION_IN"
        Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn.Name = "OVERRIDEVALIDATIONINDataGridViewTextBoxColumn"
        Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn.ReadOnly = True
        Me.OVERRIDEVALIDATIONINDataGridViewTextBoxColumn.Visible = False
        '
        'VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn
        '
        Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn.DataPropertyName = "VALIDATION_OVERRIDDEN_BY"
        Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn.HeaderText = "VALIDATION_OVERRIDDEN_BY"
        Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn.Name = "VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn"
        Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn.ReadOnly = True
        Me.VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn.Visible = False
        '
        'WKIDDataGridViewTextBoxColumn
        '
        Me.WKIDDataGridViewTextBoxColumn.DataPropertyName = "WK_ID"
        Me.WKIDDataGridViewTextBoxColumn.HeaderText = "WK_ID"
        Me.WKIDDataGridViewTextBoxColumn.Name = "WKIDDataGridViewTextBoxColumn"
        Me.WKIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.WKIDDataGridViewTextBoxColumn.Visible = False
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taPaymentHistory
        '
        Me.taPaymentHistory.ClearBeforeFill = True
        '
        'frmRentalPaymentHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.pnlCase)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.pnlPaymentHistory)
        Me.KeyPreview = True
        Me.Name = "frmRentalPaymentHistory"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Rental Payment History"
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.pnlPaymentHistory.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvPaymentHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPaymentHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsRentalPaymentHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlQueryCase.ResumeLayout(False)
        Me.pnlQueryCase.PerformLayout()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.pnlCase.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvCase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents pnlPaymentHistory As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvPaymentHistory As System.Windows.Forms.DataGridView
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlQueryCase As System.Windows.Forms.Panel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents pnlCase As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvCase As System.Windows.Forms.DataGridView
    Friend WithEvents DsRentalPaymentHistory As clsRental.dsRentalPaymentHistory
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents taCase As clsRental.dsRentalPaymentHistoryTableAdapters.V_frmRentalPaymentHistoryTableAdapter
    Friend WithEvents bsPaymentHistory As System.Windows.Forms.BindingSource
    Friend WithEvents taPaymentHistory As clsRental.dsRentalPaymentHistoryTableAdapters.V_fsubRentalPaymentHistoryTableAdapter
    Friend WithEvents SWRCASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SWRCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVINVOICEDTINVDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASEORDERNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICEITEMAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ITEMVATAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSVSERVICECDSVCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MANUALLYPAIDDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APPORTIONEDDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATERANGEDSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPCCASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypeCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClientPackageDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCPGENERATEDNOCCPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCPADDRASTTYPECDCCPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STARTDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ENDDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPECIALPROVISIONSTXDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CAPABILITYCODEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CREDITAUTHINDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OVERRIDEVALIDATIONINDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALIDATIONOVERRIDDENBYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WKIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
