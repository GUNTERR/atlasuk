Imports System.IO
Public Class fdlgTenancyHistory
    Private dsLookups As New Data.DataSet
    Private strLookupFile As String
    Dim bsiX As New clsBindingSourceItem

    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Dim mfdcFormDatatableCollection As New clnFormDatatable
    Dim mcbcComboSourceCollection As New clnComboBoxSource
    Dim mdstForm As New DataSet, mblnQueryableForm As Boolean, mblnEditableForm As Boolean
    Dim mintCaseNo, mintPackageID, mintGeneratedNo As Integer, mdteStartDt As Date
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Public Sub SetOpeningParameters(ByVal intCaseNo As Integer, ByVal intPackageID As Integer, _
            ByVal intGeneratedNo As Integer, ByVal dteStartDt As Date)

        mintCaseNo = intCaseNo
        mintPackageID = intPackageID
        mintGeneratedNo = intGeneratedNo
        mdteStartDt = dteStartDt

    End Sub

    Private Sub InitialiseForm()

        ' Specify form master dataset

        mfdcFormDatatableCollection.Clear()
        mcbcComboSourceCollection.Clear()

        ' **** Specify dataset here ****************
        mdstForm = Me.DsMaintainRentalProperty

        ' **** Modify these below for each data table ****************
        ' Specify all datasets for the form
        Me.taTenancyHistory.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsMaintainRentalProperty.V_fdlgTenancyHistory, _
                Me.bsTenancy, Me.taTenancyHistory, Nothing, Me.pnlDetails, False, True, False, False))

        ' Now define combo boxes - make sure first one sets boolean to reset counter.
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboNoticePeriod, "NOTICE_PERIOD_CODE", True))


    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim fdtFormDatatable As clsFormDatatable

            InitialiseForm()

            For Each fdtFormDatatable In mfdcFormDatatableCollection
                With fdtFormDatatable
                    If .AllowAdd Or .AllowUpdate Or .AllowDelete Then
                        mblnEditableForm = True
                        bsiX.Add(.BindingSource)
                    End If
                    If .DataGridView IsNot Nothing Then
                        .DataGridView.AllowUserToAddRows = .AllowAdd
                        .DataGridView.AllowUserToDeleteRows = .AllowDelete
                    End If
                    If .QueryingDatatable Then mblnQueryableForm = True
                End With
            Next

            'blnReadOnly = clsGlobal.SetupForm(Me, Not mblnQueryableForm, Not mblnEditableForm)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, Not mblnQueryableForm, Not mblnEditableForm)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            If Not blnReadOnly Then blnReadOnly = Not mblnEditableForm

            ReadOnlyFields(blnReadOnly)

            strLookupFile = Me.Name + "LookUpsDataSet.xml"
            If File.Exists(clsGlobal.LookupFilePath + strLookupFile) Then
                dsLookups.ReadXml(clsGlobal.LookupFilePath + strLookupFile)
            Else
                LoadLookupLists()
            End If

            LoadAndBindComboBoxes()

            FillForm()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, mdstForm, e, blnIsNewRow)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormClosing")
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DefineQuery(e.KeyValue, blnShiftPressed, Me)
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "KeyDown")
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Me.taTenancyHistory.Connection = clsGlobal.Connection
            'Me.taTenancy.Fill(Me.DsMaintainRentalProperty.V_fdlgTenancyHistory, mintCaseNo, mintPackageID, _
            '                    mintGeneratedNo, mdteStartDt)
            Me.taTenancyHistory.Fill(Me.DsMaintainRentalProperty.V_fdlgTenancyHistory, mintCaseNo, mintPackageID, _
                    mintGeneratedNo, mdteStartDt)
            If Me.txbCaseNo.Text <> "" Then
                ReadOnlyFields(blnReadOnly)
            Else
                MsgBox("No records returned", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Me.Text)
                ReadOnlyFields(True)
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub ExitQueryMode()
        Try
            If mblnQueryableForm And blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                FillForm()
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ExitQueryMode")
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DefineQuery")
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            If mblnQueryableForm Then
                ExitQueryMode()
                FillForm()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "EscapeQueryMode")
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            If mblnQueryableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                If blnIsNewRow Then
                    UndoForm()
                    ReadOnlyFields(False)
                    blnIsNewRow = False
                End If
                blnInQueryMode = True

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    If fdtFormDatatable.QueryingDatatable AndAlso fdtFormDatatable.Datatable IsNot Nothing Then
                        fdtFormDatatable.Datatable.Clear()
                    End If
                Next

                clsGlobal.EnterQueryMode(Me, blnReadOnly)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "RunQuery")
        End Try
    End Sub


    Public Function UpdateBaseTables(ByVal blnTest As Boolean, ByVal blnSilent As Boolean) As Boolean
        If mdstForm.HasChanges Then
            Dim fdtFormDatatable As clsFormDatatable

            Dim intChanges As Integer
            Try
                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .Datatable.GetChanges IsNot Nothing Then
                            .TableAdapter.Update(.Datatable.GetChanges)
                            intChanges += .Datatable.GetChanges.Rows.Count
                        End If
                    End With
                Next

                If intChanges > 0 And Not blnSilent Then
                    Dim strMsg As String = intChanges.ToString & " record" & IIf(intChanges <> 1, "s", "") & " added/altered/deleted"
                    MsgBox(strMsg, MsgBoxStyle.Information, Me.Text & " Save")
                End If
                Return True
            Catch exc As Exception
                If Not blnSilent Then MsgBox(exc.Message, MsgBoxStyle.Exclamation, "Database Updates Failed")
                Return False
            End Try
        Else
            If Not blnSilent Then MsgBox("There are no data updates to save.", MsgBoxStyle.Information, _
                    "Save Requested Without Updates")
            Return False
        End If
    End Function

    Private Sub SaveForm(Optional ByVal blnSilent As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            If mblnEditableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                ' **** Needed for tables with generated numbers ***************
                'If blnIsNewRow Then
                '    Me.txbPersonNo.Text = clsGlobal.GetGeneratedNo("PERSON")
                'End If

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .DataGridView IsNot Nothing Then
                            ' This fixes a bug where user clicks on the new record and saves - it thinks user has edited
                            If Not .DataGridView.IsCurrentCellDirty And Not .DataGridView.IsCurrentRowDirty Then
                                .BindingSource.CancelEdit()
                            End If
                            .DataGridView.EndEdit()
                        End If
                        If .BindingSource IsNot Nothing Then
                            .BindingSource.EndEdit()
                        End If
                    End With
                Next

                If UpdateBaseTables(False, blnSilent) Then
                    For Each fdtFormDatatable In mfdcFormDatatableCollection
                        fdtFormDatatable.Datatable.AcceptChanges()
                    Next
                    blnIsNewRow = False
                    EnableBindingNavigators(Me.Controls)
                    ReadOnlyFields(blnReadOnly)
                    'Me.taCase.FillCase(Me.dsSUPPLIERMAINTENANCE.V_frmCseNew, CType(Me.txtCaseNo.Text, Integer))
                Else
                    Return
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SaveForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub LoadLookupLists()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim cbcComboSource As clsComboBoxSource, strSQL As String = ""
            Dim aArray(mcbcComboSourceCollection.Count - 1) As String, i As Integer = 0

            For Each cbcComboSource In mcbcComboSourceCollection
                strSQL += "exec maarten.OMNI_FILL_COMBO '" & cbcComboSource.Domain & "';"
                aArray(i) = cbcComboSource.LookupListName
                i += 1
            Next

            If i > 0 Then clsGlobal.LoadLookupLists(strSQL, strLookupFile, dsLookups, aArray)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "LoadLookupLists")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "LoadLookupLists", dteStartDt, Date.Now, Nothing)
        End Try
    End Sub

    Private Sub LoadAndBindComboBoxes()
        Try
            Dim cbcComboSource As clsComboBoxSource

            For Each cbcComboSource In mcbcComboSourceCollection
                With cbcComboSource.ComboBoxControl
                    .DataSource = dsLookups.Tables(cbcComboSource.LookupListName)
                    .DisplayMember = "PROMPT"
                    .ValueMember = "COLUMN_VALUE"
                End With
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "LoadAndBindComboBoxes")
        End Try
    End Sub


    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspMain_ItemClicked")
        End Try
    End Sub
    Private Sub UndoForm()
        If mblnEditableForm Then
            clsGlobal.UndoForm(Me, bsiX, mdstForm)
            If blnIsNewRow Then blnIsNewRow = False
            EnableBindingNavigators(Me.Controls)
            ReadOnlyFields(blnReadOnly)
        End If
    End Sub


    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DataControlKey")
        End Try
    End Sub

    'Private Sub BindingNavigatorAddNewItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    blnIsNewRow = True
    '    ReadOnlyFields(blnReadOnly)
    '    sender.owner.Enabled = False
    'End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        Dim fdtFormDatatable As clsFormDatatable, blnPermanentReadOnly As Boolean

        For Each fdtFormDatatable In mfdcFormDatatableCollection
            With fdtFormDatatable
                blnPermanentReadOnly = Not .AllowAdd And Not .AllowUpdate And Not .AllowDelete
                If Not blnPermanentReadOnly Then blnPermanentReadOnly = blnReadOnly
                clsGlobal.ReadOnlyFields(blnPermanentReadOnly, .HostPanel)
                If .DataGridView IsNot Nothing Then
                    .DataGridView.AllowUserToAddRows = .AllowAdd
                    .DataGridView.AllowUserToDeleteRows = .AllowDelete
                End If
            End With
        Next

        ' Extras specific to this form to be added here
        Me.txbCaseNo.ReadOnly = True
        Me.txbTenancyStartDt.ReadOnly = True
        Me.txbTenancyEndDt.ReadOnly = True
        Me.txbNoticeDt.ReadOnly = True

        clsGlobal.SetBackColor(True, Me.txbCaseNo)
        clsGlobal.SetBackColor(True, Me.txbTenancyStartDt)
        clsGlobal.SetBackColor(True, Me.txbTenancyEndDt)
        clsGlobal.SetBackColor(True, Me.txbNoticeDt)

        'Me.dgvContactRole.Columns("CANCEL_DT").ReadOnly = True

    End Sub

    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspRight_ItemClicked")
        End Try
    End Sub

    Public Sub EnableBindingNavigators(ByVal cncX As Object)

        Dim ctlX As Control
        For Each ctlX In cncX
            If TypeOf ctlX Is Panel Then
                EnableBindingNavigators(ctlX.Controls)
            ElseIf TypeOf ctlX Is BindingNavigator Then
                If Not ctlX.Enabled Then ctlX.Enabled = True
            End If
        Next
    End Sub

    
End Class