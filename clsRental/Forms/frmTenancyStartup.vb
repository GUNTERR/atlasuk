﻿
Public Class frmTenancyStartup
    Dim bsiX As New clsBindingSourceItem
    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Private mclsGlobal As New clsGlobal
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property

    Private Sub frmTenancyStartup_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, Me.DsTenancyStartup, e, blnIsNewRow)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormClosing")
        End Try
    End Sub
    Private Sub frmTenancyStartup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ReadOnlyFields(True)

            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, False, False)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            If blnReadOnly Then
                Me.tsbAdd.Visible = False
                Me.pbxTenancyStartDt.Enabled = False
                Me.pbxTenancyEndDt.Enabled = False
                Me.pbxDepositPaidDt.Enabled = False
                Me.pbxDepositReturnedDt.Enabled = False

            End If

            clsGlobal.CreateFormStripItems(Me, pnlFormStrip)

            HidePrimaryKeyFields()

            bsiX.Add(Me.bsTenancy)

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                FillForm()
            Else    ' Start in query mode - can be removed if required
                RunQuery()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormLoad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                strQueryText = CType(clsGlobal.ToolStripCaseId, String)
            End If

            If txtCaseNo.Text <> "" Then
                If IsNumeric(txtCaseNo.Text) And CType(txtCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txtCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taCase.Connection = clsGlobal.Connection
                Me.taCase.Fill(Me.DsTenancyStartup.V_frmTenancyStartup, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.txtCaseNo.Text <> "" Then
                    FillFormTenancy()

                    mclsGlobal.CalculateRemainingText(Me.lblCharactersRemaining, Me.txtClaimComments)

                    If Me.bsTenancy.Count = 0 Then
                        ReadOnlyFields(True)
                    Else
                        ReadOnlyFields(False)
                    End If
                End If

                If Me.bsCase.Count = 0 Or Me.bsTenancy.Count = 0 Then
                    MessageBox.Show("No records returned", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ReadOnlyFields(True)
                End If

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "FillForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub
    Private Sub FillFormTenancy()
        Dim dteStartDt As Date = Date.Now
        Try
            Me.taTenancy.Connection = clsGlobal.Connection
            Me.taTenancy.Fill(Me.DsTenancyStartup.V_fsubTenancyStartup, Integer.Parse(Me.txtCaseNo.Text), Integer.Parse(Me.txtPackageId.Text))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "FillFormTenancy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillFormTenancy", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub

    Private Sub ExitQueryMode()
        Try
            If blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                HidePrimaryKeyFields()
                FillForm()
            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "ExitQueryMode", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DefineQuery", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            ExitQueryMode()
            FillForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "EscapeQueryMode", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            If blnIsNewRow Then
                UndoForm()
                ReadOnlyFields(False)
                blnIsNewRow = False
            End If
            blnInQueryMode = True

            Me.DsTenancyStartup.V_frmTenancyStartup.Clear()
            Me.DsTenancyStartup.V_fsubTenancyStartup.Clear()

            clsGlobal.EnterQueryMode(Me, blnReadOnly)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "RunQuery", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SaveForm()
        Dim dteStartDt As Date = Date.Now
        Try
            If Not blnReadOnly Then
                Dim strErrorMessage As String

                Me.bsTenancy.EndEdit()

                strErrorMessage = modTenancyStartup.CheckConstraint(Me)

                If strErrorMessage = "" Then
                    If UpdateBaseTables() Then
                        Me.DsTenancyStartup.V_fsubTenancyStartup.AcceptChanges()

                        blnIsNewRow = False

                        FillFormTenancy()

                    End If
                Else
                    MessageBox.Show(strErrorMessage, "Tenancy Startup Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show("You do not have permission to change this screen.", "Client Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "SaveForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub

    Public Function UpdateBaseTables() As Boolean
        If DsTenancyStartup.HasChanges Then

            Dim newTennacy As dsTenancyStartup.V_fsubTenancyStartupDataTable =
                CType(DsTenancyStartup.V_fsubTenancyStartup.GetChanges(DataRowState.Added),
                dsTenancyStartup.V_fsubTenancyStartupDataTable)

            Dim modTennacy As dsTenancyStartup.V_fsubTenancyStartupDataTable =
                CType(DsTenancyStartup.V_fsubTenancyStartup.GetChanges(DataRowState.Modified),
                dsTenancyStartup.V_fsubTenancyStartupDataTable)

            Dim intChanges As Integer
            Try
                If Not newTennacy Is Nothing Then
                    Me.taTenancy.Connection = clsGlobal.Connection
                    Me.taTenancy.Update(newTennacy)
                    intChanges += newTennacy.Count
                End If

                If Not modTennacy Is Nothing Then
                    Me.taTenancy.Connection = clsGlobal.Connection
                    Me.taTenancy.Update(modTennacy)
                    intChanges += modTennacy.Count
                End If


                If intChanges > 0 Then
                    Dim strMsg As String = intChanges.ToString + " record added/altered/deleted"
                    MessageBox.Show(strMsg, Me.Text + " Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                Return True
            Catch exc As Exception
                MessageBox.Show(exc.Message, "Database Updates Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            Finally
                If Not newTennacy Is Nothing Then
                    newTennacy.Dispose()
                End If
                If Not modTennacy Is Nothing Then
                    modTennacy.Dispose()
                End If

            End Try
        Else
            MessageBox.Show("There are no data updates to save.", "Save Requested Without Updates", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

    End Function

    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspMain_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub UndoForm()

        clsGlobal.UndoForm(Me, bsiX, Me.DsTenancyStartup)
        If blnIsNewRow Then blnIsNewRow = False
        If Not Me.bnTenancy.Enabled Then Me.bnTenancy.Enabled = True
        ReadOnlyFields(blnReadOnly)

    End Sub

    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
                Case Keys.F8 'Copy to number field
                    clsGlobal.CopyToFormStrip(Me.txtCaseNo, Me.pnlFormStrip)
                    intKeyCode = 0
                Case Keys.F9 'Clear number field
                    clsGlobal.ClickClearCase(Me.pnlFormStrip)
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DataControlKey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        clsGlobal.ReadOnlyFields(True, Me.pnlCase)
        clsGlobal.ReadOnlyFields(blnReadOnly, Me.pnlTenancy)

        ReadOnlyPermanent()

    End Sub
    Private Sub ReadOnlyPermanent()
        Try
            Me.txtTenancyStartDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtTenancyStartDt)
            Me.txtTenancyEndDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtTenancyEndDt)
            Me.txtDepositPaidDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtDepositPaidDt)
            Me.txtDepositReturnedDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtDepositReturnedDt)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "ReadOnlyPermanent")
        End Try
    End Sub

    Private Sub pbxCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxCopy.Click
        clsGlobal.CopyToFormStrip(Me.txtCaseNo, Me.pnlFormStrip)
    End Sub

    Private Sub pbxCopy_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxCopy_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                    Case "tsbCostSaving"
                        Dim frmX As Object
                        For Each frmX In Application.OpenForms
                            If frmX.Name = "frmMainTabMenu" Then
                                clsGlobal.CopyToFormStripFromGrid(Me.txtCaseNo.Text, Me.pnlFormStrip)
                                frmX.LoadForm("Case Cost Savings", Nothing)
                                Me.Close()
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspRight_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub HidePrimaryKeyFields()
        Me.txtPackageId.Hide()
        Me.txtAddressAstType.Hide()
        Me.txtVIP.Hide()
        Me.txtTSPGeneratedNo.Hide()
    End Sub

    Private Sub pbxTenancyStartDt_Click(sender As Object, e As EventArgs) Handles pbxTenancyStartDt.Click
        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtTenancyStartDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxTenancyStartDt_MouseHover(sender As Object, e As EventArgs) Handles pbxTenancyStartDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxTenancyStartDt_MouseLeave(sender As Object, e As EventArgs) Handles pbxTenancyStartDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxTenancyEndDt_Click(sender As Object, e As EventArgs) Handles pbxTenancyEndDt.Click
        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtTenancyEndDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxTenancyEndDt_MouseHover(sender As Object, e As EventArgs) Handles pbxTenancyEndDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxTenancyEndDt_MouseLeave(sender As Object, e As EventArgs) Handles pbxTenancyEndDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxDepositPaidDt_Click(sender As Object, e As EventArgs) Handles pbxDepositPaidDt.Click
        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtDepositPaidDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxDepositPaidDt_MouseHover(sender As Object, e As EventArgs) Handles pbxDepositPaidDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxDepositPaidDt_MouseLeave(sender As Object, e As EventArgs) Handles pbxDepositPaidDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxDepositReturnedDt_Click(sender As Object, e As EventArgs) Handles pbxDepositReturnedDt.Click
        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtDepositReturnedDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxDepositReturnedDt_MouseHover(sender As Object, e As EventArgs) Handles pbxDepositReturnedDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxDepositReturnedDt_MouseLeave(sender As Object, e As EventArgs) Handles pbxDepositReturnedDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub tsbAdd_Click(sender As Object, e As EventArgs) Handles tsbAdd.Click
        blnIsNewRow = True
        Me.bnTenancy.Enabled = False
        clsGlobal.ReadOnlyFields(blnReadOnly, Me.pnlTenancy)
        ReadOnlyPermanent()

    End Sub

    Private Sub txtDepositPaidAm_TextChanged(sender As Object, e As EventArgs) Handles txtDepositPaidAm.TextChanged
        Dim dcmDepositPaidAm As Decimal

        If Decimal.TryParse(Me.txtDepositPaidAm.Text, dcmDepositPaidAm) Then
            CalculateTotal()

        End If
    End Sub

    Private Sub txtFinalAgreedClaimAm_TextChanged(sender As Object, e As EventArgs) Handles txtFinalAgreedClaimAm.TextChanged
        Dim dcmFinalAgreedClaimAm As Decimal

        If Decimal.TryParse(Me.txtFinalAgreedClaimAm.Text, dcmFinalAgreedClaimAm) Then
            CalculateTotal()

        End If
    End Sub

    Private Sub CalculateTotal()

        Dim dcmDepositPaidAm As Decimal
        Dim dcmFinalAgreedClaimAm As Decimal

        If Decimal.TryParse(Me.txtDepositPaidAm.Text, dcmDepositPaidAm) Then
            If Decimal.TryParse(Me.txtFinalAgreedClaimAm.Text, dcmFinalAgreedClaimAm) Then

                Me.txtDepositReturnedAm.Text = dcmDepositPaidAm - dcmFinalAgreedClaimAm
            End If
        End If

    End Sub

    Private Sub txtClaimComments_KeyUp(sender As Object, e As KeyEventArgs) Handles txtClaimComments.KeyUp
        mclsGlobal.CalculateRemainingText(Me.lblCharactersRemaining, Me.txtClaimComments)
    End Sub

    Private Sub txtClaimComments_MouseClick(sender As Object, e As MouseEventArgs) Handles txtClaimComments.MouseClick
        mclsGlobal.CalculateRemainingText(Me.lblCharactersRemaining, Me.txtClaimComments)
    End Sub

    Private Sub txtNotes_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNotes.KeyUp
        mclsGlobal.CalculateRemainingText(Me.lblCharactersRemainingNotes, Me.txtClaimComments)
    End Sub

    Private Sub txtNotes_MouseClick(sender As Object, e As MouseEventArgs) Handles txtNotes.MouseClick
        mclsGlobal.CalculateRemainingText(Me.lblCharactersRemainingNotes, Me.txtClaimComments)
    End Sub
End Class