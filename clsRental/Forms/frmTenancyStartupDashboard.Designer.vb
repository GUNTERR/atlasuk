﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmTenancyStartupDashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label14 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTenancyStartupDashboard))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.cboDepositReturned = New ControlLibrary.SuperComboBox()
        Me.bsConsultant = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsTenancyStartupDashboard = New clsRental.dsTenancyStartupDashboard()
        Me.chkNoTenancyEndDt = New System.Windows.Forms.CheckBox()
        Me.chkConsultant = New System.Windows.Forms.CheckBox()
        Me.cboConsultant = New ControlLibrary.SuperComboBox()
        Me.txtTenancyStartDt2 = New System.Windows.Forms.TextBox()
        Me.txtTenancyEndDt1 = New System.Windows.Forms.TextBox()
        Me.txtTenancyEndDt2 = New System.Windows.Forms.TextBox()
        Me.txtTenancyStartDt1 = New System.Windows.Forms.TextBox()
        Me.chkTenancyEndDt = New System.Windows.Forms.CheckBox()
        Me.pbxTenancyEndDtEnd = New System.Windows.Forms.PictureBox()
        Me.pbxTenancyEndDtStart = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkDepositNotPaid = New System.Windows.Forms.CheckBox()
        Me.chkFinalAgreedClaimAm = New System.Windows.Forms.CheckBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.chkOriginalClaimAm = New System.Windows.Forms.CheckBox()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.chkDepositReturned = New System.Windows.Forms.CheckBox()
        Me.chkTenancyStartDt = New System.Windows.Forms.CheckBox()
        Me.pbxTenancyStartDtEnd = New System.Windows.Forms.PictureBox()
        Me.pbxTenancyStartDtStart = New System.Windows.Forms.PictureBox()
        Me.lblCreatedStartDt = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.bnLegalRequest = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.bsTenancyStartup = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvTenancyStartup = New System.Windows.Forms.DataGridView()
        Me.GENERATEDNOTSPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lnkCaseNo = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Employee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Address = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TENANCYSTARTDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TENANCYENDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEPOSITPAIDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEPOSITPAIDAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PERSON_USER_CD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOTESTXDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.taTenancyStartup = New clsRental.dsTenancyStartupDashboardTableAdapters.OMNI_VIEW_TENANCY_STARTUP_DASHBOARDTableAdapter()
        Me.TableAdapterManager = New clsRental.dsTenancyStartupDashboardTableAdapters.TableAdapterManager()
        Me.taConsultant = New clsRental.dsTenancyStartupDashboardTableAdapters.V_fsubTenancyStartupDashboardConsultantTableAdapter()
        Label14 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFilter.SuspendLayout()
        CType(Me.bsConsultant, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsTenancyStartupDashboard, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxTenancyEndDtEnd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxTenancyEndDtStart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxTenancyStartDtEnd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxTenancyStartDtStart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.bnLegalRequest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnLegalRequest.SuspendLayout()
        CType(Me.bsTenancyStartup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvTenancyStartup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label14.Location = New System.Drawing.Point(296, 50)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(85, 14)
        Label14.TabIndex = 257
        Label14.Text = "Deposit Not Paid"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label10.Location = New System.Drawing.Point(296, 102)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(150, 14)
        Label10.TabIndex = 253
        Label10.Text = "No Final Agreed Claim Amount"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label13.Location = New System.Drawing.Point(296, 76)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(126, 14)
        Label13.TabIndex = 251
        Label13.Text = "No Original Claim Amount"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(296, 23)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(90, 14)
        Label5.TabIndex = 209
        Label5.Text = "Deposit Returned"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label12.Location = New System.Drawing.Point(133, 50)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(25, 14)
        Label12.TabIndex = 195
        Label12.Text = "and"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label9.Location = New System.Drawing.Point(133, 102)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(25, 14)
        Label9.TabIndex = 275
        Label9.Text = "and"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label1.Location = New System.Drawing.Point(548, 22)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(58, 14)
        Label1.TabIndex = 284
        Label1.Text = "Consultant"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(42, 131)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(110, 14)
        Label4.TabIndex = 286
        Label4.Text = "No Tenancy End Date"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.pnlFilter)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(0, 64)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(795, 217)
        Me.Panel1.TabIndex = 125
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'pnlFilter
        '
        Me.pnlFilter.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlFilter.Controls.Add(Me.cboDepositReturned)
        Me.pnlFilter.Controls.Add(Label4)
        Me.pnlFilter.Controls.Add(Me.chkNoTenancyEndDt)
        Me.pnlFilter.Controls.Add(Me.chkConsultant)
        Me.pnlFilter.Controls.Add(Label1)
        Me.pnlFilter.Controls.Add(Me.cboConsultant)
        Me.pnlFilter.Controls.Add(Me.txtTenancyStartDt2)
        Me.pnlFilter.Controls.Add(Me.txtTenancyEndDt1)
        Me.pnlFilter.Controls.Add(Me.txtTenancyEndDt2)
        Me.pnlFilter.Controls.Add(Me.txtTenancyStartDt1)
        Me.pnlFilter.Controls.Add(Me.chkTenancyEndDt)
        Me.pnlFilter.Controls.Add(Me.pbxTenancyEndDtEnd)
        Me.pnlFilter.Controls.Add(Me.pbxTenancyEndDtStart)
        Me.pnlFilter.Controls.Add(Me.Label8)
        Me.pnlFilter.Controls.Add(Label9)
        Me.pnlFilter.Controls.Add(Me.chkDepositNotPaid)
        Me.pnlFilter.Controls.Add(Label14)
        Me.pnlFilter.Controls.Add(Label10)
        Me.pnlFilter.Controls.Add(Me.chkFinalAgreedClaimAm)
        Me.pnlFilter.Controls.Add(Label13)
        Me.pnlFilter.Controls.Add(Me.btnClear)
        Me.pnlFilter.Controls.Add(Me.chkOriginalClaimAm)
        Me.pnlFilter.Controls.Add(Me.btnGo)
        Me.pnlFilter.Controls.Add(Me.chkDepositReturned)
        Me.pnlFilter.Controls.Add(Label5)
        Me.pnlFilter.Controls.Add(Me.chkTenancyStartDt)
        Me.pnlFilter.Controls.Add(Me.pbxTenancyStartDtEnd)
        Me.pnlFilter.Controls.Add(Me.pbxTenancyStartDtStart)
        Me.pnlFilter.Controls.Add(Me.lblCreatedStartDt)
        Me.pnlFilter.Controls.Add(Label12)
        Me.pnlFilter.Location = New System.Drawing.Point(8, 32)
        Me.pnlFilter.Name = "pnlFilter"
        Me.pnlFilter.Size = New System.Drawing.Size(775, 173)
        Me.pnlFilter.TabIndex = 1
        '
        'cboDepositReturned
        '
        Me.cboDepositReturned.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboDepositReturned.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDepositReturned.DataSource = Me.bsConsultant
        Me.cboDepositReturned.DisplayMember = "INTERNAL_CONTACT"
        Me.cboDepositReturned.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepositReturned.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepositReturned.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboDepositReturned.FormattingEnabled = True
        Me.cboDepositReturned.Location = New System.Drawing.Point(392, 19)
        Me.cboDepositReturned.Name = "cboDepositReturned"
        Me.cboDepositReturned.Queryable = False
        Me.cboDepositReturned.Size = New System.Drawing.Size(44, 22)
        Me.cboDepositReturned.TabIndex = 289
        Me.cboDepositReturned.Updateable = True
        Me.cboDepositReturned.ValueMember = "PERSON_USER_CD"
        '
        'bsConsultant
        '
        Me.bsConsultant.AllowNew = False
        Me.bsConsultant.DataMember = "V_fsubTenancyStartupDashboardConsultant"
        Me.bsConsultant.DataSource = Me.DsTenancyStartupDashboard
        '
        'DsTenancyStartupDashboard
        '
        Me.DsTenancyStartupDashboard.DataSetName = "dsTenancyStartupDashboard"
        Me.DsTenancyStartupDashboard.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'chkNoTenancyEndDt
        '
        Me.chkNoTenancyEndDt.AutoSize = True
        Me.chkNoTenancyEndDt.Location = New System.Drawing.Point(21, 131)
        Me.chkNoTenancyEndDt.Name = "chkNoTenancyEndDt"
        Me.chkNoTenancyEndDt.Size = New System.Drawing.Size(15, 14)
        Me.chkNoTenancyEndDt.TabIndex = 6
        Me.chkNoTenancyEndDt.UseVisualStyleBackColor = True
        '
        'chkConsultant
        '
        Me.chkConsultant.AutoSize = True
        Me.chkConsultant.Location = New System.Drawing.Point(527, 22)
        Me.chkConsultant.Name = "chkConsultant"
        Me.chkConsultant.Size = New System.Drawing.Size(15, 14)
        Me.chkConsultant.TabIndex = 11
        Me.chkConsultant.UseVisualStyleBackColor = True
        '
        'cboConsultant
        '
        Me.cboConsultant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboConsultant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboConsultant.DataSource = Me.bsConsultant
        Me.cboConsultant.DisplayMember = "INTERNAL_CONTACT"
        Me.cboConsultant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConsultant.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboConsultant.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboConsultant.FormattingEnabled = True
        Me.cboConsultant.Location = New System.Drawing.Point(612, 19)
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Queryable = False
        Me.cboConsultant.Size = New System.Drawing.Size(143, 22)
        Me.cboConsultant.TabIndex = 282
        Me.cboConsultant.Updateable = True
        Me.cboConsultant.ValueMember = "PERSON_USER_CD"
        '
        'txtTenancyStartDt2
        '
        Me.txtTenancyStartDt2.BackColor = System.Drawing.Color.White
        Me.txtTenancyStartDt2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyStartDt2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyStartDt2.Location = New System.Drawing.Point(169, 47)
        Me.txtTenancyStartDt2.Name = "txtTenancyStartDt2"
        Me.txtTenancyStartDt2.Size = New System.Drawing.Size(60, 20)
        Me.txtTenancyStartDt2.TabIndex = 2
        Me.txtTenancyStartDt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTenancyEndDt1
        '
        Me.txtTenancyEndDt1.BackColor = System.Drawing.Color.White
        Me.txtTenancyEndDt1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyEndDt1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyEndDt1.Location = New System.Drawing.Point(169, 73)
        Me.txtTenancyEndDt1.Name = "txtTenancyEndDt1"
        Me.txtTenancyEndDt1.Size = New System.Drawing.Size(60, 20)
        Me.txtTenancyEndDt1.TabIndex = 4
        Me.txtTenancyEndDt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTenancyEndDt2
        '
        Me.txtTenancyEndDt2.BackColor = System.Drawing.Color.White
        Me.txtTenancyEndDt2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyEndDt2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyEndDt2.Location = New System.Drawing.Point(169, 99)
        Me.txtTenancyEndDt2.Name = "txtTenancyEndDt2"
        Me.txtTenancyEndDt2.Size = New System.Drawing.Size(60, 20)
        Me.txtTenancyEndDt2.TabIndex = 5
        Me.txtTenancyEndDt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTenancyStartDt1
        '
        Me.txtTenancyStartDt1.BackColor = System.Drawing.Color.White
        Me.txtTenancyStartDt1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTenancyStartDt1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTenancyStartDt1.Location = New System.Drawing.Point(169, 19)
        Me.txtTenancyStartDt1.Name = "txtTenancyStartDt1"
        Me.txtTenancyStartDt1.Size = New System.Drawing.Size(60, 20)
        Me.txtTenancyStartDt1.TabIndex = 1
        Me.txtTenancyStartDt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkTenancyEndDt
        '
        Me.chkTenancyEndDt.AutoSize = True
        Me.chkTenancyEndDt.Location = New System.Drawing.Point(21, 75)
        Me.chkTenancyEndDt.Name = "chkTenancyEndDt"
        Me.chkTenancyEndDt.Size = New System.Drawing.Size(15, 14)
        Me.chkTenancyEndDt.TabIndex = 3
        Me.chkTenancyEndDt.UseVisualStyleBackColor = True
        '
        'pbxTenancyEndDtEnd
        '
        Me.pbxTenancyEndDtEnd.Image = CType(resources.GetObject("pbxTenancyEndDtEnd.Image"), System.Drawing.Image)
        Me.pbxTenancyEndDtEnd.Location = New System.Drawing.Point(235, 99)
        Me.pbxTenancyEndDtEnd.Name = "pbxTenancyEndDtEnd"
        Me.pbxTenancyEndDtEnd.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyEndDtEnd.TabIndex = 277
        Me.pbxTenancyEndDtEnd.TabStop = False
        '
        'pbxTenancyEndDtStart
        '
        Me.pbxTenancyEndDtStart.Image = CType(resources.GetObject("pbxTenancyEndDtStart.Image"), System.Drawing.Image)
        Me.pbxTenancyEndDtStart.Location = New System.Drawing.Point(235, 73)
        Me.pbxTenancyEndDtStart.Name = "pbxTenancyEndDtStart"
        Me.pbxTenancyEndDtStart.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyEndDtStart.TabIndex = 276
        Me.pbxTenancyEndDtStart.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(42, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(116, 14)
        Me.Label8.TabIndex = 274
        Me.Label8.Text = "Tenancy End Between"
        '
        'chkDepositNotPaid
        '
        Me.chkDepositNotPaid.AutoSize = True
        Me.chkDepositNotPaid.Location = New System.Drawing.Point(275, 50)
        Me.chkDepositNotPaid.Name = "chkDepositNotPaid"
        Me.chkDepositNotPaid.Size = New System.Drawing.Size(15, 14)
        Me.chkDepositNotPaid.TabIndex = 8
        Me.chkDepositNotPaid.UseVisualStyleBackColor = True
        '
        'chkFinalAgreedClaimAm
        '
        Me.chkFinalAgreedClaimAm.AutoSize = True
        Me.chkFinalAgreedClaimAm.Location = New System.Drawing.Point(275, 102)
        Me.chkFinalAgreedClaimAm.Name = "chkFinalAgreedClaimAm"
        Me.chkFinalAgreedClaimAm.Size = New System.Drawing.Size(15, 14)
        Me.chkFinalAgreedClaimAm.TabIndex = 10
        Me.chkFinalAgreedClaimAm.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnClear.Location = New System.Drawing.Point(708, 131)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(47, 22)
        Me.btnClear.TabIndex = 19
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'chkOriginalClaimAm
        '
        Me.chkOriginalClaimAm.AutoSize = True
        Me.chkOriginalClaimAm.Location = New System.Drawing.Point(275, 74)
        Me.chkOriginalClaimAm.Name = "chkOriginalClaimAm"
        Me.chkOriginalClaimAm.Size = New System.Drawing.Size(15, 14)
        Me.chkOriginalClaimAm.TabIndex = 9
        Me.chkOriginalClaimAm.UseVisualStyleBackColor = True
        '
        'btnGo
        '
        Me.btnGo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnGo.Location = New System.Drawing.Point(654, 131)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(48, 22)
        Me.btnGo.TabIndex = 20
        Me.btnGo.Text = "Go"
        Me.btnGo.UseVisualStyleBackColor = False
        '
        'chkDepositReturned
        '
        Me.chkDepositReturned.AutoSize = True
        Me.chkDepositReturned.Location = New System.Drawing.Point(275, 23)
        Me.chkDepositReturned.Name = "chkDepositReturned"
        Me.chkDepositReturned.Size = New System.Drawing.Size(15, 14)
        Me.chkDepositReturned.TabIndex = 7
        Me.chkDepositReturned.UseVisualStyleBackColor = True
        '
        'chkTenancyStartDt
        '
        Me.chkTenancyStartDt.AutoSize = True
        Me.chkTenancyStartDt.Location = New System.Drawing.Point(21, 23)
        Me.chkTenancyStartDt.Name = "chkTenancyStartDt"
        Me.chkTenancyStartDt.Size = New System.Drawing.Size(15, 14)
        Me.chkTenancyStartDt.TabIndex = 0
        Me.chkTenancyStartDt.UseVisualStyleBackColor = True
        '
        'pbxTenancyStartDtEnd
        '
        Me.pbxTenancyStartDtEnd.Image = CType(resources.GetObject("pbxTenancyStartDtEnd.Image"), System.Drawing.Image)
        Me.pbxTenancyStartDtEnd.Location = New System.Drawing.Point(235, 47)
        Me.pbxTenancyStartDtEnd.Name = "pbxTenancyStartDtEnd"
        Me.pbxTenancyStartDtEnd.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyStartDtEnd.TabIndex = 199
        Me.pbxTenancyStartDtEnd.TabStop = False
        '
        'pbxTenancyStartDtStart
        '
        Me.pbxTenancyStartDtStart.Image = CType(resources.GetObject("pbxTenancyStartDtStart.Image"), System.Drawing.Image)
        Me.pbxTenancyStartDtStart.Location = New System.Drawing.Point(235, 21)
        Me.pbxTenancyStartDtStart.Name = "pbxTenancyStartDtStart"
        Me.pbxTenancyStartDtStart.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyStartDtStart.TabIndex = 196
        Me.pbxTenancyStartDtStart.TabStop = False
        '
        'lblCreatedStartDt
        '
        Me.lblCreatedStartDt.AutoSize = True
        Me.lblCreatedStartDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.lblCreatedStartDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreatedStartDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCreatedStartDt.Location = New System.Drawing.Point(42, 22)
        Me.lblCreatedStartDt.Name = "lblCreatedStartDt"
        Me.lblCreatedStartDt.Size = New System.Drawing.Size(121, 14)
        Me.lblCreatedStartDt.TabIndex = 194
        Me.lblCreatedStartDt.Text = "Tenancy Start Between"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(750, 24)
        Me.Panel2.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Filter"
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(223, 19)
        Me.SuperLabel2.TabIndex = 130
        Me.SuperLabel2.Text = "Tenancy Startup Dashboard"
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 132
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 131
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 133
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(772, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 10
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(557, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.bnLegalRequest)
        Me.Panel5.Controls.Add(Me.PictureBox5)
        Me.Panel5.Controls.Add(Me.Panel4)
        Me.Panel5.Controls.Add(Me.dgvTenancyStartup)
        Me.Panel5.Location = New System.Drawing.Point(0, 287)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(795, 411)
        Me.Panel5.TabIndex = 134
        '
        'bnLegalRequest
        '
        Me.bnLegalRequest.AddNewItem = Nothing
        Me.bnLegalRequest.BindingSource = Me.bsTenancyStartup
        Me.bnLegalRequest.CountItem = Me.BindingNavigatorCountItem
        Me.bnLegalRequest.DeleteItem = Nothing
        Me.bnLegalRequest.Dock = System.Windows.Forms.DockStyle.None
        Me.bnLegalRequest.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnLegalRequest.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.bnLegalRequest.Location = New System.Drawing.Point(8, 368)
        Me.bnLegalRequest.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.bnLegalRequest.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.bnLegalRequest.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.bnLegalRequest.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.bnLegalRequest.Name = "bnLegalRequest"
        Me.bnLegalRequest.PositionItem = Me.BindingNavigatorPositionItem
        Me.bnLegalRequest.Size = New System.Drawing.Size(169, 25)
        Me.bnLegalRequest.TabIndex = 81
        Me.bnLegalRequest.Text = "BindingNavigator1"
        '
        'bsTenancyStartup
        '
        Me.bsTenancyStartup.AllowNew = False
        Me.bsTenancyStartup.DataMember = "OMNI_VIEW_TENANCY_STARTUP_DASHBOARD"
        Me.bsTenancyStartup.DataSource = Me.DsTenancyStartupDashboard
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(25, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(750, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Tenancy Startups"
        '
        'dgvTenancyStartup
        '
        Me.dgvTenancyStartup.AllowUserToAddRows = False
        Me.dgvTenancyStartup.AllowUserToDeleteRows = False
        Me.dgvTenancyStartup.AllowUserToResizeColumns = False
        Me.dgvTenancyStartup.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvTenancyStartup.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTenancyStartup.AutoGenerateColumns = False
        Me.dgvTenancyStartup.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvTenancyStartup.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTenancyStartup.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTenancyStartup.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTenancyStartup.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTenancyStartup.ColumnHeadersHeight = 30
        Me.dgvTenancyStartup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTenancyStartup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GENERATEDNOTSPDataGridViewTextBoxColumn, Me.lnkCaseNo, Me.Employee, Me.Address, Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.TENANCYSTARTDTDataGridViewTextBoxColumn, Me.TENANCYENDDTDataGridViewTextBoxColumn, Me.DEPOSITPAIDDTDataGridViewTextBoxColumn, Me.DEPOSITPAIDAMDataGridViewTextBoxColumn, Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn, Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn, Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn, Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn, Me.PERSON_USER_CD, Me.DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn, Me.NOTESTXDataGridViewTextBoxColumn})
        Me.dgvTenancyStartup.DataSource = Me.bsTenancyStartup
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTenancyStartup.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvTenancyStartup.EnableHeadersVisualStyles = False
        Me.dgvTenancyStartup.Location = New System.Drawing.Point(8, 32)
        Me.dgvTenancyStartup.MultiSelect = False
        Me.dgvTenancyStartup.Name = "dgvTenancyStartup"
        Me.dgvTenancyStartup.ReadOnly = True
        Me.dgvTenancyStartup.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTenancyStartup.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvTenancyStartup.RowHeadersWidth = 18
        Me.dgvTenancyStartup.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTenancyStartup.RowTemplate.Height = 20
        Me.dgvTenancyStartup.Size = New System.Drawing.Size(775, 318)
        Me.dgvTenancyStartup.TabIndex = 76
        '
        'GENERATEDNOTSPDataGridViewTextBoxColumn
        '
        Me.GENERATEDNOTSPDataGridViewTextBoxColumn.DataPropertyName = "GENERATED_NO_TSP"
        Me.GENERATEDNOTSPDataGridViewTextBoxColumn.HeaderText = "GENERATED_NO_TSP"
        Me.GENERATEDNOTSPDataGridViewTextBoxColumn.Name = "GENERATEDNOTSPDataGridViewTextBoxColumn"
        Me.GENERATEDNOTSPDataGridViewTextBoxColumn.ReadOnly = True
        Me.GENERATEDNOTSPDataGridViewTextBoxColumn.Visible = False
        '
        'lnkCaseNo
        '
        Me.lnkCaseNo.ActiveLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkCaseNo.DataPropertyName = "CPC_CASE_NO_CSE"
        Me.lnkCaseNo.HeaderText = "Case No"
        Me.lnkCaseNo.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkCaseNo.LinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkCaseNo.Name = "lnkCaseNo"
        Me.lnkCaseNo.ReadOnly = True
        Me.lnkCaseNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.lnkCaseNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.lnkCaseNo.VisitedLinkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.lnkCaseNo.Width = 60
        '
        'Employee
        '
        Me.Employee.DataPropertyName = "Employee"
        Me.Employee.HeaderText = "Employee"
        Me.Employee.Name = "Employee"
        Me.Employee.ReadOnly = True
        '
        'Address
        '
        Me.Address.DataPropertyName = "Address"
        Me.Address.HeaderText = "Address"
        Me.Address.Name = "Address"
        Me.Address.ReadOnly = True
        '
        'CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CPC_CLIENT_PACKAGE_ID_CGS"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'TENANCYSTARTDTDataGridViewTextBoxColumn
        '
        Me.TENANCYSTARTDTDataGridViewTextBoxColumn.DataPropertyName = "TENANCY_START_DT"
        Me.TENANCYSTARTDTDataGridViewTextBoxColumn.HeaderText = "Tenancy Start Dt"
        Me.TENANCYSTARTDTDataGridViewTextBoxColumn.Name = "TENANCYSTARTDTDataGridViewTextBoxColumn"
        Me.TENANCYSTARTDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.TENANCYSTARTDTDataGridViewTextBoxColumn.Width = 65
        '
        'TENANCYENDDTDataGridViewTextBoxColumn
        '
        Me.TENANCYENDDTDataGridViewTextBoxColumn.DataPropertyName = "TENANCY_END_DT"
        Me.TENANCYENDDTDataGridViewTextBoxColumn.HeaderText = "Tenancy End Dt"
        Me.TENANCYENDDTDataGridViewTextBoxColumn.Name = "TENANCYENDDTDataGridViewTextBoxColumn"
        Me.TENANCYENDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.TENANCYENDDTDataGridViewTextBoxColumn.Width = 65
        '
        'DEPOSITPAIDDTDataGridViewTextBoxColumn
        '
        Me.DEPOSITPAIDDTDataGridViewTextBoxColumn.DataPropertyName = "DEPOSIT_PAID_DT"
        Me.DEPOSITPAIDDTDataGridViewTextBoxColumn.HeaderText = "Deposit Paid Dt"
        Me.DEPOSITPAIDDTDataGridViewTextBoxColumn.Name = "DEPOSITPAIDDTDataGridViewTextBoxColumn"
        Me.DEPOSITPAIDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.DEPOSITPAIDDTDataGridViewTextBoxColumn.Width = 65
        '
        'DEPOSITPAIDAMDataGridViewTextBoxColumn
        '
        Me.DEPOSITPAIDAMDataGridViewTextBoxColumn.DataPropertyName = "DEPOSIT_PAID_AM"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DEPOSITPAIDAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.DEPOSITPAIDAMDataGridViewTextBoxColumn.HeaderText = "Deposit Paid Amount"
        Me.DEPOSITPAIDAMDataGridViewTextBoxColumn.Name = "DEPOSITPAIDAMDataGridViewTextBoxColumn"
        Me.DEPOSITPAIDAMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn
        '
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn.DataPropertyName = "ORIGINAL_DILAP_CLAIM_AM"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn.HeaderText = "Original Claim Amount"
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn.Name = "ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn"
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn.Width = 90
        '
        'FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn
        '
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn.DataPropertyName = "FINAL_AGREED_DILAP_CLAIM_AM"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn.HeaderText = "Final Claim Amount"
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn.Name = "FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn"
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn.Width = 80
        '
        'DEPOSITRETURNEDAMDataGridViewTextBoxColumn
        '
        Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn.DataPropertyName = "DEPOSIT_RETURNED_AM"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn.HeaderText = "Deposit Returned Am"
        Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn.Name = "DEPOSITRETURNEDAMDataGridViewTextBoxColumn"
        Me.DEPOSITRETURNEDAMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DEPOSITRETURNEDDTDataGridViewTextBoxColumn
        '
        Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn.DataPropertyName = "DEPOSIT_RETURNED_DT"
        Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn.HeaderText = "Deposit Returned Dt"
        Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn.Name = "DEPOSITRETURNEDDTDataGridViewTextBoxColumn"
        Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.DEPOSITRETURNEDDTDataGridViewTextBoxColumn.Width = 85
        '
        'PERSON_USER_CD
        '
        Me.PERSON_USER_CD.DataPropertyName = "PERSON_USER_CD"
        Me.PERSON_USER_CD.HeaderText = "Consultant"
        Me.PERSON_USER_CD.Name = "PERSON_USER_CD"
        Me.PERSON_USER_CD.ReadOnly = True
        '
        'DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn
        '
        Me.DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn.DataPropertyName = "DILAP_CLAIM_COMMENTS_TX"
        Me.DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn.HeaderText = "Claim Comments"
        Me.DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn.Name = "DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn"
        Me.DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NOTESTXDataGridViewTextBoxColumn
        '
        Me.NOTESTXDataGridViewTextBoxColumn.DataPropertyName = "NOTES_TX"
        Me.NOTESTXDataGridViewTextBoxColumn.HeaderText = "Notes"
        Me.NOTESTXDataGridViewTextBoxColumn.Name = "NOTESTXDataGridViewTextBoxColumn"
        Me.NOTESTXDataGridViewTextBoxColumn.ReadOnly = True
        '
        'taTenancyStartup
        '
        Me.taTenancyStartup.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.UpdateOrder = clsRental.dsTenancyStartupDashboardTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'taConsultant
        '
        Me.taConsultant.ClearBeforeFill = True
        '
        'frmTenancyStartupDashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 699)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.SuperLabel2)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmTenancyStartupDashboard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Tenancy Startup Dashboard"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        CType(Me.bsConsultant, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsTenancyStartupDashboard, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxTenancyEndDtEnd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxTenancyEndDtStart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxTenancyStartDtEnd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxTenancyStartDtStart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.bnLegalRequest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnLegalRequest.ResumeLayout(False)
        Me.bnLegalRequest.PerformLayout()
        CType(Me.bsTenancyStartup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvTenancyStartup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents pnlFilter As Panel
    Friend WithEvents chkDepositNotPaid As CheckBox
    Friend WithEvents chkFinalAgreedClaimAm As CheckBox
    Friend WithEvents btnClear As Button
    Friend WithEvents chkOriginalClaimAm As CheckBox
    Friend WithEvents btnGo As Button
    Friend WithEvents chkDepositReturned As CheckBox
    Friend WithEvents chkTenancyStartDt As CheckBox
    Friend WithEvents pbxTenancyStartDtEnd As PictureBox
    Friend WithEvents pbxTenancyStartDtStart As PictureBox
    Friend WithEvents lblCreatedStartDt As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents pnlFormStrip As Panel
    Friend WithEvents pnlMainToolStrip As Panel
    Friend WithEvents tspRight As ToolStrip
    Friend WithEvents tspMain As ToolStrip
    Friend WithEvents Panel5 As Panel
    Friend WithEvents bnLegalRequest As BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents dgvTenancyStartup As DataGridView
    Friend WithEvents chkTenancyEndDt As CheckBox
    Friend WithEvents pbxTenancyEndDtEnd As PictureBox
    Friend WithEvents pbxTenancyEndDtStart As PictureBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtTenancyStartDt2 As TextBox
    Friend WithEvents txtTenancyEndDt1 As TextBox
    Friend WithEvents txtTenancyEndDt2 As TextBox
    Friend WithEvents txtTenancyStartDt1 As TextBox
    Friend WithEvents DsTenancyStartupDashboard As dsTenancyStartupDashboard
    Friend WithEvents bsTenancyStartup As BindingSource
    Friend WithEvents taTenancyStartup As dsTenancyStartupDashboardTableAdapters.OMNI_VIEW_TENANCY_STARTUP_DASHBOARDTableAdapter
    Friend WithEvents TableAdapterManager As dsTenancyStartupDashboardTableAdapters.TableAdapterManager
    Friend WithEvents bsConsultant As BindingSource
    Friend WithEvents taConsultant As dsTenancyStartupDashboardTableAdapters.V_fsubTenancyStartupDashboardConsultantTableAdapter
    Friend WithEvents chkConsultant As CheckBox
    Friend WithEvents cboConsultant As ControlLibrary.SuperComboBox
    Friend WithEvents chkNoTenancyEndDt As CheckBox
    Friend WithEvents GENERATEDNOTSPDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents lnkCaseNo As DataGridViewLinkColumn
    Friend WithEvents Employee As DataGridViewTextBoxColumn
    Friend WithEvents Address As DataGridViewTextBoxColumn
    Friend WithEvents CPCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TENANCYSTARTDTDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TENANCYENDDTDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DEPOSITPAIDDTDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DEPOSITPAIDAMDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ORIGINALDILAPCLAIMAMDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FINALAGREEDDILAPCLAIMAMDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DEPOSITRETURNEDAMDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DEPOSITRETURNEDDTDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PERSON_USER_CD As DataGridViewTextBoxColumn
    Friend WithEvents DILAPCLAIMCOMMENTSTXDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NOTESTXDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents cboDepositReturned As ControlLibrary.SuperComboBox
End Class
