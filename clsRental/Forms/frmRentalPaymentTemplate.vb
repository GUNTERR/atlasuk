Imports System.IO
Public Class frmRentalPaymentTemplate

    Private dsLookups As New Data.DataSet
    Private strLookupFile As String
    Dim bsiX As New clsBindingSourceItem
    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Dim mfdcFormDatatableCollection As New clnFormDatatable
    Dim mcbcComboSourceCollection As New clnComboBoxSource
    Dim mdstForm As New DataSet, mblnQueryableForm As Boolean, mblnEditableForm As Boolean
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Private Sub InitialiseForm()

        ' Specify form master dataset

        mfdcFormDatatableCollection.Clear()
        mcbcComboSourceCollection.Clear()

        ' **** Specify dataset here ****************
        mdstForm = Me.DsRentalPaymentTemplate

        ' **** Modify these below for each data table ****************
        ' Specify all datasets for the form
        Me.taInstruction.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPaymentTemplate.V_frmRentalPaymentTemplate, _
                Me.bsInstruction, Me.taInstruction, Nothing, Me.pnlInstruction, False, True, False, True))
        Me.taInstructionDetails.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPaymentTemplate.V_fsubRentalPaymentTemplateSPI, _
                Me.bsInstructionDetails, Me.taInstructionDetails, Me.dgvInstructionDetail, Me.pnlInstructionDetails, False, True, False, False))
        Me.taRental.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPaymentTemplate.V_fsubRentalPaymentTemplateRNI, _
                Me.bsRental, Me.taRental, Nothing, Me.pnlRental, True, True, False, False))
        ' Now define combo boxes - make sure first one sets boolean to reset counter
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboDismissalReasonCd, "DISMISSED_RSN_CODE", True))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboRentalCaseType, "PROP_OR_PSN_CODE"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboRequestStatus, "REQUEST_STATUS_CD"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboRentalInstructionType, "ONEOFF_TYPE_CD"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboPaymentFrequency, "PAYMENT_FREQUENCY_CODE"))

        ' Bind any supercheck boxes
        Me.chkOngoing.BindData("Checked", Me.bsInstruction, "ONGOING_IN")

    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim fdtFormDatatable As clsFormDatatable

            InitialiseForm()

            For Each fdtFormDatatable In mfdcFormDatatableCollection
                With fdtFormDatatable
                    If .AllowAdd Or .AllowUpdate Or .AllowDelete Then
                        mblnEditableForm = True
                        bsiX.Add(.BindingSource)
                    End If
                    If .DataGridView IsNot Nothing Then
                        .DataGridView.AllowUserToAddRows = .AllowAdd
                        .DataGridView.AllowUserToDeleteRows = .AllowDelete
                    End If
                    If .QueryingDatatable Then mblnQueryableForm = True
                End With
            Next

            'blnReadOnly = clsGlobal.SetupForm(Me, Not mblnQueryableForm, Not mblnEditableForm)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, Not mblnQueryableForm, Not mblnEditableForm)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            ReadOnlyFields(blnReadOnly)

            Me.lnkCancel.Enabled = Not blnReadOnly

            clsGlobal.CreateFormStripItems(Me, pnlFormStrip)

            strLookupFile = Me.Name + "LookUpsDataSet.xml"
            If File.Exists(clsGlobal.LookupFilePath + strLookupFile) Then
                dsLookups.ReadXml(clsGlobal.LookupFilePath + strLookupFile)
            Else
                LoadLookupLists()
            End If

            LoadAndBindComboBoxes()

            Me.cboDismissalReasonCd.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboPaymentFrequency.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboRentalCaseType.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboRentalInstructionType.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboRequestStatus.DropDownStyle = ComboBoxStyle.DropDown

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                FillForm()
            Else    ' Start in query mode - can be removed if required
                RunQuery()
            End If

            HidePrimaryKeyFields()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, mdstForm, e, blnIsNewRow)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormClosing")
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DefineQuery(e.KeyValue, blnShiftPressed, Me)
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "KeyDown")
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                strQueryText = CType(clsGlobal.ToolStripCaseId, String)
            End If

            If txbCaseNo.Text <> "" Then
                If IsNumeric(txbCaseNo.Text) And CType(txbCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txbCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taInstruction.Connection = clsGlobal.Connection
                Me.taInstruction.Fill(Me.DsRentalPaymentTemplate.V_frmRentalPaymentTemplate, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.txbCaseNo.Text <> "" Then
                    Me.taInstructionDetails.Connection = clsGlobal.Connection
                    Me.taInstructionDetails.Fill(Me.DsRentalPaymentTemplate.V_fsubRentalPaymentTemplateSPI, CType(strQueryText, Integer))
                    Me.taRental.Connection = clsGlobal.Connection
                    Me.taRental.Fill(Me.DsRentalPaymentTemplate.V_fsubRentalPaymentTemplateRNI, CType(strQueryText, Integer))
                    ReadOnlyFields(blnReadOnly)
                    clsGlobal.ToolStripCaseId = CType(Me.txbCaseNo.Text, Integer)
                Else
                    MsgBox("No records returned", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Me.Text)
                    ReadOnlyFields(True)
                End If
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub ExitQueryMode()
        Try
            If mblnQueryableForm And blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                HidePrimaryKeyFields()
                FillForm()
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ExitQueryMode")
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DefineQuery")
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            If mblnQueryableForm Then
                ExitQueryMode()
                FillForm()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "EscapeQueryMode")
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            If clsGlobal.FormIsDirty(Me, bsiX, Me.DsRentalPaymentTemplate, blnIsNewRow) Then
                Dim strMsg As String = "Data has been changed. Click on OK to abandon changes and continue with your query, or click Cancel to return to " + _
                                        "the form where you can click on the Save button"

                If MsgBox(strMsg, MsgBoxStyle.Question Or MsgBoxStyle.OkCancel, "Pending Updates Not Saved") = MsgBoxResult.Cancel Then
                    Exit Sub
                End If
            End If

            If mblnQueryableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                If blnIsNewRow Then
                    UndoForm()
                    ReadOnlyFields(False)
                    blnIsNewRow = False
                End If
                blnInQueryMode = True

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    If fdtFormDatatable.QueryingDatatable AndAlso fdtFormDatatable.Datatable IsNot Nothing Then
                        fdtFormDatatable.Datatable.Clear()
                    End If
                Next

                clsGlobal.EnterQueryMode(Me, blnReadOnly)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "RunQuery")
        End Try
    End Sub


    Public Function UpdateBaseTables(ByVal blnSilent As Boolean) As Boolean
        If mdstForm.HasChanges Then
            Dim fdtFormDatatable As clsFormDatatable

            Dim intChanges As Integer
            Try
                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .Datatable.GetChanges IsNot Nothing Then
                            .TableAdapter.Update(.Datatable.GetChanges)
                            intChanges += .Datatable.GetChanges.Rows.Count
                        End If
                    End With
                Next

                If intChanges > 0 And Not blnSilent Then
                    Dim strMsg As String = intChanges.ToString & " record" & IIf(intChanges <> 1, "s", "") & " added/altered/deleted"
                    MsgBox(strMsg, MsgBoxStyle.Information, Me.Text & " Save")
                End If
                Return True
            Catch exc As Exception
                If Not blnSilent Then MsgBox(exc.Message, MsgBoxStyle.Exclamation, "Database Updates Failed")
                Return False
            End Try
        Else
            If Not blnSilent Then MsgBox("There are no data updates to save.", MsgBoxStyle.Information, _
                    "Save Requested Without Updates")
            Return False
        End If
    End Function
    Private Sub CheckBudget(Optional ByVal blnSilent As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strMsg As String
            Dim dblTotalNetAm As Double

            If blnIsNewRow Then
                If Me.txbBudgetAm.Text <> "" AndAlso CType(Me.txbBudgetAm.Text, Double) > 0 AndAlso Me.dgvInstructionDetail.CurrentRow.Cells("SSV_SERVICE_CD_SVC").Value.ToString = "RENT" AndAlso CType(Me.dgvInstructionDetail.CurrentRow.Cells("txtQuoteAm").Value.ToString, Double) > 0 Then

                    Dim commSQL As New SqlClient.SqlCommand
                    Dim datRead As SqlClient.SqlDataReader
                    commSQL.Connection = clsGlobal.Connection
                    commSQL.CommandType = CommandType.StoredProcedure
                    commSQL.CommandText = "maarten.OMNI_GET_TOTAL_SPENT"
                    commSQL.Parameters.Add("@CaseNo", SqlDbType.Int).Value = CType(Me.txbCaseNo.Text, Integer)

                    datRead = commSQL.ExecuteReader
                    While datRead.Read
                        dblTotalNetAm = CType(datRead("TOTAL_NET_AM").ToString(), Double)
                    End While
                    datRead.Close()

                    If IsDate(Me.txbRentStartDt.Text) And Me.txbPayments.Text <> "" Then
                        'Initial or Final Payment
                        If dblTotalNetAm + CType(Me.dgvInstructionDetail.CurrentRow.Cells("txtQuoteAm").Value.ToString, Double) >= CType(Me.txbBudgetAm.Text, Double) Then
                            strMsg = "Cannot save because it will exceed the budget. You need to get a manager to increase the case budget and then you will be able to save."
                            MsgBox(strMsg, MsgBoxStyle.Information, "Budget Violation")
                            Exit Sub
                        End If

                    ElseIf Me.txbPayments.Text <> "" Then
                        'Ongoing Payment
                        If Me.txbPayments.Text = Me.txbPaymentsRemaining.Text Then
                            Dim dblTotalRent As Double
                            dblTotalRent = CType(Me.dgvInstructionDetail.CurrentRow.Cells("txtQuoteAm").Value.ToString, Double) * CType(Me.txbPayments.Text, Double)
                            If dblTotalNetAm + dblTotalRent >= CType(Me.txbBudgetAm.Text, Double) Then
                                strMsg = "Cannot save because it will exceed the budget. You need to get a manager to increase the case budget and then you will be able to save."
                                MsgBox(strMsg, MsgBoxStyle.Information, "Budget Violation")
                                Exit Sub
                            End If
                        End If
                    Else
                        SaveForm()
                    End If
                Else
                    SaveForm()
                End If
            Else
                SaveForm()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "CheckBudget")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "CheckBudget", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub
    Private Sub SaveForm(Optional ByVal blnSilent As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            If mblnEditableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .DataGridView IsNot Nothing Then
                            ' This fixes a bug where user clicks on the new record and saves - it thinks user has edited
                            If Not .DataGridView.IsCurrentCellDirty And Not .DataGridView.IsCurrentRowDirty Then
                                .BindingSource.CancelEdit()
                            End If
                            .DataGridView.EndEdit()
                        End If
                        If .BindingSource IsNot Nothing Then
                            .BindingSource.EndEdit()
                        End If
                    End With
                Next

                If UpdateBaseTables(blnSilent) Then
                    For Each fdtFormDatatable In mfdcFormDatatableCollection
                        fdtFormDatatable.Datatable.AcceptChanges()
                    Next
                    blnIsNewRow = False
                    EnableBindingNavigators(Me.Controls)
                    ReadOnlyFields(blnReadOnly)
                    'Me.taCase.FillCase(Me.dsSUPPLIERMAINTENANCE.V_frmCseNew, CType(Me.txtCaseNo.Text, Integer))
                Else
                    Return
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SaveForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub LoadLookupLists()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim cbcComboSource As clsComboBoxSource, strSQL As String = ""
            Dim aArray(mcbcComboSourceCollection.Count - 1) As String, i As Integer = 0

            For Each cbcComboSource In mcbcComboSourceCollection
                strSQL += "exec maarten.OMNI_FILL_COMBO '" & cbcComboSource.Domain & "';"
                aArray(i) = cbcComboSource.LookupListName
                i += 1
            Next

            If i > 0 Then clsGlobal.LoadLookupLists(strSQL, strLookupFile, dsLookups, aArray)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "LoadLookupLists")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "LoadLookupLists", dteStartDt, Date.Now, Nothing)
        End Try
    End Sub

    Private Sub LoadAndBindComboBoxes()
        Try
            Dim cbcComboSource As clsComboBoxSource

            For Each cbcComboSource In mcbcComboSourceCollection
                With cbcComboSource.ComboBoxControl
                    .DataSource = dsLookups.Tables(cbcComboSource.LookupListName)
                    .DisplayMember = "PROMPT"
                    .ValueMember = "COLUMN_VALUE"
                End With
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "LoadAndBindComboBoxes")
        End Try
    End Sub

    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        CheckBudget()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspMain_ItemClicked")
        End Try
    End Sub
    Private Sub UndoForm()
        If mblnEditableForm Then
            clsGlobal.UndoForm(Me, bsiX, mdstForm)
            If blnIsNewRow Then blnIsNewRow = False
            EnableBindingNavigators(Me.Controls)
            ReadOnlyFields(blnReadOnly)
        End If
    End Sub

    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    CheckBudget()
                    intKeyCode = 0
                Case Keys.F8 'Copy to number field
                    clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
                    intKeyCode = 0
                Case Keys.F9 'Clear number field
                    clsGlobal.ClickClearCase(Me.pnlFormStrip)
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DataControlKey")
        End Try
    End Sub

    'Private Sub BindingNavigatorAddNewItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    blnIsNewRow = True
    '    ReadOnlyFields(blnReadOnly)
    '    sender.owner.Enabled = False
    'End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        Dim fdtFormDatatable As clsFormDatatable, blnPermanentReadOnly As Boolean

        For Each fdtFormDatatable In mfdcFormDatatableCollection
            With fdtFormDatatable
                blnPermanentReadOnly = Not .AllowAdd And Not .AllowUpdate And Not .AllowDelete
                If Not blnPermanentReadOnly Then blnPermanentReadOnly = blnReadOnly
                clsGlobal.ReadOnlyFields(blnPermanentReadOnly, .HostPanel)
                If .DataGridView IsNot Nothing Then
                    .DataGridView.AllowUserToAddRows = .AllowAdd
                    .DataGridView.AllowUserToDeleteRows = .AllowDelete
                End If
            End With
        Next

        ' Extras specific to this form to be added here
        Me.txbSupplierNo.Hide()
        Me.txbBudgetAm.Hide()

        Me.txbCaseNo.ReadOnly = True
        Me.txbPackageNm.ReadOnly = True
        Me.cboRentalCaseType.ReadOnly = True
        Me.chkOngoing.Enabled = False
        Me.txbSupplierNm.ReadOnly = True
        Me.txbAddress.ReadOnly = True
        Me.txbInstructionDt.ReadOnly = True
        Me.cboRequestStatus.ReadOnly = True
        Me.txbSupplierAddress.ReadOnly = True
        Me.txbNoDaysCovered.ReadOnly = True
        If Me.bsRental.Count = 0 And Not blnIsNewRow Then clsGlobal.ReadOnlyFields(True, Me.pnlRental)
        Me.tsbAdd.Enabled = blnReadOnly Or (Me.bsRental.Count = 0 And Not blnIsNewRow)

        clsGlobal.SetBackColor(True, Me.txbCaseNo)
        clsGlobal.SetBackColor(True, Me.txbPackageNm)
        clsGlobal.SetBackColor(True, Me.cboRentalCaseType)
        clsGlobal.SetBackColor(True, Me.txbSupplierNm)
        clsGlobal.SetBackColor(True, Me.txbAddress)
        clsGlobal.SetBackColor(True, Me.txbInstructionDt)
        clsGlobal.SetBackColor(True, Me.cboRequestStatus)
        clsGlobal.SetBackColor(True, Me.txbSupplierAddress)
        clsGlobal.SetBackColor(True, Me.txbNoDaysCovered)

        Me.txbDismissedDt.ReadOnly = blnReadOnly Or IsDate(Me.txbDismissedDt.Text)
        clsGlobal.SetBackColor(Me.txbDismissedDt.ReadOnly, Me.txbDismissedDt)

        Me.dgvInstructionDetail.Columns("SSV_SERVICE_CD_SVC").ReadOnly = True
        Me.dgvInstructionDetail.Columns("PURCHASE_ORDER_NO").ReadOnly = True
        Me.dgvInstructionDetail.Columns("WORKS_DESC_DS").ReadOnly = True
        Me.dgvInstructionDetail.Columns("CANCEL_DT").ReadOnly = True
    End Sub

    Private Sub pbxCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxCopy.Click
        clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
    End Sub

    Private Sub pbxCopy_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxCopy.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxCopy_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxCopy.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub


    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                    Case "tsbCostSaving"
                        Dim frmX As Object
                        For Each frmX In Application.OpenForms
                            If frmX.Name = "frmMainTabMenu" Then
                                clsGlobal.CopyToFormStripFromGrid(Me.txbCaseNo.Text, Me.pnlFormStrip)
                                frmX.LoadForm("Case Cost Savings", Nothing)
                                Me.Close()
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspRight_ItemClicked")
        End Try
    End Sub

    Public Sub EnableBindingNavigators(ByVal cncX As Object)

        Dim ctlX As Control
        For Each ctlX In cncX
            If TypeOf ctlX Is Panel Then
                EnableBindingNavigators(ctlX.Controls)
            ElseIf TypeOf ctlX Is BindingNavigator Then
                If Not ctlX.Enabled Then ctlX.Enabled = True
            End If
        Next
    End Sub


    Private Sub pbxDismissedDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxDismissedDt.Click

        If Not Me.txbDismissedDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbDismissedDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxDismissedDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxDismissedDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxDismissedDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxDismissedDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub lnlTAndP_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnlTAndP.LinkClicked

        'If Me.txbCaseNo.Text <> "" And Me.txbSupplierNo.Text <> "" Then
        '    Dim frmX As New clsInstruction.frmTroubleAndPraiseUser
        '    frmX.SetOpeningParameters(CType(Me.txbSupplierNo.Text, Integer), CType(Me.txbCaseNo.Text, Integer))
        '    frmX.ShowDialog()
        '    frmX.Dispose()
        'End If

    End Sub


    Private Sub pbxFirstPaymentDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxFirstPaymentDt.Click

        If Not Me.txbFirstPaymentDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbFirstPaymentDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxFirstPaymentDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxFirstPaymentDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxFirstPaymentDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxFirstPaymentDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub tsbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAdd.Click

        blnIsNewRow = True
        ReadOnlyFields(blnReadOnly)
        sender.owner.Enabled = False

    End Sub

    Private Sub bsInstruction_PositionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bsInstruction.PositionChanged

        ReadOnlyFields(blnReadOnly)

    End Sub

    Private Sub pbxRentStartDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxRentStartDt.Click

        If Not Me.txbRentStartDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbRentStartDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxRentStartDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxRentStartDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxRentStartDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxRentStartDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxRentEndDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxRentEndDt.Click

        If Not Me.txbRentEndDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbRentEndDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxRentEndDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxRentEndDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxRentEndDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxRentEndDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxPasteAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxPasteAddress.Click

        If Not Me.txbAgentRef.ReadOnly Then Me.txbAgentRef.Text = Microsoft.VisualBasic.Left(Me.txbAddress.Text, 15)

    End Sub

    Private Sub txbPayments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txbPayments.TextChanged

        Me.txbPaymentsRemaining.Text = Me.txbPayments.Text

    End Sub

    Private Sub txbRentEndDt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txbRentEndDt.TextChanged

        If IsDate(Me.txbRentEndDt.Text) And IsDate(Me.txbRentStartDt.Text) Then
            Me.txbNoDaysCovered.Text = DateDiff(DateInterval.DayOfYear, CType(Me.txbRentStartDt.Text, Date), CType(Me.txbRentEndDt.Text, Date))
        End If
    End Sub

    Private Sub lnkCancel_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCancel.LinkClicked
        If Me.dgvInstructionDetail.Rows.Count > 0 Then
            Me.dgvInstructionDetail.CurrentRow.Cells("CANCEL_DT").Value = Date.Now.ToShortDateString
        End If
    End Sub

    Private Sub HidePrimaryKeyFields()
        Me.txtVIP.Hide()
    End Sub
End Class