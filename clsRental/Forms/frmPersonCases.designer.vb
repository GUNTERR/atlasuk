<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPersonCases
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CustomerLabel As System.Windows.Forms.Label
        Dim CASE_NOLabel As System.Windows.Forms.Label
        Dim FULL_NMLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPersonCases))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.V_fsubPackageDataGridView = New System.Windows.Forms.DataGridView()
        Me.CASENODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboRentalCaseType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClientPackageDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TenantDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RELATEDCASENODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsPersonCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsPersonCases = New clsRental.dsPersonCases()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtCaseNo = New ControlLibrary.SuperTextBox()
        Me.txtRelatedCase = New System.Windows.Forms.TextBox()
        Me.txtClientNm = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.taCase = New clsRental.dsPersonCasesTableAdapters.V_frmPersonCaseTableAdapter()
        Me.taPersonCase = New clsRental.dsPersonCasesTableAdapters.V_fsubPersonCaseTableAdapter()
        CustomerLabel = New System.Windows.Forms.Label()
        CASE_NOLabel = New System.Windows.Forms.Label()
        FULL_NMLabel = New System.Windows.Forms.Label()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.V_fsubPackageDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPersonCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPersonCases, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CustomerLabel
        '
        CustomerLabel.AutoSize = True
        CustomerLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        CustomerLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CustomerLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CustomerLabel.Location = New System.Drawing.Point(171, 23)
        CustomerLabel.Name = "CustomerLabel"
        CustomerLabel.Size = New System.Drawing.Size(53, 14)
        CustomerLabel.TabIndex = 91
        CustomerLabel.Text = "Customer"
        '
        'CASE_NOLabel
        '
        CASE_NOLabel.AutoSize = True
        CASE_NOLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        CASE_NOLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CASE_NOLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CASE_NOLabel.Location = New System.Drawing.Point(22, 22)
        CASE_NOLabel.Name = "CASE_NOLabel"
        CASE_NOLabel.Size = New System.Drawing.Size(48, 14)
        CASE_NOLabel.TabIndex = 81
        CASE_NOLabel.Text = "Case No"
        '
        'FULL_NMLabel
        '
        FULL_NMLabel.AutoSize = True
        FULL_NMLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        FULL_NMLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FULL_NMLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        FULL_NMLabel.Location = New System.Drawing.Point(419, 23)
        FULL_NMLabel.Name = "FULL_NMLabel"
        FULL_NMLabel.Size = New System.Drawing.Size(63, 14)
        FULL_NMLabel.TabIndex = 96
        FULL_NMLabel.Text = "Client Name"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(135, 14)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 190
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(760, 24)
        Me.pnlMainToolStrip.TabIndex = 127
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(717, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 10
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(302, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.PictureBox6)
        Me.Panel5.Controls.Add(Me.PictureBox5)
        Me.Panel5.Controls.Add(Me.Panel4)
        Me.Panel5.Controls.Add(Me.V_fsubPackageDataGridView)
        Me.Panel5.Location = New System.Drawing.Point(8, 147)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(743, 228)
        Me.Panel5.TabIndex = 125
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(707, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(687, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Person Cases"
        '
        'V_fsubPackageDataGridView
        '
        Me.V_fsubPackageDataGridView.AllowUserToAddRows = False
        Me.V_fsubPackageDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.V_fsubPackageDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.V_fsubPackageDataGridView.AutoGenerateColumns = False
        Me.V_fsubPackageDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.V_fsubPackageDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.V_fsubPackageDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.V_fsubPackageDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.V_fsubPackageDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.V_fsubPackageDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CASENODataGridViewTextBoxColumn, Me.cboRentalCaseType, Me.ClientPackageDataGridViewTextBoxColumn, Me.TenantDataGridViewTextBoxColumn, Me.ACTUALMOVEINDTDataGridViewTextBoxColumn, Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.RELATEDCASENODataGridViewTextBoxColumn})
        Me.V_fsubPackageDataGridView.DataSource = Me.bsPersonCase
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.V_fsubPackageDataGridView.DefaultCellStyle = DataGridViewCellStyle9
        Me.V_fsubPackageDataGridView.EnableHeadersVisualStyles = False
        Me.V_fsubPackageDataGridView.Location = New System.Drawing.Point(8, 32)
        Me.V_fsubPackageDataGridView.Name = "V_fsubPackageDataGridView"
        Me.V_fsubPackageDataGridView.ReadOnly = True
        Me.V_fsubPackageDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.V_fsubPackageDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.V_fsubPackageDataGridView.RowHeadersWidth = 18
        Me.V_fsubPackageDataGridView.RowTemplate.Height = 20
        Me.V_fsubPackageDataGridView.Size = New System.Drawing.Size(724, 180)
        Me.V_fsubPackageDataGridView.TabIndex = 76
        '
        'CASENODataGridViewTextBoxColumn
        '
        Me.CASENODataGridViewTextBoxColumn.DataPropertyName = "CASE_NO"
        Me.CASENODataGridViewTextBoxColumn.HeaderText = "Case No"
        Me.CASENODataGridViewTextBoxColumn.Name = "CASENODataGridViewTextBoxColumn"
        Me.CASENODataGridViewTextBoxColumn.ReadOnly = True
        Me.CASENODataGridViewTextBoxColumn.Width = 60
        '
        'cboRentalCaseType
        '
        Me.cboRentalCaseType.DataPropertyName = "PROP_OR_PSN_CD"
        Me.cboRentalCaseType.HeaderText = "Type"
        Me.cboRentalCaseType.Name = "cboRentalCaseType"
        Me.cboRentalCaseType.ReadOnly = True
        Me.cboRentalCaseType.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cboRentalCaseType.Width = 120
        '
        'ClientPackageDataGridViewTextBoxColumn
        '
        Me.ClientPackageDataGridViewTextBoxColumn.DataPropertyName = "ClientPackage"
        Me.ClientPackageDataGridViewTextBoxColumn.HeaderText = "Client Package"
        Me.ClientPackageDataGridViewTextBoxColumn.Name = "ClientPackageDataGridViewTextBoxColumn"
        Me.ClientPackageDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClientPackageDataGridViewTextBoxColumn.Width = 290
        '
        'TenantDataGridViewTextBoxColumn
        '
        Me.TenantDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TenantDataGridViewTextBoxColumn.DataPropertyName = "Tenant"
        Me.TenantDataGridViewTextBoxColumn.HeaderText = "Tenant"
        Me.TenantDataGridViewTextBoxColumn.Name = "TenantDataGridViewTextBoxColumn"
        Me.TenantDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ACTUALMOVEINDTDataGridViewTextBoxColumn
        '
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.DataPropertyName = "ACTUAL_MOVE_IN_DT"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.HeaderText = "Actual Move In Dt"
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.Name = "ACTUALMOVEINDTDataGridViewTextBoxColumn"
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.Width = 120
        '
        'CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'RELATEDCASENODataGridViewTextBoxColumn
        '
        Me.RELATEDCASENODataGridViewTextBoxColumn.DataPropertyName = "RELATED_CASE_NO"
        Me.RELATEDCASENODataGridViewTextBoxColumn.HeaderText = "RELATED_CASE_NO"
        Me.RELATEDCASENODataGridViewTextBoxColumn.Name = "RELATEDCASENODataGridViewTextBoxColumn"
        Me.RELATEDCASENODataGridViewTextBoxColumn.ReadOnly = True
        Me.RELATEDCASENODataGridViewTextBoxColumn.Visible = False
        '
        'bsPersonCase
        '
        Me.bsPersonCase.DataMember = "V_frmPersonCase_V_fsubPersonCase"
        Me.bsPersonCase.DataSource = Me.bsCase
        '
        'bsCase
        '
        Me.bsCase.AllowNew = False
        Me.bsCase.DataMember = "V_frmPersonCase"
        Me.bsCase.DataSource = Me.DsPersonCases
        '
        'DsPersonCases
        '
        Me.DsPersonCases.DataSetName = "dsPersonCases"
        Me.DsPersonCases.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox7)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(8, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(743, 109)
        Me.Panel1.TabIndex = 133
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(707, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 79
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.Panel3.Controls.Add(Me.pbxVIP)
        Me.Panel3.Controls.Add(Me.txtCaseNo)
        Me.Panel3.Controls.Add(CustomerLabel)
        Me.Panel3.Controls.Add(Me.txtRelatedCase)
        Me.Panel3.Controls.Add(CASE_NOLabel)
        Me.Panel3.Controls.Add(FULL_NMLabel)
        Me.Panel3.Controls.Add(Me.txtClientNm)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(724, 62)
        Me.Panel3.TabIndex = 1
        '
        'txtCaseNo
        '
        Me.txtCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CASE_NO", True))
        Me.txtCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtCaseNo.Location = New System.Drawing.Point(76, 19)
        Me.txtCaseNo.Name = "txtCaseNo"
        Me.txtCaseNo.PreviousQuery = Nothing
        Me.txtCaseNo.Queryable = True
        Me.txtCaseNo.QueryMandatory = False
        Me.txtCaseNo.ReadOnly = True
        Me.txtCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txtCaseNo.TabIndex = 1
        Me.txtCaseNo.Updateable = False
        '
        'txtRelatedCase
        '
        Me.txtRelatedCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtRelatedCase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "Customer", True))
        Me.txtRelatedCase.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRelatedCase.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtRelatedCase.Location = New System.Drawing.Point(228, 19)
        Me.txtRelatedCase.Name = "txtRelatedCase"
        Me.txtRelatedCase.ReadOnly = True
        Me.txtRelatedCase.Size = New System.Drawing.Size(185, 20)
        Me.txtRelatedCase.TabIndex = 3
        '
        'txtClientNm
        '
        Me.txtClientNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtClientNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "ClientName", True))
        Me.txtClientNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClientNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtClientNm.Location = New System.Drawing.Point(488, 19)
        Me.txtClientNm.Name = "txtClientNm"
        Me.txtClientNm.ReadOnly = True
        Me.txtClientNm.Size = New System.Drawing.Size(214, 20)
        Me.txtClientNm.TabIndex = 97
        Me.txtClientNm.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.txtVIP)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(687, 24)
        Me.Panel2.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(352, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 185
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(0, 5)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 14)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Case"
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taPersonCase
        '
        Me.taPersonCase.ClearBeforeFill = True
        '
        'frmPersonCases
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gold
        Me.ClientSize = New System.Drawing.Size(760, 384)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.Panel5)
        Me.KeyPreview = True
        Me.Name = "frmPersonCases"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Active Person Cases"
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.V_fsubPackageDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPersonCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPersonCases, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents V_fsubPackageDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DsPersonCases As clsRental.dsPersonCases
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents txtRelatedCase As System.Windows.Forms.TextBox
    Friend WithEvents txtClientNm As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents taCase As clsRental.dsPersonCasesTableAdapters.V_frmPersonCaseTableAdapter
    Friend WithEvents bsPersonCase As System.Windows.Forms.BindingSource
    Friend WithEvents taPersonCase As clsRental.dsPersonCasesTableAdapters.V_fsubPersonCaseTableAdapter
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents CASENODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboRentalCaseType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClientPackageDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TenantDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACTUALMOVEINDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RELATEDCASENODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
