Imports Microsoft.Office.Interop
Imports System.IO
Imports System.Data.SqlClient

Public Class frmMaintainRentalProperty
    Private dsLookups As New Data.DataSet
    Private strLookupFile As String
    Dim bsiX As New clsBindingSourceItem
    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Dim mfdcFormDatatableCollection As New clnFormDatatable
    Dim mcbcComboSourceCollection As New clnComboBoxSource
    Dim mdstForm As New DataSet
    Dim mblnQueryableForm As Boolean
    Dim mblnEditableForm As Boolean
    Dim blnFinalAgreedClaimAmChange As Boolean = False
    Dim decFinalAgreedClaimAm As Decimal = 0

    Dim intTenancyHistoryRows As Integer = 0
    Dim blnLoaded As Boolean = False
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property

    'Dim mblnEmpRentContributionEmpty As Boolean
    'Private Property EmpRentContributionEmpty() As Boolean
    '    Get
    '        Return mblnEmpRentContributionEmpty
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnEmpRentContributionEmpty = value
    '    End Set
    'End Property
    Private Sub InitialiseForm()

        ' Specify form master dataset

        mfdcFormDatatableCollection.Clear()
        mcbcComboSourceCollection.Clear()

        ' **** Specify dataset here ****************
        mdstForm = Me.DsMaintainRentalProperty

        ' **** Modify these below for each data table ****************
        ' Specify all datasets for the form
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsMaintainRentalProperty.V_frmMaintainRentalProperty, _
                Me.bsCase, Me.taCase, Nothing, Me.pnlCase, False, False, False, True))
        Me.taAsset.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsMaintainRentalProperty.V_fsubMaintainRentalPropDetail, _
                Me.bsAsset, Me.taAsset, Nothing, Me.pnlAsset, True, True, False, False))
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsMaintainRentalProperty.V_vrtLettingAgent, _
                Me.bsAgent, Me.taAgent, Nothing, Nothing, False, False, False, False))
        Me.taTenancy.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsMaintainRentalProperty.V_fsubMaintainRentlPropTenancy, _
                Me.bsTenancy, Me.taTenancy, Me.dgvTenancy, Me.pnlTenancy, True, True, False, False))
        Me.taRent.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsMaintainRentalProperty.V_fsubMaintainRentalPropRent, _
                Me.bsRent, Me.taRent, Me.dgvRent, Me.pnlRent, True, True, False, False))

        ' Now define combo boxes - make sure first one sets boolean to reset counter.
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboPropertyStructure, "AST_PROP_STRUCT_CODE", True))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboPropertyType, "AST_PROPERTY_TYPE_CODE"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboFurnishingCode, "AST_FURNISH_CODE"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboManagementCode, "AST_MANAGE_CODE"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboLeaseType, "LEASE_TYPE_CODE"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboDeposit, "DEPOSIT_CD"))
        'mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboCancelType, "CPC_CANCELLATION_CODE"))

        ' Bind any supercheck boxes
        Me.chkFrontGarden.BindData("Checked", Me.bsAsset, "FRONT_GARDEN_IN")
        Me.chkRearGarden.BindData("Checked", Me.bsAsset, "REAR_GARDEN_IN")
        Me.chkGarage.BindData("Checked", Me.bsAsset, "GARAGE_IN")
        Me.chkPetsAllowed.BindData("Checked", Me.bsAsset, "PETS_ALLOWED_IN")
        Me.chkSmokersAllowed.BindData("Checked", Me.bsAsset, "SMOKERS_OK_IN")
        Me.chkChildrenAllowed.BindData("Checked", Me.bsAsset, "CHILDREN_ALLOWED_IN")
        Me.chkWaterMeter.BindData("Checked", Me.bsAsset, "WATER_METER_IN")
        Me.chkInterestBearing.BindData("Checked", Me.bsAsset, "INTEREST_ON_DEPOSIT_IN")
        Me.chkBreakClause.BindData("Checked", Me.bsAsset, "BREAK_CLAUSE_IN")
        Me.chkStandingOrder.BindData("Checked", Me.bsAsset, "STANDING_ORDER_SETUP_IN")


    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim fdtFormDatatable As clsFormDatatable

            InitialiseForm()

            For Each fdtFormDatatable In mfdcFormDatatableCollection
                With fdtFormDatatable
                    If .AllowAdd Or .AllowUpdate Or .AllowDelete Then
                        mblnEditableForm = True
                        bsiX.Add(.BindingSource)
                    End If
                    If .DataGridView IsNot Nothing Then
                        .DataGridView.AllowUserToAddRows = .AllowAdd
                        .DataGridView.AllowUserToDeleteRows = .AllowDelete
                    End If
                    If .QueryingDatatable Then mblnQueryableForm = True
                End With
            Next

            'blnReadOnly = clsGlobal.SetupForm(Me, Not mblnQueryableForm, Not mblnEditableForm)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, Not mblnQueryableForm, Not mblnEditableForm)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            If Not blnReadOnly Then blnReadOnly = Not mblnEditableForm

            ReadOnlyFields(blnReadOnly)
            Me.tsbAdd.Visible = Not blnReadOnly

            clsGlobal.CreateFormStripItems(Me, pnlFormStrip)

            strLookupFile = Me.Name + "LookUpsDataSet.xml"
            If File.Exists(clsGlobal.LookupFilePath + strLookupFile) Then
                dsLookups.ReadXml(clsGlobal.LookupFilePath + strLookupFile)
            Else
                LoadLookupLists()
            End If

            LoadAndBindComboBoxes()

            HidePrimaryKeyFields()

            Me.cboPropertyStructure.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboDeposit.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboFurnishingCode.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboLeaseType.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboManagementCode.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboPropertyType.DropDownStyle = ComboBoxStyle.DropDown

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                FillForm()
            Else    ' Start in query mode - can be removed if required
                RunQuery()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormLoad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, mdstForm, e, blnIsNewRow)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormClosing", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DefineQuery(e.KeyValue, blnShiftPressed, Me)
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "KeyDown", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                strQueryText = CType(clsGlobal.ToolStripCaseId, String)
            End If

            If txbCaseNo.Text <> "" Then
                If IsNumeric(txbCaseNo.Text) And CType(txbCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txbCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taCase.Connection = clsGlobal.Connection
                Me.taCase.Fill(Me.DsMaintainRentalProperty.V_frmMaintainRentalProperty, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.txbCaseNo.Text <> "" Then
                    Me.taAsset.Connection = clsGlobal.Connection
                    Me.taAsset.Fill(Me.DsMaintainRentalProperty.V_fsubMaintainRentalPropDetail, CType(strQueryText, Integer))
                    decFinalAgreedClaimAm = 0
                    blnFinalAgreedClaimAmChange = False
                    FillLowerForm(CType(strQueryText, Integer))
                Else
                    MessageBox.Show("No records returned", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ReadOnlyFields(True)
                End If
            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "FillForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub
    Private Sub FillLowerForm(ByVal intCaseNo As Integer)
        Dim dteStartDt As Date = Date.Now
        Try
            Me.taAgent.Connection = clsGlobal.Connection
            Me.taAgent.Fill(Me.DsMaintainRentalProperty.V_vrtLettingAgent, intCaseNo)
            Me.taTenancy.Connection = clsGlobal.Connection
            Me.taTenancy.Fill(Me.DsMaintainRentalProperty.V_fsubMaintainRentlPropTenancy, intCaseNo)
            Me.taRent.Connection = clsGlobal.Connection
            Me.taRent.Fill(Me.DsMaintainRentalProperty.V_fsubMaintainRentalPropRent, intCaseNo)
            ReadOnlyFields(blnReadOnly)

            ' store the count of tenancy rows when the form is first loaded
            If Not blnLoaded Then
                intTenancyHistoryRows = dgvTenancy.Rows.Count
                blnLoaded = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillLowerForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillLowerForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try


    End Sub
    Private Sub ExitQueryMode()
        Try
            If mblnQueryableForm And blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                HidePrimaryKeyFields()
                FillForm()
            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "ExitQueryMode", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DefineQuery", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            If mblnQueryableForm Then
                ExitQueryMode()
                FillForm()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "EscapeQueryMode", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            Me.ErrorProvider1.Clear()
            If mblnQueryableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                If blnIsNewRow Then
                    UndoForm()
                    ReadOnlyFields(False)
                    blnIsNewRow = False
                End If
                blnInQueryMode = True

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    If fdtFormDatatable.QueryingDatatable AndAlso fdtFormDatatable.Datatable IsNot Nothing Then
                        fdtFormDatatable.Datatable.Clear()
                    End If
                Next

                clsGlobal.EnterQueryMode(Me, blnReadOnly)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "RunQuery", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Public Function UpdateBaseTables(ByVal blnSilent As Boolean) As Boolean
        If mdstForm.HasChanges Then
            Dim fdtFormDatatable As clsFormDatatable

            Dim intChanges As Integer
            Try
                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .Datatable.GetChanges IsNot Nothing Then
                            .TableAdapter.Update(.Datatable.GetChanges)
                            intChanges += .Datatable.GetChanges.Rows.Count
                        End If
                    End With
                Next

                If intChanges > 0 And Not blnSilent Then
                    Dim strMsg As String = intChanges.ToString & " record" & IIf(intChanges <> 1, "s", "") & " added/altered/deleted"
                    MessageBox.Show(strMsg, Me.Text & " Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                Return True
            Catch exc As Exception
                If Not blnSilent Then MessageBox.Show(exc.Message, "Database Updates Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            End Try
        Else
            If Not blnSilent Then MessageBox.Show("There are no data updates to save.", "Save Requested Without Updates", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If
    End Function

    Private Sub SaveForm(Optional ByVal blnSilent As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            If mblnEditableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                ' **** Needed for tables with generated numbers ***************
                'If blnIsNewRow Then
                '    Me.txbPersonNo.Text = clsGlobal.GetGeneratedNo("PERSON")
                'End If

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .DataGridView IsNot Nothing Then
                            ' This fixes a bug where user clicks on the new record and saves - it thinks user has edited
                            If Not .DataGridView.IsCurrentCellDirty And Not .DataGridView.IsCurrentRowDirty Then
                                .BindingSource.CancelEdit()
                            End If
                            .DataGridView.EndEdit()
                        End If
                        If .BindingSource IsNot Nothing Then
                            .BindingSource.EndEdit()
                        End If
                    End With
                Next

                'StandingOrderReminder()

                'Dim a As DataGridViewRow
                'For Each a In Me.dgvTenancy.Rows
                '    If Not a.IsNewRow Then
                '        'MsgBox(a.Cells("MONTHLYRENTAMDataGridViewTextBoxColumn").Value.ToString)
                '        If CType(Me.txtTenancyBudget.Text, Decimal) < CType(a.Cells("MONTHLYRENTAMDataGridViewTextBoxColumn").Value.ToString, Decimal) Then
                '            MessageBox.Show("The Actual Monthly Rent exceeds the Rental Budget.", "Maintain Rental Property", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '        End If
                '    End If
                'Next

                'Dim strErrorMessage As String
                'strErrorMessage = modMaintainRentalProperty.CheckConstraint(Me)

                'If strErrorMessage = "" Then
                If UpdateBaseTables(blnSilent) Then
                    For Each fdtFormDatatable In mfdcFormDatatableCollection
                        fdtFormDatatable.Datatable.AcceptChanges()
                    Next
                    blnIsNewRow = False
                    EnableBindingNavigators(Me.Controls)
                    ReadOnlyFields(blnReadOnly)
                    FillLowerForm(CType(Me.txbCaseNo.Text, Integer))

                    If blnFinalAgreedClaimAmChange AndAlso Me.txtFinalAgreedClaimAm.Text <> "" AndAlso decFinalAgreedClaimAm <> CType(Me.txtFinalAgreedClaimAm.Text, Decimal) Then
                        If MessageBox.Show("Do you have the chance to record a cost saving?", "Cost Savings", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            clsGlobal.CopyToFormStripFromGrid(Me.txbCaseNo.Text, Me.pnlFormStrip)
                            Dim frmX As Object
                            For Each frmX In Application.OpenForms
                                If frmX.Name = "frmMainTabMenu" AndAlso clsGlobal.UserCanOpenForm("Case Cost Savings") Then
                                    frmX.LoadForm("Case Cost Savings", Nothing)
                                    Me.Close()
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                    ' warn about stamp duty
                    If (dgvTenancy.Rows.Count > intTenancyHistoryRows) Then

                        ' row(s) have been inserted so need to check if total rental exceeds �175,000

                        Dim startDate As DateTime
                        Dim endDate As DateTime
                        Dim rent As Decimal = 0
                        Dim months As Integer = 0

                        Dim totalRent As Decimal = 0
                        Dim lineRent As Decimal = 0

                        Dim valid As Boolean = True

                        For Each r As DataGridViewRow In dgvTenancy.Rows

                            Try
                                valid = True

                                Try
                                    Dim tmp As String = r.Cells("TENANCY_START_DT").Value.ToString()
                                    startDate = CDate(tmp)
                                Catch ex As Exception
                                    valid = False
                                End Try

                                Try
                                    ' endDate = r.Cells("TENANCY_END_DT").Value
                                    Dim tmp As String = r.Cells("TENANCYENDDTDataGridViewTextBoxColumn").Value.ToString()
                                    endDate = CDate(tmp)
                                Catch ex As Exception
                                    valid = False
                                End Try

                                Try
                                    If valid Then
                                        months = CInt(DateDiff(DateInterval.Month, startDate, endDate)) + 1
                                    End If
                                Catch ex As Exception
                                    valid = False
                                End Try

                                Try
                                    ' rent = r.Cells("MONTHLY_RENT_AM").Value
                                    Dim tmp As String = r.Cells("MONTHLYRENTAMDataGridViewTextBoxColumn").Value.ToString()
                                    rent = CDec(tmp)
                                Catch ex As Exception
                                    valid = False
                                End Try

                                If valid Then

                                    Try
                                        lineRent = rent * months
                                    Catch ex As Exception
                                        MessageBox.Show("Error calculating rental amount from Tenancy History", "Tenancy History Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        lineRent = 0.0
                                    End Try

                                    Try
                                        totalRent = totalRent + lineRent
                                    Catch ex As Exception
                                        MessageBox.Show("Error calculating total rental amount from Tenancy History", "Tenancy History Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End Try

                                End If

                                ' MsgBox("days " + days.ToString() + " start " + startDate.ToString() + " end " + endDate.ToString() + " months " + months.ToString() + " rent " + rent.ToString() + " line rent " + lineRent.ToString() + " total " + totalRent.ToString())
                                'MsgBox(totalRent.ToString())
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try

                        Next

                        If totalRent > 125000 Then
                            ' update rowcount stored so we don't warn user again (unless
                            ' they add more rows
                            intTenancyHistoryRows = dgvTenancy.Rows.Count
                            MessageBox.Show("You must remember to pay Stamp Duty if " + System.Environment.NewLine + "the total rent exceeds �125,000", "Remember to pay Stamp Duty", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                            Dim EmailRecipient As String = ""

                            '--------------------------------------------
                            ' if TEST use IT email address
                            '--------------------------------------------
                            Dim ITEmailAddress As String = "richard.gunter@cartus.com"
                            'If clsGlobal.Connection.ConnectionString.ToLower().Contains("topwin_test") Then
                            '    ' code is running in TEST
                            '    EmailRecipient = ITEmailAddress
                            'Else
                            ' production
                            ' if no email address found, default to IT email address
                            EmailRecipient = GetRentalCounsellorEmail(txbCaseNo.Text, ITEmailAddress)
                            'End If

                            Dim mailOutlook As Outlook.MailItem
                            Dim appOutlook As New Outlook.Application()
                            mailOutlook = appOutlook.CreateItem(Outlook.OlItemType.olMailItem)

                            mailOutlook.Recipients.Add(EmailRecipient)

                            mailOutlook.Subject = "Case " & txbCaseNo.Text & " - stamp duty may need to be paid"

                            Dim body As String = "<THIS IS AN AUTOMATED MESSAGE. PLEASE DO NOT REPLY>" + System.Environment.NewLine + System.Environment.NewLine
                            body += "CASE " + txbCaseNo.Text + System.Environment.NewLine
                            body += "---------------------------------------------" + System.Environment.NewLine
                            body += "A new Tenancy History row has been added which may take the rent above �125,000." + System.Environment.NewLine + System.Environment.NewLine
                            body += "You must remember to pay Stamp Duty if the total rent exceeds �125,000. " + System.Environment.NewLine + System.Environment.NewLine
                            body += "Atlas UK"
                            mailOutlook.Body = body

                            ' mailOutlook.Display()

                            mailOutlook.Send()

                        End If

                    End If

                Else
                    Return
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "SaveForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Public Function GetRentalCounsellorEmail(ByVal caseNo As String, ByVal defaultEmail As String) As String

        Dim email As String = ""
        Dim cmd As New SqlCommand
        Dim par As SqlParameter
        Dim ds As New DataSet
        Dim da As SqlDataAdapter

        Try

            cmd.CommandText = "maarten.OMNI_CASE_PERSON_EMAIL"
            cmd.CommandType = CommandType.StoredProcedure
            par = New SqlParameter("@CaseNo", SqlDbType.Int)
            par.Value = caseNo
            cmd.Parameters.Add(par)
            par = New SqlParameter("@PersonRole", SqlDbType.VarChar, 4)
            par.Value = "RENC"
            cmd.Parameters.Add(par)

            cmd.Connection = clsGlobal.Connection
            da = New SqlDataAdapter(cmd)
            da.Fill(ds)

            email = ds.Tables(0).Rows(0)("Email").ToString()

            If email.Length > 0 Then
                Return email
            Else
                Return defaultEmail
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error obtaining Email Address of Rental Counsellor ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            If email.Length > 0 Then
                Return email
            Else
                Return defaultEmail
            End If

        End Try

    End Function

    Private Sub LoadLookupLists()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim cbcComboSource As clsComboBoxSource, strSQL As String = ""
            Dim aArray(mcbcComboSourceCollection.Count - 1) As String, i As Integer = 0

            For Each cbcComboSource In mcbcComboSourceCollection
                strSQL += "exec maarten.OMNI_FILL_COMBO '" & cbcComboSource.Domain & "';"
                aArray(i) = cbcComboSource.LookupListName
                i += 1
            Next

            If i > 0 Then clsGlobal.LoadLookupLists(strSQL, strLookupFile, dsLookups, aArray)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadLookupLists", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "LoadLookupLists", dteStartDt, Date.Now, Nothing)
        End Try
    End Sub

    Private Sub LoadAndBindComboBoxes()
        Try
            Dim cbcComboSource As clsComboBoxSource

            For Each cbcComboSource In mcbcComboSourceCollection
                With cbcComboSource.ComboBoxControl
                    .DataSource = dsLookups.Tables(cbcComboSource.LookupListName)
                    .DisplayMember = "PROMPT"
                    .ValueMember = "COLUMN_VALUE"
                End With
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadAndBindComboBoxes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspMain_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub UndoForm()
        If mblnEditableForm Then
            clsGlobal.UndoForm(Me, bsiX, mdstForm)
            If blnIsNewRow Then blnIsNewRow = False
            EnableBindingNavigators(Me.Controls)
            ReadOnlyFields(blnReadOnly)
        End If
    End Sub


    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
                Case Keys.F8 'Copy to number field
                    clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
                    intKeyCode = 0
                Case Keys.F9 'Clear number field
                    clsGlobal.ClickClearCase(Me.pnlFormStrip)
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DataControlKey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub BindingNavigatorAddNewItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAdd.Click
        blnIsNewRow = True
        ReadOnlyFields(blnReadOnly)
        sender.owner.Enabled = False
    End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        Dim fdtFormDatatable As clsFormDatatable, blnPermanentReadOnly As Boolean

        For Each fdtFormDatatable In mfdcFormDatatableCollection
            With fdtFormDatatable
                blnPermanentReadOnly = Not .AllowAdd And Not .AllowUpdate And Not .AllowDelete
                If Not blnPermanentReadOnly Then blnPermanentReadOnly = blnReadOnly
                If .HostPanel IsNot Nothing Then clsGlobal.ReadOnlyFields(blnPermanentReadOnly, .HostPanel)
                If .DataGridView IsNot Nothing Then
                    .DataGridView.AllowUserToAddRows = .AllowAdd
                    .DataGridView.AllowUserToDeleteRows = .AllowDelete
                End If
            End With
        Next

        ' Extras specific to this form to be added here
        Me.txbCaseNo.ReadOnly = True
        Me.txbPackageNm.ReadOnly = True
        Me.txbAddress.ReadOnly = True
        Me.txbLettingAgent.ReadOnly = True
        Me.txbAgentAddress.ReadOnly = True
        Me.txbAgentContact.ReadOnly = True
        clsGlobal.SetBackColor(True, Me.txbCaseNo)
        clsGlobal.SetBackColor(True, Me.txbPackageNm)
        clsGlobal.SetBackColor(True, Me.txbAddress)
        clsGlobal.SetBackColor(True, Me.txbLettingAgent)
        clsGlobal.SetBackColor(True, Me.txbAgentAddress)
        clsGlobal.SetBackColor(True, Me.txbAgentContact)

        If Me.bsAsset.Count = 0 And Not blnIsNewRow Then clsGlobal.ReadOnlyFields(True, Me.pnlAsset)
        Me.tsbAdd.Enabled = blnReadOnly Or (Me.bsAsset.Count = 0 And Not blnIsNewRow)

        Me.dgvTenancy.Columns("TENANCY_DURATION_DAYS_QT").ReadOnly = True
        Me.dgvRent.Columns("Freq").ReadOnly = True

        Me.txtEmpRentContribution.ReadOnly = True
        clsGlobal.SetBackColor(True, Me.txtEmpRentContribution)
        Me.txtTenancyBudget.ReadOnly = True
        clsGlobal.SetBackColor(True, Me.txtTenancyBudget)
        Me.chkStandingOrder.Enabled = False
        clsGlobal.SetBackColor(False, Me.chkStandingOrder)

        Me.txtTenancyCompleteDt.Enabled = True
        clsGlobal.SetBackColor(True, Me.txtTenancyCompleteDt)

        Me.lnlCompleteTenancy.Enabled = Not blnReadOnly

    End Sub

    Private Sub pbxCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxCopy.Click
        clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
    End Sub

    Private Sub pbxCopy_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxCopy_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorDefault(Me)
    End Sub


    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                    Case "tsbCostSaving"
                        Dim frmX As Object
                        For Each frmX In Application.OpenForms
                            If frmX.Name = "frmMainTabMenu" Then
                                clsGlobal.CopyToFormStripFromGrid(Me.txbCaseNo.Text, Me.pnlFormStrip)
                                frmX.LoadForm("Case Cost Savings", Nothing)
                                Me.Close()
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspRight_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub EnableBindingNavigators(ByVal cncX As Object)

        Dim ctlX As Control
        For Each ctlX In cncX
            If TypeOf ctlX Is Panel Then
                EnableBindingNavigators(ctlX.Controls)
            ElseIf TypeOf ctlX Is BindingNavigator Then
                If Not ctlX.Enabled Then ctlX.Enabled = True
            End If
        Next
    End Sub

    Private Sub pbxFormReceivedDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxFormReceivedDt.Click

        If Not Me.txbFormReceivedDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbFormReceivedDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxFormReceivedDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxFormReceivedDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxFormReceivedDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxFormReceivedDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxDepositReturnedDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxDepositReturnedDt.Click

        If Not Me.txbDepositReturnedDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbDepositReturnedDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxDepositReturnedDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxDepositReturnedDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxDepositReturnedDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxDepositReturnedDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub lnlTAndP_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnlTAndP.LinkClicked

        If Me.dgvTenancy.Rows.Count > 0 Then
            Dim rowX As DataGridViewRow
            rowX = Me.dgvTenancy.CurrentRow
            If rowX IsNot Nothing Then
                Dim frmX As New fdlgTenancyHistory
                frmX.SetOpeningParameters(rowX.Cells("AST_CASE_NO_CSE").Value, rowX.Cells("AST_CLIENT_PACKAGE_ID_CGS").Value, _
                        rowX.Cells("AST_GENERATED_NO_ADD").Value, rowX.Cells("TENANCY_START_DT").Value)
                frmX.ShowDialog()
                frmX.Dispose()
            End If
        End If

    End Sub

    Private Sub dgvTenancy_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvTenancy.DataError

        MessageBox.Show(e.Exception.Message, "Tenancy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    End Sub

    Private Sub dgvRent_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvRent.DataError

        MessageBox.Show(e.Exception.Message, "Rent", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    End Sub

    Private Sub bsCase_PositionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bsCase.PositionChanged

        ReadOnlyFields(blnReadOnly)
        If Me.txtFinalAgreedClaimAm.Text <> "" Then
            decFinalAgreedClaimAm = CType(Me.txtFinalAgreedClaimAm.Text, Decimal)
        End If
    End Sub

    Private Sub txtFinalAgreedClaimAm_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFinalAgreedClaimAm.Enter
        If Me.txtFinalAgreedClaimAm.Text <> "" Then
            decFinalAgreedClaimAm = CType(Me.txtFinalAgreedClaimAm.Text, Decimal)
        End If
        blnFinalAgreedClaimAmChange = True
    End Sub

   
    Private Sub lnlCompleteTenancy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnlCompleteTenancy.LinkClicked
        Dim dteStartDt As Date = Date.Now
        Try
            If Me.txbCaseNo.Text <> "" Then
                'If IsDate(Me.txbActualMoveOutDt.Text) Then
                If Not IsDate(Me.txtTenancyCompleteDt.Text) Then
                    If MessageBox.Show("Are you sure you want to complete the tenancy? Once the tenancy is complete the monthly management fees will not be generated.", "Complete Tenancy", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        'Me.txtTenancyCompleteDt.Text = Date.Now
                        Dim frmX As New fdlgCompleteTenancy
                        frmX.SetOpeningParameters(Me.txtTenancyCompleteDt)
                        frmX.ShowDialog()

                        If frmX.DialogResult = Windows.Forms.DialogResult.OK Then
                            If IsDate(Me.txtTenancyCompleteDt.Text) Then
                                Me.Cursor = Cursors.WaitCursor
                                Dim commSQL As New SqlClient.SqlCommand
                                commSQL.CommandType = CommandType.StoredProcedure
                                commSQL.Connection = clsGlobal.Connection
                                commSQL.CommandText = "maarten.OMNI_COMPLETE_TENANCY"
                                commSQL.Parameters.Add("@CaseNo", SqlDbType.Int).Value = CType(Me.txbCaseNo.Text, Integer)
                                commSQL.Parameters.Add("@PackageId", SqlDbType.Int).Value = CType(Me.txtPackageId.Text, Integer)
                                commSQL.Parameters.Add("@TenancyCompeteDt", SqlDbType.DateTime).Value = CType(Me.txtTenancyCompleteDt.Text, Date)

                                commSQL.ExecuteNonQuery()
                                Me.Cursor = Cursors.Default

                                MessageBox.Show("The tenancy has now been completed.", "Complete Tenancy", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                'Me.taProperty.Connection = clsGlobal.Connection
                                'Me.taProperty.Fill(Me.DsSubtenantMaintenance.V_frmSubTenantMaintenance, CType(Me.txbCaseNo.Text, Integer))
                            Else
                                Dim strError As String = "Please enter a tenancy completion date."
                                MessageBox.Show(strError, "Complete Tenancy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Me.ErrorProvider1.SetError(Me.txtTenancyCompleteDt, strError)
                            End If
                        End If
                    End If
                Else
                    Dim strError As String = "The tenancy has already been completed."
                    MessageBox.Show(strError, "Complete Tenancy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.ErrorProvider1.SetError(Me.txtTenancyCompleteDt, strError)
                End If
                'Else
                '    Dim strError As String = "You cannot complete the tenancy until the tenant has moved out."
                '    MessageBox.Show(strError, "Complete Tenancy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Me.ErrorProvider1.SetError(Me.txbActualMoveOutDt, strError)
                'End If
                'Else
                '    Dim strError As String = "You cannot complete the tenancy because this case has no related property case."
                '    MessageBox.Show(strError, "Complete Tenancy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Me.ErrorProvider1.SetError(Me.txbPropertyCaseNo, strError)
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "btnCompleteTenancy_Click", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "btnCompleteTenancy_Click", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub
    Private Sub HidePrimaryKeyFields()
        Me.txtPackageId.Hide()
        Me.txtAddressAstType.Hide()
        Me.txtVIP.Hide()
    End Sub

    Private Sub pbxLetter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxLetter.Click
        'Dim letX As clsLetters.clsLetter
        'If Me.txbCaseNo.Text <> "" Then
        '    If Me.bsTenancy.Count > 0 Then
        '        letX = New clsLetters.clsLetter(CType(Me.txbCaseNo.Text, Integer), _
        '                CType(Me.txtPackageId.Text, Integer), _
        '                Me.txtAddressAstType.Text, 1, _
        '                "frmMaintainRentalProperty", False)
        '        letX.StartLetter(Me)
        '    Else
        '        MsgBox("A valid tenancy must be present to select Letters.", MsgBoxStyle.Exclamation, "Letters System")
        '    End If
        'Else
        '    MsgBox("A valid case must be present to select Letters.", MsgBoxStyle.Exclamation, "Letters System")
        'End If
    End Sub

    Private Sub pbxLetter_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxLetter.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxLetter_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxLetter.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub
End Class