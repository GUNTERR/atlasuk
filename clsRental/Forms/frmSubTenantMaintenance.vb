Imports System.IO

Public Class frmSubTenantMaintenance
    Private dsLookups As New Data.DataSet
    Private strLookupFile As String
    Dim bsiX As New clsBindingSourceItem
    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Dim mfdcFormDatatableCollection As New clnFormDatatable
    Dim mcbcComboSourceCollection As New clnComboBoxSource
    Dim mdstForm As New DataSet, mblnQueryableForm As Boolean, mblnEditableForm As Boolean
    Dim mblnEmpRentContributionEmpty As Boolean
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Private Property EmpRentContributionEmpty() As Boolean
        Get
            Return mblnEmpRentContributionEmpty
        End Get
        Set(ByVal value As Boolean)
            mblnEmpRentContributionEmpty = value
        End Set
    End Property
    Private Sub InitialiseForm()

        ' Specify form master dataset

        mfdcFormDatatableCollection.Clear()
        mcbcComboSourceCollection.Clear()

        ' **** Specify dataset here ****************
        mdstForm = Me.DsSubtenantMaintenance

        ' **** Modify these below for each data table ****************
        ' Specify all datasets for the form
        Me.taProperty.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsSubtenantMaintenance.V_frmSubTenantMaintenance, _
                Me.bsProperty, Me.taProperty, Nothing, Me.pnlProperty, False, True, False, True))
        Me.taSubtenant.Connection = clsGlobal.Connection
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsSubtenantMaintenance.V_fsubSubTenantMaintSubTenant, _
                Me.bsSubtenant, Me.taSubtenant, Nothing, Me.pnlSubtenant, False, True, False, False))

        ' Now define combo boxes - make sure first one sets boolean to reset counter.
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboBusinessLine, "BUSINESS_LINE_CD", True))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboRoleTitle, "ROLE_TITLE_CD"))
        mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboFamilySize, "FAMILY_SIZE_CD"))

        ' Bind any supercheck boxes


        ' **** Now modify FillForm, add any extras to ReadOnlyFields, generated number to SaveForm and specific customisation, e.g. code behind form.
        ' **** Oh, and don't forget tab order.

    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim fdtFormDatatable As clsFormDatatable

            InitialiseForm()

            For Each fdtFormDatatable In mfdcFormDatatableCollection
                With fdtFormDatatable
                    If .AllowAdd Or .AllowUpdate Or .AllowDelete Then
                        mblnEditableForm = True
                        bsiX.Add(.BindingSource)
                    End If
                    If .DataGridView IsNot Nothing Then
                        .DataGridView.AllowUserToAddRows = .AllowAdd
                        .DataGridView.AllowUserToDeleteRows = .AllowDelete
                    End If
                    If .QueryingDatatable Then mblnQueryableForm = True
                End With
            Next

            'blnReadOnly = clsGlobal.SetupForm(Me, Not mblnQueryableForm, Not mblnEditableForm)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, Not mblnQueryableForm, Not mblnEditableForm)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            If Not blnReadOnly Then blnReadOnly = Not mblnEditableForm

            ReadOnlyFields(blnReadOnly)
            ReadOnlyFields(True)

            clsGlobal.CreateFormStripItems(Me, pnlFormStrip)

            Me.chkGuidelinesChased.BindData("Checked", Me.bsProperty, "SUBTENANT_GUIDELINES_CHASED_TWICE_IN")
            Me.chkStandingOrder.BindData("Checked", Me.bsSubtenantDetails, "STANDING_ORDER_SETUP_IN")

            HidePrimaryKeyFields()

            Me.cboBusinessLine.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboRoleTitle.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboFamilySize.DropDownStyle = ComboBoxStyle.DropDown

            strLookupFile = Me.Name + "LookUpsDataSet.xml"
            If File.Exists(clsGlobal.LookupFilePath + strLookupFile) Then
                dsLookups.ReadXml(clsGlobal.LookupFilePath + strLookupFile)
            Else
                LoadLookupLists()
            End If

            LoadAndBindComboBoxes()

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                FillForm()
            Else    ' Start in query mode - can be removed if required
                RunQuery()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormLoad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, mdstForm, e, blnIsNewRow)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormClosing", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub HidePrimaryKeyFields()
        Me.txtPackageId.Hide()
        Me.txtVIP.Hide()
    End Sub
    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DefineQuery(e.KeyValue, blnShiftPressed, Me)
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "KeyDown", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                strQueryText = CType(clsGlobal.ToolStripCaseId, String)
            End If

            If txbCaseNo.Text <> "" Then
                If IsNumeric(txbCaseNo.Text) And CType(txbCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txbCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taProperty.Connection = clsGlobal.Connection
                Me.taProperty.Fill(Me.DsSubtenantMaintenance.V_frmSubTenantMaintenance, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.bsProperty.Count > 0 Then
                    Me.taSubtenant.Connection = clsGlobal.Connection
                    Me.taSubtenant.Fill(Me.DsSubtenantMaintenance.V_fsubSubTenantMaintSubTenant, CType(strQueryText, Integer))

                    Me.taSubtenantDetails.Connection = clsGlobal.Connection
                    Me.taSubtenantDetails.Fill(Me.DsSubtenantMaintenance.V_fsubSubTenantMaintenance)

                    clsGlobal.SubMenuCaseId = CType(Me.txbCaseNo.Text, Integer)
                    ReadOnlyFields(blnReadOnly)
                Else
                    MessageBox.Show("No records returned", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ReadOnlyFields(True)
                End If
            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "FillForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub ExitQueryMode()
        Try
            If mblnQueryableForm And blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                HidePrimaryKeyFields()
                FillForm()
            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "ExitQueryMode", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DefineQuery", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            If mblnQueryableForm Then
                ExitQueryMode()
                FillForm()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "EscapeQueryMode", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            If clsGlobal.FormIsDirty(Me, bsiX, Me.DsSubtenantMaintenance, blnIsNewRow) Then
                Dim strMsg As String = "Data has been changed. Click on OK to abandon changes and continue with your query, or click Cancel to return to " + _
                                        "the form where you can click on the Save button"

                If MessageBox.Show(strMsg, "Pending Updates Not Saved", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                    Exit Sub
                End If
            End If

            If mblnQueryableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                If blnIsNewRow Then
                    UndoForm()
                    ReadOnlyFields(False)
                    blnIsNewRow = False
                End If
                blnInQueryMode = True

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    If fdtFormDatatable.QueryingDatatable AndAlso fdtFormDatatable.Datatable IsNot Nothing Then
                        fdtFormDatatable.Datatable.Clear()
                    End If
                Next

                clsGlobal.EnterQueryMode(Me, blnReadOnly)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "RunQuery", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Function UpdateBaseTables(ByVal blnSilent As Boolean) As Boolean
        If Me.DsSubtenantMaintenance.HasChanges Then

            Dim modProperty As dsSubtenantMaintenance.V_frmSubTenantMaintenanceDataTable = _
                            CType(DsSubtenantMaintenance.V_frmSubTenantMaintenance.GetChanges(DataRowState.Modified), _
                            dsSubtenantMaintenance.V_frmSubTenantMaintenanceDataTable)

            Dim modSubtenant As dsSubtenantMaintenance.V_fsubSubTenantMaintSubTenantDataTable = _
                            CType(DsSubtenantMaintenance.V_fsubSubTenantMaintSubTenant.GetChanges(DataRowState.Modified), _
                            dsSubtenantMaintenance.V_fsubSubTenantMaintSubTenantDataTable)

            Dim newSubtenantDetails As dsSubtenantMaintenance.V_fsubSubTenantMaintenanceDataTable = _
                CType(DsSubtenantMaintenance.V_fsubSubTenantMaintenance.GetChanges(DataRowState.Added), _
                dsSubtenantMaintenance.V_fsubSubTenantMaintenanceDataTable)

            Dim modSubtenantDetails As dsSubtenantMaintenance.V_fsubSubTenantMaintenanceDataTable = _
                CType(DsSubtenantMaintenance.V_fsubSubTenantMaintenance.GetChanges(DataRowState.Modified), _
                dsSubtenantMaintenance.V_fsubSubTenantMaintenanceDataTable)

            Dim intChanges As Integer
            Try
                If Not modProperty Is Nothing Then
                    Me.taProperty.Connection = clsGlobal.Connection
                    Me.taProperty.Update(modProperty)
                    intChanges += modProperty.Count
                End If

                If Not newSubtenantDetails Is Nothing Then
                    Me.taSubtenantDetails.Connection = clsGlobal.Connection
                    Me.taSubtenantDetails.Update(newSubtenantDetails)
                    intChanges += newSubtenantDetails.Count
                End If

                If Not modSubtenantDetails Is Nothing Then
                    Me.taSubtenantDetails.Connection = clsGlobal.Connection
                    Me.taSubtenantDetails.Update(modSubtenantDetails)
                    intChanges += modSubtenantDetails.Count
                End If

                If Not modSubtenant Is Nothing Then
                    Me.taSubtenant.Connection = clsGlobal.Connection
                    Me.taSubtenant.Update(modSubtenant)
                    intChanges += modSubtenant.Count
                End If

                If intChanges > 0 And Not blnSilent Then
                    Dim strMsg As String = intChanges.ToString + " record added/altered/deleted"
                    MessageBox.Show(strMsg, Me.Text + " Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                Return True
            Catch exc As Exception
                If Not blnSilent Then MessageBox.Show(exc.Message, "Database Updates Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            Finally
                'Dispose the new objects
                If Not newSubtenantDetails Is Nothing Then
                    newSubtenantDetails.Dispose()
                End If
                If Not modSubtenantDetails Is Nothing Then
                    modSubtenantDetails.Dispose()
                End If
                If Not modSubtenant Is Nothing Then
                    modSubtenant.Dispose()
                End If
            End Try
        Else
            If Not blnSilent Then MessageBox.Show("There are no data updates to save.", "Save Requested Without Updates", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If

    End Function

    Private Sub SaveForm(Optional ByVal blnSilent As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            If Not blnReadOnly Then

                Me.bsSubtenant.EndEdit()
                Me.bsSubtenantDetails.EndEdit()
                Me.bsProperty.EndEdit()

                StandingOrderReminder()

                If Me.txtRentalBudget.Text <> "" Then
                    Dim dcmEmpRentContribution As Decimal = 0
                    Dim comSQL As New SqlClient.SqlCommand
                    comSQL.Connection = clsGlobal.Connection
                    comSQL.CommandType = CommandType.StoredProcedure
                    comSQL.CommandText = "maarten.OMNI_GET_TENANCY_HISTORY"

                    comSQL.Parameters.Add("@CaseNo", SqlDbType.Int).Value = Me.txbPropertyCaseNo.Text

                    Dim dcmMontlyRent As Decimal = 0

                    dcmMontlyRent = comSQL.ExecuteScalar()

                    If dcmMontlyRent > 0 Then
                        If Me.txtEmpRentContribution.Text = "" Then
                            dcmEmpRentContribution = 0
                        Else
                            dcmEmpRentContribution = CType(Me.txtEmpRentContribution.Text, Decimal)
                        End If

                        If (dcmEmpRentContribution + CType(Me.txtRentalBudget.Text, Decimal)) < dcmMontlyRent Then
                            MessageBox.Show("The Actual Monthly Rent of �" & dcmMontlyRent & " exceeds the Rental Budget + Employee Rent Contribution.", "Subtenant Maintenance", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                        If (dcmEmpRentContribution + CType(Me.txtRentalBudget.Text, Decimal)) > dcmMontlyRent Then
                            MessageBox.Show("The Rental Budget + Employee Rent Contribution exceeds the Actual Monthly Rent of �" & dcmMontlyRent, "Subtenant Maintenance", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                End If

                If UpdateBaseTables(blnSilent) Then
                    Me.DsSubtenantMaintenance.V_frmSubTenantMaintenance.AcceptChanges()
                    Me.DsSubtenantMaintenance.V_fsubSubTenantMaintSubTenant.AcceptChanges()
                    Me.DsSubtenantMaintenance.V_fsubSubTenantMaintenance.AcceptChanges()

                    blnIsNewRow = False
                    EnableBindingNavigators(Me.Controls)
                    ReadOnlyFields(blnReadOnly)
                Else
                    Return
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "SaveForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub LoadLookupLists()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim cbcComboSource As clsComboBoxSource, strSQL As String = ""
            Dim aArray(mcbcComboSourceCollection.Count - 1) As String, i As Integer = 0

            For Each cbcComboSource In mcbcComboSourceCollection
                strSQL += "exec maarten.OMNI_FILL_COMBO '" & cbcComboSource.Domain & "';"
                aArray(i) = cbcComboSource.LookupListName
                i += 1
            Next

            If i > 0 Then clsGlobal.LoadLookupLists(strSQL, strLookupFile, dsLookups, aArray)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadLookupLists", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "LoadLookupLists", dteStartDt, Date.Now, Nothing)
        End Try
    End Sub

    Private Sub LoadAndBindComboBoxes()
        Try
            Dim cbcComboSource As clsComboBoxSource

            For Each cbcComboSource In mcbcComboSourceCollection
                With cbcComboSource.ComboBoxControl
                    .DataSource = dsLookups.Tables(cbcComboSource.LookupListName)
                    .DisplayMember = "PROMPT"
                    .ValueMember = "COLUMN_VALUE"
                End With
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadAndBindComboBoxes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspMain_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub UndoForm()
        'If mblnEditableForm Then
        clsGlobal.UndoForm(Me, bsiX, mdstForm)
        If blnIsNewRow Then blnIsNewRow = False
        EnableBindingNavigators(Me.Controls)
        ReadOnlyFields(blnReadOnly)
        'End If
    End Sub


    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DataControlKey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        Dim fdtFormDatatable As clsFormDatatable, blnPermanentReadOnly As Boolean

        For Each fdtFormDatatable In mfdcFormDatatableCollection
            With fdtFormDatatable
                blnPermanentReadOnly = Not .AllowAdd And Not .AllowUpdate And Not .AllowDelete
                If Not blnPermanentReadOnly Then blnPermanentReadOnly = blnReadOnly
                clsGlobal.ReadOnlyFields(blnPermanentReadOnly, .HostPanel)
                If .DataGridView IsNot Nothing Then
                    .DataGridView.AllowUserToAddRows = .AllowAdd
                    .DataGridView.AllowUserToDeleteRows = .AllowDelete
                End If
            End With
        Next

        ' Extras specific to this form to be added here
        clsGlobal.ReadOnlyFields(True, Me.pnlProperty)
        clsGlobal.ReadOnlyFields(True, Me.pnlSubtenant)
        clsGlobal.ReadOnlyFields(blnReadOnly, Me.pnlSubtenantDetails)

        Me.txbDepositDeductions.ReadOnly = blnReadOnly
        Me.txbDep.ReadOnly = blnReadOnly
        'Me.txbInvCheckRetDt.ReadOnly = blnReadOnly
        Me.chkGuidelinesChased.Enabled = Not blnReadOnly
        'Me.txbSTGuidelinesRetDt.ReadOnly = blnReadOnly
        'Me.txbProvMoveInDt.ReadOnly = blnReadOnly
        'Me.txbProvMoveOutDt.ReadOnly = blnReadOnly
        Me.txbPreviousAddress.ReadOnly = blnReadOnly
        Me.cboBusinessLine.ReadOnly = blnReadOnly
        Me.cboRoleTitle.ReadOnly = blnReadOnly
        Me.cboFamilySize.ReadOnly = blnReadOnly
        'Me.txbTenancyRenewalAction.ReadOnly = blnReadOnly

        clsGlobal.SetBackColor(blnReadOnly, Me.txbDepositDeductions)
        clsGlobal.SetBackColor(blnReadOnly, Me.txbDep)
        'clsGlobal.SetBackColor(blnReadOnly, Me.txbInvCheckRetDt)
        clsGlobal.SetBackColor(blnReadOnly, Me.chkGuidelinesChased)
        'clsGlobal.SetBackColor(blnReadOnly, Me.txbSTGuidelinesRetDt)
        'clsGlobal.SetBackColor(blnReadOnly, Me.txbProvMoveInDt)
        'clsGlobal.SetBackColor(blnReadOnly, Me.txbProvMoveOutDt)
        clsGlobal.SetBackColor(blnReadOnly, Me.txbPreviousAddress)
        clsGlobal.SetBackColor(blnReadOnly, Me.cboBusinessLine)
        clsGlobal.SetBackColor(blnReadOnly, Me.cboRoleTitle)
        clsGlobal.SetBackColor(blnReadOnly, Me.cboFamilySize)
        'clsGlobal.SetBackColor(blnReadOnly, Me.txbTenancyRenewalAction)

        ReadOnlyPermanent()

        EditableFields()

        Me.tsbAdd.Enabled = Not blnReadOnly And Me.bsSubtenantDetails.Count = 0

        If Me.bsSubtenantDetails.Count = 0 Then
            If Not blnIsNewRow Then
                clsGlobal.ReadOnlyFields(True, Me.pnlSubtenantDetails)
            End If
            If Not blnReadOnly Then
                Me.tsbAdd.Enabled = Not blnReadOnly
            End If
        End If
    End Sub
    Private Sub ReadOnlyPermanent()
        Try
            Me.txbProvMoveInDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbProvMoveInDt)

            Me.txbProvMoveOutDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbProvMoveOutDt)

            Me.txbSTGuidelinesRetDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbSTGuidelinesRetDt)

            Me.txbInvCheckRetDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbInvCheckRetDt)

            Me.txbTenancyRenewalAction.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbTenancyRenewalAction)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ReadOnlyPermanent", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub pbxCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxCopy.Click
        clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
    End Sub

    Private Sub pbxCopy_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxCopy_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                    Case "tsbCostSaving"
                        Dim frmX As Object
                        For Each frmX In Application.OpenForms
                            If frmX.Name = "frmMainTabMenu" Then
                                clsGlobal.CopyToFormStripFromGrid(Me.txbCaseNo.Text, Me.pnlFormStrip)
                                frmX.LoadForm("Case Cost Savings", Nothing)
                                Me.Close()
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspRight_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub EnableBindingNavigators(ByVal cncX As Object)

        Dim ctlX As Control
        For Each ctlX In cncX
            If TypeOf ctlX Is Panel Then
                EnableBindingNavigators(ctlX.Controls)
            ElseIf TypeOf ctlX Is BindingNavigator Then
                If Not ctlX.Enabled Then ctlX.Enabled = True
            End If
        Next
    End Sub

    Private Sub pbxProvMoveInDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxProvMoveInDt.Click

        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbProvMoveInDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxProvMoveInDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxProvMoveInDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxProvMoveInDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxProvMoveInDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxProvMoveOutDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxProvMoveOutDt.Click

        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbProvMoveOutDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxProvMoveOutDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxProvMoveOutDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxProvMoveOutDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxProvMoveOutDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxActualMoveInDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxActualMoveInDt.Click

        If Not blnReadOnly Then
            If Not IsDate(Me.txbActualMoveInDt.Text) Then
                Dim frmX As New frmDatePicker
                frmX.CallingForm(Me.txbActualMoveInDt)
                frmX.ShowDialog()
            End If
        End If


    End Sub

    Private Sub pbxActualMoveInDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveInDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxActualMoveInDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveInDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxActualMoveOutDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxActualMoveOutDt.Click

        If Not blnReadOnly Then
            If Not IsDate(Me.txbActualMoveOutDt.Text) Then
                Dim frmX As New frmDatePicker
                frmX.CallingForm(Me.txbActualMoveOutDt)
                frmX.ShowDialog()
            End If
        End If


    End Sub

    Private Sub pbxActualMoveOutDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveOutDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxActualMoveOutDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveOutDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxSTGuidelinesRetDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxSTGuidelinesRetDt.Click

        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbSTGuidelinesRetDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxSTGuidelinesRetDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxSTGuidelinesRetDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxSTGuidelinesRetDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxSTGuidelinesRetDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxInvCheckRetDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxInvCheckRetDt.Click

        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbInvCheckRetDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxInvCheckRetDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxInvCheckRetDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxInvCheckRetDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxInvCheckRetDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxEndDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxEndDt.Click

        If Not Me.txbEndDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbEndDt)
            frmX.ShowDialog()
        End If

    End Sub

    Private Sub pbxEndDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxEndDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxEndDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxEndDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxContactLookup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxContactLookup.Click

        'Dim frmX As New clsCase.frmContactsForACase
        'frmX.CallingForm(CType(Me.txbCaseNo.Text, Integer), Me)
        'frmX.ShowDialog()

    End Sub
    Private Sub pbxContactLookup_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxContactLookup.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxContactLookup_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxContactLookup.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub bsProperty_PositionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bsProperty.PositionChanged
        EditableFields()
    End Sub
    Private Sub EditableFields()
        If Me.bsProperty.Count > 0 Then

            Me.txbActualMoveInDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbActualMoveInDt)
            If Not IsDate(Me.txbActualMoveInDt.Text) Then
                Me.pbxActualMoveInDt.Enabled = Not blnReadOnly
                'Me.txbActualMoveInDt.ReadOnly = blnReadOnly
                'clsGlobal.SetBackColor(blnReadOnly, Me.txbActualMoveInDt)
            End If

            Me.txbActualMoveOutDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbActualMoveOutDt)
            If Not IsDate(Me.txbActualMoveOutDt.Text) Then
                Me.pbxActualMoveOutDt.Enabled = Not blnReadOnly
                'Me.txbActualMoveOutDt.ReadOnly = blnReadOnly
                'clsGlobal.SetBackColor(blnReadOnly, Me.txbActualMoveOutDt)
            End If

            Me.txbEndDt.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txbEndDt)
            If Not IsDate(Me.txbEndDt.Text) Then
                Me.pbxEndDt.Enabled = Not blnReadOnly
                'Me.txbEndDt.ReadOnly = blnReadOnly
                'clsGlobal.SetBackColor(blnReadOnly, Me.txbEndDt)
            End If
        End If

    End Sub

    Private Sub pbxTenancyRenewalAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxTenancyRenewalAction.Click
        If Not blnReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txbTenancyRenewalAction)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxTenancyRenewalAction_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxTenancyRenewalAction.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxTenancyRenewalAction_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxTenancyRenewalAction.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub tsbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAdd.Click
        blnIsNewRow = True
        ReadOnlyFields(blnReadOnly)
        sender.owner.Enabled = False
    End Sub
    Private Sub StandingOrderReminder()
        If Me.txtEmpRentContribution.Text <> "" And EmpRentContributionEmpty Then
            If CType(Me.txtEmpRentContribution.Text, Decimal) > 0 Then
                MessageBox.Show("Don't forget to instruct the subtenant to set up a standing order.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                EmpRentContributionEmpty = False
            End If
        End If
    End Sub

    Private Sub txtEmpRentContribution_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpRentContribution.GotFocus
        If Me.txtEmpRentContribution.Text = "" Then
            EmpRentContributionEmpty = True
        End If
    End Sub

    Private Sub txtEmpRentContribution_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmpRentContribution.Validating
        StandingOrderReminder()
    End Sub

    Private Sub pbxLetter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxLetter.Click
        'Dim letX As clsLetters.clsLetter
        'If Me.txbCaseNo.Text <> "" Then
        '    If Me.bsProperty.Count > 0 Then
        '        letX = New clsLetters.clsLetter(CType(Me.txbCaseNo.Text, Integer), _
        '                CType(Me.txtPackageId.Text, Integer), _
        '                "RENT", 1, _
        '                "frmSubTenantMaintenance", False)
        '        letX.StartLetter(Me)
        '    Else
        '        MsgBox("A valid package must be present to select Letters.", MsgBoxStyle.Exclamation, "Letters System")
        '    End If
        'Else
        '    MsgBox("A valid case must be present to select Letters.", MsgBoxStyle.Exclamation, "Letters System")
        'End If
    End Sub

    Private Sub pbxLetter_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxLetter.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxLetter_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxLetter.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub
End Class