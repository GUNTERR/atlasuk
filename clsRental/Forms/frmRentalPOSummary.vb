Public Class frmRentalPOSummary
    Dim bsiX As New clsBindingSourceItem
    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Dim mfdcFormDatatableCollection As New clnFormDatatable, mblnFillForm As Boolean
    Dim mcbcComboSourceCollection As New clnComboBoxSource
    Dim mdstForm As New DataSet, mblnQueryableForm As Boolean, mblnEditableForm As Boolean
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Private Sub InitialiseForm()

        ' Specify form master dataset

        mfdcFormDatatableCollection.Clear()
        mcbcComboSourceCollection.Clear()

        ' **** Specify dataset here ****************
        mdstForm = Me.DsRentalPOSummary

        ' **** Modify these below for each data table ****************
        ' Specify all datasets for the form
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPOSummary.V_frmRentalPurchaseOrderSumary, _
                Me.bsCase, Me.taCase, Nothing, Me.pnlCase, False, False, False, True))
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderPrpty, _
                Me.bsCasePackage, Me.taCasePackage, Nothing, Me.pnlCase, False, False, False, False))
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderPersn, _
                Me.bsPackage, Me.taPackage, Me.dgvPackage, Me.pnlPackage, False, False, False, False))
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderProp, _
                Me.bsPropertyPO, Me.taPropertyPO, Me.dgvPropertyPO, Me.pnlPropertyPO, False, False, False, False))
        mfdcFormDatatableCollection.Add(New clsFormDatatable(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderPers, _
                Me.bsPersonPO, Me.taPersonPO, Me.dgvPersonPO, Me.pnlPersonPO, False, False, False, False))

        'mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboLatestStatusProp, "INV_LATEST_STATUS_CODE", True))
        'mcbcComboSourceCollection.Add(New clsComboBoxSource(Me.cboInvoiceStatusPerson, "INV_LATEST_STATUS_CODE"))

    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim fdtFormDatatable As clsFormDatatable

            InitialiseForm()

            For Each fdtFormDatatable In mfdcFormDatatableCollection
                With fdtFormDatatable
                    If .AllowAdd Or .AllowUpdate Or .AllowDelete Then
                        mblnEditableForm = True
                        bsiX.Add(.BindingSource)
                    End If
                    If .DataGridView IsNot Nothing Then
                        .DataGridView.AllowUserToAddRows = .AllowAdd
                        .DataGridView.AllowUserToDeleteRows = .AllowDelete
                    End If
                    If .QueryingDatatable Then mblnQueryableForm = True
                End With
            Next

            'blnReadOnly = clsGlobal.SetupForm(Me, Not mblnQueryableForm, Not mblnEditableForm)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, Not mblnQueryableForm, Not mblnEditableForm)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            ReadOnlyFields(blnReadOnly)

            clsGlobal.CreateFormStripItems(Me, pnlFormStrip)

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                FillForm()
            Else    ' Start in query mode - can be removed if required
                RunQuery()
            End If

            HidePrimaryKeyFields()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, mdstForm, e, blnIsNewRow)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormClosing")
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DefineQuery(e.KeyValue, blnShiftPressed, Me)
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "KeyDown")
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = "", intCaseNo, intRelatedCaseNo, intCompositeCaseNo As Integer

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                strQueryText = CType(clsGlobal.ToolStripCaseId, String)
            End If

            If txbCaseNo.Text <> "" Then
                If IsNumeric(txbCaseNo.Text) And CType(txbCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txbCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then intCaseNo = CType(strQueryText, Integer)
            If Me.txbRelatedCaseNo.Text <> "" Then intRelatedCaseNo = CType(Me.txbRelatedCaseNo.Text, Integer)

            Me.taCase.Connection = clsGlobal.Connection
            If strQueryText = "" And Me.txbRelatedCaseNo.Text = "" Then
            ElseIf strQueryText = "" Then
                Me.taCase.FillRelatedCaseNo(Me.DsRentalPOSummary.V_frmRentalPurchaseOrderSumary, intRelatedCaseNo)
            ElseIf Me.txbRelatedCaseNo.Text = "" Then
                Me.taCase.FillCaseNo(Me.DsRentalPOSummary.V_frmRentalPurchaseOrderSumary, intCaseNo)
            Else
                Me.taCase.Fill(Me.DsRentalPOSummary.V_frmRentalPurchaseOrderSumary, intCaseNo, intRelatedCaseNo)
            End If

            If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                Me.pbxVIP.Visible = True
            Else
                Me.pbxVIP.Visible = False
            End If

            If Me.txbCompositeCaseNo.Text <> "" Then
                intCompositeCaseNo = CType(Me.txbCompositeCaseNo.Text, Integer)
                Me.taCasePackage.Connection = clsGlobal.Connection
                Me.taCasePackage.Fill(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderPrpty, intCompositeCaseNo)
                If Me.bsCasePackage.Count > 0 Then
                    Me.taPackage.Connection = clsGlobal.Connection
                    Me.taPackage.Fill(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderPersn, _
                            CType(Me.bsCasePackage.Current("CSE_CASE_NO_CSE").ToString, Integer))
                    Me.taPropertyPO.Connection = clsGlobal.Connection
                    Me.taPropertyPO.Fill(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderProp, _
                            CType(Me.bsCasePackage.Current("CSE_CASE_NO_CSE").ToString, Integer))
                    Me.taPersonPO.Connection = clsGlobal.Connection
                    Me.taPersonPO.Fill(Me.DsRentalPOSummary.V_fsubRentalPurchaseOrderPers, _
                            CType(Me.bsCasePackage.Current("CSE_CASE_NO_CSE").ToString, Integer))
                End If
                ReadOnlyFields(blnReadOnly)
                'clsGlobal.ToolStripCaseId = CType(Me.txbCaseNo.Text, Integer)
            Else
                MsgBox("No records returned", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Me.Text)
                ReadOnlyFields(True)
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub

    Private Sub ExitQueryMode()
        Try
            If mblnQueryableForm And blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                HidePrimaryKeyFields()
                FillForm()
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ExitQueryMode")
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DefineQuery")
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            If mblnQueryableForm Then
                ExitQueryMode()
                FillForm()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "EscapeQueryMode")
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            If clsGlobal.FormIsDirty(Me, bsiX, Me.DsRentalPOSummary, blnIsNewRow) Then
                Dim strMsg As String = "Data has been changed. Click on OK to abandon changes and continue with your query, or click Cancel to return to " + _
                                        "the form where you can click on the Save button"

                If MsgBox(strMsg, MsgBoxStyle.Question Or MsgBoxStyle.OkCancel, "Pending Updates Not Saved") = MsgBoxResult.Cancel Then
                    Exit Sub
                End If
            End If

            If mblnQueryableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                If blnIsNewRow Then
                    UndoForm()
                    ReadOnlyFields(False)
                    blnIsNewRow = False
                End If
                blnInQueryMode = True

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    If fdtFormDatatable.QueryingDatatable AndAlso fdtFormDatatable.Datatable IsNot Nothing Then
                        fdtFormDatatable.Datatable.Clear()
                    End If
                Next

                clsGlobal.EnterQueryMode(Me, blnReadOnly)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "RunQuery")
        End Try
    End Sub

    Public Function UpdateBaseTables(ByVal blnSilent As Boolean) As Boolean
        If mdstForm.HasChanges Then
            Dim fdtFormDatatable As clsFormDatatable

            Dim intChanges As Integer
            Try
                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .Datatable.GetChanges IsNot Nothing Then
                            .TableAdapter.Update(.Datatable.GetChanges)
                            intChanges += .Datatable.GetChanges.Rows.Count
                        End If
                    End With
                Next

                If intChanges > 0 And Not blnSilent Then
                    Dim strMsg As String = intChanges.ToString & " record" & IIf(intChanges <> 1, "s", "") & " added/altered/deleted"
                    MsgBox(strMsg, MsgBoxStyle.Information, Me.Text & " Save")
                End If
                Return True
            Catch exc As Exception
                If Not blnSilent Then MsgBox(exc.Message, MsgBoxStyle.Exclamation, "Database Updates Failed")
                Return False
            End Try
        Else
            If Not blnSilent Then MsgBox("There are no data updates to save.", MsgBoxStyle.Information, _
                    "Save Requested Without Updates")
            Return False
        End If
    End Function

    Private Sub SaveForm(Optional ByVal blnSilent As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            If mblnEditableForm Then
                Dim fdtFormDatatable As clsFormDatatable

                For Each fdtFormDatatable In mfdcFormDatatableCollection
                    With fdtFormDatatable
                        If .DataGridView IsNot Nothing Then
                            ' This fixes a bug where user clicks on the new record and saves - it thinks user has edited
                            If Not .DataGridView.IsCurrentCellDirty And Not .DataGridView.IsCurrentRowDirty Then
                                .BindingSource.CancelEdit()
                            End If
                            .DataGridView.EndEdit()
                        End If
                        If .BindingSource IsNot Nothing Then
                            .BindingSource.EndEdit()
                        End If
                    End With
                Next

                If UpdateBaseTables(blnSilent) Then
                    For Each fdtFormDatatable In mfdcFormDatatableCollection
                        fdtFormDatatable.Datatable.AcceptChanges()
                    Next
                    blnIsNewRow = False
                    EnableBindingNavigators(Me.Controls)
                    ReadOnlyFields(blnReadOnly)
                    'Me.taCase.FillCase(Me.dsSUPPLIERMAINTENANCE.V_frmCseNew, CType(Me.txtCaseNo.Text, Integer))
                Else
                    Return
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SaveForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txbCaseNo.Text)
        End Try
    End Sub


    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspMain_ItemClicked")
        End Try
    End Sub
    Private Sub UndoForm()
        If mblnEditableForm Then
            clsGlobal.UndoForm(Me, bsiX, mdstForm)
            If blnIsNewRow Then blnIsNewRow = False
            EnableBindingNavigators(Me.Controls)
            ReadOnlyFields(blnReadOnly)
        End If
    End Sub


    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
                Case Keys.F8 'Copy to number field
                    clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
                    intKeyCode = 0
                Case Keys.F9 'Clear number field
                    clsGlobal.ClickClearCase(Me.pnlFormStrip)
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DataControlKey")
        End Try
    End Sub


    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        Dim fdtFormDatatable As clsFormDatatable, blnPermanentReadOnly As Boolean

        For Each fdtFormDatatable In mfdcFormDatatableCollection
            With fdtFormDatatable
                blnPermanentReadOnly = Not .AllowAdd And Not .AllowUpdate And Not .AllowDelete
                If Not blnPermanentReadOnly Then blnPermanentReadOnly = blnReadOnly
                clsGlobal.ReadOnlyFields(blnPermanentReadOnly, .HostPanel)
                If .DataGridView IsNot Nothing Then
                    .DataGridView.AllowUserToAddRows = .AllowAdd
                    .DataGridView.AllowUserToDeleteRows = .AllowDelete
                End If
            End With
        Next

        Me.txbCompositeCaseNo.Hide()

    End Sub

    Private Sub pbxCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxCopy.Click
        clsGlobal.CopyToFormStrip(Me.txbCaseNo, Me.pnlFormStrip)
    End Sub

    Private Sub pbxCopy_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxCopy.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txbCaseNo)
    End Sub

    Private Sub pbxCopy_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxCopy.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub


    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                    Case "tsbCostSaving"
                        Dim frmX As Object
                        For Each frmX In Application.OpenForms
                            If frmX.Name = "frmMainTabMenu" Then
                                clsGlobal.CopyToFormStripFromGrid(Me.txbCaseNo.Text, Me.pnlFormStrip)
                                frmX.LoadForm("Case Cost Savings", Nothing)
                                Me.Close()
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspRight_ItemClicked")
        End Try
    End Sub

    Public Sub EnableBindingNavigators(ByVal cncX As Object)

        Dim ctlX As Control
        For Each ctlX In cncX
            If TypeOf ctlX Is Panel Then
                EnableBindingNavigators(ctlX.Controls)
            ElseIf TypeOf ctlX Is BindingNavigator Then
                If Not ctlX.Enabled Then ctlX.Enabled = True
            End If
        Next
    End Sub

    Private Sub dgvPackage_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvPackage.DataError

        MsgBox(e.Exception.Message, MsgBoxStyle.Exclamation, "Case")

    End Sub

    Private Sub dgvPropertyPO_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvPropertyPO.DataError

        MsgBox(e.Exception.Message, MsgBoxStyle.Exclamation, "Invoice")

    End Sub

    Private Sub dgvPersonPO_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvPersonPO.DataError

        MsgBox(e.Exception.Message, MsgBoxStyle.Exclamation, "Apportionment")

    End Sub
    Private Sub HidePrimaryKeyFields()
        Me.txtVIP.Hide()
    End Sub
End Class