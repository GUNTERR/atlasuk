Imports System.IO
Public Class frmMODTenancyKPI
    Dim bsiX As New clsBindingSourceItem
    Private blnReadOnly As Boolean
    Public blnIsNewRow As Boolean = False
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property

    Private Sub frmMODTenancyKPI_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, Me.DsMODTenancyKPI, e, blnIsNewRow)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormClosing", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub frmMODTenancyKPI_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "KeyDown", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ReadOnlyFields(True)

            'blnReadOnly = clsGlobal.SetupForm(Me, True, False)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, True, False)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            If blnReadOnly Then
                Me.tsbAdd.Visible = False
                Me.tssAdd.Visible = False
            End If

            bsiX.Add(Me.bsKPI)

            If CType(clsGlobal.SubMenuCaseId, String) <> "" And clsGlobal.SubMenuCaseId > 0 Then
                FillForm()
            End If

            HidePrimaryKeyFields()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormLoad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.SubMenuCaseId, String) <> "" And clsGlobal.SubMenuCaseId > 0 Then
                strQueryText = CType(clsGlobal.SubMenuCaseId, String)
            End If

            If txtCaseNo.Text <> "" Then
                If IsNumeric(txtCaseNo.Text) And CType(txtCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txtCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taCase.Connection = clsGlobal.Connection
                Me.taCase.FillCase(Me.DsMODTenancyKPI.V_frmMODTenancyKPI, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.txtCaseNo.Text <> "" Then
                    Me.taPackage.Connection = clsGlobal.Connection
                    Me.taPackage.FillPackage(Me.DsMODTenancyKPI.V_fsubMODTenancyKPIPackage, CType(strQueryText, Integer))

                    If Not Me.dgvPackage.CurrentRow Is Nothing Then
                        Me.taKPI.Connection = clsGlobal.Connection
                        Me.taKPI.FillKPI(Me.DsMODTenancyKPI.V_fsubMODTenancyKPI, CType(Me.dgvPackage.CurrentRow.Cells("CSE_CASE_NO_CSE").Value.ToString, Integer), CType(Me.dgvPackage.CurrentRow.Cells("CGS_CLIENT_PACKAGE_ID_CGS").Value.ToString, Integer))
                        If Not blnReadOnly Then ReadOnlyFields(False)
                    End If
                End If

                If Me.bsCase.Count = 0 Then
                    MessageBox.Show("No records returned", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "FillForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub
    Private Sub UndoForm()
        clsGlobal.UndoForm(Me, bsiX, Me.DsMODTenancyKPI)
        If blnIsNewRow Then blnIsNewRow = False
    End Sub
    Private Sub SaveForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Me.bsKPI.EndEdit()

            If UpdateBaseTables() Then
                Me.DsMODTenancyKPI.V_fsubMODTenancyKPI.AcceptChanges()
                blnIsNewRow = False
                Me.bnKPI.Enabled = True
                ReadOnlyFields(blnReadOnly)
            Else
                Return
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "SaveForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub
    Public Function UpdateBaseTables() As Boolean
        If Me.DsMODTenancyKPI.HasChanges Then
            Dim newCaseDetails As dsMODTenancyKPI.V_fsubMODTenancyKPIDataTable = _
                CType(DsMODTenancyKPI.V_fsubMODTenancyKPI.GetChanges(DataRowState.Added), _
                dsMODTenancyKPI.V_fsubMODTenancyKPIDataTable)

            Dim modCaseDetails As dsMODTenancyKPI.V_fsubMODTenancyKPIDataTable = _
                CType(DsMODTenancyKPI.V_fsubMODTenancyKPI.GetChanges(DataRowState.Modified), _
                dsMODTenancyKPI.V_fsubMODTenancyKPIDataTable)

            Dim intChanges As Integer
            Try
                If Not newCaseDetails Is Nothing Then
                    Me.taKPI.Connection = clsGlobal.Connection
                    Me.taKPI.Update(newCaseDetails)
                    intChanges += newCaseDetails.Count
                End If

                If Not modCaseDetails Is Nothing Then
                    Me.taKPI.Connection = clsGlobal.Connection
                    Me.taKPI.Update(modCaseDetails)
                    intChanges += modCaseDetails.Count
                End If

                If intChanges > 0 Then
                    Dim strMsg As String = intChanges.ToString + " record added/altered/deleted"
                    MessageBox.Show(strMsg, Me.Text + " Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                Return True
            Catch exc As Exception
                MessageBox.Show(exc.Message, "Database Updates Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            Finally
                'Dispose the new objects
                If Not newCaseDetails Is Nothing Then
                    newCaseDetails.Dispose()
                End If
                If Not modCaseDetails Is Nothing Then
                    modCaseDetails.Dispose()
                End If
            End Try
        Else
            MessageBox.Show("There are no data updates to save.", "Save Requested Without Updates", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If
    End Function
    Private Sub ReadOnlyFields(ByVal blnFlag As Boolean)
        clsGlobal.ReadOnlyFields(blnFlag, Me.pnlKPI)

        If Me.bsKPI.Count = 0 Then
            If Not blnIsNewRow Then
                clsGlobal.ReadOnlyFields(True, Me.pnlKPI)
            End If
            If Not blnReadOnly Then
                Me.tsbAdd.Enabled = Not blnReadOnly
            End If
        Else
            Me.tsbAdd.Enabled = False
        End If
    End Sub

    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape ' Undo field/record
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "DataControlKey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    
    Private Sub tspRight_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspRight_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspMain_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub tsbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAdd.Click
        blnIsNewRow = True
        ReadOnlyFields(blnReadOnly)
        sender.owner.Enabled = False
    End Sub

    Private Sub pbxPreferredMoveInDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxPreferredMoveInDt.Click
        If Not Me.txtPreferredMoveInDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtPreferredMoveInDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxPreferredMoveInDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxPreferredMoveInDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxPreferredMoveInDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxPreferredMoveInDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxActualMoveInDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxActualMoveInDt.Click
        If Not Me.txtActualMoveInDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtActualMoveInDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxActualMoveInDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveInDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxActualMoveInDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveInDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxAnticipatedMoveOutDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxAnticipatedMoveOutDt.Click
        If Not Me.txtAnticipatedMoveOutDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtAnticipatedMoveOutDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxAnticipatedMoveOutDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxAnticipatedMoveOutDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxAnticipatedMoveOutDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxAnticipatedMoveOutDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxActualMoveOutDt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxActualMoveOutDt.Click
        If Not Me.txtActualMoveOutDt.ReadOnly Then
            Dim frmX As New frmDatePicker
            frmX.CallingForm(Me.txtActualMoveOutDt)
            frmX.ShowDialog()
        End If
    End Sub

    Private Sub pbxActualMoveOutDt_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveOutDt.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxActualMoveOutDt_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxActualMoveOutDt.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub
    Private Sub HidePrimaryKeyFields()
        Me.txtVIP.Hide()
    End Sub
End Class