<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSubTenantMaintenance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FULL_NMLabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim CANCELLATION_DTLabel As System.Windows.Forms.Label
        Dim COMPLETION_DTLabel As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSubTenantMaintenance))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxContactLookup = New System.Windows.Forms.PictureBox()
        Me.pbxLetter = New System.Windows.Forms.PictureBox()
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.pnlSubtenant = New System.Windows.Forms.Panel()
        Me.pbxEndDt = New System.Windows.Forms.PictureBox()
        Me.txbEndDt = New System.Windows.Forms.MaskedTextBox()
        Me.bsSubtenant = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsProperty = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsSubtenantMaintenance = New clsRental.dsSubtenantMaintenance()
        Me.cboFamilySize = New ControlLibrary.SuperComboBox()
        Me.cboRoleTitle = New ControlLibrary.SuperComboBox()
        Me.cboBusinessLine = New ControlLibrary.SuperComboBox()
        Me.txbStartDt = New System.Windows.Forms.TextBox()
        Me.txbPreviousAddress = New System.Windows.Forms.TextBox()
        Me.txbSubtenantNm = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlProperty = New System.Windows.Forms.Panel()
        Me.pbxTenancyRenewalAction = New System.Windows.Forms.PictureBox()
        Me.txbTenancyRenewalAction = New System.Windows.Forms.MaskedTextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.chkGuidelinesChased = New ControlLibrary.SuperCheckBox()
        Me.txbDepositDeductions = New System.Windows.Forms.TextBox()
        Me.txbDep = New System.Windows.Forms.TextBox()
        Me.pbxInvCheckRetDt = New System.Windows.Forms.PictureBox()
        Me.txbInvCheckRetDt = New System.Windows.Forms.MaskedTextBox()
        Me.pbxSTGuidelinesRetDt = New System.Windows.Forms.PictureBox()
        Me.txbSTGuidelinesRetDt = New System.Windows.Forms.MaskedTextBox()
        Me.txbPropertyAddress = New System.Windows.Forms.TextBox()
        Me.txbClientNm = New System.Windows.Forms.TextBox()
        Me.pbxActualMoveOutDt = New System.Windows.Forms.PictureBox()
        Me.txbActualMoveOutDt = New System.Windows.Forms.MaskedTextBox()
        Me.pbxActualMoveInDt = New System.Windows.Forms.PictureBox()
        Me.txbActualMoveInDt = New System.Windows.Forms.MaskedTextBox()
        Me.pbxProvMoveOutDt = New System.Windows.Forms.PictureBox()
        Me.txbProvMoveOutDt = New System.Windows.Forms.MaskedTextBox()
        Me.pbxProvMoveInDt = New System.Windows.Forms.PictureBox()
        Me.txbProvMoveInDt = New System.Windows.Forms.MaskedTextBox()
        Me.txbPropertyCaseNo = New System.Windows.Forms.TextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.txbPackageNm = New System.Windows.Forms.TextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.txtPackageId = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.pnlSubtenantDetails = New System.Windows.Forms.Panel()
        Me.bnSubtenantDetails = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbAdd = New System.Windows.Forms.ToolStripButton()
        Me.bsSubtenantDetails = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox2 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.tssAdd = New System.Windows.Forms.ToolStripSeparator()
        Me.chkStandingOrder = New ControlLibrary.SuperCheckBox()
        Me.txtEmpRentContribution = New System.Windows.Forms.TextBox()
        Me.txtRentalBudget = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.taProperty = New clsRental.dsSubtenantMaintenanceTableAdapters.V_frmSubTenantMaintenanceTableAdapter()
        Me.taSubtenant = New clsRental.dsSubtenantMaintenanceTableAdapters.V_fsubSubTenantMaintSubTenantTableAdapter()
        Me.taSubtenantDetails = New clsRental.dsSubtenantMaintenanceTableAdapters.V_fsubSubTenantMaintenanceTableAdapter()
        FULL_NMLabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        CANCELLATION_DTLabel = New System.Windows.Forms.Label()
        COMPLETION_DTLabel = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        CType(Me.pbxContactLookup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxLetter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFormStrip.SuspendLayout()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSubtenant.SuspendLayout()
        CType(Me.pbxEndDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsSubtenant, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsProperty, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsSubtenantMaintenance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProperty.SuspendLayout()
        CType(Me.pbxTenancyRenewalAction, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxInvCheckRetDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxSTGuidelinesRetDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxActualMoveOutDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxActualMoveInDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxProvMoveOutDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxProvMoveInDt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSubtenantDetails.SuspendLayout()
        CType(Me.bnSubtenantDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnSubtenantDetails.SuspendLayout()
        CType(Me.bsSubtenantDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'FULL_NMLabel
        '
        FULL_NMLabel.AutoSize = True
        FULL_NMLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        FULL_NMLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FULL_NMLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        FULL_NMLabel.Location = New System.Drawing.Point(25, 23)
        FULL_NMLabel.Name = "FULL_NMLabel"
        FULL_NMLabel.Size = New System.Drawing.Size(62, 14)
        FULL_NMLabel.TabIndex = 0
        FULL_NMLabel.Text = "Sub-Tenant"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(26, 49)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(75, 14)
        Label4.TabIndex = 2
        Label4.Text = "Business Line"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label18.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label18.Location = New System.Drawing.Point(26, 76)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(50, 14)
        Label18.TabIndex = 12
        Label18.Text = "Role Title"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label19.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label19.Location = New System.Drawing.Point(26, 103)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(61, 14)
        Label19.TabIndex = 14
        Label19.Text = "Family Size"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label20.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label20.Location = New System.Drawing.Point(26, 182)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(93, 14)
        Label20.TabIndex = 16
        Label20.Text = "Previous Address"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label22.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label22.Location = New System.Drawing.Point(26, 130)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(55, 14)
        Label22.TabIndex = 20
        Label22.Text = "Start Date"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label1.Location = New System.Drawing.Point(35, 49)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(92, 14)
        Label1.TabIndex = 116
        Label1.Text = "Property Case No"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label3.Location = New System.Drawing.Point(35, 75)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(124, 14)
        Label3.TabIndex = 140
        Label3.Text = "Provisional Move In Date"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label6.Location = New System.Drawing.Point(35, 101)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(133, 14)
        Label6.TabIndex = 143
        Label6.Text = "Provisional Move Out Date"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label14.Location = New System.Drawing.Point(294, 23)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(77, 14)
        Label14.TabIndex = 2
        Label14.Text = "Client Package"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label7.Location = New System.Drawing.Point(35, 127)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(103, 14)
        Label7.TabIndex = 146
        Label7.Text = "Actual Move In Date"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label8.Location = New System.Drawing.Point(35, 153)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(112, 14)
        Label8.TabIndex = 149
        Label8.Text = "Actual Move Out Date"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label9.Location = New System.Drawing.Point(294, 49)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(63, 14)
        Label9.TabIndex = 152
        Label9.Text = "Client Name"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label10.Location = New System.Drawing.Point(294, 75)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(92, 14)
        Label10.TabIndex = 154
        Label10.Text = "Property Address"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label12.Location = New System.Drawing.Point(294, 101)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(136, 14)
        Label12.TabIndex = 156
        Label12.Text = "SubTenant Guidelines Retd"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label13.Location = New System.Drawing.Point(294, 165)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(132, 14)
        Label13.TabIndex = 161
        Label13.Text = "Inventory Check Returned"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label29.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label29.Location = New System.Drawing.Point(294, 191)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(101, 14)
        Label29.TabIndex = 163
        Label29.Text = "Duration Allowance"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label30.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label30.Location = New System.Drawing.Point(294, 217)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(100, 14)
        Label30.TabIndex = 165
        Label30.Text = "Deposit Deductions"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(26, 156)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(50, 14)
        Label5.TabIndex = 152
        Label5.Text = "End Date"
        '
        'Label23
        '
        Label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label23.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label23.Location = New System.Drawing.Point(294, 127)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(138, 28)
        Label23.TabIndex = 174
        Label23.Text = "Subtenant Guidelines Chased Twice"
        '
        'CANCELLATION_DTLabel
        '
        CANCELLATION_DTLabel.AutoSize = True
        CANCELLATION_DTLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CANCELLATION_DTLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CANCELLATION_DTLabel.Location = New System.Drawing.Point(571, 127)
        CANCELLATION_DTLabel.Name = "CANCELLATION_DTLabel"
        CANCELLATION_DTLabel.Size = New System.Drawing.Size(122, 14)
        CANCELLATION_DTLabel.TabIndex = 177
        CANCELLATION_DTLabel.Text = "Package Cancellation Dt"
        '
        'COMPLETION_DTLabel
        '
        COMPLETION_DTLabel.AutoSize = True
        COMPLETION_DTLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COMPLETION_DTLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        COMPLETION_DTLabel.Location = New System.Drawing.Point(571, 101)
        COMPLETION_DTLabel.Name = "COMPLETION_DTLabel"
        COMPLETION_DTLabel.Size = New System.Drawing.Size(116, 14)
        COMPLETION_DTLabel.TabIndex = 175
        COMPLETION_DTLabel.Text = "Package Completion Dt"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label11.Location = New System.Drawing.Point(531, 165)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(127, 14)
        Label11.TabIndex = 181
        Label11.Text = "Tenancy Renewal Action"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label16.Location = New System.Drawing.Point(26, 71)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(115, 14)
        Label16.TabIndex = 152
        Label16.Text = "Standing Order Set Up"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label17.Location = New System.Drawing.Point(26, 23)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(74, 14)
        Label17.TabIndex = 20
        Label17.Text = "Rental Budget"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label34.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label34.Location = New System.Drawing.Point(26, 49)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(138, 14)
        Label34.TabIndex = 203
        Label34.Text = "Employee Rent Contribution"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxContactLookup
        '
        Me.pbxContactLookup.Image = CType(resources.GetObject("pbxContactLookup.Image"), System.Drawing.Image)
        Me.pbxContactLookup.Location = New System.Drawing.Point(344, 21)
        Me.pbxContactLookup.Name = "pbxContactLookup"
        Me.pbxContactLookup.Size = New System.Drawing.Size(15, 15)
        Me.pbxContactLookup.TabIndex = 147
        Me.pbxContactLookup.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxContactLookup, "View Subtenant Details")
        '
        'pbxLetter
        '
        Me.pbxLetter.Image = CType(resources.GetObject("pbxLetter.Image"), System.Drawing.Image)
        Me.pbxLetter.Location = New System.Drawing.Point(477, 2)
        Me.pbxLetter.Name = "pbxLetter"
        Me.pbxLetter.Size = New System.Drawing.Size(40, 29)
        Me.pbxLetter.TabIndex = 2
        Me.pbxLetter.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxLetter, "Generate Letters")
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(250, 14)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Controls.Add(Me.pbxLetter)
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox7)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.pnlSubtenant)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(0, 395)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(405, 305)
        Me.Panel1.TabIndex = 5
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(370, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 79
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'pnlSubtenant
        '
        Me.pnlSubtenant.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlSubtenant.Controls.Add(Me.pbxEndDt)
        Me.pnlSubtenant.Controls.Add(Me.txbEndDt)
        Me.pnlSubtenant.Controls.Add(Label5)
        Me.pnlSubtenant.Controls.Add(Me.pbxContactLookup)
        Me.pnlSubtenant.Controls.Add(Me.cboFamilySize)
        Me.pnlSubtenant.Controls.Add(Me.cboRoleTitle)
        Me.pnlSubtenant.Controls.Add(Me.cboBusinessLine)
        Me.pnlSubtenant.Controls.Add(Label22)
        Me.pnlSubtenant.Controls.Add(Me.txbStartDt)
        Me.pnlSubtenant.Controls.Add(Label20)
        Me.pnlSubtenant.Controls.Add(Me.txbPreviousAddress)
        Me.pnlSubtenant.Controls.Add(Label19)
        Me.pnlSubtenant.Controls.Add(Label18)
        Me.pnlSubtenant.Controls.Add(Label4)
        Me.pnlSubtenant.Controls.Add(FULL_NMLabel)
        Me.pnlSubtenant.Controls.Add(Me.txbSubtenantNm)
        Me.pnlSubtenant.Location = New System.Drawing.Point(8, 32)
        Me.pnlSubtenant.Name = "pnlSubtenant"
        Me.pnlSubtenant.Size = New System.Drawing.Size(387, 262)
        Me.pnlSubtenant.TabIndex = 1
        '
        'pbxEndDt
        '
        Me.pbxEndDt.Image = CType(resources.GetObject("pbxEndDt.Image"), System.Drawing.Image)
        Me.pbxEndDt.Location = New System.Drawing.Point(211, 156)
        Me.pbxEndDt.Name = "pbxEndDt"
        Me.pbxEndDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxEndDt.TabIndex = 153
        Me.pbxEndDt.TabStop = False
        '
        'txbEndDt
        '
        Me.txbEndDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbEndDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSubtenant, "END_DT", True))
        Me.txbEndDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbEndDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbEndDt.Location = New System.Drawing.Point(140, 153)
        Me.txbEndDt.Name = "txbEndDt"
        Me.txbEndDt.Size = New System.Drawing.Size(65, 20)
        Me.txbEndDt.TabIndex = 5
        Me.txbEndDt.ValidatingType = GetType(Date)
        '
        'bsSubtenant
        '
        Me.bsSubtenant.DataMember = "V_frmSubTenantMaintenance_V_fsubSubTenantMaintSubTenant"
        Me.bsSubtenant.DataSource = Me.bsProperty
        '
        'bsProperty
        '
        Me.bsProperty.DataMember = "V_frmSubTenantMaintenance"
        Me.bsProperty.DataSource = Me.DsSubtenantMaintenance
        '
        'DsSubtenantMaintenance
        '
        Me.DsSubtenantMaintenance.DataSetName = "dsSubtenantMaintenance"
        Me.DsSubtenantMaintenance.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'cboFamilySize
        '
        Me.cboFamilySize.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboFamilySize.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFamilySize.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsSubtenant, "FAMILY_SIZE_QT", True))
        Me.cboFamilySize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFamilySize.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFamilySize.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboFamilySize.FormattingEnabled = True
        Me.cboFamilySize.Location = New System.Drawing.Point(140, 100)
        Me.cboFamilySize.Name = "cboFamilySize"
        Me.cboFamilySize.Queryable = False
        Me.cboFamilySize.Size = New System.Drawing.Size(140, 22)
        Me.cboFamilySize.TabIndex = 3
        Me.cboFamilySize.Updateable = True
        '
        'cboRoleTitle
        '
        Me.cboRoleTitle.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboRoleTitle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRoleTitle.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsSubtenant, "ROLE_TITLE_CD", True))
        Me.cboRoleTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRoleTitle.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRoleTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboRoleTitle.FormattingEnabled = True
        Me.cboRoleTitle.Location = New System.Drawing.Point(140, 73)
        Me.cboRoleTitle.Name = "cboRoleTitle"
        Me.cboRoleTitle.Queryable = False
        Me.cboRoleTitle.Size = New System.Drawing.Size(140, 22)
        Me.cboRoleTitle.TabIndex = 2
        Me.cboRoleTitle.Updateable = True
        '
        'cboBusinessLine
        '
        Me.cboBusinessLine.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboBusinessLine.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBusinessLine.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsSubtenant, "BUSINESS_LINE_CD", True))
        Me.cboBusinessLine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBusinessLine.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBusinessLine.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboBusinessLine.FormattingEnabled = True
        Me.cboBusinessLine.Location = New System.Drawing.Point(140, 46)
        Me.cboBusinessLine.Name = "cboBusinessLine"
        Me.cboBusinessLine.Queryable = False
        Me.cboBusinessLine.Size = New System.Drawing.Size(140, 22)
        Me.cboBusinessLine.TabIndex = 1
        Me.cboBusinessLine.Updateable = True
        '
        'txbStartDt
        '
        Me.txbStartDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbStartDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSubtenant, "START_DT", True))
        Me.txbStartDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbStartDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbStartDt.Location = New System.Drawing.Point(140, 127)
        Me.txbStartDt.Name = "txbStartDt"
        Me.txbStartDt.ReadOnly = True
        Me.txbStartDt.Size = New System.Drawing.Size(65, 20)
        Me.txbStartDt.TabIndex = 4
        '
        'txbPreviousAddress
        '
        Me.txbPreviousAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPreviousAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSubtenant, "PREVIOUS_ADDRESS_DS", True))
        Me.txbPreviousAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPreviousAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPreviousAddress.Location = New System.Drawing.Point(140, 179)
        Me.txbPreviousAddress.Multiline = True
        Me.txbPreviousAddress.Name = "txbPreviousAddress"
        Me.txbPreviousAddress.ReadOnly = True
        Me.txbPreviousAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txbPreviousAddress.Size = New System.Drawing.Size(138, 20)
        Me.txbPreviousAddress.TabIndex = 6
        '
        'txbSubtenantNm
        '
        Me.txbSubtenantNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbSubtenantNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSubtenant, "SubTenant", True))
        Me.txbSubtenantNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbSubtenantNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbSubtenantNm.Location = New System.Drawing.Point(140, 20)
        Me.txbSubtenantNm.Name = "txbSubtenantNm"
        Me.txbSubtenantNm.ReadOnly = True
        Me.txbSubtenantNm.Size = New System.Drawing.Size(198, 20)
        Me.txbSubtenantNm.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(346, 24)
        Me.Panel2.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Sub-Tenant"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.BindingSource = Me.bsProperty
        Me.BindingNavigator1.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.BindingNavigator1.Location = New System.Drawing.Point(18, 239)
        Me.BindingNavigator1.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator1.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator1.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator1.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator1.Size = New System.Drawing.Size(169, 25)
        Me.BindingNavigator1.TabIndex = 35
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(25, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlProperty)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(795, 323)
        Me.Panel8.TabIndex = 4
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlProperty
        '
        Me.pnlProperty.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlProperty.Controls.Add(Me.pbxVIP)
        Me.pnlProperty.Controls.Add(Label11)
        Me.pnlProperty.Controls.Add(Me.pbxTenancyRenewalAction)
        Me.pnlProperty.Controls.Add(Me.txbTenancyRenewalAction)
        Me.pnlProperty.Controls.Add(Me.TextBox3)
        Me.pnlProperty.Controls.Add(CANCELLATION_DTLabel)
        Me.pnlProperty.Controls.Add(Me.TextBox4)
        Me.pnlProperty.Controls.Add(COMPLETION_DTLabel)
        Me.pnlProperty.Controls.Add(Me.chkGuidelinesChased)
        Me.pnlProperty.Controls.Add(Label23)
        Me.pnlProperty.Controls.Add(Label30)
        Me.pnlProperty.Controls.Add(Me.txbDepositDeductions)
        Me.pnlProperty.Controls.Add(Label29)
        Me.pnlProperty.Controls.Add(Me.txbDep)
        Me.pnlProperty.Controls.Add(Label13)
        Me.pnlProperty.Controls.Add(Me.pbxInvCheckRetDt)
        Me.pnlProperty.Controls.Add(Me.txbInvCheckRetDt)
        Me.pnlProperty.Controls.Add(Me.pbxSTGuidelinesRetDt)
        Me.pnlProperty.Controls.Add(Me.txbSTGuidelinesRetDt)
        Me.pnlProperty.Controls.Add(Label12)
        Me.pnlProperty.Controls.Add(Label10)
        Me.pnlProperty.Controls.Add(Me.txbPropertyAddress)
        Me.pnlProperty.Controls.Add(Label9)
        Me.pnlProperty.Controls.Add(Me.txbClientNm)
        Me.pnlProperty.Controls.Add(Me.pbxActualMoveOutDt)
        Me.pnlProperty.Controls.Add(Me.txbActualMoveOutDt)
        Me.pnlProperty.Controls.Add(Label8)
        Me.pnlProperty.Controls.Add(Me.pbxActualMoveInDt)
        Me.pnlProperty.Controls.Add(Me.txbActualMoveInDt)
        Me.pnlProperty.Controls.Add(Label7)
        Me.pnlProperty.Controls.Add(Me.pbxProvMoveOutDt)
        Me.pnlProperty.Controls.Add(Me.txbProvMoveOutDt)
        Me.pnlProperty.Controls.Add(Label6)
        Me.pnlProperty.Controls.Add(Me.pbxProvMoveInDt)
        Me.pnlProperty.Controls.Add(Me.txbProvMoveInDt)
        Me.pnlProperty.Controls.Add(Label3)
        Me.pnlProperty.Controls.Add(Label1)
        Me.pnlProperty.Controls.Add(Me.txbPropertyCaseNo)
        Me.pnlProperty.Controls.Add(Me.SuperLabel2)
        Me.pnlProperty.Controls.Add(Me.pbxCopy)
        Me.pnlProperty.Controls.Add(Me.txbCaseNo)
        Me.pnlProperty.Controls.Add(Label14)
        Me.pnlProperty.Controls.Add(Me.txbPackageNm)
        Me.pnlProperty.Controls.Add(Me.BindingNavigator1)
        Me.pnlProperty.Location = New System.Drawing.Point(8, 32)
        Me.pnlProperty.Name = "pnlProperty"
        Me.pnlProperty.Size = New System.Drawing.Size(775, 280)
        Me.pnlProperty.TabIndex = 1
        '
        'pbxTenancyRenewalAction
        '
        Me.pbxTenancyRenewalAction.Image = CType(resources.GetObject("pbxTenancyRenewalAction.Image"), System.Drawing.Image)
        Me.pbxTenancyRenewalAction.Location = New System.Drawing.Point(737, 162)
        Me.pbxTenancyRenewalAction.Name = "pbxTenancyRenewalAction"
        Me.pbxTenancyRenewalAction.Size = New System.Drawing.Size(15, 15)
        Me.pbxTenancyRenewalAction.TabIndex = 180
        Me.pbxTenancyRenewalAction.TabStop = False
        '
        'txbTenancyRenewalAction
        '
        Me.txbTenancyRenewalAction.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbTenancyRenewalAction.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "TENANCY_RENEWAL_ACTION_DT", True))
        Me.txbTenancyRenewalAction.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbTenancyRenewalAction.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbTenancyRenewalAction.Location = New System.Drawing.Point(666, 159)
        Me.txbTenancyRenewalAction.Name = "txbTenancyRenewalAction"
        Me.txbTenancyRenewalAction.Size = New System.Drawing.Size(65, 20)
        Me.txbTenancyRenewalAction.TabIndex = 16
        Me.txbTenancyRenewalAction.ValidatingType = GetType(Date)
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "CANCELLATION_DT", True))
        Me.TextBox3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox3.Location = New System.Drawing.Point(702, 124)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(65, 20)
        Me.TextBox3.TabIndex = 15
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "COMPLETION_DT", True))
        Me.TextBox4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox4.Location = New System.Drawing.Point(702, 98)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(65, 20)
        Me.TextBox4.TabIndex = 14
        '
        'chkGuidelinesChased
        '
        Me.chkGuidelinesChased.AutoSize = True
        Me.chkGuidelinesChased.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGuidelinesChased.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkGuidelinesChased.Location = New System.Drawing.Point(438, 127)
        Me.chkGuidelinesChased.Name = "chkGuidelinesChased"
        Me.chkGuidelinesChased.Size = New System.Drawing.Size(15, 14)
        Me.chkGuidelinesChased.TabIndex = 10
        Me.chkGuidelinesChased.UseVisualStyleBackColor = True
        '
        'txbDepositDeductions
        '
        Me.txbDepositDeductions.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbDepositDeductions.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "DILAPIDATION_DS", True))
        Me.txbDepositDeductions.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDepositDeductions.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbDepositDeductions.Location = New System.Drawing.Point(438, 214)
        Me.txbDepositDeductions.Name = "txbDepositDeductions"
        Me.txbDepositDeductions.ReadOnly = True
        Me.txbDepositDeductions.Size = New System.Drawing.Size(329, 20)
        Me.txbDepositDeductions.TabIndex = 13
        '
        'txbDep
        '
        Me.txbDep.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbDep.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "TENANCY_ALLOWANCE_DS", True))
        Me.txbDep.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDep.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbDep.Location = New System.Drawing.Point(438, 188)
        Me.txbDep.Name = "txbDep"
        Me.txbDep.ReadOnly = True
        Me.txbDep.Size = New System.Drawing.Size(158, 20)
        Me.txbDep.TabIndex = 12
        '
        'pbxInvCheckRetDt
        '
        Me.pbxInvCheckRetDt.Image = CType(resources.GetObject("pbxInvCheckRetDt.Image"), System.Drawing.Image)
        Me.pbxInvCheckRetDt.Location = New System.Drawing.Point(510, 165)
        Me.pbxInvCheckRetDt.Name = "pbxInvCheckRetDt"
        Me.pbxInvCheckRetDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxInvCheckRetDt.TabIndex = 160
        Me.pbxInvCheckRetDt.TabStop = False
        '
        'txbInvCheckRetDt
        '
        Me.txbInvCheckRetDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbInvCheckRetDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "INVENTORY_RETD_DT", True))
        Me.txbInvCheckRetDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbInvCheckRetDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbInvCheckRetDt.Location = New System.Drawing.Point(438, 162)
        Me.txbInvCheckRetDt.Name = "txbInvCheckRetDt"
        Me.txbInvCheckRetDt.Size = New System.Drawing.Size(65, 20)
        Me.txbInvCheckRetDt.TabIndex = 11
        Me.txbInvCheckRetDt.ValidatingType = GetType(Date)
        '
        'pbxSTGuidelinesRetDt
        '
        Me.pbxSTGuidelinesRetDt.Image = CType(resources.GetObject("pbxSTGuidelinesRetDt.Image"), System.Drawing.Image)
        Me.pbxSTGuidelinesRetDt.Location = New System.Drawing.Point(510, 101)
        Me.pbxSTGuidelinesRetDt.Name = "pbxSTGuidelinesRetDt"
        Me.pbxSTGuidelinesRetDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxSTGuidelinesRetDt.TabIndex = 158
        Me.pbxSTGuidelinesRetDt.TabStop = False
        '
        'txbSTGuidelinesRetDt
        '
        Me.txbSTGuidelinesRetDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbSTGuidelinesRetDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "SUBT_GUIDE_RETD_DT", True))
        Me.txbSTGuidelinesRetDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbSTGuidelinesRetDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbSTGuidelinesRetDt.Location = New System.Drawing.Point(438, 98)
        Me.txbSTGuidelinesRetDt.Name = "txbSTGuidelinesRetDt"
        Me.txbSTGuidelinesRetDt.Size = New System.Drawing.Size(65, 20)
        Me.txbSTGuidelinesRetDt.TabIndex = 9
        Me.txbSTGuidelinesRetDt.ValidatingType = GetType(Date)
        '
        'txbPropertyAddress
        '
        Me.txbPropertyAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPropertyAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "PropertyAddress", True))
        Me.txbPropertyAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPropertyAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPropertyAddress.Location = New System.Drawing.Point(438, 72)
        Me.txbPropertyAddress.Name = "txbPropertyAddress"
        Me.txbPropertyAddress.ReadOnly = True
        Me.txbPropertyAddress.Size = New System.Drawing.Size(329, 20)
        Me.txbPropertyAddress.TabIndex = 8
        '
        'txbClientNm
        '
        Me.txbClientNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbClientNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "ClientName", True))
        Me.txbClientNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbClientNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbClientNm.Location = New System.Drawing.Point(438, 46)
        Me.txbClientNm.Name = "txbClientNm"
        Me.txbClientNm.ReadOnly = True
        Me.txbClientNm.Size = New System.Drawing.Size(329, 20)
        Me.txbClientNm.TabIndex = 7
        '
        'pbxActualMoveOutDt
        '
        Me.pbxActualMoveOutDt.Image = CType(resources.GetObject("pbxActualMoveOutDt.Image"), System.Drawing.Image)
        Me.pbxActualMoveOutDt.Location = New System.Drawing.Point(247, 153)
        Me.pbxActualMoveOutDt.Name = "pbxActualMoveOutDt"
        Me.pbxActualMoveOutDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxActualMoveOutDt.TabIndex = 150
        Me.pbxActualMoveOutDt.TabStop = False
        '
        'txbActualMoveOutDt
        '
        Me.txbActualMoveOutDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbActualMoveOutDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "VACATION_DT", True))
        Me.txbActualMoveOutDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbActualMoveOutDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbActualMoveOutDt.Location = New System.Drawing.Point(175, 150)
        Me.txbActualMoveOutDt.Name = "txbActualMoveOutDt"
        Me.txbActualMoveOutDt.Size = New System.Drawing.Size(65, 20)
        Me.txbActualMoveOutDt.TabIndex = 5
        Me.txbActualMoveOutDt.ValidatingType = GetType(Date)
        '
        'pbxActualMoveInDt
        '
        Me.pbxActualMoveInDt.Image = CType(resources.GetObject("pbxActualMoveInDt.Image"), System.Drawing.Image)
        Me.pbxActualMoveInDt.Location = New System.Drawing.Point(247, 127)
        Me.pbxActualMoveInDt.Name = "pbxActualMoveInDt"
        Me.pbxActualMoveInDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxActualMoveInDt.TabIndex = 147
        Me.pbxActualMoveInDt.TabStop = False
        '
        'txbActualMoveInDt
        '
        Me.txbActualMoveInDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbActualMoveInDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "ACTUAL_MOVE_IN_DT", True))
        Me.txbActualMoveInDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbActualMoveInDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbActualMoveInDt.Location = New System.Drawing.Point(175, 124)
        Me.txbActualMoveInDt.Name = "txbActualMoveInDt"
        Me.txbActualMoveInDt.Size = New System.Drawing.Size(65, 20)
        Me.txbActualMoveInDt.TabIndex = 4
        Me.txbActualMoveInDt.ValidatingType = GetType(Date)
        '
        'pbxProvMoveOutDt
        '
        Me.pbxProvMoveOutDt.Image = CType(resources.GetObject("pbxProvMoveOutDt.Image"), System.Drawing.Image)
        Me.pbxProvMoveOutDt.Location = New System.Drawing.Point(247, 101)
        Me.pbxProvMoveOutDt.Name = "pbxProvMoveOutDt"
        Me.pbxProvMoveOutDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxProvMoveOutDt.TabIndex = 144
        Me.pbxProvMoveOutDt.TabStop = False
        '
        'txbProvMoveOutDt
        '
        Me.txbProvMoveOutDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbProvMoveOutDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "ESTM_MOVE_OUT_DT", True))
        Me.txbProvMoveOutDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbProvMoveOutDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbProvMoveOutDt.Location = New System.Drawing.Point(175, 98)
        Me.txbProvMoveOutDt.Name = "txbProvMoveOutDt"
        Me.txbProvMoveOutDt.Size = New System.Drawing.Size(65, 20)
        Me.txbProvMoveOutDt.TabIndex = 3
        Me.txbProvMoveOutDt.ValidatingType = GetType(Date)
        '
        'pbxProvMoveInDt
        '
        Me.pbxProvMoveInDt.Image = CType(resources.GetObject("pbxProvMoveInDt.Image"), System.Drawing.Image)
        Me.pbxProvMoveInDt.Location = New System.Drawing.Point(247, 75)
        Me.pbxProvMoveInDt.Name = "pbxProvMoveInDt"
        Me.pbxProvMoveInDt.Size = New System.Drawing.Size(15, 15)
        Me.pbxProvMoveInDt.TabIndex = 141
        Me.pbxProvMoveInDt.TabStop = False
        '
        'txbProvMoveInDt
        '
        Me.txbProvMoveInDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbProvMoveInDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "ESTM_MOVE_IN_DT", True))
        Me.txbProvMoveInDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbProvMoveInDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbProvMoveInDt.Location = New System.Drawing.Point(175, 72)
        Me.txbProvMoveInDt.Name = "txbProvMoveInDt"
        Me.txbProvMoveInDt.Size = New System.Drawing.Size(65, 20)
        Me.txbProvMoveInDt.TabIndex = 2
        Me.txbProvMoveInDt.ValidatingType = GetType(Date)
        '
        'txbPropertyCaseNo
        '
        Me.txbPropertyCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPropertyCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "RELATED_CASE_NO", True))
        Me.txbPropertyCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPropertyCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPropertyCaseNo.Location = New System.Drawing.Point(175, 46)
        Me.txbPropertyCaseNo.Name = "txbPropertyCaseNo"
        Me.txbPropertyCaseNo.ReadOnly = True
        Me.txbPropertyCaseNo.Size = New System.Drawing.Size(48, 20)
        Me.txbPropertyCaseNo.TabIndex = 1
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(35, 23)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(85, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Person Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(229, 23)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "CSE_CASE_NO_CSE", True))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(175, 20)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(48, 20)
        Me.txbCaseNo.TabIndex = 0
        Me.txbCaseNo.Updateable = False
        '
        'txbPackageNm
        '
        Me.txbPackageNm.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbPackageNm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "CLIENT_PACKAGE_NM", True))
        Me.txbPackageNm.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPackageNm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbPackageNm.Location = New System.Drawing.Point(438, 20)
        Me.txbPackageNm.Name = "txbPackageNm"
        Me.txbPackageNm.ReadOnly = True
        Me.txbPackageNm.Size = New System.Drawing.Size(329, 20)
        Me.txbPackageNm.TabIndex = 6
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.txtPackageId)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(732, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(310, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'txtPackageId
        '
        Me.txtPackageId.BackColor = System.Drawing.Color.Yellow
        Me.txtPackageId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsProperty, "CGS_CLIENT_PACKAGE_ID_CGS", True))
        Me.txtPackageId.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackageId.ForeColor = System.Drawing.Color.DimGray
        Me.txtPackageId.Location = New System.Drawing.Point(345, 2)
        Me.txtPackageId.Name = "txtPackageId"
        Me.txtPackageId.ReadOnly = True
        Me.txtPackageId.Size = New System.Drawing.Size(43, 20)
        Me.txtPackageId.TabIndex = 146
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Property Details"
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(198, 19)
        Me.SuperLabel1.TabIndex = 7
        Me.SuperLabel1.Text = "Sub-Tenant Maintenance"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.PictureBox3)
        Me.Panel3.Controls.Add(Me.PictureBox5)
        Me.Panel3.Controls.Add(Me.pnlSubtenantDetails)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(411, 396)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(384, 305)
        Me.Panel3.TabIndex = 80
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(347, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 79
        Me.PictureBox3.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 78
        Me.PictureBox5.TabStop = False
        '
        'pnlSubtenantDetails
        '
        Me.pnlSubtenantDetails.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlSubtenantDetails.Controls.Add(Me.bnSubtenantDetails)
        Me.pnlSubtenantDetails.Controls.Add(Me.chkStandingOrder)
        Me.pnlSubtenantDetails.Controls.Add(Label34)
        Me.pnlSubtenantDetails.Controls.Add(Me.txtEmpRentContribution)
        Me.pnlSubtenantDetails.Controls.Add(Label16)
        Me.pnlSubtenantDetails.Controls.Add(Label17)
        Me.pnlSubtenantDetails.Controls.Add(Me.txtRentalBudget)
        Me.pnlSubtenantDetails.Location = New System.Drawing.Point(8, 32)
        Me.pnlSubtenantDetails.Name = "pnlSubtenantDetails"
        Me.pnlSubtenantDetails.Size = New System.Drawing.Size(364, 262)
        Me.pnlSubtenantDetails.TabIndex = 1
        '
        'bnSubtenantDetails
        '
        Me.bnSubtenantDetails.AddNewItem = Me.tsbAdd
        Me.bnSubtenantDetails.BindingSource = Me.bsSubtenantDetails
        Me.bnSubtenantDetails.CountItem = Me.ToolStripLabel2
        Me.bnSubtenantDetails.DeleteItem = Nothing
        Me.bnSubtenantDetails.Dock = System.Windows.Forms.DockStyle.None
        Me.bnSubtenantDetails.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.bnSubtenantDetails.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton5, Me.ToolStripButton6, Me.ToolStripSeparator3, Me.ToolStripTextBox2, Me.ToolStripLabel2, Me.ToolStripSeparator4, Me.ToolStripButton7, Me.ToolStripButton8, Me.tssAdd, Me.tsbAdd})
        Me.bnSubtenantDetails.Location = New System.Drawing.Point(20, 223)
        Me.bnSubtenantDetails.MoveFirstItem = Me.ToolStripButton5
        Me.bnSubtenantDetails.MoveLastItem = Me.ToolStripButton8
        Me.bnSubtenantDetails.MoveNextItem = Me.ToolStripButton7
        Me.bnSubtenantDetails.MovePreviousItem = Me.ToolStripButton6
        Me.bnSubtenantDetails.Name = "bnSubtenantDetails"
        Me.bnSubtenantDetails.PositionItem = Me.ToolStripTextBox2
        Me.bnSubtenantDetails.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.bnSubtenantDetails.Size = New System.Drawing.Size(198, 25)
        Me.bnSubtenantDetails.TabIndex = 205
        Me.bnSubtenantDetails.Text = "BindingNavigator3"
        '
        'tsbAdd
        '
        Me.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAdd.Image = CType(resources.GetObject("tsbAdd.Image"), System.Drawing.Image)
        Me.tsbAdd.Name = "tsbAdd"
        Me.tsbAdd.RightToLeftAutoMirrorImage = True
        Me.tsbAdd.Size = New System.Drawing.Size(23, 22)
        Me.tsbAdd.Text = "Add new"
        '
        'bsSubtenantDetails
        '
        Me.bsSubtenantDetails.DataMember = "V_frmSubTenantMaintenance_V_fsubSubTenantMaintenance"
        Me.bsSubtenantDetails.DataSource = Me.bsProperty
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(35, 22)
        Me.ToolStripLabel2.Text = "of {0}"
        Me.ToolStripLabel2.ToolTipText = "Total number of items"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Move first"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton6.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton6.Text = "Move previous"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripTextBox2
        '
        Me.ToolStripTextBox2.AccessibleName = "Position"
        Me.ToolStripTextBox2.AutoSize = False
        Me.ToolStripTextBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolStripTextBox2.Name = "ToolStripTextBox2"
        Me.ToolStripTextBox2.Size = New System.Drawing.Size(25, 21)
        Me.ToolStripTextBox2.Text = "0"
        Me.ToolStripTextBox2.ToolTipText = "Current position"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton7.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton7.Text = "Move next"
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ToolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton8.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton8.Text = "Move last"
        '
        'tssAdd
        '
        Me.tssAdd.Name = "tssAdd"
        Me.tssAdd.Size = New System.Drawing.Size(6, 25)
        '
        'chkStandingOrder
        '
        Me.chkStandingOrder.AutoSize = True
        Me.chkStandingOrder.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkStandingOrder.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkStandingOrder.Location = New System.Drawing.Point(170, 72)
        Me.chkStandingOrder.Name = "chkStandingOrder"
        Me.chkStandingOrder.Size = New System.Drawing.Size(15, 14)
        Me.chkStandingOrder.TabIndex = 2
        Me.chkStandingOrder.UseVisualStyleBackColor = True
        '
        'txtEmpRentContribution
        '
        Me.txtEmpRentContribution.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtEmpRentContribution.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSubtenantDetails, "RENT_EMPLOYEE_CONTRIBUTION_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtEmpRentContribution.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpRentContribution.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtEmpRentContribution.Location = New System.Drawing.Point(170, 46)
        Me.txtEmpRentContribution.Name = "txtEmpRentContribution"
        Me.txtEmpRentContribution.ReadOnly = True
        Me.txtEmpRentContribution.Size = New System.Drawing.Size(65, 20)
        Me.txtEmpRentContribution.TabIndex = 1
        Me.txtEmpRentContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRentalBudget
        '
        Me.txtRentalBudget.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txtRentalBudget.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsSubtenantDetails, "RENTAL_BUDGET_AM", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtRentalBudget.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRentalBudget.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtRentalBudget.Location = New System.Drawing.Point(170, 20)
        Me.txtRentalBudget.Name = "txtRentalBudget"
        Me.txtRentalBudget.ReadOnly = True
        Me.txtRentalBudget.Size = New System.Drawing.Size(65, 20)
        Me.txtRentalBudget.TabIndex = 0
        Me.txtRentalBudget.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Silver
        Me.Panel5.Controls.Add(Me.Label28)
        Me.Panel5.Location = New System.Drawing.Point(33, 8)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(321, 24)
        Me.Panel5.TabIndex = 0
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(0, 5)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(84, 14)
        Me.Label28.TabIndex = 73
        Me.Label28.Text = "Tenant Details"
        '
        'taProperty
        '
        Me.taProperty.ClearBeforeFill = True
        '
        'taSubtenant
        '
        Me.taSubtenant.ClearBeforeFill = True
        '
        'taSubtenantDetails
        '
        Me.taSubtenantDetails.ClearBeforeFill = True
        '
        'frmSubTenantMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "frmSubTenantMaintenance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Sub-Tenant Maintenance"
        CType(Me.pbxContactLookup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxLetter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFormStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSubtenant.ResumeLayout(False)
        Me.pnlSubtenant.PerformLayout()
        CType(Me.pbxEndDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsSubtenant, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsProperty, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsSubtenantMaintenance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProperty.ResumeLayout(False)
        Me.pnlProperty.PerformLayout()
        CType(Me.pbxTenancyRenewalAction, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxInvCheckRetDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxSTGuidelinesRetDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxActualMoveOutDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxActualMoveInDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxProvMoveOutDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxProvMoveInDt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSubtenantDetails.ResumeLayout(False)
        Me.pnlSubtenantDetails.PerformLayout()
        CType(Me.bnSubtenantDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnSubtenantDetails.ResumeLayout(False)
        Me.bnSubtenantDetails.PerformLayout()
        CType(Me.bsSubtenantDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlSubtenant As System.Windows.Forms.Panel
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents txbSubtenantNm As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlProperty As System.Windows.Forms.Panel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents txbPackageNm As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txbStartDt As System.Windows.Forms.TextBox
    Friend WithEvents txbPreviousAddress As System.Windows.Forms.TextBox
    Friend WithEvents cboFamilySize As ControlLibrary.SuperComboBox
    Friend WithEvents cboRoleTitle As ControlLibrary.SuperComboBox
    Friend WithEvents cboBusinessLine As ControlLibrary.SuperComboBox
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents txbPropertyCaseNo As System.Windows.Forms.TextBox
    Friend WithEvents pbxProvMoveOutDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbProvMoveOutDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxProvMoveInDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbProvMoveInDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxInvCheckRetDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbInvCheckRetDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxSTGuidelinesRetDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbSTGuidelinesRetDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txbPropertyAddress As System.Windows.Forms.TextBox
    Friend WithEvents txbClientNm As System.Windows.Forms.TextBox
    Friend WithEvents pbxActualMoveOutDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbActualMoveOutDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents pbxActualMoveInDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbActualMoveInDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txbDepositDeductions As System.Windows.Forms.TextBox
    Friend WithEvents txbDep As System.Windows.Forms.TextBox
    Friend WithEvents pbxContactLookup As System.Windows.Forms.PictureBox
    Friend WithEvents pbxEndDt As System.Windows.Forms.PictureBox
    Friend WithEvents txbEndDt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents bsProperty As System.Windows.Forms.BindingSource
    Friend WithEvents DsSubtenantMaintenance As clsRental.dsSubtenantMaintenance
    Friend WithEvents taProperty As clsRental.dsSubtenantMaintenanceTableAdapters.V_frmSubTenantMaintenanceTableAdapter
    Friend WithEvents bsSubtenant As System.Windows.Forms.BindingSource
    Friend WithEvents taSubtenant As clsRental.dsSubtenantMaintenanceTableAdapters.V_fsubSubTenantMaintSubTenantTableAdapter
    Friend WithEvents chkGuidelinesChased As ControlLibrary.SuperCheckBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents pbxTenancyRenewalAction As System.Windows.Forms.PictureBox
    Friend WithEvents txbTenancyRenewalAction As System.Windows.Forms.MaskedTextBox
    Friend WithEvents bsSubtenantDetails As System.Windows.Forms.BindingSource
    Friend WithEvents taSubtenantDetails As clsRental.dsSubtenantMaintenanceTableAdapters.V_fsubSubTenantMaintenanceTableAdapter
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlSubtenantDetails As System.Windows.Forms.Panel
    Friend WithEvents txtRentalBudget As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtEmpRentContribution As System.Windows.Forms.TextBox
    Friend WithEvents chkStandingOrder As ControlLibrary.SuperCheckBox
    Friend WithEvents bnSubtenantDetails As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox2 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tssAdd As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents txtPackageId As System.Windows.Forms.TextBox
    Friend WithEvents pbxLetter As System.Windows.Forms.PictureBox
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
