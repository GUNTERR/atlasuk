<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fdlgTenancyHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FULL_NMLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fdlgTenancyHistory))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.pnlDetails = New System.Windows.Forms.Panel()
        Me.txbTenancyStartDt = New System.Windows.Forms.TextBox()
        Me.bsTenancy = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsMaintainRentalProperty = New clsRental.dsMaintainRentalProperty()
        Me.cboNoticePeriod = New ControlLibrary.SuperComboBox()
        Me.txbNoticePeriod = New System.Windows.Forms.TextBox()
        Me.txbStatus = New System.Windows.Forms.TextBox()
        Me.txbNoticeDt = New System.Windows.Forms.TextBox()
        Me.txbTenancyEndDt = New System.Windows.Forms.TextBox()
        Me.txbCaseNo = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.taTenancyHistory = New clsRental.dsMaintainRentalPropertyTableAdapters.V_fdlgTenancyHistoryTableAdapter()
        FULL_NMLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDetails.SuspendLayout()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMaintainRentalProperty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FULL_NMLabel
        '
        FULL_NMLabel.AutoSize = True
        FULL_NMLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        FULL_NMLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FULL_NMLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        FULL_NMLabel.Location = New System.Drawing.Point(26, 20)
        FULL_NMLabel.Name = "FULL_NMLabel"
        FULL_NMLabel.Size = New System.Drawing.Size(48, 14)
        FULL_NMLabel.TabIndex = 0
        FULL_NMLabel.Text = "Case No"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(26, 72)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(69, 14)
        Label5.TabIndex = 4
        Label5.Text = "Tenancy End"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label11.Location = New System.Drawing.Point(26, 98)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(62, 14)
        Label11.TabIndex = 6
        Label11.Text = "Notice Date"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label16.Location = New System.Drawing.Point(26, 124)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(38, 14)
        Label16.TabIndex = 8
        Label16.Text = "Status"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label17.Location = New System.Drawing.Point(26, 150)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(70, 14)
        Label17.TabIndex = 10
        Label17.Text = "Notice Period"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label1.Location = New System.Drawing.Point(26, 46)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(74, 14)
        Label1.TabIndex = 36
        Label1.Text = "Tenancy Start"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(352, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(305, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(70, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(255, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox7)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.pnlDetails)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(8, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(335, 239)
        Me.Panel1.TabIndex = 5
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(300, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 79
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 78
        Me.PictureBox4.TabStop = False
        '
        'pnlDetails
        '
        Me.pnlDetails.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlDetails.Controls.Add(Label1)
        Me.pnlDetails.Controls.Add(Me.txbTenancyStartDt)
        Me.pnlDetails.Controls.Add(Me.cboNoticePeriod)
        Me.pnlDetails.Controls.Add(Label17)
        Me.pnlDetails.Controls.Add(Me.txbNoticePeriod)
        Me.pnlDetails.Controls.Add(Label16)
        Me.pnlDetails.Controls.Add(Me.txbStatus)
        Me.pnlDetails.Controls.Add(Label11)
        Me.pnlDetails.Controls.Add(Me.txbNoticeDt)
        Me.pnlDetails.Controls.Add(Label5)
        Me.pnlDetails.Controls.Add(Me.txbTenancyEndDt)
        Me.pnlDetails.Controls.Add(FULL_NMLabel)
        Me.pnlDetails.Controls.Add(Me.txbCaseNo)
        Me.pnlDetails.Enabled = False
        Me.pnlDetails.Location = New System.Drawing.Point(8, 32)
        Me.pnlDetails.Name = "pnlDetails"
        Me.pnlDetails.Size = New System.Drawing.Size(317, 195)
        Me.pnlDetails.TabIndex = 1
        '
        'txbTenancyStartDt
        '
        Me.txbTenancyStartDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbTenancyStartDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "TENANCY_START_DT", True))
        Me.txbTenancyStartDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbTenancyStartDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbTenancyStartDt.Location = New System.Drawing.Point(140, 43)
        Me.txbTenancyStartDt.Name = "txbTenancyStartDt"
        Me.txbTenancyStartDt.ReadOnly = True
        Me.txbTenancyStartDt.Size = New System.Drawing.Size(77, 20)
        Me.txbTenancyStartDt.TabIndex = 1
        '
        'bsTenancy
        '
        Me.bsTenancy.DataMember = "V_fdlgTenancyHistory"
        Me.bsTenancy.DataSource = Me.DsMaintainRentalProperty
        '
        'DsMaintainRentalProperty
        '
        Me.DsMaintainRentalProperty.DataSetName = "dsMaintainRentalProperty"
        Me.DsMaintainRentalProperty.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'cboNoticePeriod
        '
        Me.cboNoticePeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboNoticePeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboNoticePeriod.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsTenancy, "NOTICE_PERIOD_CD", True))
        Me.cboNoticePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNoticePeriod.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNoticePeriod.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cboNoticePeriod.FormattingEnabled = True
        Me.cboNoticePeriod.Location = New System.Drawing.Point(206, 147)
        Me.cboNoticePeriod.Name = "cboNoticePeriod"
        Me.cboNoticePeriod.Queryable = False
        Me.cboNoticePeriod.Size = New System.Drawing.Size(74, 22)
        Me.cboNoticePeriod.TabIndex = 6
        Me.cboNoticePeriod.Updateable = True
        '
        'txbNoticePeriod
        '
        Me.txbNoticePeriod.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbNoticePeriod.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "NOTICE_PERIOD_QT", True))
        Me.txbNoticePeriod.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbNoticePeriod.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbNoticePeriod.Location = New System.Drawing.Point(140, 147)
        Me.txbNoticePeriod.Name = "txbNoticePeriod"
        Me.txbNoticePeriod.ReadOnly = True
        Me.txbNoticePeriod.Size = New System.Drawing.Size(60, 20)
        Me.txbNoticePeriod.TabIndex = 5
        '
        'txbStatus
        '
        Me.txbStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "STATUS_TX", True))
        Me.txbStatus.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbStatus.Location = New System.Drawing.Point(140, 121)
        Me.txbStatus.Name = "txbStatus"
        Me.txbStatus.ReadOnly = True
        Me.txbStatus.Size = New System.Drawing.Size(140, 20)
        Me.txbStatus.TabIndex = 4
        '
        'txbNoticeDt
        '
        Me.txbNoticeDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbNoticeDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "NOTICE_DT", True))
        Me.txbNoticeDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbNoticeDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbNoticeDt.Location = New System.Drawing.Point(140, 95)
        Me.txbNoticeDt.Name = "txbNoticeDt"
        Me.txbNoticeDt.ReadOnly = True
        Me.txbNoticeDt.Size = New System.Drawing.Size(77, 20)
        Me.txbNoticeDt.TabIndex = 3
        '
        'txbTenancyEndDt
        '
        Me.txbTenancyEndDt.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbTenancyEndDt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "TENANCY_END_DT", True))
        Me.txbTenancyEndDt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbTenancyEndDt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbTenancyEndDt.Location = New System.Drawing.Point(140, 69)
        Me.txbTenancyEndDt.Name = "txbTenancyEndDt"
        Me.txbTenancyEndDt.ReadOnly = True
        Me.txbTenancyEndDt.Size = New System.Drawing.Size(77, 20)
        Me.txbTenancyEndDt.TabIndex = 2
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsTenancy, "AST_CASE_NO_CSE", True))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(140, 17)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(77, 20)
        Me.txbCaseNo.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(273, 24)
        Me.Panel2.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 14)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Details"
        '
        'taTenancyHistory
        '
        Me.taTenancyHistory.ClearBeforeFill = True
        '
        'fdlgTenancyHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(352, 281)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fdlgTenancyHistory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tenancy History Details"
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDetails.ResumeLayout(False)
        Me.pnlDetails.PerformLayout()
        CType(Me.bsTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMaintainRentalProperty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlDetails As System.Windows.Forms.Panel
    Friend WithEvents txbCaseNo As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents txbNoticePeriod As System.Windows.Forms.TextBox
    Friend WithEvents txbStatus As System.Windows.Forms.TextBox
    Friend WithEvents txbNoticeDt As System.Windows.Forms.TextBox
    Friend WithEvents txbTenancyEndDt As System.Windows.Forms.TextBox
    Friend WithEvents cboNoticePeriod As ControlLibrary.SuperComboBox
    Friend WithEvents txbTenancyStartDt As System.Windows.Forms.TextBox
    Friend WithEvents bsTenancy As System.Windows.Forms.BindingSource
    Friend WithEvents DsMaintainRentalProperty As clsRental.dsMaintainRentalProperty
    Friend WithEvents taTenancyHistory As clsRental.dsMaintainRentalPropertyTableAdapters.V_fdlgTenancyHistoryTableAdapter
End Class
