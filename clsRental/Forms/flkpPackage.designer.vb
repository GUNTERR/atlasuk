<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class flkpPackage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(flkpPackage))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlClient = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvClient = New System.Windows.Forms.DataGridView()
        Me.CSE_CASE_NO_CSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGS_CLIENT_PACKAGE_ID_CGS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLIENT_PACKAGE_NM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsPackage = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsManualApportionments = New clsRental.dsManualApportionments()
        Me.DsARInvoice = New clsRental.dsARInvoice()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.taPackage = New clsRental.dsManualApportionmentsTableAdapters.V_flkpPackageTableAdapter()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.pnlClient.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsManualApportionments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsARInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pnlClient
        '
        Me.pnlClient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlClient.Controls.Add(Me.PictureBox6)
        Me.pnlClient.Controls.Add(Me.PictureBox5)
        Me.pnlClient.Controls.Add(Me.Panel4)
        Me.pnlClient.Controls.Add(Me.dgvClient)
        Me.pnlClient.Location = New System.Drawing.Point(8, 33)
        Me.pnlClient.Name = "pnlClient"
        Me.pnlClient.Size = New System.Drawing.Size(355, 249)
        Me.pnlClient.TabIndex = 6
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(319, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(293, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Client"
        '
        'dgvClient
        '
        Me.dgvClient.AllowUserToAddRows = False
        Me.dgvClient.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvClient.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvClient.AutoGenerateColumns = False
        Me.dgvClient.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvClient.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvClient.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvClient.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvClient.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvClient.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSE_CASE_NO_CSE, Me.CGS_CLIENT_PACKAGE_ID_CGS, Me.CLIENT_PACKAGE_NM})
        Me.dgvClient.DataSource = Me.bsPackage
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvClient.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvClient.EnableHeadersVisualStyles = False
        Me.dgvClient.Location = New System.Drawing.Point(8, 32)
        Me.dgvClient.Name = "dgvClient"
        Me.dgvClient.ReadOnly = True
        Me.dgvClient.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvClient.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvClient.RowHeadersWidth = 18
        Me.dgvClient.RowTemplate.Height = 20
        Me.dgvClient.Size = New System.Drawing.Size(336, 203)
        Me.dgvClient.TabIndex = 76
        Me.dgvClient.TabStop = False
        '
        'CSE_CASE_NO_CSE
        '
        Me.CSE_CASE_NO_CSE.DataPropertyName = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.HeaderText = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.Name = "CSE_CASE_NO_CSE"
        Me.CSE_CASE_NO_CSE.ReadOnly = True
        Me.CSE_CASE_NO_CSE.Visible = False
        '
        'CGS_CLIENT_PACKAGE_ID_CGS
        '
        Me.CGS_CLIENT_PACKAGE_ID_CGS.DataPropertyName = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGS_CLIENT_PACKAGE_ID_CGS.HeaderText = "ID"
        Me.CGS_CLIENT_PACKAGE_ID_CGS.Name = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGS_CLIENT_PACKAGE_ID_CGS.ReadOnly = True
        Me.CGS_CLIENT_PACKAGE_ID_CGS.Width = 80
        '
        'CLIENT_PACKAGE_NM
        '
        Me.CLIENT_PACKAGE_NM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CLIENT_PACKAGE_NM.DataPropertyName = "CLIENT_PACKAGE_NM"
        Me.CLIENT_PACKAGE_NM.HeaderText = "Name"
        Me.CLIENT_PACKAGE_NM.Name = "CLIENT_PACKAGE_NM"
        Me.CLIENT_PACKAGE_NM.ReadOnly = True
        '
        'bsPackage
        '
        Me.bsPackage.DataMember = "V_flkpPackage"
        Me.bsPackage.DataSource = Me.DsManualApportionments
        '
        'DsManualApportionments
        '
        Me.DsManualApportionments.DataSetName = "dsManualApportionments"
        Me.DsManualApportionments.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DsARInvoice
        '
        Me.DsARInvoice.DataSetName = "dsARInvoice"
        Me.DsARInvoice.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.46473!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.53527!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(172, 291)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(193, 29)
        Me.TableLayoutPanel1.TabIndex = 145
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.OK_Button.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK_Button.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(126, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Accept and Close"
        Me.OK_Button.UseVisualStyleBackColor = False
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Cancel_Button.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel_Button.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Cancel_Button.Location = New System.Drawing.Point(135, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(55, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Close"
        Me.Cancel_Button.UseVisualStyleBackColor = False
        '
        'taPackage
        '
        Me.taPackage.ClearBeforeFill = True
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(373, 24)
        Me.pnlMainToolStrip.TabIndex = 146
        '
        'flkpPackage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(373, 329)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.pnlClient)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "flkpPackage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Package Lookup"
        Me.pnlClient.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsManualApportionments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsARInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlClient As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvClient As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents DsARInvoice As clsRental.dsARInvoice
    Friend WithEvents bsPackage As System.Windows.Forms.BindingSource
    Friend WithEvents DsManualApportionments As clsRental.dsManualApportionments
    Friend WithEvents taPackage As clsRental.dsManualApportionmentsTableAdapters.V_flkpPackageTableAdapter
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents CSE_CASE_NO_CSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGS_CLIENT_PACKAGE_ID_CGS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLIENT_PACKAGE_NM As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
