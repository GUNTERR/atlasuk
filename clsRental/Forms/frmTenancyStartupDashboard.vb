﻿Imports System.IO
Public Class frmTenancyStartupDashboard
    Private dsLookups As New Data.DataSet
    Private strLookupFile As String
    Private blnReadOnly As Boolean = False
    Private blnFormLoaded As Boolean = False
    Private mblnBenchmarkOn As Boolean
    Private strFilterLookupFilePath As String

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            clsGlobal.CreateExcelToolStrip(Me)

            strLookupFile = Me.Name + "LookUpsDataSet.xml"
            strFilterLookupFilePath = clsGlobal.LookupFilePath & Me.Name & "Filter.xml"

            ReadOnlyFields(True)

            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, True, blnReadOnly)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            Me.taConsultant.Connection = clsGlobal.Connection
            Me.taConsultant.Fill(Me.DsTenancyStartupDashboard.V_fsubTenancyStartupDashboardConsultant)

            Directory.CreateDirectory(clsGlobal.LookupFilePath)

            If File.Exists(clsGlobal.LookupFilePath + strLookupFile) Then
                dsLookups.ReadXml(clsGlobal.LookupFilePath + strLookupFile)
            Else
                LoadLookupLists()
            End If

            LoadAndBindComboBoxes()

            Me.cboDepositReturned.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboConsultant.DropDownStyle = ComboBoxStyle.DropDown

            TurnOffFilters()

            SetDefaultFilter()

            FillForm()

            blnFormLoaded = True

            Me.tspRight.Items.RemoveByKey("tsbWord")
            Me.tspRight.Items.RemoveByKey("tsbCostSaving")
            Me.tspRight.Items.RemoveByKey("tsbZoom")

            Me.tspMain.Items("tslSave").Enabled = False
            Me.tspMain.Items("tslUndo").Enabled = False

        Catch ex As Exception
            MessageBox.Show(ex.Message, "FormLoad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now

        Dim strDepositNotPaid As String = Nothing
        Dim strDepositReturned As String = Nothing
        Dim strFinalAgreedClaimAm As String = Nothing
        Dim strOriginalClaimAm As String = Nothing
        Dim strConsultant As String = Nothing
        Dim strNoTenancyEndDt As String = Nothing

        Try
            Me.btnGo.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            If Me.chkDepositNotPaid.Checked Then
                strDepositNotPaid = "Y"
            End If

            If Me.chkDepositReturned.Checked Then
                If Me.cboDepositReturned.SelectedValue = "N" Then
                    strDepositReturned = "N"
                ElseIf Me.cboDepositReturned.SelectedValue = "Y" Then
                    strDepositReturned = "Y"
                Else
                    strDepositReturned = Nothing
                End If
            End If

            If Me.chkOriginalClaimAm.Checked Then
                strOriginalClaimAm = "Y"
            End If

            If Me.chkFinalAgreedClaimAm.Checked Then
                strFinalAgreedClaimAm = "Y"
            End If

            If Me.chkNoTenancyEndDt.Checked Then
                strNoTenancyEndDt = "Y"
            End If

            If Me.cboConsultant.SelectedIndex > -1 Then
                strConsultant = Me.cboConsultant.SelectedValue
            Else
                strConsultant = Nothing
            End If

            Me.taTenancyStartup.Connection = clsGlobal.Connection
            Me.taTenancyStartup.Fill(Me.DsTenancyStartupDashboard.OMNI_VIEW_TENANCY_STARTUP_DASHBOARD,
                    IIf(IsDate(Me.txtTenancyStartDt1.Text), Me.txtTenancyStartDt1.Text, Nothing),
                    IIf(IsDate(Me.txtTenancyStartDt2.Text), Me.txtTenancyStartDt2.Text, Nothing),
                    IIf(IsDate(Me.txtTenancyEndDt1.Text), Me.txtTenancyEndDt1.Text, Nothing),
                    IIf(IsDate(Me.txtTenancyEndDt2.Text), Me.txtTenancyEndDt2.Text, Nothing),
                    strDepositNotPaid,
                    strDepositReturned,
                    strOriginalClaimAm,
                    strFinalAgreedClaimAm,
                    strConsultant,
                    strNoTenancyEndDt)

            If Me.bsTenancyStartup.Count > 0 Then
                ReadOnlyFields(blnReadOnly)
            Else
                MsgBox("No records returned", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Me.Text)
                ReadOnlyFields(True)
            End If

            Me.btnGo.Focus()

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "FillForm", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            Me.Cursor = Cursors.Default
            Me.btnGo.Enabled = True
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now,
                Me.txtTenancyStartDt1.Text & ", " &
                Me.txtTenancyStartDt2.Text & ", " &
                Me.txtTenancyEndDt1.Text & ", " &
                Me.txtTenancyEndDt2.Text & ", " &
                strDepositNotPaid & ", " &
                strDepositReturned & ", " &
                strOriginalClaimAm & ", " &
                strFinalAgreedClaimAm)
        End Try
    End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)

        'Me.txtTenancyStartDt1.ReadOnly = True
        'clsGlobal.SetBackColor(True, Me.txtTenancyStartDt1)

    End Sub


    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                End Select
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "tspRight_ItemClicked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub chkTenancyStartDt_CheckedChanged(sender As Object, e As EventArgs) Handles chkTenancyStartDt.CheckedChanged
        If Me.chkTenancyStartDt.Checked Then
            Me.txtTenancyStartDt1.Enabled = True
            Me.txtTenancyStartDt2.Enabled = True

            Me.txtTenancyStartDt1.Text = Microsoft.VisualBasic.DateAdd(DateInterval.Month, -6, Date.Now.Date)
            Me.txtTenancyStartDt2.Text = Microsoft.VisualBasic.DateAdd(DateInterval.Day, 1, Date.Now.Date)
            Me.pbxTenancyStartDtStart.Enabled = True
            Me.pbxTenancyStartDtEnd.Enabled = True
        Else
            Me.txtTenancyStartDt1.Enabled = False
            Me.txtTenancyStartDt2.Enabled = False
            Me.txtTenancyStartDt1.Clear()
            Me.txtTenancyStartDt2.Clear()
            Me.pbxTenancyStartDtStart.Enabled = False
            Me.pbxTenancyStartDtEnd.Enabled = False
        End If
    End Sub

    Private Sub chkTenancyEndDt_CheckedChanged(sender As Object, e As EventArgs) Handles chkTenancyEndDt.CheckedChanged
        If Me.chkTenancyEndDt.Checked Then
            Me.txtTenancyEndDt1.Enabled = True
            Me.txtTenancyEndDt2.Enabled = True

            Me.txtTenancyEndDt1.Text = Microsoft.VisualBasic.DateAdd(DateInterval.Month, -6, Date.Now.Date)
            Me.txtTenancyEndDt2.Text = Microsoft.VisualBasic.DateAdd(DateInterval.Day, 1, Date.Now.Date)
            Me.pbxTenancyStartDtStart.Enabled = True
            Me.pbxTenancyStartDtEnd.Enabled = True
        Else
            Me.txtTenancyEndDt1.Enabled = False
            Me.txtTenancyEndDt2.Enabled = False
            Me.txtTenancyEndDt1.Clear()
            Me.txtTenancyEndDt2.Clear()
            Me.pbxTenancyEndDtStart.Enabled = False
            Me.pbxTenancyEndDtEnd.Enabled = False
        End If
    End Sub

    Private Sub TurnOffFilters()

        Me.chkTenancyEndDt.Checked = False
        Me.chkDepositNotPaid.Checked = False
        Me.chkDepositReturned.Checked = False
        Me.chkOriginalClaimAm.Checked = False
        Me.chkFinalAgreedClaimAm.Checked = False
        Me.chkNoTenancyEndDt.Checked = False
        Me.cboConsultant.SelectedIndex = -1
        Me.cboConsultant.Enabled = False
        Me.cboDepositReturned.SelectedIndex = -1
        Me.cboDepositReturned.Enabled = False
    End Sub
    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click

        Me.chkTenancyEndDt.Checked = False
        Me.chkDepositNotPaid.Checked = False
        Me.chkDepositReturned.Checked = False
        Me.chkFinalAgreedClaimAm.Checked = False
        Me.chkOriginalClaimAm.Checked = False
        Me.chkNoTenancyEndDt.Checked = False
        Me.cboConsultant.SelectedIndex = -1
        Me.cboConsultant.Enabled = False

        SetDefaultFilter()
    End Sub

    Private Sub SetDefaultFilter()
        Me.txtTenancyStartDt1.Text = Microsoft.VisualBasic.DateAdd(DateInterval.Month, -6, Date.Now.Date)
        Me.txtTenancyStartDt2.Text = Microsoft.VisualBasic.DateAdd(DateInterval.Day, 1, Date.Now.Date)

        Me.txtTenancyEndDt1.Enabled = False
        Me.txtTenancyEndDt2.Enabled = False
        Me.txtTenancyEndDt1.Clear()
        Me.txtTenancyEndDt2.Clear()
        Me.pbxTenancyEndDtStart.Enabled = False
        Me.pbxTenancyEndDtEnd.Enabled = False
        Me.txtTenancyEndDt1.ReadOnly = True
        clsGlobal.SetBackColor(True, Me.txtTenancyEndDt1)
        Me.txtTenancyEndDt2.ReadOnly = True
        clsGlobal.SetBackColor(True, Me.txtTenancyEndDt2)
        Me.cboDepositReturned.SelectedIndex = -1
        Me.cboDepositReturned.Enabled = False

        Me.chkTenancyStartDt.Checked = True
    End Sub


    Private Sub LoadLookupLists()
        Try
            'Deposit Returned
            Dim strSQL As String = "exec maarten.OMNI_FILL_COMBO 'INDICATOR_YES';"

            Dim aArray(1) As String
            aArray(0) = "DepositReturnedLookup"

            clsGlobal.LoadLookupLists(strSQL, strLookupFile, dsLookups, aArray)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadLookupLists", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub LoadAndBindComboBoxes()
        Try
            With Me.cboDepositReturned
                .DataSource = dsLookups.Tables("DepositReturnedLookup")
                .DisplayMember = "PROMPT"
                .ValueMember = "COLUMN_VALUE"
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadAndBindComboBoxes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Dim blnError As Boolean = False

        If Me.chkTenancyStartDt.Checked Then
            If IsDate(Me.txtTenancyStartDt1.Text) Then
                If IsDate(Me.txtTenancyStartDt2.Text) Then
                    If CType(Me.txtTenancyStartDt1.Text, Date) < CType(Me.txtTenancyStartDt2.Text, Date) Then
                        blnError = False
                    Else
                        MessageBox.Show("Start Date Must Be Before The End Date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        blnError = True
                    End If
                Else
                    MessageBox.Show("Invalid End Date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    blnError = True
                End If
            Else
                MessageBox.Show("Invalid Start Date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                blnError = True
            End If
        End If

        If Me.chkTenancyEndDt.Checked Then
            If IsDate(Me.txtTenancyEndDt1.Text) Then
                If IsDate(Me.txtTenancyEndDt2.Text) Then
                    If CType(Me.txtTenancyEndDt1.Text, Date) < CType(Me.txtTenancyEndDt2.Text, Date) Then
                        blnError = False
                    Else
                        MessageBox.Show("Start Date Must Be Before The End Date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        blnError = True
                    End If
                Else
                    MessageBox.Show("Invalid End Date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    blnError = True
                End If
            Else
                MessageBox.Show("Invalid Start Date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                blnError = True
            End If
        End If

        If Me.chkDepositReturned.Checked Then
            If Me.cboDepositReturned.SelectedIndex = -1 Then
                MessageBox.Show("Please select Deposit Returned or uncheck the Deposit Returned tick box.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                blnError = True
            End If
        End If

        If blnError = False Then
            If Me.chkTenancyStartDt.Checked Or Me.chkTenancyEndDt.Checked Or Me.chkDepositNotPaid.Checked Or
                Me.chkDepositReturned.Checked Or Me.chkFinalAgreedClaimAm.Checked Or Me.chkOriginalClaimAm.Checked Or
                Me.chkNoTenancyEndDt.Checked Then
                FillForm()
            End If

        End If

    End Sub

    Private Sub pbxTenancyStartDtStart_Click(sender As Object, e As EventArgs) Handles pbxTenancyStartDtStart.Click
        Dim frmX As New frmDatePicker
        frmX.CallingForm(Me.txtTenancyStartDt1)
        frmX.ShowDialog()
    End Sub

    Private Sub pbxTenancyStartDtStart_MouseHover(sender As Object, e As EventArgs) Handles pbxTenancyStartDtStart.MouseHover
        clsGlobal.SetCursorHand(Me, Me.lblCreatedStartDt)
    End Sub

    Private Sub pbxTenancyStartDtStart_MouseLeave(sender As Object, e As EventArgs) Handles pbxTenancyStartDtStart.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub
    Private Sub pbxTenancyStartDtEnd_Click(sender As Object, e As EventArgs) Handles pbxTenancyStartDtEnd.Click
        Dim frmX As New frmDatePicker
        frmX.CallingForm(Me.txtTenancyStartDt2)
        frmX.ShowDialog()
    End Sub

    Private Sub pbxTenancyStartDtEnd_MouseHover(sender As Object, e As EventArgs) Handles pbxTenancyStartDtEnd.MouseHover
        clsGlobal.SetCursorHand(Me, Me.lblCreatedStartDt)
    End Sub

    Private Sub pbxTenancyStartDtEnd_MouseLeave(sender As Object, e As EventArgs) Handles pbxTenancyStartDtEnd.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub pbxTenancyEndDtStart_Click(sender As Object, e As EventArgs) Handles pbxTenancyEndDtStart.Click
        Dim frmX As New frmDatePicker
        frmX.CallingForm(Me.txtTenancyEndDt1)
        frmX.ShowDialog()
    End Sub
    Private Sub pbxTenancyEndDtStart_MouseHover(sender As Object, e As EventArgs) Handles pbxTenancyEndDtStart.MouseHover
        clsGlobal.SetCursorHand(Me, Me.lblCreatedStartDt)
    End Sub

    Private Sub pbxTenancyEndDtStart_MouseLeave(sender As Object, e As EventArgs) Handles pbxTenancyEndDtStart.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub
    Private Sub pbxTenancyEndDtEnd_Click(sender As Object, e As EventArgs) Handles pbxTenancyEndDtEnd.Click
        Dim frmX As New frmDatePicker
        frmX.CallingForm(Me.txtTenancyEndDt2)
        frmX.ShowDialog()
    End Sub

    Private Sub pbxTenancyEndDtEnd_MouseHover(sender As Object, e As EventArgs) Handles pbxTenancyEndDtEnd.MouseHover
        clsGlobal.SetCursorHand(Me, Me.lblCreatedStartDt)
    End Sub

    Private Sub pbxTenancyEndDtEnd_MouseLeave(sender As Object, e As EventArgs) Handles pbxTenancyEndDtEnd.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub OpenTenancyStartup()
        Try
            clsGlobal.CopyToFormStripFromGrid(Me.dgvTenancyStartup.CurrentRow.Cells("lnkCaseNo").Value, Me.pnlFormStrip)

            Dim frmX As Object
            For Each frmX In Application.OpenForms
                If frmX.Name = "frmMainTabMenu" Then
                    frmX.LoadForm("Tenancy Startup", Nothing)
                    Me.Close()
                    Exit For
                End If
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message, "OpenLegalRequest", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub dgvTenancyStartup_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTenancyStartup.CellContentClick
        If e.RowIndex >= 0 Then
            If Me.dgvTenancyStartup.Columns(e.ColumnIndex).Name = "lnkCaseNo" Then
                OpenTenancyStartup()
            End If

        End If
    End Sub

    Private Sub dgvTenancyStartup_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTenancyStartup.CellDoubleClick
        OpenTenancyStartup()
    End Sub

    Private Sub chkConsultant_CheckedChanged(sender As Object, e As EventArgs) Handles chkConsultant.CheckedChanged
        If Me.chkConsultant.Checked Then
            Me.cboConsultant.Enabled = True
        Else
            Me.cboConsultant.SelectedIndex = -1
            Me.cboConsultant.Enabled = False
        End If
    End Sub

    Private Sub chkDepositReturned_CheckedChanged(sender As Object, e As EventArgs) Handles chkDepositReturned.CheckedChanged
        If Me.chkDepositReturned.Checked Then
            Me.cboDepositReturned.Enabled = True
        Else
            Me.cboDepositReturned.SelectedIndex = -1
            Me.cboDepositReturned.Enabled = False
        End If
    End Sub


End Class