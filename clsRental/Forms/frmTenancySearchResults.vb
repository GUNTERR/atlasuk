Imports System.IO
Imports System.ComponentModel

Public Class frmTenancySearchResults
    Private dsLookups As New Data.DataSet
    Private strLookupFile As String
    Dim bsiX As New clsBindingSourceItem
    Private blnInQueryMode As Boolean
    Private blnIsNewRow As Boolean = False
    Private blnReadOnly As Boolean = False
    Private mintCurrentRecord As Integer
    Private mblnFillForm As Boolean
    Private mclsGlobal As New clsGlobal
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strLookupFile = Me.Name + "LookUpsDataSet.xml"
            ReadOnlyFields(True)

            'blnReadOnly = clsGlobal.SetupForm(Me, False, blnReadOnly)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, False, blnReadOnly)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            If blnReadOnly Then
                Me.tsbAdd.Visible = False
            End If

            HidePrimaryKeyFields()

            clsGlobal.CreateFormStripItems(Me, pnlFormStrip)

            Me.chkGarage.BindData("Checked", Me.bsTenancy, "GARAGE_IN")
            Me.chkPetsAllowed.BindData("Checked", Me.bsTenancy, "PETS_ALLOWED_IN")
            Me.chkSmokersAllowed.BindData("Checked", Me.bsTenancy, "SMOKERS_OK_IN")
            Me.chkChildrenAllowed.BindData("Checked", Me.bsTenancy, "CHILDREN_ALLOWED_IN")
            Me.chkGarden.BindData("Checked", Me.bsTenancy, "GARDEN_IN")
            Me.chkSendToEmployee.BindData("Checked", Me.bsTenancy, "SEND_TO_EMPLOYEE_IN")

            Directory.CreateDirectory(clsGlobal.LookupFilePath)

            If File.Exists(clsGlobal.LookupFilePath + strLookupFile) Then
                dsLookups.ReadXml(clsGlobal.LookupFilePath + strLookupFile)
            Else
                LoadLookupLists()
            End If

            LoadAndBindComboBoxes()

            Me.cboFurnishingCode.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboParkingDetails.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboPropertyType.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboRentFrequency.DropDownStyle = ComboBoxStyle.DropDown
            Me.cboTenancySearchStatus.DropDownStyle = ComboBoxStyle.DropDown


            bsiX.Add(Me.bsCase)
            bsiX.Add(Me.bsTenancy)

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                FillForm()
            Else    ' Start in query mode - can be removed if required
                RunQuery()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            clsGlobal.CheckFormHasChanges(Me, bsiX, DsTenancySearchResults, e, blnIsNewRow)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormClosing")
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Dim blnShiftPressed As Boolean = False
            If Control.ModifierKeys = Keys.Shift Then blnShiftPressed = True
            DefineQuery(e.KeyValue, blnShiftPressed, Me)
            DataControlKey(e.KeyValue, blnShiftPressed, Me)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "KeyDown")
        End Try
    End Sub
    Private Sub FillForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim strQueryText As String = ""

            If CType(clsGlobal.ToolStripCaseId, String) <> "" And clsGlobal.ToolStripCaseId > 0 Then
                strQueryText = CType(clsGlobal.ToolStripCaseId, String)
            End If

            If txtCaseNo.Text <> "" Then
                If IsNumeric(txtCaseNo.Text) And CType(txtCaseNo.Text, Integer) > 0 Then
                    strQueryText = Me.txtCaseNo.Text
                End If
            End If

            If strQueryText <> "" Then
                Me.taCase.Connection = clsGlobal.Connection
                Me.taCase.FillCase(Me.DsTenancySearchResults.V_frmTenancySearchResults, CType(strQueryText, Integer))

                If Microsoft.VisualBasic.Trim(Me.txtVIP.Text) = "Y" Then
                    Me.pbxVIP.Visible = True
                Else
                    Me.pbxVIP.Visible = False
                End If

                If Me.txtCaseNo.Text <> "" Then

                    Me.taTenancy.Connection = clsGlobal.Connection
                    Me.taTenancy.FillTenancy(Me.DsTenancySearchResults.V_fsubTenancySearchResults, CType(Me.txtCaseNo.Text, Integer))

                    mclsGlobal.CalculateRemainingText(Me.lblCharactersRemaining, Me.txtDescription)
                End If
            End If

            If Me.bsCase.Count = 0 Then
                MsgBox("No records returned", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Me.Text)
                ReadOnlyFields(True)
            Else
                clsGlobal.SubMenuCaseId = CType(Me.txtCaseNo.Text, Integer)
            End If

            If Me.bsTenancy.Count > 0 Then
                ReadOnlyFields(blnReadOnly)
                Me.cboTenancySearchStatus.Enabled = Not blnReadOnly
            Else
                ReadOnlyFields(True)
                Me.cboTenancySearchStatus.Enabled = False
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub

    Private Sub ExitQueryMode()
        Try
            If blnInQueryMode Then
                Me.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                clsGlobal.SwitchToFormView(Me)
                clsGlobal.CreateMainToolStripItems(Me, False, False, blnReadOnly)
                blnInQueryMode = False
                HidePrimaryKeyFields()
                FillForm()
            End If
        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ExitQueryMode")
        End Try
    End Sub
    Public Sub DefineQuery(ByRef intKeyCode As Short, ByRef intShift As Short, ByRef frmX As Form, Optional ByRef blnPrevious As Boolean = False, Optional ByVal blnQBFOverride As Boolean = False)
        Try
            If (intKeyCode = Keys.F2 Or intKeyCode = 0) Then
                RunQuery()
            ElseIf intKeyCode = Keys.F3 And intShift = 0 Then
                ExitQueryMode()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DefineQuery")
        End Try
    End Sub
    Private Sub EscapeQueryMode()
        Try
            ExitQueryMode()
            FillForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "EscapeQueryMode")
        End Try
    End Sub

    Public Sub RunQuery()
        Try
            If blnIsNewRow Then
                UndoForm()
                ReadOnlyFields(False)
                blnIsNewRow = False
            End If
            blnInQueryMode = True
            ' Line below fixes 'closing form' problem and also tabbing problems when there is more than one queryable field
            Me.DsTenancySearchResults.V_frmTenancySearchResults.Clear()
            Me.DsTenancySearchResults.V_fsubTenancySearchResults.Clear()
            clsGlobal.EnterQueryMode(Me, blnReadOnly)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "RunQuery")
        End Try
    End Sub


    Public Function UpdateBaseTables() As Boolean
        If DsTenancySearchResults.HasChanges Then
            Dim modCase As dsTenancySearchResults.V_frmTenancySearchResultsDataTable = _
                CType(DsTenancySearchResults.V_frmTenancySearchResults.GetChanges(DataRowState.Modified), _
                dsTenancySearchResults.V_frmTenancySearchResultsDataTable)

            Dim modTenancy As dsTenancySearchResults.V_fsubTenancySearchResultsDataTable = _
                CType(DsTenancySearchResults.V_fsubTenancySearchResults.GetChanges(DataRowState.Modified), _
                dsTenancySearchResults.V_fsubTenancySearchResultsDataTable)

            Dim newTenancy As dsTenancySearchResults.V_fsubTenancySearchResultsDataTable = _
                CType(DsTenancySearchResults.V_fsubTenancySearchResults.GetChanges(DataRowState.Added), _
                dsTenancySearchResults.V_fsubTenancySearchResultsDataTable)

            Dim intChanges As Integer
            Try
                If Not modCase Is Nothing Then
                    Me.taCase.Connection = clsGlobal.Connection
                    Me.taCase.Update(modCase)
                    intChanges += modCase.Count
                End If

                If Not modTenancy Is Nothing Then
                    Me.taTenancy.Connection = clsGlobal.Connection
                    Me.taTenancy.Update(modTenancy)
                    intChanges += modTenancy.Count
                End If

                If Not newTenancy Is Nothing Then
                    Me.taTenancy.Connection = clsGlobal.Connection
                    Me.taTenancy.Update(newTenancy)
                    intChanges += newTenancy.Count
                End If

                If intChanges > 0 Then
                    Dim strMsg As String = intChanges.ToString + " record added/altered/deleted"
                    MsgBox(strMsg, MsgBoxStyle.Information, "Tenancy Search Results Save")
                End If
                Return True
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Exclamation, "Database Updates Failed")
                Return False
            Finally
                If Not modCase Is Nothing Then
                    modCase.Dispose()
                End If
                If Not newTenancy Is Nothing Then
                    newTenancy.Dispose()
                End If
                If Not modTenancy Is Nothing Then
                    modTenancy.Dispose()
                End If
            End Try
        Else
            MsgBox("There are no data updates to save.", MsgBoxStyle.Information, _
                    "Save Requested Without Updates")
            Return False
        End If
    End Function

    Private Sub SaveForm()
        Dim dteStartDt As Date = Date.Now
        Try
            Dim intTimestamp As Int64 = 0
            Me.bsTenancy.EndEdit()
            Me.bsCase.EndEdit()

            If Not blnIsNewRow And Me.txtCaseNo.Text <> "" Then
                Dim commSQL As New SqlClient.SqlCommand
                commSQL.Connection = clsGlobal.Connection
                commSQL.CommandType = CommandType.Text
                commSQL.CommandText = "select convert( bigint, [timestamp]) " & _
                                    "from maarten.TENANCY_SEARCH_RESULTS " & _
                                    "where GENERATED_NO = " & Me.txtGeneratedNo.Text

                intTimestamp = commSQL.ExecuteScalar()
            End If

            If intTimestamp <> 0 AndAlso intTimestamp.ToString <> Me.txtTimestamp.Text Then
                MsgBox("Another user has updated this record before you. You will need to requery this record and apply your changes again before saving.", MsgBoxStyle.Critical, "Tenancy Search Results Save")
            Else
                Dim strErrorMessage As String
                strErrorMessage = modTenancySearchResults.CheckConstraint(Me)

                If strErrorMessage = "" Then
                    If UpdateBaseTables() Then
                        Me.DsTenancySearchResults.V_frmTenancySearchResults.AcceptChanges()
                        Me.DsTenancySearchResults.V_fsubTenancySearchResults.AcceptChanges()

                        mintCurrentRecord = Me.bsTenancy.Position

                        blnIsNewRow = False

                        If Not Me.bnTenancy.Enabled Then Me.bnTenancy.Enabled = True

                        If Me.bsTenancy.Count > 0 Then
                            Me.cboTenancySearchStatus.Enabled = Not blnReadOnly
                        Else
                            Me.cboTenancySearchStatus.Enabled = False
                        End If

                        FillDetails()
                    Else
                        Return
                    End If
                Else
                    MsgBox(strErrorMessage, MsgBoxStyle.Information, "Tenancy Search Results Save")
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SaveForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "SaveForm", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try
    End Sub

    Private Sub LoadLookupLists()
        Dim dteStartDt As Date = Date.Now
        Try
            'Property Type
            Dim strSQL As String = "exec maarten.OMNI_FILL_COMBO 'AST_PROPERTY_TYPE_CODE';"
            'Furnishing Type
            strSQL += "exec maarten.OMNI_FILL_COMBO 'AST_FURNISH_CODE';"
            'Parking Type
            strSQL += "exec maarten.OMNI_FILL_COMBO 'PARKING_CODE';"
            'Payment Frequency
            strSQL += "exec maarten.OMNI_FILL_COMBO 'PAYMENT_FREQUENCY_CODE';"
            'Search Status
            strSQL += "exec maarten.OMNI_FILL_COMBO 'TENANCY_SEARCH_STATUS_CODE'"

            Dim aArray(5) As String
            aArray(0) = "PropertyTypeLookup"
            aArray(1) = "FurnishingCodeLookup"
            aArray(2) = "ParkingTypeLookup"
            aArray(3) = "PaymentFrequencyLookup"
            aArray(4) = "SearchStatusLookup"

            clsGlobal.LoadLookupLists(strSQL, strLookupFile, dsLookups, aArray)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "LoadLookupLists")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "LoadLookupLists", dteStartDt, Date.Now, Nothing)
        End Try
    End Sub

    Private Sub LoadAndBindComboBoxes()
        Try
            With Me.cboPropertyType
                .DataSource = dsLookups.Tables("PropertyTypeLookup")
                .DisplayMember = "PROMPT"
                .ValueMember = "COLUMN_VALUE"
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("SelectedValue", Me.bsTenancy, "PROPERTY_TYPE_CD", True))
            End With

            With Me.cboFurnishingCode
                .DataSource = dsLookups.Tables("FurnishingCodeLookup")
                .DisplayMember = "PROMPT"
                .ValueMember = "COLUMN_VALUE"
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("SelectedValue", Me.bsTenancy, "FURNISH_CD", True))
            End With

            With Me.cboParkingDetails
                .DataSource = dsLookups.Tables("ParkingTypeLookup")
                .DisplayMember = "PROMPT"
                .ValueMember = "COLUMN_VALUE"
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("SelectedValue", Me.bsTenancy, "PARKING_DETAILS_CD", True))
            End With

            With Me.cboRentFrequency
                .DataSource = dsLookups.Tables("PaymentFrequencyLookup")
                .DisplayMember = "PROMPT"
                .ValueMember = "COLUMN_VALUE"
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("SelectedValue", Me.bsTenancy, "PAYMENT_FREQUENCY_CD", True))
            End With

            With Me.cboTenancySearchStatus
                .DataSource = dsLookups.Tables("SearchStatusLookup")
                .DisplayMember = "PROMPT"
                .ValueMember = "COLUMN_VALUE"
                .DataBindings.Clear()
                .DataBindings.Add(New Binding("SelectedValue", Me.bsCase, "TENANCY_SEARCH_STATUS_CD", True))
            End With

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "LoadAndBindComboBoxes")
        End Try
    End Sub

    Private Sub tspMain_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspMain.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripLabel Then
                Select Case e.ClickedItem.Name
                    Case "tslQuery"
                        RunQuery()
                    Case "tslRun"
                        ExitQueryMode()
                    Case "tslSave"
                        SaveForm()
                    Case "tslUndo"
                        UndoForm()
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspMain_ItemClicked")
        End Try
    End Sub
    Private Sub UndoForm()
        clsGlobal.UndoForm(Me, bsiX, Me.DsTenancySearchResults)
        If blnIsNewRow Then blnIsNewRow = False
        If Not Me.bnTenancy.Enabled Then Me.bnTenancy.Enabled = True
    End Sub

    Private Sub pbxClearCase_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxClearCase_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Public Sub DataControlKey(ByVal intKeyCode As Integer, ByVal intShift As Integer, ByVal frmX As Form)
        ' ***************************************************************************
        ' Purpose:    Handles data control keys such as Esc, F12 etc
        ' ***************************************************************************
        Try
            Select Case intKeyCode
                Case Keys.Escape
                    If blnInQueryMode Then ' Take out of query mode
                        EscapeQueryMode()
                        intKeyCode = 0
                    Else ' Undo field/record
                        UndoForm()
                        intKeyCode = 0
                    End If
                Case Keys.F5 'Undo
                    UndoForm()
                    intKeyCode = 0
                Case Keys.F12 'Save
                    SaveForm()
                    intKeyCode = 0
                Case Keys.F8 'Copy to number field
                    clsGlobal.CopyToFormStrip(Me.txtCaseNo, Me.pnlFormStrip)
                    intKeyCode = 0
                Case Keys.F9 'Clear number field
                    clsGlobal.ClickClearCase(Me.pnlFormStrip)
                    intKeyCode = 0
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "DataControlKey")
        End Try
    End Sub

    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)
        clsGlobal.ReadOnlyFields(blnReadOnly, Me.pnlCase)
        clsGlobal.ReadOnlyFields(blnReadOnly, Me.pnlTenancy)
        Me.tsbAdd.Enabled = (Not blnReadOnly Or Me.bsCase.Count > 0)

        ReadOnlyPermanant(True)
    End Sub
    Private Sub ReadOnlyPermanant(ByVal blnReadOnly As Boolean)
        Try
            Me.txtCaseNo.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtCaseNo)

            Me.txtPackage.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtPackage)

            Me.txtCustomerNm.ReadOnly = True
            clsGlobal.SetBackColor(True, Me.txtCustomerNm)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "ReadOnlyPermanent")
        End Try

    End Sub
    Private Sub pbxCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxCopy.Click
        clsGlobal.CopyToFormStrip(Me.txtCaseNo, Me.pnlFormStrip)
    End Sub

    Private Sub pbxCopy_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxCopy.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxCopy_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxCopy.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub HidePrimaryKeyFields()
        Me.txtPackageId.Hide()
        Me.txtGeneratedNo.Hide()
        Me.txtTenancyCaseNo.Hide()
        Me.txtTenancyPackageId.Hide()
        Me.txtClientId.Hide()
        Me.txtTimestamp.Hide()
        Me.txtVIP.Hide()
    End Sub

    Private Sub pbxLetter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxLetter.Click
        'clsGlobal.CreateLetter(Me, Me.txtCaseNo.Text, Me.txtPackageId.Text, , )

        'Dim letX As clsLetters.clsLetter
        'If Me.txtCaseNo.Text <> "" Then
        '    If Me.bsTenancy.Count > 0 Then
        '        letX = New clsLetters.clsLetter(CType(Me.txtCaseNo.Text, Integer), _
        '                CType(Me.txtPackageId.Text, Integer), _
        '                "RENT", CType(Me.txtClientId.Text, Integer), "frmTenancySearchResults", False)
        '        letX.StartLetter(Me)
        '    Else
        '        MsgBox("A valid package must be present to select Letters.", MsgBoxStyle.Exclamation, "Letters System")
        '    End If
        'Else
        '    MsgBox("A valid case must be present to select Letters.", MsgBoxStyle.Exclamation, "Letters System")
        'End If
    End Sub

    Private Sub tspRight_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tspRight.ItemClicked
        Try
            If TypeOf (e.ClickedItem) Is ToolStripButton Then
                Select Case e.ClickedItem.Name
                    Case "tsbZoom"
                        clsGlobal.ZoomClicked(Me)
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "tspRight_ItemClicked")
        End Try
    End Sub

    Private Sub pbxAvailable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxAvailable.Click
        Dim frmX As New frmDatePicker
        frmX.CallingForm(Me.txtAvailable)
        frmX.ShowDialog()
    End Sub

    Private Sub pbxAvailable_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxAvailable.MouseHover
        clsGlobal.SetCursorHand(Me, Me.txtCaseNo)
    End Sub

    Private Sub pbxAvailable_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxAvailable.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub

    Private Sub tsbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAdd.Click
        blnIsNewRow = True
        ReadOnlyFields(blnReadOnly)
        Me.bnTenancy.Enabled = False
    End Sub

    Private Sub FillDetails()
        Dim dteStartDt As Date = Date.Now
        Try
            If Me.bsCase.Count > 0 Then
                Me.taTenancy.Connection = clsGlobal.Connection
                Me.taTenancy.FillTenancy(Me.DsTenancySearchResults.V_fsubTenancySearchResults, CType(Me.txtCaseNo.Text, Integer))
                If mintCurrentRecord > -1 Then
                    Me.bsTenancy.Position = mintCurrentRecord
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillDetails")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillDetails", dteStartDt, Date.Now, Me.txtCaseNo.Text)
        End Try

    End Sub


    Private Sub txtDescription_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDescription.KeyUp
        mclsGlobal.CalculateRemainingText(Me.lblCharactersRemaining, Me.txtDescription)
    End Sub

    Private Sub txtDescription_MouseClick(sender As Object, e As MouseEventArgs) Handles txtDescription.MouseClick
        mclsGlobal.CalculateRemainingText(Me.lblCharactersRemaining, Me.txtDescription)
    End Sub
End Class