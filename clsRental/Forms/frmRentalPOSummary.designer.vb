<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRentalPOSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label5 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRentalPOSummary))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbxVIP = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlFormStrip = New System.Windows.Forms.Panel()
        Me.pnlMainToolStrip = New System.Windows.Forms.Panel()
        Me.tspRight = New System.Windows.Forms.ToolStrip()
        Me.tspMain = New System.Windows.Forms.ToolStrip()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlCase = New System.Windows.Forms.Panel()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.bsCasePackage = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsCase = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsRentalPOSummary = New clsRental.dsRentalPOSummary()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.SuperLabel3 = New ControlLibrary.SuperLabel(Me.components)
        Me.txbRelatedCaseNo = New ControlLibrary.SuperTextBox()
        Me.SuperLabel2 = New ControlLibrary.SuperLabel(Me.components)
        Me.pbxCopy = New System.Windows.Forms.PictureBox()
        Me.txbCaseNo = New ControlLibrary.SuperTextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.txtVIP = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txbCompositeCaseNo = New ControlLibrary.SuperTextBox()
        Me.SuperLabel1 = New ControlLibrary.SuperLabel(Me.components)
        Me.pnlPackage = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvPackage = New System.Windows.Forms.DataGridView()
        Me.CSECASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RELATEDCASENODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OccupantDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VACATIONDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsPackage = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlPropertyPO = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvPropertyPO = New System.Windows.Forms.DataGridView()
        Me.CSCCASENOCSEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ONGOINGINDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PONumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUOTEAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CancelledDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboLatestStatusProp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICEITEMAMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BUSINESSNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsPropertyPO = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlPersonPO = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvPersonPO = New System.Windows.Forms.DataGridView()
        Me.CSCCASENOCSEDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RELATEDCASENODataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ONGOINGINDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PONumberDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUOTEAMDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CancelledDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboInvoiceStatusPerson = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BUSINESSNODataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsPersonPO = New System.Windows.Forms.BindingSource(Me.components)
        Me.taCase = New clsRental.dsRentalPOSummaryTableAdapters.V_frmRentalPurchaseOrderSumaryTableAdapter()
        Me.taCasePackage = New clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPrptyTableAdapter()
        Me.taPackage = New clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPersnTableAdapter()
        Me.taPropertyPO = New clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPropTableAdapter()
        Me.taPersonPO = New clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPersTableAdapter()
        Label5 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMainToolStrip.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCase.SuspendLayout()
        CType(Me.bsCasePackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsRentalPOSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.pnlPackage.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPropertyPO.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvPropertyPO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPropertyPO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPersonPO.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvPersonPO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsPersonPO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label5.Location = New System.Drawing.Point(25, 49)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(92, 14)
        Label5.TabIndex = 118
        Label5.Text = "Property Case No"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label4.Location = New System.Drawing.Point(25, 75)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(77, 14)
        Label4.TabIndex = 120
        Label4.Text = "Client Package"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label6.Location = New System.Drawing.Point(322, 49)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(49, 14)
        Label6.TabIndex = 122
        Label6.Text = "Address"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label7.Location = New System.Drawing.Point(320, 75)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(74, 14)
        Label7.TabIndex = 130
        Label7.Text = "Tenancy Start"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Label8.Location = New System.Drawing.Point(598, 75)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(84, 14)
        Label8.TabIndex = 132
        Label8.Text = "Completion Date"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 200
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'pbxVIP
        '
        Me.pbxVIP.Image = Global.clsRental.My.Resources.Resources.Cheese
        Me.pbxVIP.Location = New System.Drawing.Point(201, 8)
        Me.pbxVIP.Name = "pbxVIP"
        Me.pbxVIP.Size = New System.Drawing.Size(32, 32)
        Me.pbxVIP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxVIP.TabIndex = 191
        Me.pbxVIP.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbxVIP, "VIP Case")
        Me.pbxVIP.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = CType(resources.GetObject("Panel6.BackgroundImage"), System.Drawing.Image)
        Me.Panel6.Location = New System.Drawing.Point(238, 25)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(30, 35)
        Me.Panel6.TabIndex = 2
        '
        'pnlFormStrip
        '
        Me.pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        Me.pnlFormStrip.Name = "pnlFormStrip"
        Me.pnlFormStrip.Size = New System.Drawing.Size(528, 35)
        Me.pnlFormStrip.TabIndex = 3
        '
        'pnlMainToolStrip
        '
        Me.pnlMainToolStrip.BackColor = System.Drawing.Color.Black
        Me.pnlMainToolStrip.Controls.Add(Me.tspRight)
        Me.pnlMainToolStrip.Controls.Add(Me.tspMain)
        Me.pnlMainToolStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainToolStrip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.pnlMainToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainToolStrip.Name = "pnlMainToolStrip"
        Me.pnlMainToolStrip.Size = New System.Drawing.Size(796, 24)
        Me.pnlMainToolStrip.TabIndex = 0
        '
        'tspRight
        '
        Me.tspRight.BackColor = System.Drawing.Color.Black
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.None
        Me.tspRight.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspRight.Location = New System.Drawing.Point(727, 0)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Size = New System.Drawing.Size(102, 25)
        Me.tspRight.TabIndex = 0
        Me.tspRight.Text = "ToolStrip1"
        '
        'tspMain
        '
        Me.tspMain.AutoSize = False
        Me.tspMain.BackColor = System.Drawing.Color.Black
        Me.tspMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tspMain.GripMargin = New System.Windows.Forms.Padding(0)
        Me.tspMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tspMain.Location = New System.Drawing.Point(238, 0)
        Me.tspMain.Name = "tspMain"
        Me.tspMain.Padding = New System.Windows.Forms.Padding(0)
        Me.tspMain.Size = New System.Drawing.Size(534, 25)
        Me.tspMain.TabIndex = 7
        Me.tspMain.Text = "ToolStrip1"
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.PictureBox1)
        Me.Panel8.Controls.Add(Me.PictureBox2)
        Me.Panel8.Controls.Add(Me.pnlCase)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(0, 66)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(796, 150)
        Me.Panel8.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(763, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox1.TabIndex = 79
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'pnlCase
        '
        Me.pnlCase.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.pnlCase.Controls.Add(Me.pbxVIP)
        Me.pnlCase.Controls.Add(Label8)
        Me.pnlCase.Controls.Add(Me.TextBox5)
        Me.pnlCase.Controls.Add(Label7)
        Me.pnlCase.Controls.Add(Me.TextBox4)
        Me.pnlCase.Controls.Add(Label6)
        Me.pnlCase.Controls.Add(Me.TextBox3)
        Me.pnlCase.Controls.Add(Label4)
        Me.pnlCase.Controls.Add(Me.TextBox2)
        Me.pnlCase.Controls.Add(Label5)
        Me.pnlCase.Controls.Add(Me.TextBox1)
        Me.pnlCase.Controls.Add(Me.SuperLabel3)
        Me.pnlCase.Controls.Add(Me.txbRelatedCaseNo)
        Me.pnlCase.Controls.Add(Me.SuperLabel2)
        Me.pnlCase.Controls.Add(Me.pbxCopy)
        Me.pnlCase.Controls.Add(Me.txbCaseNo)
        Me.pnlCase.Location = New System.Drawing.Point(8, 32)
        Me.pnlCase.Name = "pnlCase"
        Me.pnlCase.Size = New System.Drawing.Size(780, 107)
        Me.pnlCase.TabIndex = 0
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCasePackage, "COMPLETION_DT", True))
        Me.TextBox5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox5.Location = New System.Drawing.Point(694, 72)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(69, 20)
        Me.TextBox5.TabIndex = 6
        '
        'bsCasePackage
        '
        Me.bsCasePackage.DataMember = "V_frmRentalPurchaseOrderSumary_V_fsubRentalPurchaseOrderPrpty"
        Me.bsCasePackage.DataSource = Me.bsCase
        '
        'bsCase
        '
        Me.bsCase.DataMember = "V_frmRentalPurchaseOrderSumary"
        Me.bsCase.DataSource = Me.DsRentalPOSummary
        '
        'DsRentalPOSummary
        '
        Me.DsRentalPOSummary.DataSetName = "dsRentalPOSummary"
        Me.DsRentalPOSummary.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCasePackage, "TenancyStartDt", True))
        Me.TextBox4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox4.Location = New System.Drawing.Point(416, 72)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(69, 20)
        Me.TextBox4.TabIndex = 5
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCasePackage, "Address", True))
        Me.TextBox3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox3.Location = New System.Drawing.Point(416, 46)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(347, 20)
        Me.TextBox3.TabIndex = 4
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCasePackage, "CLIENT_PACKAGE_NM", True))
        Me.TextBox2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox2.Location = New System.Drawing.Point(121, 72)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(184, 20)
        Me.TextBox2.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCasePackage, "CSE_CASE_NO_CSE", True))
        Me.TextBox1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox1.Location = New System.Drawing.Point(121, 46)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(53, 20)
        Me.TextBox1.TabIndex = 1
        '
        'SuperLabel3
        '
        Me.SuperLabel3.AutoSize = True
        Me.SuperLabel3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel3.Location = New System.Drawing.Point(322, 23)
        Me.SuperLabel3.Name = "SuperLabel3"
        Me.SuperLabel3.Size = New System.Drawing.Size(87, 14)
        Me.SuperLabel3.TabIndex = 116
        Me.SuperLabel3.Text = "Related Case No"
        '
        'txbRelatedCaseNo
        '
        Me.txbRelatedCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbRelatedCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "RELATED_CASE_NO", True))
        Me.txbRelatedCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbRelatedCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbRelatedCaseNo.Location = New System.Drawing.Point(416, 20)
        Me.txbRelatedCaseNo.Name = "txbRelatedCaseNo"
        Me.txbRelatedCaseNo.PreviousQuery = Nothing
        Me.txbRelatedCaseNo.Queryable = True
        Me.txbRelatedCaseNo.QueryMandatory = False
        Me.txbRelatedCaseNo.ReadOnly = True
        Me.txbRelatedCaseNo.Size = New System.Drawing.Size(61, 20)
        Me.txbRelatedCaseNo.TabIndex = 3
        Me.txbRelatedCaseNo.Updateable = False
        '
        'SuperLabel2
        '
        Me.SuperLabel2.AutoSize = True
        Me.SuperLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SuperLabel2.Location = New System.Drawing.Point(25, 23)
        Me.SuperLabel2.Name = "SuperLabel2"
        Me.SuperLabel2.Size = New System.Drawing.Size(48, 14)
        Me.SuperLabel2.TabIndex = 0
        Me.SuperLabel2.Text = "Case No"
        '
        'pbxCopy
        '
        Me.pbxCopy.Image = CType(resources.GetObject("pbxCopy.Image"), System.Drawing.Image)
        Me.pbxCopy.Location = New System.Drawing.Point(180, 23)
        Me.pbxCopy.Name = "pbxCopy"
        Me.pbxCopy.Size = New System.Drawing.Size(15, 15)
        Me.pbxCopy.TabIndex = 114
        Me.pbxCopy.TabStop = False
        '
        'txbCaseNo
        '
        Me.txbCaseNo.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.txbCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CASE_NO", True))
        Me.txbCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCaseNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txbCaseNo.Location = New System.Drawing.Point(121, 20)
        Me.txbCaseNo.Name = "txbCaseNo"
        Me.txbCaseNo.PreviousQuery = Nothing
        Me.txbCaseNo.Queryable = True
        Me.txbCaseNo.QueryMandatory = False
        Me.txbCaseNo.ReadOnly = True
        Me.txbCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txbCaseNo.TabIndex = 0
        Me.txbCaseNo.Updateable = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Silver
        Me.Panel10.Controls.Add(Me.txtVIP)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Controls.Add(Me.txbCompositeCaseNo)
        Me.Panel10.Location = New System.Drawing.Point(33, 8)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(731, 24)
        Me.Panel10.TabIndex = 0
        '
        'txtVIP
        '
        Me.txtVIP.BackColor = System.Drawing.Color.Yellow
        Me.txtVIP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "VIP_IN", True))
        Me.txtVIP.Location = New System.Drawing.Point(354, 2)
        Me.txtVIP.Name = "txtVIP"
        Me.txtVIP.Size = New System.Drawing.Size(22, 20)
        Me.txtVIP.TabIndex = 186
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 14)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Query Case"
        '
        'txbCompositeCaseNo
        '
        Me.txbCompositeCaseNo.BackColor = System.Drawing.Color.Yellow
        Me.txbCompositeCaseNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsCase, "CompositeCaseNo", True))
        Me.txbCompositeCaseNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbCompositeCaseNo.ForeColor = System.Drawing.Color.DimGray
        Me.txbCompositeCaseNo.Location = New System.Drawing.Point(270, 2)
        Me.txbCompositeCaseNo.Name = "txbCompositeCaseNo"
        Me.txbCompositeCaseNo.PreviousQuery = Nothing
        Me.txbCompositeCaseNo.Queryable = True
        Me.txbCompositeCaseNo.QueryMandatory = False
        Me.txbCompositeCaseNo.ReadOnly = True
        Me.txbCompositeCaseNo.Size = New System.Drawing.Size(53, 20)
        Me.txbCompositeCaseNo.TabIndex = 128
        Me.txbCompositeCaseNo.Updateable = False
        '
        'SuperLabel1
        '
        Me.SuperLabel1.AutoSize = True
        Me.SuperLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.SuperLabel1.Location = New System.Drawing.Point(0, 41)
        Me.SuperLabel1.Name = "SuperLabel1"
        Me.SuperLabel1.Size = New System.Drawing.Size(162, 19)
        Me.SuperLabel1.TabIndex = 5
        Me.SuperLabel1.Text = "Rental PO Summary"
        '
        'pnlPackage
        '
        Me.pnlPackage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPackage.Controls.Add(Me.PictureBox6)
        Me.pnlPackage.Controls.Add(Me.PictureBox5)
        Me.pnlPackage.Controls.Add(Me.Panel4)
        Me.pnlPackage.Controls.Add(Me.dgvPackage)
        Me.pnlPackage.Location = New System.Drawing.Point(0, 222)
        Me.pnlPackage.Name = "pnlPackage"
        Me.pnlPackage.Size = New System.Drawing.Size(795, 112)
        Me.pnlPackage.TabIndex = 9
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox6.TabIndex = 80
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox5.TabIndex = 79
        Me.PictureBox5.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Silver
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(33, 8)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(732, 24)
        Me.Panel4.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Packages"
        '
        'dgvPackage
        '
        Me.dgvPackage.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPackage.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPackage.AutoGenerateColumns = False
        Me.dgvPackage.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPackage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPackage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPackage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPackage.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvPackage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSECASENOCSEDataGridViewTextBoxColumn, Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.RELATEDCASENODataGridViewTextBoxColumn, Me.CLIENTPACKAGENMDataGridViewTextBoxColumn, Me.OccupantDataGridViewTextBoxColumn, Me.ACTUALMOVEINDTDataGridViewTextBoxColumn, Me.VACATIONDTDataGridViewTextBoxColumn})
        Me.dgvPackage.DataSource = Me.bsPackage
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPackage.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvPackage.EnableHeadersVisualStyles = False
        Me.dgvPackage.Location = New System.Drawing.Point(8, 32)
        Me.dgvPackage.Name = "dgvPackage"
        Me.dgvPackage.ReadOnly = True
        Me.dgvPackage.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPackage.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvPackage.RowHeadersWidth = 18
        Me.dgvPackage.RowTemplate.Height = 20
        Me.dgvPackage.Size = New System.Drawing.Size(775, 69)
        Me.dgvPackage.TabIndex = 76
        '
        'CSECASENOCSEDataGridViewTextBoxColumn
        '
        Me.CSECASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "CSE_CASE_NO_CSE"
        Me.CSECASENOCSEDataGridViewTextBoxColumn.HeaderText = "Pers Case No"
        Me.CSECASENOCSEDataGridViewTextBoxColumn.Name = "CSECASENOCSEDataGridViewTextBoxColumn"
        Me.CSECASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CGS_CLIENT_PACKAGE_ID_CGS"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'RELATEDCASENODataGridViewTextBoxColumn
        '
        Me.RELATEDCASENODataGridViewTextBoxColumn.DataPropertyName = "RELATED_CASE_NO"
        Me.RELATEDCASENODataGridViewTextBoxColumn.HeaderText = "RELATED_CASE_NO"
        Me.RELATEDCASENODataGridViewTextBoxColumn.Name = "RELATEDCASENODataGridViewTextBoxColumn"
        Me.RELATEDCASENODataGridViewTextBoxColumn.ReadOnly = True
        Me.RELATEDCASENODataGridViewTextBoxColumn.Visible = False
        '
        'CLIENTPACKAGENMDataGridViewTextBoxColumn
        '
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.DataPropertyName = "CLIENT_PACKAGE_NM"
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.HeaderText = "Client Package"
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.Name = "CLIENTPACKAGENMDataGridViewTextBoxColumn"
        Me.CLIENTPACKAGENMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'OccupantDataGridViewTextBoxColumn
        '
        Me.OccupantDataGridViewTextBoxColumn.DataPropertyName = "Occupant"
        Me.OccupantDataGridViewTextBoxColumn.HeaderText = "Occupant"
        Me.OccupantDataGridViewTextBoxColumn.Name = "OccupantDataGridViewTextBoxColumn"
        Me.OccupantDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ACTUALMOVEINDTDataGridViewTextBoxColumn
        '
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.DataPropertyName = "ACTUAL_MOVE_IN_DT"
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.HeaderText = "Move in Date"
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.Name = "ACTUALMOVEINDTDataGridViewTextBoxColumn"
        Me.ACTUALMOVEINDTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'VACATIONDTDataGridViewTextBoxColumn
        '
        Me.VACATIONDTDataGridViewTextBoxColumn.DataPropertyName = "VACATION_DT"
        Me.VACATIONDTDataGridViewTextBoxColumn.HeaderText = "Move out Date"
        Me.VACATIONDTDataGridViewTextBoxColumn.Name = "VACATIONDTDataGridViewTextBoxColumn"
        Me.VACATIONDTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'bsPackage
        '
        Me.bsPackage.DataMember = "V_fsubRentalPurchaseOrderPrpty_V_fsubRentalPurchaseOrderPersn"
        Me.bsPackage.DataSource = Me.bsCasePackage
        '
        'pnlPropertyPO
        '
        Me.pnlPropertyPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPropertyPO.Controls.Add(Me.PictureBox3)
        Me.pnlPropertyPO.Controls.Add(Me.PictureBox4)
        Me.pnlPropertyPO.Controls.Add(Me.Panel2)
        Me.pnlPropertyPO.Controls.Add(Me.dgvPropertyPO)
        Me.pnlPropertyPO.Location = New System.Drawing.Point(0, 340)
        Me.pnlPropertyPO.Name = "pnlPropertyPO"
        Me.pnlPropertyPO.Size = New System.Drawing.Size(795, 153)
        Me.pnlPropertyPO.TabIndex = 10
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox3.TabIndex = 80
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox4.TabIndex = 79
        Me.PictureBox4.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(33, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(732, 24)
        Me.Panel2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(183, 14)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Property Case Purchase Orders"
        '
        'dgvPropertyPO
        '
        Me.dgvPropertyPO.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPropertyPO.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvPropertyPO.AutoGenerateColumns = False
        Me.dgvPropertyPO.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPropertyPO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPropertyPO.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPropertyPO.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPropertyPO.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvPropertyPO.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSCCASENOCSEDataGridViewTextBoxColumn, Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn, Me.ONGOINGINDataGridViewTextBoxColumn, Me.SupplierNameDataGridViewTextBoxColumn, Me.PONumberDataGridViewTextBoxColumn, Me.QUOTEAMDataGridViewTextBoxColumn, Me.CancelledDataGridViewTextBoxColumn, Me.cboLatestStatusProp, Me.INVINVOICEDTINVDataGridViewTextBoxColumn, Me.INVOICEITEMAMDataGridViewTextBoxColumn, Me.BUSINESSNODataGridViewTextBoxColumn, Me.INSTRUCTIONDTDataGridViewTextBoxColumn})
        Me.dgvPropertyPO.DataSource = Me.bsPropertyPO
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPropertyPO.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgvPropertyPO.EnableHeadersVisualStyles = False
        Me.dgvPropertyPO.Location = New System.Drawing.Point(8, 32)
        Me.dgvPropertyPO.Name = "dgvPropertyPO"
        Me.dgvPropertyPO.ReadOnly = True
        Me.dgvPropertyPO.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPropertyPO.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvPropertyPO.RowHeadersWidth = 18
        Me.dgvPropertyPO.RowTemplate.Height = 20
        Me.dgvPropertyPO.Size = New System.Drawing.Size(775, 112)
        Me.dgvPropertyPO.TabIndex = 76
        '
        'CSCCASENOCSEDataGridViewTextBoxColumn
        '
        Me.CSCCASENOCSEDataGridViewTextBoxColumn.DataPropertyName = "CSC_CASE_NO_CSE"
        Me.CSCCASENOCSEDataGridViewTextBoxColumn.HeaderText = "Prop Case"
        Me.CSCCASENOCSEDataGridViewTextBoxColumn.Name = "CSCCASENOCSEDataGridViewTextBoxColumn"
        Me.CSCCASENOCSEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CSCCASENOCSEDataGridViewTextBoxColumn.Width = 70
        '
        'CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn
        '
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.DataPropertyName = "CSC_CLIENT_PACKAGE_ID_CGS"
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.HeaderText = "CSC_CLIENT_PACKAGE_ID_CGS"
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Name = "CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn"
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.ReadOnly = True
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn.Visible = False
        '
        'ONGOINGINDataGridViewTextBoxColumn
        '
        Me.ONGOINGINDataGridViewTextBoxColumn.DataPropertyName = "ONGOING_IN"
        Me.ONGOINGINDataGridViewTextBoxColumn.HeaderText = "On"
        Me.ONGOINGINDataGridViewTextBoxColumn.Name = "ONGOINGINDataGridViewTextBoxColumn"
        Me.ONGOINGINDataGridViewTextBoxColumn.ReadOnly = True
        Me.ONGOINGINDataGridViewTextBoxColumn.Width = 25
        '
        'SupplierNameDataGridViewTextBoxColumn
        '
        Me.SupplierNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SupplierNameDataGridViewTextBoxColumn.DataPropertyName = "SupplierName"
        Me.SupplierNameDataGridViewTextBoxColumn.HeaderText = "Supplier Name"
        Me.SupplierNameDataGridViewTextBoxColumn.Name = "SupplierNameDataGridViewTextBoxColumn"
        Me.SupplierNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PONumberDataGridViewTextBoxColumn
        '
        Me.PONumberDataGridViewTextBoxColumn.DataPropertyName = "PONumber"
        Me.PONumberDataGridViewTextBoxColumn.HeaderText = "PO Number"
        Me.PONumberDataGridViewTextBoxColumn.Name = "PONumberDataGridViewTextBoxColumn"
        Me.PONumberDataGridViewTextBoxColumn.ReadOnly = True
        Me.PONumberDataGridViewTextBoxColumn.Width = 150
        '
        'QUOTEAMDataGridViewTextBoxColumn
        '
        Me.QUOTEAMDataGridViewTextBoxColumn.DataPropertyName = "QUOTE_AM"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.QUOTEAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.QUOTEAMDataGridViewTextBoxColumn.HeaderText = "PO Am"
        Me.QUOTEAMDataGridViewTextBoxColumn.Name = "QUOTEAMDataGridViewTextBoxColumn"
        Me.QUOTEAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.QUOTEAMDataGridViewTextBoxColumn.Width = 80
        '
        'CancelledDataGridViewTextBoxColumn
        '
        Me.CancelledDataGridViewTextBoxColumn.DataPropertyName = "Cancelled"
        Me.CancelledDataGridViewTextBoxColumn.HeaderText = "Cancelled"
        Me.CancelledDataGridViewTextBoxColumn.Name = "CancelledDataGridViewTextBoxColumn"
        Me.CancelledDataGridViewTextBoxColumn.ReadOnly = True
        Me.CancelledDataGridViewTextBoxColumn.Width = 70
        '
        'cboLatestStatusProp
        '
        Me.cboLatestStatusProp.DataPropertyName = "INV_LATEST_STATUS_CD_INV"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cboLatestStatusProp.DefaultCellStyle = DataGridViewCellStyle8
        Me.cboLatestStatusProp.HeaderText = "Latest Stat."
        Me.cboLatestStatusProp.Name = "cboLatestStatusProp"
        Me.cboLatestStatusProp.ReadOnly = True
        Me.cboLatestStatusProp.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cboLatestStatusProp.Width = 80
        '
        'INVINVOICEDTINVDataGridViewTextBoxColumn
        '
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.DataPropertyName = "INV_INVOICE_DT_INV"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.HeaderText = "Invoice Dt"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.Name = "INVINVOICEDTINVDataGridViewTextBoxColumn"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.ReadOnly = True
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn.Width = 80
        '
        'INVOICEITEMAMDataGridViewTextBoxColumn
        '
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.DataPropertyName = "INVOICE_ITEM_AM"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.HeaderText = "Item Am"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.Name = "INVOICEITEMAMDataGridViewTextBoxColumn"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.ReadOnly = True
        Me.INVOICEITEMAMDataGridViewTextBoxColumn.Width = 80
        '
        'BUSINESSNODataGridViewTextBoxColumn
        '
        Me.BUSINESSNODataGridViewTextBoxColumn.DataPropertyName = "BUSINESS_NO"
        Me.BUSINESSNODataGridViewTextBoxColumn.HeaderText = "BUSINESS_NO"
        Me.BUSINESSNODataGridViewTextBoxColumn.Name = "BUSINESSNODataGridViewTextBoxColumn"
        Me.BUSINESSNODataGridViewTextBoxColumn.ReadOnly = True
        Me.BUSINESSNODataGridViewTextBoxColumn.Visible = False
        '
        'INSTRUCTIONDTDataGridViewTextBoxColumn
        '
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn.DataPropertyName = "INSTRUCTION_DT"
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn.HeaderText = "INSTRUCTION_DT"
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn.Name = "INSTRUCTIONDTDataGridViewTextBoxColumn"
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn.ReadOnly = True
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn.Visible = False
        '
        'bsPropertyPO
        '
        Me.bsPropertyPO.DataMember = "V_fsubRentalPurchaseOrderPrpty_V_fsubRentalPurchaseOrderProp"
        Me.bsPropertyPO.DataSource = Me.bsCasePackage
        '
        'pnlPersonPO
        '
        Me.pnlPersonPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPersonPO.Controls.Add(Me.PictureBox7)
        Me.pnlPersonPO.Controls.Add(Me.PictureBox8)
        Me.pnlPersonPO.Controls.Add(Me.Panel5)
        Me.pnlPersonPO.Controls.Add(Me.dgvPersonPO)
        Me.pnlPersonPO.Location = New System.Drawing.Point(0, 499)
        Me.pnlPersonPO.Name = "pnlPersonPO"
        Me.pnlPersonPO.Size = New System.Drawing.Size(795, 202)
        Me.pnlPersonPO.TabIndex = 11
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox7.TabIndex = 80
        Me.PictureBox7.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(25, 24)
        Me.PictureBox8.TabIndex = 79
        Me.PictureBox8.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Silver
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Location = New System.Drawing.Point(33, 8)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(732, 24)
        Me.Panel5.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(175, 14)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Person Case Purchase Orders"
        '
        'dgvPersonPO
        '
        Me.dgvPersonPO.AllowUserToDeleteRows = False
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPersonPO.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvPersonPO.AutoGenerateColumns = False
        Me.dgvPersonPO.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.dgvPersonPO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPersonPO.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPersonPO.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPersonPO.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvPersonPO.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSCCASENOCSEDataGridViewTextBoxColumn1, Me.RELATEDCASENODataGridViewTextBoxColumn1, Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1, Me.ONGOINGINDataGridViewTextBoxColumn1, Me.SupplierNameDataGridViewTextBoxColumn1, Me.PONumberDataGridViewTextBoxColumn1, Me.QUOTEAMDataGridViewTextBoxColumn1, Me.CancelledDataGridViewTextBoxColumn1, Me.cboInvoiceStatusPerson, Me.INVINVOICEDTINVDataGridViewTextBoxColumn1, Me.INVOICEITEMAMDataGridViewTextBoxColumn1, Me.BUSINESSNODataGridViewTextBoxColumn1, Me.INSTRUCTIONDTDataGridViewTextBoxColumn1})
        Me.dgvPersonPO.DataSource = Me.bsPersonPO
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(247, Byte), Integer))
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(123, Byte), Integer))
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Navy
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPersonPO.DefaultCellStyle = DataGridViewCellStyle17
        Me.dgvPersonPO.EnableHeadersVisualStyles = False
        Me.dgvPersonPO.Location = New System.Drawing.Point(8, 32)
        Me.dgvPersonPO.Name = "dgvPersonPO"
        Me.dgvPersonPO.ReadOnly = True
        Me.dgvPersonPO.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPersonPO.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvPersonPO.RowHeadersWidth = 18
        Me.dgvPersonPO.RowTemplate.Height = 20
        Me.dgvPersonPO.Size = New System.Drawing.Size(775, 157)
        Me.dgvPersonPO.TabIndex = 76
        '
        'CSCCASENOCSEDataGridViewTextBoxColumn1
        '
        Me.CSCCASENOCSEDataGridViewTextBoxColumn1.DataPropertyName = "CSC_CASE_NO_CSE"
        Me.CSCCASENOCSEDataGridViewTextBoxColumn1.HeaderText = "Pers Case"
        Me.CSCCASENOCSEDataGridViewTextBoxColumn1.Name = "CSCCASENOCSEDataGridViewTextBoxColumn1"
        Me.CSCCASENOCSEDataGridViewTextBoxColumn1.ReadOnly = True
        Me.CSCCASENOCSEDataGridViewTextBoxColumn1.Width = 70
        '
        'RELATEDCASENODataGridViewTextBoxColumn1
        '
        Me.RELATEDCASENODataGridViewTextBoxColumn1.DataPropertyName = "RELATED_CASE_NO"
        Me.RELATEDCASENODataGridViewTextBoxColumn1.HeaderText = "RELATED_CASE_NO"
        Me.RELATEDCASENODataGridViewTextBoxColumn1.Name = "RELATEDCASENODataGridViewTextBoxColumn1"
        Me.RELATEDCASENODataGridViewTextBoxColumn1.ReadOnly = True
        Me.RELATEDCASENODataGridViewTextBoxColumn1.Visible = False
        '
        'CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1
        '
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.DataPropertyName = "CSC_CLIENT_PACKAGE_ID_CGS"
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.HeaderText = "CSC_CLIENT_PACKAGE_ID_CGS"
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.Name = "CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1"
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.ReadOnly = True
        Me.CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1.Visible = False
        '
        'ONGOINGINDataGridViewTextBoxColumn1
        '
        Me.ONGOINGINDataGridViewTextBoxColumn1.DataPropertyName = "ONGOING_IN"
        Me.ONGOINGINDataGridViewTextBoxColumn1.HeaderText = "On"
        Me.ONGOINGINDataGridViewTextBoxColumn1.Name = "ONGOINGINDataGridViewTextBoxColumn1"
        Me.ONGOINGINDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ONGOINGINDataGridViewTextBoxColumn1.Width = 25
        '
        'SupplierNameDataGridViewTextBoxColumn1
        '
        Me.SupplierNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SupplierNameDataGridViewTextBoxColumn1.DataPropertyName = "SupplierName"
        Me.SupplierNameDataGridViewTextBoxColumn1.HeaderText = "Supplier Name"
        Me.SupplierNameDataGridViewTextBoxColumn1.Name = "SupplierNameDataGridViewTextBoxColumn1"
        Me.SupplierNameDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'PONumberDataGridViewTextBoxColumn1
        '
        Me.PONumberDataGridViewTextBoxColumn1.DataPropertyName = "PONumber"
        Me.PONumberDataGridViewTextBoxColumn1.HeaderText = "PO Number"
        Me.PONumberDataGridViewTextBoxColumn1.Name = "PONumberDataGridViewTextBoxColumn1"
        Me.PONumberDataGridViewTextBoxColumn1.ReadOnly = True
        Me.PONumberDataGridViewTextBoxColumn1.Width = 150
        '
        'QUOTEAMDataGridViewTextBoxColumn1
        '
        Me.QUOTEAMDataGridViewTextBoxColumn1.DataPropertyName = "QUOTE_AM"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        Me.QUOTEAMDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle14
        Me.QUOTEAMDataGridViewTextBoxColumn1.HeaderText = "PO Am"
        Me.QUOTEAMDataGridViewTextBoxColumn1.Name = "QUOTEAMDataGridViewTextBoxColumn1"
        Me.QUOTEAMDataGridViewTextBoxColumn1.ReadOnly = True
        Me.QUOTEAMDataGridViewTextBoxColumn1.Width = 80
        '
        'CancelledDataGridViewTextBoxColumn1
        '
        Me.CancelledDataGridViewTextBoxColumn1.DataPropertyName = "Cancelled"
        Me.CancelledDataGridViewTextBoxColumn1.HeaderText = "Cancelled"
        Me.CancelledDataGridViewTextBoxColumn1.Name = "CancelledDataGridViewTextBoxColumn1"
        Me.CancelledDataGridViewTextBoxColumn1.ReadOnly = True
        Me.CancelledDataGridViewTextBoxColumn1.Width = 70
        '
        'cboInvoiceStatusPerson
        '
        Me.cboInvoiceStatusPerson.DataPropertyName = "INV_LATEST_STATUS_CD_INV"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cboInvoiceStatusPerson.DefaultCellStyle = DataGridViewCellStyle15
        Me.cboInvoiceStatusPerson.HeaderText = "Latest Stat."
        Me.cboInvoiceStatusPerson.Name = "cboInvoiceStatusPerson"
        Me.cboInvoiceStatusPerson.ReadOnly = True
        Me.cboInvoiceStatusPerson.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cboInvoiceStatusPerson.Width = 80
        '
        'INVINVOICEDTINVDataGridViewTextBoxColumn1
        '
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn1.DataPropertyName = "INV_INVOICE_DT_INV"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn1.HeaderText = "Invoice Dt"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn1.Name = "INVINVOICEDTINVDataGridViewTextBoxColumn1"
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn1.ReadOnly = True
        Me.INVINVOICEDTINVDataGridViewTextBoxColumn1.Width = 80
        '
        'INVOICEITEMAMDataGridViewTextBoxColumn1
        '
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1.DataPropertyName = "INVOICE_ITEM_AM"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N2"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle16
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1.HeaderText = "Item Am"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1.Name = "INVOICEITEMAMDataGridViewTextBoxColumn1"
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1.ReadOnly = True
        Me.INVOICEITEMAMDataGridViewTextBoxColumn1.Width = 80
        '
        'BUSINESSNODataGridViewTextBoxColumn1
        '
        Me.BUSINESSNODataGridViewTextBoxColumn1.DataPropertyName = "BUSINESS_NO"
        Me.BUSINESSNODataGridViewTextBoxColumn1.HeaderText = "BUSINESS_NO"
        Me.BUSINESSNODataGridViewTextBoxColumn1.Name = "BUSINESSNODataGridViewTextBoxColumn1"
        Me.BUSINESSNODataGridViewTextBoxColumn1.ReadOnly = True
        Me.BUSINESSNODataGridViewTextBoxColumn1.Visible = False
        '
        'INSTRUCTIONDTDataGridViewTextBoxColumn1
        '
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn1.DataPropertyName = "INSTRUCTION_DT"
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn1.HeaderText = "INSTRUCTION_DT"
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn1.Name = "INSTRUCTIONDTDataGridViewTextBoxColumn1"
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn1.ReadOnly = True
        Me.INSTRUCTIONDTDataGridViewTextBoxColumn1.Visible = False
        '
        'bsPersonPO
        '
        Me.bsPersonPO.DataMember = "V_fsubRentalPurchaseOrderPrpty_V_fsubRentalPurchaseOrderPers"
        Me.bsPersonPO.DataSource = Me.bsCasePackage
        '
        'taCase
        '
        Me.taCase.ClearBeforeFill = True
        '
        'taCasePackage
        '
        Me.taCasePackage.ClearBeforeFill = True
        '
        'taPackage
        '
        Me.taPackage.ClearBeforeFill = True
        '
        'taPropertyPO
        '
        Me.taPropertyPO.ClearBeforeFill = True
        '
        'taPersonPO
        '
        Me.taPersonPO.ClearBeforeFill = True
        '
        'frmRentalPOSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 702)
        Me.Controls.Add(Me.pnlPersonPO)
        Me.Controls.Add(Me.pnlPropertyPO)
        Me.Controls.Add(Me.pnlPackage)
        Me.Controls.Add(Me.SuperLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.pnlFormStrip)
        Me.Controls.Add(Me.pnlMainToolStrip)
        Me.KeyPreview = True
        Me.Name = "frmRentalPOSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Rental PO Summary"
        CType(Me.pbxVIP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMainToolStrip.ResumeLayout(False)
        Me.pnlMainToolStrip.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCase.ResumeLayout(False)
        Me.pnlCase.PerformLayout()
        CType(Me.bsCasePackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsRentalPOSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxCopy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.pnlPackage.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlPropertyPO.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvPropertyPO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPropertyPO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlPersonPO.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvPersonPO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsPersonPO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents pnlFormStrip As System.Windows.Forms.Panel
    Friend WithEvents pnlMainToolStrip As System.Windows.Forms.Panel
    Friend WithEvents tspMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tspRight As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlCase As System.Windows.Forms.Panel
    Friend WithEvents SuperLabel2 As ControlLibrary.SuperLabel
    Friend WithEvents pbxCopy As System.Windows.Forms.PictureBox
    Friend WithEvents txbCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SuperLabel1 As ControlLibrary.SuperLabel
    Friend WithEvents pnlPackage As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvPackage As System.Windows.Forms.DataGridView
    Friend WithEvents pnlPropertyPO As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvPropertyPO As System.Windows.Forms.DataGridView
    Friend WithEvents pnlPersonPO As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvPersonPO As System.Windows.Forms.DataGridView
    Friend WithEvents bsCase As System.Windows.Forms.BindingSource
    Friend WithEvents SuperLabel3 As ControlLibrary.SuperLabel
    Friend WithEvents txbRelatedCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents DsRentalPOSummary As clsRental.dsRentalPOSummary
    Friend WithEvents taCase As clsRental.dsRentalPOSummaryTableAdapters.V_frmRentalPurchaseOrderSumaryTableAdapter
    Friend WithEvents bsCasePackage As System.Windows.Forms.BindingSource
    Friend WithEvents taCasePackage As clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPrptyTableAdapter
    Friend WithEvents bsPackage As System.Windows.Forms.BindingSource
    Friend WithEvents taPackage As clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPersnTableAdapter
    Friend WithEvents bsPropertyPO As System.Windows.Forms.BindingSource
    Friend WithEvents taPropertyPO As clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPropTableAdapter
    Friend WithEvents bsPersonPO As System.Windows.Forms.BindingSource
    Friend WithEvents taPersonPO As clsRental.dsRentalPOSummaryTableAdapters.V_fsubRentalPurchaseOrderPersTableAdapter
    Friend WithEvents txbCompositeCaseNo As ControlLibrary.SuperTextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents CSECASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGSCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RELATEDCASENODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLIENTPACKAGENMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OccupantDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACTUALMOVEINDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VACATIONDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSCCASENOCSEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ONGOINGINDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PONumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUOTEAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CancelledDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboLatestStatusProp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVINVOICEDTINVDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICEITEMAMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BUSINESSNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INSTRUCTIONDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSCCASENOCSEDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RELATEDCASENODataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CSCCLIENTPACKAGEIDCGSDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ONGOINGINDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierNameDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PONumberDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUOTEAMDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CancelledDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboInvoiceStatusPerson As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVINVOICEDTINVDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICEITEMAMDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BUSINESSNODataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INSTRUCTIONDTDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pbxVIP As System.Windows.Forms.PictureBox
    Friend WithEvents txtVIP As System.Windows.Forms.TextBox
End Class
