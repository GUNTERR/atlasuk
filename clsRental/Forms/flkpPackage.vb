Public Class flkpPackage
    Dim mdgvX As DataGridView
    Dim mstrPackageIDColNm As String
    Dim mstrPackageNmColNm As String
    Dim mintCaseNo As Integer
    Private blnReadOnly As Boolean
    Private mblnBenchmarkOn As Boolean

    Public Property BenchmarkOn As Boolean
        Get
            Return mblnBenchmarkOn
        End Get
        Set(value As Boolean)
            mblnBenchmarkOn = value
        End Set
    End Property
    Public Sub SetOpeningParameters(ByVal dgvX As DataGridView, ByVal strPackageIDColNm As String, _
            ByVal strPackageNmColNm As String, ByVal intCaseNo As Integer)
        mdgvX = dgvX
        mstrPackageIDColNm = strPackageIDColNm
        mstrPackageNmColNm = strPackageNmColNm
        mintCaseNo = intCaseNo
    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'clsGlobal.SetupForm(Me, True, True)
            Dim mclsFormProperties As New clsGlobal.clsFormProperties
            mclsFormProperties = clsGlobal.SetupForm1(Me, True, True)
            blnReadOnly = mclsFormProperties.blnFormReadOnly
            BenchmarkOn = mclsFormProperties.blnBenchmarkOn

            FillForm(True)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub FillForm(Optional ByVal blnOpenForm As Boolean = False)
        Dim dteStartDt As Date = Date.Now
        Try
            Me.taPackage.Connection = clsGlobal.Connection
            Me.taPackage.Fill(Me.DsManualApportionments.V_flkpPackage, mintCaseNo)

        Catch ex As System.Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FillForm")
        Finally
            If BenchmarkOn Then clsGlobal.SaveBenchmark(Me.Name.ToString, "FillForm", dteStartDt, Date.Now, mintCaseNo.ToString)
        End Try
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.Close()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        mdgvX.CurrentRow.Cells(mstrPackageIDColNm).Value = Me.dgvClient.CurrentRow.Cells("CGS_CLIENT_PACKAGE_ID_CGS").Value
        mdgvX.CurrentRow.Cells(mstrPackageNmColNm).Value = Me.dgvClient.CurrentRow.Cells("CLIENT_PACKAGE_NM").Value
        Me.Close()
    End Sub

End Class