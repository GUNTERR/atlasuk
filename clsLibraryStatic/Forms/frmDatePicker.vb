Public Class frmDatePicker
    Dim dtmSelectedDate As Date
    Dim txtCallingControl As Control, mrowX As DataGridViewRow, mintColNo As Integer

    Public Sub CallingForm(ByVal vCtrl As Control)
        txtCallingControl = vCtrl
    End Sub
    Public Sub CallingForm(ByVal rowX As DataGridViewRow, ByVal intColNo As Integer)
        mrowX = rowX
        mintColNo = intColNo
    End Sub
    Private Sub MonthCalendar1_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateChanged
        dtmSelectedDate = e.Start.ToShortDateString()
    End Sub

    Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateSelected
        dtmSelectedDate = e.Start.ToShortDateString()
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtmSelectedDate = DateAndTime.Today
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If txtCallingControl IsNot Nothing Then
            txtCallingControl.Text = Format(dtmSelectedDate, "Short date").ToString
        Else
            mrowX.Cells(mintColNo).Value = Format(dtmSelectedDate, "Short date").ToString
        End If
        Me.Close()

    End Sub
End Class