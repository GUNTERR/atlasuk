Imports Allies.PostCoderNet
Imports System.Resources
Imports System.IO
Imports System.Drawing
Imports Microsoft.VisualBasic

Public Class frmPAFSearch
    Private PCoder As PostCoderAPI = Nothing
    Private rm As ResourceManager = Nothing
    Dim strAddress(3) As String
    Dim strResolvedAddress(3) As String
    Dim mtxtAddressLine1 As TextBox
    Dim mtxtAddressLine2 As TextBox
    Dim mtxtAddressLine3 As TextBox
    Dim mtxtPostTown As TextBox
    Dim mtxtCounty As TextBox
    Dim mtxtPostOutCode As TextBox
    Dim mtxtPostInCode As TextBox
    Dim mtxtPOBox As TextBox

    Public Sub CallingForm(ByVal txtAddressLine1 As TextBox, ByVal txtAddressLine2 As TextBox, ByVal txtAddressLine3 As TextBox, _
                        ByVal txtPostTown As TextBox, ByVal txtCounty As TextBox, ByVal txtPostOutCode As TextBox, _
                        ByVal txtPostInCode As TextBox, ByVal txtPOBox As TextBox)
        mtxtAddressLine1 = txtAddressLine1
        mtxtAddressLine2 = txtAddressLine2
        mtxtAddressLine3 = txtAddressLine3
        mtxtPostTown = txtPostTown
        mtxtCounty = txtCounty
        mtxtPostOutCode = txtPostOutCode
        mtxtPostInCode = txtPostInCode
        mtxtPOBox = txtPOBox
    End Sub
    Private Sub frmPAFSearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReadOnlyFields(True)
        PCoder = New PostCoderAPI()
        OpenPAF()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim intHits As Integer = 0
        Dim intRecNo As Integer = 0
        Dim dpno As Integer = 0
        Dim intLastError As Integer = 0
        Dim strAddr As String = ""
        Dim strBuffer As String = ""
        Dim i As Integer = 0

        If Me.txtSearch.Text <> "" Then
            Me.lstResults.Items.Clear()
            Me.lstPremises.Items.Clear()
            ClearAddress()

            Dim intRetCode As Integer = PCoder.MatchAddress(Me.txtSearch.Text, intHits, intRecNo, dpno)

            If (intRetCode = 1) Or (intRetCode = 2) Then
                intRetCode = PCoder.GetAddress(intRecNo, dpno, strAddr)
                SelectAddress(0, dpno)
            ElseIf (intRetCode = 3) Or (intRetCode = 4) Then
                For i = 0 To intHits - 1
                    intRetCode = PCoder.GetThorough(i, 0, strBuffer)
                    lstResults.Items.Add(strBuffer)
                Next i
            ElseIf (intRetCode = 0) Then
                MsgBox("No hits were found.", MsgBoxStyle.Exclamation, "Address Search")
            ElseIf (intRetCode = 5) Then
                MsgBox("Too many hits, please refine search criteria.", MsgBoxStyle.Exclamation, "Address Search")
            Else
                PCoder.GetLastErrMsg(strBuffer, intLastError)
                MsgBox(strBuffer, MsgBoxStyle.Exclamation, "Address Search")
            End If

            If intHits = 1 Then
                Me.lblResult.Text = intHits & " Match"
            Else
                Me.lblResult.Text = intHits & " Matches"
            End If

            If Me.lstResults.Items.Count > 0 Then
                Me.lstResults.SetSelected(0, True)
                Me.lstPremises.Items.Clear()
            End If

            If Me.lstResults.Items.Count > 0 Then
                Dim title As String = ""
                intRetCode = PCoder.GetPremsForPCR(0, title, strBuffer)
                i = 0
                Dim sChars() As Char = {Chr(13), Chr(10)}
                Dim strPremises() As String = strBuffer.Split(sChars)
                For i = strPremises.GetLowerBound(0) To strPremises.GetUpperBound(0) - 1
                    If (strPremises(i).Length > 0) Then
                        Me.lstPremises.Items.Add(strPremises(i))
                    End If
                Next
            End If
        Else
            MsgBox("Please enter the search criteria before clicking the Search button.", MsgBoxStyle.Exclamation, "Address Search")
        End If

    End Sub
    Private Sub SelectAddress(ByVal intRECNo As Integer, ByVal intDPNo As Integer)
        ClearAddress()

        ReDim strResolvedAddress(3)
        ReDim strAddress(3)

        Dim strSubBuildingName As String = GetTag(intRECNo, intDPNo, "SBN:")
        Dim strBuildingName As String = GetTag(intRECNo, intDPNo, "BNA:")
        strAddress(0) = strSubBuildingName
        If strSubBuildingName <> "" Then
            strAddress(0) = strSubBuildingName
        End If
        If strBuildingName <> "" Then
            If strAddress(0) <> "" Then
                strAddress(0) += ", " & strBuildingName
            Else
                strAddress(0) = strBuildingName
            End If
        End If

        Dim strBuildingNo As String = GetTag(intRECNo, intDPNo, "NUM:")
        Dim strThoroughfare As String = GetTag(intRECNo, intDPNo, "STM:")
        If strBuildingNo <> "" Then
            strAddress(1) = strBuildingNo
        End If
        If strThoroughfare <> "" Then
            If strAddress(1) <> "" Then
                strAddress(1) += " " & strThoroughfare
            Else
                strAddress(1) = strThoroughfare
            End If
        End If

        strAddress(2) = GetTag(intRECNo, intDPNo, "DLO:")

        Dim x As Integer
        Dim y As Integer = 0

        For x = 0 To strAddress.GetUpperBound(0) - 1
            If strAddress(x) <> "" Then
                strResolvedAddress(y) = strAddress(x)
                y += 1
            End If
        Next

        Me.txtCounty.Text = clsGlobal.UpperFirst(GetTag(intRECNo, intDPNo, "CCN:"))

        Dim strPostOutCode As String = GetTag(intRECNo, intDPNo, "PCD:")
        Me.txtPostOutCode.Text = Microsoft.VisualBasic.Left(strPostOutCode, Microsoft.VisualBasic.InStr(strPostOutCode, " "))

        Me.txtPostInCode.Text = Microsoft.VisualBasic.Mid(strPostOutCode, Microsoft.VisualBasic.InStr(strPostOutCode, " ") + 1)

        Me.txtPostTown.Text = clsGlobal.UpperFirst(GetTag(intRECNo, intDPNo, "PTN:"))

        Dim strPOBoxNo As String = GetTag(intRECNo, intDPNo, "POB:")
        If strPOBoxNo <> "" Then
            Me.txtPOBox.Text = "PO Box " & strPOBoxNo
        End If

        Me.txtAddressLine1.Text = clsGlobal.UpperFirst(strResolvedAddress(0))
        Me.txtAddressLine2.Text = clsGlobal.UpperFirst(strResolvedAddress(1))
        Me.txtAddressLine3.Text = clsGlobal.UpperFirst(strResolvedAddress(2))
    End Sub

    Private Sub ClearAddress()
        Me.txtAddressLine1.Clear()
        Me.txtAddressLine2.Clear()
        Me.txtAddressLine3.Clear()
        Me.txtPostTown.Clear()
        Me.txtCounty.Clear()
        Me.txtPostOutCode.Clear()
        Me.txtPostInCode.Clear()
        Me.txtPOBox.Clear()
    End Sub
    Private Sub lstPremises_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstPremises.SelectedIndexChanged
        SelectAddress(-1, Convert.ToInt16(Me.lstPremises.SelectedIndex))

    End Sub

    'Private Function GetTag(ByVal DpNo As Integer, ByVal TagNm As String) As String
    '    Dim buff1 As String = ""
    '    Dim intRetCode As Integer = PCoder.GetTag(-1, DpNo, TagNm, buff1)
    '    GetTag = buff1
    'End Function
    Private Function GetTag(ByVal intRECNo As Integer, ByVal DpNo As Integer, ByVal TagNm As String) As String
        Dim buff1 As String = ""
        Dim intRetCode As Integer = PCoder.GetTag(intRECNo, DpNo, TagNm, buff1)
        GetTag = buff1
    End Function

    Private Sub lstResults_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstResults.MouseHover
        If Me.lstResults.Items.Count > 0 Then
            Me.ToolTip1.Show(Me.lstResults.Items.Item(Me.lstResults.SelectedIndex), Me.lstResults)
        End If
    End Sub

    Private Sub lstResults_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstResults.SelectedIndexChanged
        Dim strBuffer As String = ""
        Dim strTitle As String = ""
        Dim intRetCode As Integer = PCoder.GetPremsForPCR(Me.lstResults.SelectedIndex, strTitle, strBuffer)
        Dim i As Integer = 0

        Me.lstPremises.Items.Clear()

        Dim sChars() As Char = {Chr(13), Chr(10)}
        Dim strPremises() As String = strBuffer.Split(sChars)
        For i = strPremises.GetLowerBound(0) To strPremises.GetUpperBound(0) - 1
            If (strPremises(i).Length > 0) Then
                Me.lstPremises.Items.Add(strPremises(i))
            End If
        Next
    End Sub

    Private Sub OpenPAF()
        Try
            Dim strBuffer As String = ""
            Dim intLastError As Integer = 0
            Dim intRetCode As Integer = PCoder.Init("C:\Program Files (x86)\Allies Computing\PostCoder API\pcutils.ctl")

            If ((intRetCode <> PostCoderAPI.PAFRC_SUCCESS) And _
                (intRetCode <> PostCoderAPI.PAFRC_DATA_DYING) And _
                (intRetCode <> PostCoderAPI.PAFRC_EXE_DYING)) Then

                PCoder.GetLastErrMsg(strBuffer, intLastError)
                MsgBox(strBuffer, MsgBoxStyle.Exclamation, "Address Search")
            Else
                If (intRetCode <> PostCoderAPI.PAFRC_SUCCESS) Then
                    PCoder.GetLastErrMsg(strBuffer, intLastError)
                    MsgBox(strBuffer, MsgBoxStyle.Exclamation, "Address Search")
                End If
                PCoder.GetVersion(strBuffer)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "OpenPAF")
        End Try
    End Sub

    Private Sub frmPAFSearch_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim intRetCode As Int16 = PCoder.Close()
    End Sub

    Private Sub frmPAFSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnSearch_Click(sender, e)
        End If
    End Sub


    Private Sub ReadOnlyFields(ByVal blnReadOnly As Boolean)
        Me.txtAddressLine1.ReadOnly = blnReadOnly
        Me.txtAddressLine2.ReadOnly = blnReadOnly
        Me.txtAddressLine3.ReadOnly = blnReadOnly
        Me.txtPostTown.ReadOnly = blnReadOnly
        Me.txtCounty.ReadOnly = blnReadOnly
        Me.txtPostOutCode.ReadOnly = blnReadOnly
        Me.txtPostInCode.ReadOnly = blnReadOnly
        Me.txtPOBox.ReadOnly = blnReadOnly
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        If Me.txtAddressLine1.Text = "" And Me.txtPOBox.Text <> "" Then
            mtxtAddressLine1.Text = Me.txtPOBox.Text
        Else
            mtxtAddressLine1.Text = Me.txtAddressLine1.Text
        End If

        mtxtAddressLine2.Text = Me.txtAddressLine2.Text
        mtxtAddressLine3.Text = Me.txtAddressLine3.Text
        mtxtPostTown.Text = Me.txtPostTown.Text
        mtxtCounty.Text = Me.txtCounty.Text
        mtxtPostOutCode.Text = RTrim(Me.txtPostOutCode.Text)
        mtxtPostInCode.Text = RTrim(Me.txtPostInCode.Text)

        'If Me.txtPOBox.Text <> "" Then
        '    mtxtPOBox.Text = Microsoft.VisualBasic.Mid(Me.txtPOBox.Text, Microsoft.VisualBasic.InStrRev(Me.txtPOBox.Text, " ") + 1)
        'Else
        '    mtxtPOBox.Text = Me.txtPOBox.Text
        'End If


        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.pnlSearchHelp.Visible = False
    End Sub

    Private Sub pbxHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbxHelp.Click
        ShowHelp()
    End Sub
    Private Sub ShowHelp()
        Me.pnlSearchHelp.Visible = True
        Me.pnlSearchHelp.Size = New Point(248, 176)
        Me.pnlSearchHelp.Location = New Point(40, 83)
    End Sub

    Private Sub pbxHelp_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxHelp.MouseHover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub pbxHelp_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbxHelp.MouseLeave
        clsGlobal.SetCursorDefault(Me)
    End Sub


End Class