Imports System.IO
Public Class frmZoom
    Private ctlX As Control

    Public Sub CallingForm(ByVal ctlZoom As Control)
        ctlX = ctlZoom
    End Sub

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If TypeOf (ctlX) Is DataGridView Then
                Me.txtText.Text = CType(ctlX, DataGridView).CurrentCell.Value.ToString
            ElseIf TypeOf (ctlX) Is TextBox Then
                Me.txtText.Text = CType(ctlX, TextBox).Text
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "FormLoad")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If TypeOf (ctlX) Is DataGridView Then
                If Not CType(ctlX, DataGridView).ReadOnly Then
                    CType(ctlX, DataGridView).CurrentCell.Value = Me.txtText.Text
                End If
            ElseIf TypeOf (ctlX) Is TextBox Then
                If Not CType(ctlX, TextBox).ReadOnly Then
                    CType(ctlX, TextBox).Text = Me.txtText.Text
                End If
            End If
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "btnOk_Click")
        End Try
    End Sub
End Class