Option Compare Text
Imports System.Windows.Forms
Imports Microsoft.Win32
Imports System.Net.Mail
Imports System.Net
Imports System.Net.Mime
Imports System.IO
Imports System.Drawing
Imports Microsoft.Office.Interop
Imports Microsoft.VisualBasic.Devices

Public Class clsGlobal
    'Here is a change

    'private shared gdsLookups As New Data.DataSet
    Private Shared mdstX As New Data.DataSet
    Private Shared mstrRecentCase(5) As String

    Private Shared mstrCurrentLinkItem As String
    Private Shared mstrCurrentMenuItem As String
    Private Shared mstrCurrentTab As String
    Private Shared mstrCurrentHeader As String
    Private Shared mstrCurrentForm As String

    Private Shared mintFormBackColor As Integer
    Private Shared mfrmCurrentNonModalForm As Form

    Private Shared mfrmCurrentForm As Form
    Private Shared mconX As New SqlClient.SqlConnection
    'Private Shared mconProd As New SqlClient.SqlConnection

    Private Shared mstrLookupFilePath As String '= "C:\AtlasUk\Lookups\"

    Private Shared mintSubMenuOrgNo As Integer = 0
    Private Shared mintSubMenuCaseId As Integer = 0
    Private Shared mintSubMenuPackageId As Integer = 0 'Used by frmBILLPackageView
    Private Shared mstrSubMenuPackageNm As String  'Used by frmBILLPackageView
    Private Shared mintSubMenuBILLPackageId As Integer = 0 'Used by frmBILLPackageView
    Private Shared mstrSubMenuBusinessArea As String 'Used by frmBILLPackageView
    Private Shared mstrToolStripHSNAgent As String 'Used by frmHSNAgentSummary
    Private Shared mintToolStripHSNAgentNo As String 'Used by frmHSNAgentSummary

    Private Shared mintToolStripCaseId As Integer = 0
    Private Shared mintToolStripPackageId As Integer = 0

    Private Shared mstrToolStripGroupId As String = ""

    Private Shared mintToolStripSupplierNo As Integer
    Private Shared mstrToolStripInvNumber As String
    Private Shared gdtmToolStripInvDt As Date

    Private Shared mstrAddressSearchAddressLine1 As String ' Used by Address Search
    Private Shared mstrAddressSearchCounty As String ' Used by Address Search
    Private Shared mstrAddressSearchPostCodeOut As String ' Used by Address Search
    Private Shared mstrAddressSearchPostCodeIn As String ' Used by Address Search

    Private Shared mstrNameSearchFirstNm As String ' Used by Name Search
    Private Shared mstrNameSearchLastNm As String ' Used by Name Search
    Private Shared mstrNameSearchRole As String ' Used by Name Search
    Private Shared mstrNameSearchPersonRefNo As String ' Used by Name Search

    Private Shared mintProspectNo As Integer
    Private Shared mstrOpportunityCd As String
    Private Shared mintContactNo As Integer = -1

    Private Shared mintBatchNo As Integer
    'Private Shared mintFormLocationX As Int32 = 224
    'Private Shared mintFormLocationY As Int32 = 0

    Public Const conFormBackColorTraining As Integer = -5247250 'Pale Turquoise
    Public Const conFormBackColorReport As Integer = -2252579 'Plum
    Public Const conFormBackColorTest As Integer = -10496 'Gold
    Public Const conFormBackColor44SQLDV001 As Integer = -10496 'Gold
    Public Const conFormBackColor44SQLPR002 As Integer = -1 'White
    'Public Const conFormBackColorQA As Integer = -1
    Public Const conFormBackColorQA As Integer = -545925
    Public Const conFormBackColorMaintenance As Integer = -43674

    'Public Const conFormBackColorTest As Integer = -1 'Gold
    Public Const conFormBackColorProd As Integer = -1 'White
    Public Const conPanelBackColor As Integer = -2170889
    Public Const conFormQueryColor As Integer = -1331 ' LemonChiffon

    'Public Const conFormBackColor As Integer = -16 'Ivory
    'Public Const conFormQueryColor As Integer = -8000
    'Public Const conTextBoxForeColor As Integer = -11 ' Dim Grey

    'Public Shared intToolStripCaseId As Integer = 0
    Private Shared mblnNonNumberEntered As Boolean
    Private Shared blnManualcboNumberEntry As Boolean = False
    Private Shared mobjMaintainPackageForm As Object
    Private Const mintFormLocationX As Int32 = 224
    Private Const mintFormLocationY As Int32 = 0
    Private Shared mintPersonNo As Int32
    Private Shared mstrPersonName As String

    Shared mintDatabase As String
    Shared mstrServer As String
    Private Shared mblnUserCanCreateLetters As Boolean = False
    Private Shared mblnUserCanSearchAddress As Boolean = False
    Private Shared mblnUserCanSavePhotos As Boolean = False
    Private Shared ci As New ComputerInfo
    Public Shared Property UserCanSavePhotos() As Boolean
        Get
            Return mblnUserCanSavePhotos
        End Get
        Set(ByVal value As Boolean)
            mblnUserCanSavePhotos = value
        End Set
    End Property
    Public Shared Property UserCanCreateLetters() As Boolean
        Get
            Return mblnUserCanCreateLetters
        End Get
        Set(ByVal value As Boolean)
            mblnUserCanCreateLetters = value
        End Set
    End Property
    Public Shared Property UserCanSearchAddress() As Boolean
        Get
            Return mblnUserCanSearchAddress
        End Get
        Set(ByVal value As Boolean)
            mblnUserCanSearchAddress = value
        End Set
    End Property
    Public Shared Property LookupFilePath() As String
        Get
            Return mstrLookupFilePath
        End Get
        Set(ByVal value As String)
            mstrLookupFilePath = value
        End Set
    End Property
    Public Shared Property MaintainPackageForm() As Object
        Get
            Return mobjMaintainPackageForm
        End Get
        Set(ByVal value As Object)
            mobjMaintainPackageForm = value
        End Set
    End Property
    Public Shared Property FormView() As DataSet
        Get
            Return mdstX
        End Get
        Set(ByVal value As DataSet)
            mdstX = value
        End Set
    End Property

    Public Shared Property RecentCase() As String()
        Get
            Return mstrRecentCase
        End Get
        Set(ByVal value As String())
            mstrRecentCase = value
        End Set
    End Property

    Public Shared Property CurrentLinkItem() As String
        Get
            Return mstrCurrentLinkItem
        End Get
        Set(ByVal value As String)
            mstrCurrentLinkItem = value
        End Set
    End Property

    Public Shared Property CurrentMenuItem() As String
        Get
            Return mstrCurrentMenuItem
        End Get
        Set(ByVal value As String)
            mstrCurrentMenuItem = value
        End Set
    End Property

    Public Shared Property CurrentTab() As String
        Get
            Return mstrCurrentTab
        End Get
        Set(ByVal value As String)
            mstrCurrentTab = value
        End Set
    End Property

    Public Shared Property CurrentHeader() As String
        Get
            Return mstrCurrentHeader
        End Get
        Set(ByVal value As String)
            mstrCurrentHeader = value
        End Set
    End Property

    Public Shared Property CurrentFormName() As String
        Get
            Return mstrCurrentForm
        End Get
        Set(ByVal value As String)
            mstrCurrentForm = value
        End Set
    End Property

    Public Shared Property FormBackColor() As Integer
        Get
            Return mintFormBackColor
        End Get
        Set(ByVal value As Integer)
            mintFormBackColor = value
        End Set
    End Property

    Public Shared Property CurrentNonModalForm() As Form
        Get
            Return mfrmCurrentNonModalForm
        End Get
        Set(ByVal value As Form)
            mfrmCurrentNonModalForm = value
        End Set
    End Property

    Public Shared Property CurrentForm() As Form
        Get
            Return mfrmCurrentForm
        End Get
        Set(ByVal value As Form)
            mfrmCurrentForm = value
        End Set
    End Property

    Public Shared Property Connection() As SqlClient.SqlConnection
        Get
            Return mconX
        End Get
        Set(ByVal value As SqlClient.SqlConnection)
            mconX = value
        End Set
    End Property
    'Public Shared Property ProductionConnection() As SqlClient.SqlConnection
    '    Get
    '        Return mconProd
    '    End Get
    '    Set(ByVal value As SqlClient.SqlConnection)
    '        mconProd = value
    '    End Set
    'End Property

    'Public Shared ReadOnly Property LookupFilePath() As String
    '    Get
    '        Return mstrLookupFilePath
    '    End Get
    'End Property

    Public Shared Property SubMenuOrgNo() As Integer
        Get
            Return mintSubMenuOrgNo
        End Get
        Set(ByVal value As Integer)
            mintSubMenuOrgNo = value
        End Set
    End Property

    Public Shared Property SubMenuCaseId() As Integer
        Get
            Return mintSubMenuCaseId
        End Get
        Set(ByVal value As Integer)
            mintSubMenuCaseId = value
        End Set
    End Property

    Public Shared Property SubMenuPackageId() As Integer
        Get
            Return mintSubMenuPackageId
        End Get
        Set(ByVal value As Integer)
            mintSubMenuPackageId = value
        End Set
    End Property

    Public Shared Property SubMenuPackageNm() As String
        Get
            Return mstrSubMenuPackageNm
        End Get
        Set(ByVal value As String)
            mstrSubMenuPackageNm = value
        End Set
    End Property

    Public Shared Property SubMenuBILLPackageId() As Integer
        Get
            Return mintSubMenuBILLPackageId
        End Get
        Set(ByVal value As Integer)
            mintSubMenuBILLPackageId = value
        End Set
    End Property

    Public Shared Property SubMenuBusinessArea() As String
        Get
            Return mstrSubMenuBusinessArea
        End Get
        Set(ByVal value As String)
            mstrSubMenuBusinessArea = value
        End Set
    End Property

    Public Shared Property ToolStripCaseId() As Integer
        Get
            Return mintToolStripCaseId
        End Get
        Set(ByVal value As Integer)
            mintToolStripCaseId = value
        End Set
    End Property

    Public Shared Property ToolStripPackageId() As Integer
        Get
            Return mintToolStripPackageId
        End Get
        Set(ByVal value As Integer)
            mintToolStripPackageId = value
        End Set
    End Property

    Public Shared Property ToolStripGroupId() As String
        Get
            Return mstrToolStripGroupId
        End Get
        Set(ByVal value As String)
            mstrToolStripGroupId = value
        End Set
    End Property

    Public Shared Property ToolStripSupplierNo() As Integer
        Get
            Return mintToolStripSupplierNo
        End Get
        Set(ByVal value As Integer)
            mintToolStripSupplierNo = value
        End Set
    End Property

    Public Shared Property ToolStripInvNumber() As String
        Get
            Return mstrToolStripInvNumber
        End Get
        Set(ByVal value As String)
            mstrToolStripInvNumber = value
        End Set
    End Property

    Public Shared Property ToolStripInvDt() As Date
        Get
            Return gdtmToolStripInvDt
        End Get
        Set(ByVal value As Date)
            gdtmToolStripInvDt = value
        End Set
    End Property

    Public Shared Property ToolStripHSNAgent As String
        Get
            Return mstrToolStripHSNAgent
        End Get
        Set(value As String)
            mstrToolStripHSNAgent = value
        End Set
    End Property

    Public Shared Property ToolStripHSNAgentNo As Integer
        Get
            Return mintToolStripHSNAgentNo
        End Get
        Set(value As Integer)
            mintToolStripHSNAgentNo = value
        End Set
    End Property
    Public Shared Property AddressSearchAddressLine1() As String
        Get
            Return mstrAddressSearchAddressLine1
        End Get
        Set(ByVal value As String)
            mstrAddressSearchAddressLine1 = value
        End Set
    End Property

    Public Shared Property AddressSearchCounty() As String
        Get
            Return mstrAddressSearchCounty
        End Get
        Set(ByVal value As String)
            mstrAddressSearchCounty = value
        End Set
    End Property

    Public Shared Property AddressSearchPostCodeOut() As String
        Get
            Return mstrAddressSearchPostCodeOut
        End Get
        Set(ByVal value As String)
            mstrAddressSearchPostCodeOut = value
        End Set
    End Property

    Public Shared Property AddressSearchPostCodeIn() As String
        Get
            Return mstrAddressSearchPostCodeIn
        End Get
        Set(ByVal value As String)
            mstrAddressSearchPostCodeIn = value
        End Set
    End Property

    Public Shared Property NameSearchFirstNm() As String
        Get
            Return mstrNameSearchFirstNm
        End Get
        Set(ByVal value As String)
            mstrNameSearchFirstNm = value
        End Set
    End Property

    Public Shared Property NameSearchLastNm() As String
        Get
            Return mstrNameSearchLastNm
        End Get
        Set(ByVal value As String)
            mstrNameSearchLastNm = value
        End Set
    End Property

    Public Shared Property NameSearchPersonRefNo() As String
        Get
            Return mstrNameSearchPersonRefNo
        End Get
        Set(ByVal value As String)
            mstrNameSearchPersonRefNo = value
        End Set
    End Property
    Public Shared Property NameSearchRole() As String
        Get
            Return mstrNameSearchRole
        End Get
        Set(ByVal value As String)
            mstrNameSearchRole = value
        End Set
    End Property
    Public Shared Property ProspectNo() As Integer
        Get
            Return mintProspectNo
        End Get
        Set(ByVal value As Integer)
            mintProspectNo = value
        End Set
    End Property

    Public Shared Property OpportunityCd() As String
        Get
            Return mstrOpportunityCd
        End Get
        Set(ByVal value As String)
            mstrOpportunityCd = value
        End Set
    End Property

    Public Shared Property ContactNo() As Integer
        Get
            Return mintContactNo
        End Get
        Set(ByVal value As Integer)
            mintContactNo = value
        End Set
    End Property

    Public Shared Property BatchNo() As Integer
        Get
            Return mintBatchNo
        End Get
        Set(ByVal value As Integer)
            mintBatchNo = value
        End Set
    End Property

    Public Shared ReadOnly Property FormLocationX() As Integer
        Get
            Return mintFormLocationX
        End Get
    End Property

    Public Shared ReadOnly Property FormLocationY() As Integer
        Get
            Return mintFormLocationY
        End Get
    End Property

    Public Shared ReadOnly Property ZoomImage() As Image
        Get
            Return My.Resources.Zoom
        End Get
    End Property
    Public Shared ReadOnly Property ClearCaseImage() As Image
        Get
            Return My.Resources.Clear
        End Get
    End Property
    Public Shared ReadOnly Property WordImage() As Image
        Get
            Return My.Resources.Word
        End Get
    End Property
    Public Shared ReadOnly Property CostSavingImage() As Image
        Get
            Return My.Resources.CostSaving
        End Get
    End Property

    Public Shared Sub EnterQueryMode(ByVal frmX As Form, Optional ByVal blnReadOnly As Boolean = False)
        CreateMainToolStripItems(frmX, True, False, blnReadOnly)
        EnableQuery(frmX, False)
        frmX.BackColor = Color.FromArgb(conFormQueryColor)
        SwitchToQueryMode(frmX)
    End Sub
    Public Shared Sub EnableQuery(ByVal frmX As Form, ByVal blnActive As Boolean)
        Dim ctlX As Control
        Dim ctlY As Control

        For Each ctlY In frmX.Controls
            If ctlY.Name = "pnlMainToolStrip" And TypeOf (ctlY) Is Panel Then
                For Each ctlX In ctlY.Controls
                    If TypeOf (ctlX) Is ToolStrip Then
                        If ctlX.Name = "tspMain" Then
                            CType(ctlX, ToolStrip).Items.Item("tslQuery").Enabled = blnActive
                        End If
                    End If
                Next
            End If
        Next

    End Sub

    Public Shared Sub SwitchToQueryMode(ByRef ctlContainer As Control, Optional ByVal blnPreviousQuery As Boolean = False)
        Try
            Dim ctlX As Control

            For Each ctlX In ctlContainer.Controls

                If TypeOf (ctlX) Is ControlLibrary.SuperTextBox Then
                    If CType(ctlX, ControlLibrary.SuperTextBox).Queryable = True Then
                        CType(ctlX, TextBox).ReadOnly = False
                        ctlX.BackColor = Color.FromArgb(conFormQueryColor)
                        ctlX.Focus()
                    Else
                        ctlX.Visible = False
                    End If
                    CType(ctlX, TextBox).Clear()
                ElseIf TypeOf (ctlX) Is ControlLibrary.SuperComboBox Then
                    If CType(ctlX, ControlLibrary.SuperComboBox).Queryable = True Then
                        CType(ctlX, ControlLibrary.SuperComboBox).ReadOnly = False
                        ctlX.BackColor = Color.FromArgb(conFormQueryColor)
                    Else
                        ctlX.Visible = False
                    End If
                    CType(ctlX, ControlLibrary.SuperComboBox).SelectedIndex = -1
                ElseIf TypeOf (ctlX) Is ControlLibrary.SuperCheckBox Then
                    ctlX.Visible = False
                ElseIf TypeOf (ctlX) Is TextBox Then
                    If ctlX.Visible Then
                        CType(ctlX, TextBox).Clear()
                        ctlX.BackColor = Color.FromArgb(conFormQueryColor)
                    End If
                    ctlX.Visible = False

                ElseIf TypeOf (ctlX) Is BindingNavigator Then
                    ctlX.Enabled = False
                ElseIf (TypeOf (ctlX) Is Panel Or TypeOf ctlX Is GroupBox) And ctlX.Name <> "pnlFormStrip" Then
                    'If ctlX.Name = "pnlFormStrip" Then

                    'Else
                    SwitchToQueryMode(ctlX)
                    'End If
                ElseIf TypeOf (ctlX) Is PictureBox Then
                    If ctlX.Name = "pbxVIP" Then
                        ctlX.Visible = False
                    End If
                    ctlX.Enabled = False
                    'ctlX.Visible = False
                ElseIf TypeOf (ctlX) Is LinkLabel Then
                    ctlX.Visible = False
                ElseIf TypeOf (ctlX) Is ControlLibrary.SuperLabel Then
                ElseIf TypeOf (ctlX) Is Label Then
                    'If ctlX.Name = "lblClientId" Or ctlX.Name = "lblFullName" Then
                    '    MsgBox(ctlX.Name)
                    'End If
                    ctlX.Visible = False

                ElseIf TypeOf (ctlX) Is DataGridView Then
                    ctlX.Visible = False
                ElseIf TypeOf (ctlX) Is MaskedTextBox Then
                    ctlX.Visible = False
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "SwitchToQueryMode")
        End Try

    End Sub

    Public Shared Sub SwitchToFormView(ByRef ctlContainer As Control)
        Dim ctlX As Control

        For Each ctlX In ctlContainer.Controls
            If TypeOf (ctlX) Is ControlLibrary.SuperTextBox Then
                If CType(ctlX, ControlLibrary.SuperTextBox).Queryable = True Then
                    If CType(ctlX, ControlLibrary.SuperTextBox).Updateable Then
                        ctlX.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                    Else
                        ctlX.BackColor = Color.FromArgb(conPanelBackColor)
                        CType(ctlX, ControlLibrary.SuperTextBox).ReadOnly = True
                    End If
                Else
                    ctlX.Visible = True
                End If
            ElseIf TypeOf (ctlX) Is ControlLibrary.SuperComboBox Then
                If CType(ctlX, ControlLibrary.SuperComboBox).Queryable = True Then
                    If CType(ctlX, ControlLibrary.SuperComboBox).Updateable Then
                        ctlX.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                    Else
                        ctlX.BackColor = Color.FromArgb(conPanelBackColor)
                        CType(ctlX, ControlLibrary.SuperComboBox).ReadOnly = True
                    End If
                Else
                    ctlX.Visible = True
                End If
            ElseIf TypeOf (ctlX) Is ControlLibrary.SuperCheckBox Then
                ctlX.Visible = True
            ElseIf TypeOf (ctlX) Is TextBox Then
                If CType(ctlX, TextBox).ReadOnly Then
                    ctlX.BackColor = Color.FromArgb(conPanelBackColor)
                Else
                    ctlX.BackColor = Color.FromArgb(clsGlobal.FormBackColor)
                End If
                ctlX.Visible = True
                'If ctlX.Tag = "Hide" Then
                '    ctlX.Width = 0
                'End If
            ElseIf TypeOf (ctlX) Is BindingNavigator Then
                ctlX.Enabled = True
            ElseIf (TypeOf (ctlX) Is Panel Or TypeOf ctlX Is GroupBox) And ctlX.Name <> "pnlFormStrip" Then
                'If ctlX.Name = "pnlFormStrip" Then

                'Else
                SwitchToFormView(ctlX)
                'End If
            ElseIf TypeOf (ctlX) Is PictureBox Then
                ctlX.Enabled = True
                ctlX.Visible = True
            ElseIf TypeOf (ctlX) Is LinkLabel Then
                ctlX.Visible = True
            ElseIf TypeOf (ctlX) Is Label Then
                ctlX.Visible = True
            ElseIf TypeOf (ctlX) Is DataGridView Then
                ctlX.Visible = True
            ElseIf TypeOf (ctlX) Is MaskedTextBox Then
                ctlX.Visible = True
            End If
        Next
    End Sub

    Private Shared Sub LeaveClearCase(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.parent.Controls("cboNumber").findform.Cursor = Cursors.Default
    End Sub
    Private Shared Sub HoverClearCase(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.parent.Controls("cboNumber").findform.Cursor = Cursors.Hand
    End Sub
    Public Shared Sub ClickClearCase(ByVal sender As Object, ByVal e As System.EventArgs)
        CType(sender.parent.Controls("cboNumber"), ComboBox).SelectedIndex = 0
    End Sub
    Public Shared Sub ClickClearCase(ByVal pnlX As Panel)
        'Called from form level F9 key
        Dim ctlX As Control

        For Each ctlX In pnlX.Controls
            If TypeOf (ctlX) Is ComboBox Then
                CType(ctlX, ComboBox).SelectedIndex = 0
            End If
        Next
    End Sub
    Private Shared Sub ClickLetter(ByVal sender As Object, ByVal e As System.EventArgs)
        MsgBox(sender.parent.parent.name)
        'ClearCase(CType(sender.parent.Controls("txtToolStripCaseId"), TextBox))
    End Sub

    Public Shared Sub SetFormStripNumber(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cboX As ComboBox
        cboX = CType(sender.parent.Controls("cboNumber"), ComboBox)
        If IsNumeric(cboX.SelectedItem) Then
            clsGlobal.ToolStripCaseId = CType(cboX.SelectedItem, Integer)
        Else
            clsGlobal.ToolStripCaseId = 0
        End If
    End Sub

    Public Shared Sub SetManualFormStripNumber(ByVal sender As Object, ByVal e As System.EventArgs)
        If blnManualcboNumberEntry Then
            Dim cboX As ComboBox
            cboX = CType(sender.parent.Controls("cboNumber"), ComboBox)
            If IsNumeric(cboX.Text) Then
                clsGlobal.ToolStripCaseId = CType(cboX.Text, Integer)
                Dim intCounter As Integer

                For intCounter = clsGlobal.RecentCase.GetUpperBound(0) To 1 Step -1
                    clsGlobal.RecentCase.SetValue(clsGlobal.RecentCase.GetValue(intCounter - 1), intCounter)
                Next
                clsGlobal.RecentCase.SetValue(CStr(clsGlobal.ToolStripCaseId), 1)

                cboX.Items.Clear()
                cboX.Items.Add("")

                For intCounter = 1 To clsGlobal.RecentCase.GetUpperBound(0)
                    If IsNumeric(clsGlobal.RecentCase(intCounter)) Then
                        cboX.Items.Add(clsGlobal.RecentCase(intCounter))
                    End If
                Next
                cboX.SelectedIndex = 1
            Else
                clsGlobal.ToolStripCaseId = 0
            End If
            blnManualcboNumberEntry = False
        End If

    End Sub
    Private Shared Sub FormStripNumber_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        mblnNonNumberEntered = False

        ' Determine whether the keystroke is a number from the top of the keyboard.
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            ' Determine whether the keystroke is a number from the keypad.
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                ' Determine whether the keystroke is a backspace.
                If e.KeyCode <> Keys.Back Then
                    ' A non-numerical keystroke was pressed. 
                    ' Set the flag to true and evaluate in KeyPress event.
                    mblnNonNumberEntered = True
                End If
            End If
        End If
    End Sub
    Private Shared Sub FormStripNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = mblnNonNumberEntered
        blnManualcboNumberEntry = True
    End Sub

    Public Shared Sub CreateFormStripItems(ByVal frmX As Form, ByVal pnlFormStrip As Panel)

        Dim pbxClearCase As New PictureBox
        Dim lblToolStripCaseId As New Label
        Dim cboNumber As New ComboBox

        cboNumber.FormattingEnabled = True
        cboNumber.Location = New System.Drawing.Point(54, 8)
        cboNumber.Name = "cboNumber"
        cboNumber.Size = New System.Drawing.Size(70, 20)
        cboNumber.TabIndex = 1
        cboNumber.ForeColor = System.Drawing.Color.DimGray
        cboNumber.Font = New System.Drawing.Font("Arial", 8, FontStyle.Regular)
        AddHandler cboNumber.SelectedIndexChanged, AddressOf SetFormStripNumber
        AddHandler cboNumber.LostFocus, AddressOf SetManualFormStripNumber
        AddHandler cboNumber.KeyDown, AddressOf FormStripNumber_KeyDown
        AddHandler cboNumber.KeyPress, AddressOf FormStripNumber_KeyPress
        'AddHandler cboNumber.LostFocus, AddressOf SetFormStripNumber

        'pbxClearCase

        pbxClearCase.Image = ClearCaseImage
        pbxClearCase.Location = New System.Drawing.Point(130, 11)
        pbxClearCase.Name = "pbxClearCase"
        pbxClearCase.Size = New System.Drawing.Size(15, 15)
        pbxClearCase.TabIndex = 4
        pbxClearCase.TabStop = False
        AddHandler pbxClearCase.Click, AddressOf ClickClearCase
        AddHandler pbxClearCase.MouseHover, AddressOf HoverClearCase
        AddHandler pbxClearCase.MouseLeave, AddressOf LeaveClearCase

        'lblToolStripCaseId
        lblToolStripCaseId.AutoSize = True
        lblToolStripCaseId.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblToolStripCaseId.ForeColor = System.Drawing.Color.White
        lblToolStripCaseId.Location = New System.Drawing.Point(0, 11)
        lblToolStripCaseId.Name = "Label3"
        lblToolStripCaseId.Size = New System.Drawing.Size(52, 13)
        lblToolStripCaseId.TabIndex = 3
        lblToolStripCaseId.Text = "Number"

        pnlFormStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(214, Byte), Integer))
        pnlFormStrip.Controls.Add(cboNumber)
        pnlFormStrip.Controls.Add(pbxClearCase)
        pnlFormStrip.Controls.Add(lblToolStripCaseId)
        pnlFormStrip.Location = New System.Drawing.Point(267, 25)
        pnlFormStrip.Size = New System.Drawing.Size(528, 35)

        Dim intCounter As Integer

        cboNumber.Items.Clear()
        cboNumber.Items.Add("")

        For intCounter = 1 To clsGlobal.RecentCase.GetUpperBound(0)
            If IsNumeric(clsGlobal.RecentCase(intCounter)) Then
                cboNumber.Items.Add(clsGlobal.RecentCase(intCounter))
            End If
        Next
        If clsGlobal.ToolStripCaseId > 0 Then cboNumber.SelectedIndex = cboNumber.FindString(clsGlobal.ToolStripCaseId.ToString)
    End Sub

    Public Shared Sub CreateRightToolStripItems(ByVal frmX As Form)
        Dim ctlX As Control
        Dim ctlY As Control

        For Each ctlY In frmX.Controls
            If ctlY.Name = "pnlMainToolStrip" And TypeOf (ctlY) Is Panel Then
                For Each ctlX In ctlY.Controls
                    If TypeOf (ctlX) Is ToolStrip Then
                        If ctlX.Name = "tspRight" Then
                            Dim tsbZoom As New System.Windows.Forms.ToolStripButton
                            Dim tsbWord As New System.Windows.Forms.ToolStripButton
                            Dim tsbCostSaving As New System.Windows.Forms.ToolStripButton

                            'tsbZoom
                            tsbZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                            tsbZoom.Image = ZoomImage
                            tsbZoom.BackColor = Color.Black
                            tsbZoom.ImageTransparentColor = System.Drawing.Color.Magenta
                            tsbZoom.Name = "tsbZoom"
                            tsbZoom.Size = New System.Drawing.Size(23, 25)
                            tsbZoom.Text = "Zoom"
                            tsbZoom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
                            tsbZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image

                            'tsbWord
                            tsbWord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                            tsbWord.Image = WordImage
                            tsbWord.BackColor = Color.Black
                            tsbWord.ImageTransparentColor = System.Drawing.Color.Magenta
                            tsbWord.Name = "tsbWord"
                            tsbWord.Size = New System.Drawing.Size(23, 25)
                            tsbWord.Text = "Output To Word"
                            tsbWord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
                            tsbWord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image

                            AddHandler tsbWord.Click, AddressOf PrintForm

                            'tsbCostSaving
                            tsbCostSaving.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                            tsbCostSaving.Image = CostSavingImage
                            tsbCostSaving.BackColor = Color.Black
                            tsbCostSaving.ImageTransparentColor = System.Drawing.Color.Magenta
                            tsbCostSaving.Name = "tsbCostSaving"
                            tsbCostSaving.Size = New System.Drawing.Size(23, 25)
                            tsbCostSaving.Text = "Cost Savings"
                            tsbCostSaving.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
                            tsbCostSaving.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image

                            CType(ctlX, ToolStrip).Items.AddRange(New System.Windows.Forms.ToolStripItem() {tsbWord, tsbZoom, tsbCostSaving})
                            CType(ctlX, ToolStrip).LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow

                        End If
                    End If
                Next
            End If
        Next
    End Sub
    Public Shared Sub RemoveRightToolStripItem(ByVal tspRight As ToolStrip, ByVal strButtonNm As String)

        tspRight.Items.RemoveByKey(strButtonNm)

    End Sub
    Public Shared Sub CreateExcelToolStrip(ByVal frmX As Form)
        Dim ctlX As Control
        Dim ctlY As Control

        For Each ctlY In frmX.Controls
            If ctlY.Name = "pnlMainToolStrip" And TypeOf (ctlY) Is Panel Then
                For Each ctlX In ctlY.Controls
                    If TypeOf (ctlX) Is ToolStrip Then
                        If ctlX.Name = "tspRight" Then
                            Dim tsbSpellCheck As New System.Windows.Forms.ToolStripButton

                            'tsbSpellCheck
                            tsbSpellCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
                            tsbSpellCheck.Image = My.Resources.Excel
                            tsbSpellCheck.BackColor = Color.Black
                            tsbSpellCheck.ImageTransparentColor = System.Drawing.Color.Magenta
                            tsbSpellCheck.Name = "tsbExcel"
                            tsbSpellCheck.Size = New System.Drawing.Size(23, 25)
                            tsbSpellCheck.Text = "Export To Excel"
                            tsbSpellCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
                            tsbSpellCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image

                            AddHandler tsbSpellCheck.Click, AddressOf ExportToExcel

                            CType(ctlX, ToolStrip).Items.AddRange(New System.Windows.Forms.ToolStripItem() {tsbSpellCheck})
                            CType(ctlX, ToolStrip).LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
                        End If
                    End If
                Next
            End If
        Next
    End Sub
    Public Shared Sub CreateMainToolStripItems(ByVal frmX As Form, ByVal blnQueryMode As Boolean, _
            ByVal blnNoQuery As Boolean, Optional ByVal blnReadOnly As Boolean = False)
        Dim ctlX As Control
        Dim ctlY As Control

        For Each ctlY In frmX.Controls
            If ctlY.Name = "pnlMainToolStrip" And TypeOf (ctlY) Is Panel Then
                For Each ctlX In ctlY.Controls
                    If TypeOf (ctlX) Is ToolStrip Then
                        If ctlX.Name = "tspMain" Then
                            CType(ctlX, ToolStrip).Items.Clear()

                            Dim tslQuery As New System.Windows.Forms.ToolStripLabel
                            Dim tslRun As New System.Windows.Forms.ToolStripLabel
                            Dim tslSave As New System.Windows.Forms.ToolStripLabel
                            Dim tslUndo As New System.Windows.Forms.ToolStripLabel

                            Dim txtToolStripSeparator1 As New System.Windows.Forms.ToolStripSeparator
                            Dim txtToolStripSeparator2 As New System.Windows.Forms.ToolStripSeparator
                            Dim txtToolStripSeparator3 As New System.Windows.Forms.ToolStripSeparator

                            'tslQuery
                            tslQuery.ActiveLinkColor = System.Drawing.Color.White
                            tslQuery.AutoSize = False
                            tslQuery.BackColor = System.Drawing.Color.Black
                            tslQuery.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
                            tslQuery.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                            tslQuery.ForeColor = System.Drawing.Color.White
                            tslQuery.IsLink = True
                            tslQuery.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
                            tslQuery.LinkColor = System.Drawing.Color.White
                            tslQuery.Name = "tslQuery"
                            tslQuery.Size = New System.Drawing.Size(41, 22)
                            tslQuery.Text = "Query"
                            tslQuery.VisitedLinkColor = System.Drawing.Color.White

                            'ToolStripSeparator1
                            txtToolStripSeparator1.AutoSize = False
                            txtToolStripSeparator1.BackColor = System.Drawing.Color.White
                            txtToolStripSeparator1.ForeColor = System.Drawing.Color.White
                            txtToolStripSeparator1.Name = "ToolStripSeparator1"
                            txtToolStripSeparator1.Padding = New System.Windows.Forms.Padding(0, 0, 20, 0)
                            txtToolStripSeparator1.Size = New System.Drawing.Size(10, 25)

                            'tslRun
                            tslRun.ActiveLinkColor = System.Drawing.Color.White
                            tslRun.AutoSize = False
                            tslRun.Enabled = False
                            tslRun.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                            tslRun.ForeColor = System.Drawing.Color.White
                            tslRun.IsLink = True
                            tslRun.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
                            tslRun.LinkColor = System.Drawing.Color.White
                            tslRun.Name = "tslRun"
                            tslRun.Size = New System.Drawing.Size(40, 22)
                            tslRun.Text = "Run"
                            tslRun.VisitedLinkColor = System.Drawing.Color.White

                            'txtToolStripSeparator2
                            txtToolStripSeparator2.AutoSize = False
                            txtToolStripSeparator2.Name = "ToolStripSeparator2"
                            txtToolStripSeparator2.Size = New System.Drawing.Size(10, 25)

                            'tslSave
                            tslSave.ActiveLinkColor = System.Drawing.Color.White
                            tslSave.AutoSize = False
                            tslSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
                            tslSave.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                            tslSave.ForeColor = System.Drawing.Color.White
                            tslSave.IsLink = True
                            tslSave.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
                            tslSave.LinkColor = System.Drawing.Color.White
                            tslSave.Name = "tslSave"
                            tslSave.Size = New System.Drawing.Size(40, 22)
                            tslSave.Text = "Save"
                            tslSave.VisitedLinkColor = System.Drawing.Color.White

                            'txtToolStripSeparator3
                            txtToolStripSeparator3.AutoSize = False
                            txtToolStripSeparator3.Name = "ToolStripSeparator3"
                            txtToolStripSeparator3.Size = New System.Drawing.Size(10, 25)

                            'tslUndo
                            tslUndo.ActiveLinkColor = System.Drawing.Color.White
                            tslUndo.AutoSize = False
                            tslUndo.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                            tslUndo.ForeColor = System.Drawing.Color.White
                            tslUndo.IsLink = True
                            tslUndo.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
                            tslUndo.LinkColor = System.Drawing.Color.White
                            tslUndo.Name = "tslUndo"
                            tslUndo.Size = New System.Drawing.Size(40, 22)
                            tslUndo.Text = "Undo"
                            tslUndo.VisitedLinkColor = System.Drawing.Color.White

                            CType(ctlX, ToolStrip).Items.AddRange(New System.Windows.Forms.ToolStripItem() {tslQuery, txtToolStripSeparator1, tslRun, txtToolStripSeparator2, tslSave, txtToolStripSeparator3, tslUndo})

                            If blnQueryMode Then
                                tslRun.Enabled = True
                                'tsbUndoForm.Enabled = False

                                'CType(ctlX, ToolStrip).Items.AddRange(New System.Windows.Forms.ToolStripItem() {txtToolStripLabel1, txtToolStripCaseNo, txtToolStripSeparator1, tsbRunQuery, tsbDefineQuery, txtToolStripSeparator2, txtToolStripLabel2, txtToolStripOrganisation})
                            Else
                                tslRun.Enabled = False
                                'tsbUndoForm.Enabled = True

                                'CType(ctlX, ToolStrip).Items.AddRange(New System.Windows.Forms.ToolStripItem() {txtToolStripLabel1, txtToolStripCaseNo, txtToolStripSeparator1, tsbDefineQuery, tsbSave, tsbUndoForm, txtToolStripSeparator2, txtToolStripLabel2, txtToolStripOrganisation})
                            End If

                            If blnNoQuery Then
                                tslQuery.Enabled = False
                                tslRun.Enabled = False
                            End If
                            If blnReadOnly Then
                                tslSave.Enabled = False
                                tslUndo.Enabled = False
                            End If


                        End If
                    End If
                Next
            End If
        Next


    End Sub

    Public Shared Sub UndoForm(ByVal frmX As Form, ByVal bsiX As clsBindingSourceItem, ByVal dstX As DataSet)
        Dim bsX As BindingSource
        'frmX.Validate()
        For Each bsX In bsiX
            bsX.CancelEdit()
        Next
    End Sub

    Private Function GetGeneratedNo(ByRef lngArrayPos As Integer) As Integer
        Try

        Catch ex As Exception

        End Try


    End Function

    Public Shared Sub CheckFormHasChanges(ByVal frmX As Form, ByVal bsiX As clsBindingSourceItem, ByVal dstX As DataSet, ByRef e As System.Windows.Forms.FormClosingEventArgs, Optional ByVal blnIsNewRow As Boolean = False)
        'frmX.Validate()
        If Not blnIsNewRow Then
            Dim bsX As BindingSource
            For Each bsX In bsiX
                bsX.EndEdit()
            Next
        End If

        If dstX.HasChanges Or blnIsNewRow Then
            Dim strMsg As String = "Data has been changed. Click on OK to abandon changes and close " + _
                                    frmX.Text + ", or click Cancel to return to " + frmX.Text + _
                                    " where you can click on the Save button"

            If Not MsgBox(strMsg, MsgBoxStyle.Question Or MsgBoxStyle.OkCancel, _
                "Pending Updates Not Saved") = MsgBoxResult.Ok Then
                e.Cancel = True
            End If
        End If
    End Sub
    Public Shared Function FormIsDirty(ByVal frmX As Form, ByVal bsiX As clsBindingSourceItem, ByVal dstX As DataSet, ByVal blnIsNewRow As Boolean) As Boolean
        Try
            If Not blnIsNewRow Then
                Dim bsX As BindingSource
                For Each bsX In bsiX
                    bsX.EndEdit()
                Next
            End If

            If dstX.HasChanges Or blnIsNewRow Then
                FormIsDirty = True
            Else
                FormIsDirty = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "FormIsDirty")
        End Try
    End Function

    Public Shared Sub LoadLookupLists(ByVal strSQL As String, ByVal strLookupFile As String, ByVal dsLookups As DataSet, ByVal aArray() As String)
        Dim daLookups As New SqlClient.SqlDataAdapter(strSQL, clsGlobal.Connection)
        Dim x As Integer

        Try
            daLookups.Fill(dsLookups)

            For x = 0 To dsLookups.Tables.Count - 1
                dsLookups.Tables(x).TableName = aArray(x).ToString
            Next
            dsLookups.WriteXml(clsGlobal.LookupFilePath + strLookupFile, XmlWriteMode.WriteSchema)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error Filling Lookup Tables")
        End Try
    End Sub

    Public Shared Function GetGeneratedNoGrid(ByRef strTableName As String, ByVal dtX As DataTable, ByVal dgX As DataGridViewRow, ByVal strGeneratedNoField As String) As Integer
        Dim x As Integer
        Dim y As Integer
        Dim strSQL As String
        Dim strGroupBy As String
        Dim intGN As Integer = 0
        Dim intGNType As Integer, intGeneratedNo As Integer = -1
        Dim bsX As BindingSource, blnFirstKeyField As Boolean = True

        bsX = CType(dgX.DataGridView.DataSource, BindingSource)
        If bsX.Count > 1 And dtX.PrimaryKey.Length > 1 Then   'Derive the generated number from existing rows
            Dim rowX As DataRowView
            For Each rowX In bsX
                If Not IsDBNull(rowX.Item(strGeneratedNoField)) AndAlso intGeneratedNo < rowX.Item(strGeneratedNoField) Then
                    intGeneratedNo = rowX.Item(strGeneratedNoField)
                End If
            Next
        End If

        If intGeneratedNo > -1 Then
            Return intGeneratedNo + 1
        Else
            strSQL = "select max(" + strGeneratedNoField + ")" & vbCrLf & "from maarten." + strTableName
            If dtX.PrimaryKey.Length > 1 Then
                intGNType = 2
                For x = 0 To dtX.PrimaryKey.GetLength(0) - 1
                    strGroupBy = "group by "
                    For y = 0 To dgX.Cells.Count - 1
                        If dgX.Cells(y).OwningColumn.DataPropertyName = dtX.PrimaryKey(x).ColumnName Then
                            If dtX.PrimaryKey(x).ColumnName = strGeneratedNoField Then

                            Else
                                If blnFirstKeyField Then
                                    strSQL += " where "
                                    blnFirstKeyField = False
                                Else
                                    strSQL += " and "
                                End If
                                If IsNumeric(dgX.Cells(y).Value.ToString) Then
                                    strSQL += dtX.PrimaryKey(x).ColumnName + " = " + dgX.Cells(y).Value.ToString
                                Else
                                    strSQL += dtX.PrimaryKey(x).ColumnName + " = '" + dgX.Cells(y).Value.ToString + "'"
                                End If
                            End If
                            'MsgBox("Key Field =" + dgX.Cells(y).Value.ToString)
                        End If

                    Next
                    'Me.DsInitialiseCase.V_frmCseNew.
                Next
            Else
                intGNType = 1
            End If


            'MsgBox(strSQL)

            Dim commSQL As New SqlClient.SqlCommand
            Dim datRead As SqlClient.SqlDataReader

            commSQL.Connection = clsGlobal.Connection

            If intGNType = 1 Then
                commSQL.CommandType = CommandType.StoredProcedure
                commSQL.CommandText = "maarten.OMNI_GET_SEQ_NO"

                Dim paramSQL As New SqlClient.SqlParameter("@Table_NM", SqlDbType.VarChar)
                '            paramSQL.Direction = ParameterDirection.ReturnValue
                paramSQL.Value = strTableName
                commSQL.Parameters.Add(paramSQL)

                datRead = commSQL.ExecuteReader

                If datRead.HasRows Then
                    While datRead.Read
                        If Not IsDBNull(datRead.Item(0)) Then
                            intGN = datRead.GetSqlInt32(0)
                        End If
                    End While
                Else
                    intGN = 1
                End If
                datRead.Close()
            Else
                commSQL.CommandText = strSQL
                datRead = commSQL.ExecuteReader

                If datRead.HasRows Then
                    While datRead.Read
                        If Not IsDBNull(datRead.Item(0)) Then
                            intGN = datRead.GetSqlInt32(0) + 1
                        End If
                    End While
                End If

                If intGN = 0 Then
                    intGN = 1
                End If
                datRead.Close()
            End If
        End If

        GetGeneratedNoGrid = intGN
    End Function

    Public Shared Function GetGeneratedNoGridCADS(ByRef strTableName As String, ByVal dtX As DataTable, ByVal dgX As DataGridViewRow, ByVal strGeneratedNoField As String) As Integer

        ' If this works then it should replace the GetGeneratedNoGrid routine. In the latter routine the assumption
        ' was made that all the records that contain the rest of the primary key are shown in the current grid. For
        ' Maintain opportunity, this is not true. Hence generating the number from the existing rows failed.

        Dim x As Integer
        Dim y As Integer
        Dim strSQL As String
        Dim strGroupBy As String
        Dim intGN As Integer = 0
        Dim intGNType As Integer, intGeneratedNo As Integer = -1
        Dim bsX As BindingSource, blnFirstKeyField As Boolean = True, intNumberOfMatchingFields As Integer

        bsX = CType(dgX.DataGridView.DataSource, BindingSource)
        If bsX.Count > 1 And dtX.PrimaryKey.Length > 1 Then   'Derive the generated number from existing rows
            Dim rowX As DataRowView
            For Each rowX In bsX
                If Not IsDBNull(rowX.Item(strGeneratedNoField)) AndAlso rowX.Item(strGeneratedNoField) > 0 _
                        AndAlso intGeneratedNo < rowX.Item(strGeneratedNoField) Then
                    ' Check that all records being checked have the same values for the rest of the fields in the primary key
                    intNumberOfMatchingFields = 0
                    For x = 0 To dtX.PrimaryKey.GetLength(0) - 1
                        If strGeneratedNoField <> dtX.PrimaryKey(x).ColumnName Then
                            For y = 0 To dgX.Cells.Count - 1
                                If dgX.Cells(y).OwningColumn.DataPropertyName = dtX.PrimaryKey(x).ColumnName AndAlso _
                                        dgX.Cells(y).Value = rowX.Item(dtX.PrimaryKey(x).ColumnName) Then
                                    intNumberOfMatchingFields += 1
                                End If
                            Next
                        End If
                    Next
                    If intNumberOfMatchingFields = dtX.PrimaryKey.GetLength(0) - 1 Then
                        intGeneratedNo = rowX.Item(strGeneratedNoField)
                    End If
                End If
            Next
        End If

        If intGeneratedNo > -1 Then
            Return intGeneratedNo + 1
        Else
            strSQL = "select max(" + strGeneratedNoField + ")" & vbCrLf & "from maarten." + strTableName
            If dtX.PrimaryKey.Length > 1 Then
                intGNType = 2
                For x = 0 To dtX.PrimaryKey.GetLength(0) - 1
                    strGroupBy = "group by "
                    For y = 0 To dgX.Cells.Count - 1
                        If dgX.Cells(y).OwningColumn.DataPropertyName = dtX.PrimaryKey(x).ColumnName Then
                            If dtX.PrimaryKey(x).ColumnName = strGeneratedNoField Then

                            Else
                                If blnFirstKeyField Then
                                    strSQL += " where "
                                    blnFirstKeyField = False
                                Else
                                    strSQL += " and "
                                End If
                                If IsNumeric(dgX.Cells(y).Value.ToString) Then
                                    strSQL += dtX.PrimaryKey(x).ColumnName + " = " + dgX.Cells(y).Value.ToString
                                Else
                                    strSQL += dtX.PrimaryKey(x).ColumnName + " = '" + dgX.Cells(y).Value.ToString + "'"
                                End If
                            End If
                            'MsgBox("Key Field =" + dgX.Cells(y).Value.ToString)
                        End If

                    Next
                    'Me.DsInitialiseCase.V_frmCseNew.
                Next
            Else
                intGNType = 1
            End If


            'MsgBox(strSQL)

            Dim commSQL As New SqlClient.SqlCommand
            Dim datRead As SqlClient.SqlDataReader

            commSQL.Connection = clsGlobal.Connection

            If intGNType = 1 Then
                commSQL.CommandType = CommandType.StoredProcedure
                commSQL.CommandText = "maarten.OMNI_GET_SEQ_NO"

                Dim paramSQL As New SqlClient.SqlParameter("@Table_NM", SqlDbType.VarChar)
                '            paramSQL.Direction = ParameterDirection.ReturnValue
                paramSQL.Value = strTableName
                commSQL.Parameters.Add(paramSQL)

                datRead = commSQL.ExecuteReader

                If datRead.HasRows Then
                    While datRead.Read
                        If Not IsDBNull(datRead.Item(0)) Then
                            intGN = datRead.GetSqlInt32(0)
                        End If
                    End While
                Else
                    intGN = 1
                End If
                datRead.Close()
            Else
                commSQL.CommandText = strSQL
                datRead = commSQL.ExecuteReader

                If datRead.HasRows Then
                    While datRead.Read
                        If Not IsDBNull(datRead.Item(0)) Then
                            intGN = datRead.GetSqlInt32(0) + 1
                        End If
                    End While
                End If

                If intGN = 0 Then
                    intGN = 1
                End If
                datRead.Close()
            End If
        End If

        Return intGN

    End Function

    Public Shared Function GetGeneratedNo(ByVal strTableName As String, ByVal dtX As DataTable, ByVal bsX As BindingSource, ByVal strGeneratedNoField As String) As Integer
        ' *********************************************************************
        ' Purpose: Function will return the next number in the sequence for a table that 
        ' is handled by the maarten.OMNI_GET_SEQ_NO stored procedure e.g. CSE
        ' *********************************************************************
        Dim intGN As Integer
        Dim commSQL As New SqlClient.SqlCommand
        Dim datRead As SqlClient.SqlDataReader '= Nothing
        Dim x As Int64, blnFirstField As Boolean = True
        Dim strSQL As String

        Try
            commSQL.Connection = clsGlobal.Connection

            strSQL = "select max(" + strGeneratedNoField + ")" & vbCrLf & "from maarten." + strTableName

            If dtX.PrimaryKey.Length > 1 Then
                For x = 0 To dtX.PrimaryKey.GetLength(0) - 1
                    'MsgBox(dtX.PrimaryKey(x).ColumnName)
                    If dtX.PrimaryKey(x).ColumnName <> strGeneratedNoField Then
                        If blnFirstField Then
                            blnFirstField = False
                            strSQL += " where "
                        Else
                            strSQL += " and "
                        End If
                        'MsgBox(bsX.Current(dtX.PrimaryKey(x).ColumnName).ToString)
                        If IsNumeric(bsX.Current(dtX.PrimaryKey(x).ColumnName).ToString) Then
                            strSQL += dtX.PrimaryKey(x).ColumnName + " = " + bsX.Current(dtX.PrimaryKey(x).ColumnName).ToString
                        Else
                            strSQL += dtX.PrimaryKey(x).ColumnName + " = '" + bsX.Current(dtX.PrimaryKey(x).ColumnName).ToString + "'"
                        End If
                    End If
                Next

                commSQL.CommandText = strSQL
                datRead = commSQL.ExecuteReader

                If datRead.HasRows Then
                    While datRead.Read
                        If Not IsDBNull(datRead.Item(0)) Then
                            intGN = datRead.GetSqlInt32(0) + 1
                        End If
                    End While
                End If

                If intGN = 0 Then
                    intGN = 1
                End If
                datRead.Close()
            Else
                'There is only one field in the primary key which means 
                'it will be stored in OMNI_GET_SEQ_NO
                commSQL.CommandType = CommandType.StoredProcedure
                commSQL.CommandText = "maarten.OMNI_GET_SEQ_NO"

                Dim paramSQL As New SqlClient.SqlParameter("@Table_NM", SqlDbType.VarChar)
                paramSQL.Direction = ParameterDirection.Input
                paramSQL.Value = strTableName
                commSQL.Parameters.Add(paramSQL)

                datRead = commSQL.ExecuteReader

                Do While datRead.Read
                    intGN = datRead.GetSqlInt32(0)
                Loop
                datRead.Close()

            End If

            GetGeneratedNo = intGN

        Catch ex As Exception
            'If Not datRead.IsClosed Then datRead.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information, "Get Generated No")
        End Try

    End Function

    Public Shared Function GetGeneratedNo(ByRef strTableName As String) As Integer
        ' *********************************************************************
        ' Purpose: Function will return the next number in the sequence for a table that 
        ' is handled by the maarten.OMNI_GET_SEQ_NO stored procedure e.g. CSE
        ' *********************************************************************
        Dim commSQL As New SqlClient.SqlCommand

        commSQL.Connection = clsGlobal.Connection

        'There is only one field in the primary key which means 
        'it will be stored in OMNI_GET_SEQ_NO
        commSQL.CommandType = CommandType.StoredProcedure
        commSQL.CommandText = "maarten.OMNI_GET_SEQ_NO"

        Dim paramSQL As New SqlClient.SqlParameter("@Table_NM", SqlDbType.VarChar)
        paramSQL.Direction = ParameterDirection.Input
        paramSQL.Value = strTableName
        commSQL.Parameters.Add(paramSQL)

        GetGeneratedNo = commSQL.ExecuteScalar

    End Function
    Public Shared Sub CopyToFormStrip(ByVal tbxX As TextBox, ByVal pnlX As Panel)
        If tbxX.Text <> "" Then
            If IsNumeric(tbxX.Text) Then
                If CType(tbxX.Text, Integer) > 0 Then
                    clsGlobal.ToolStripCaseId = CType(tbxX.Text, Integer)
                    Dim ctlX As Control
                    For Each ctlX In pnlX.Controls
                        If ctlX.Name = "cboNumber" Then
                            Dim intCounter As Integer

                            For intCounter = RecentCase.GetUpperBound(0) To 1 Step -1
                                If RecentCase(intCounter) = CStr(ToolStripCaseId) Then
                                    RecentCase(intCounter) = ""
                                End If
                            Next

                            For intCounter = clsGlobal.RecentCase.GetUpperBound(0) To 1 Step -1
                                clsGlobal.RecentCase.SetValue(clsGlobal.RecentCase.GetValue(intCounter - 1), intCounter)
                            Next
                            clsGlobal.RecentCase.SetValue(CStr(clsGlobal.ToolStripCaseId), 1)

                            CType(ctlX, ComboBox).Items.Clear()
                            CType(ctlX, ComboBox).Items.Add("")

                            For intCounter = 1 To clsGlobal.RecentCase.GetUpperBound(0)
                                If IsNumeric(clsGlobal.RecentCase(intCounter)) Then
                                    CType(ctlX, ComboBox).Items.Add(clsGlobal.RecentCase(intCounter))
                                End If
                            Next
                            CType(ctlX, ComboBox).SelectedIndex = 1
                        End If

                    Next
                End If
            End If
        End If
    End Sub
    Public Shared Sub CopyToFormStripFromGrid(ByVal intQueryNo As Integer, ByVal pnlX As Panel)
        If intQueryNo > 0 Then
            clsGlobal.ToolStripCaseId = intQueryNo
            Dim ctlX As Control
            For Each ctlX In pnlX.Controls
                If ctlX.Name = "cboNumber" Then
                    Dim intCounter As Integer

                    For intCounter = RecentCase.GetUpperBound(0) To 1 Step -1
                        If RecentCase(intCounter) = CStr(ToolStripCaseId) Then
                            RecentCase(intCounter) = ""
                        End If
                    Next

                    For intCounter = clsGlobal.RecentCase.GetUpperBound(0) To 1 Step -1
                        clsGlobal.RecentCase.SetValue(clsGlobal.RecentCase.GetValue(intCounter - 1), intCounter)
                    Next
                    clsGlobal.RecentCase.SetValue(CStr(clsGlobal.ToolStripCaseId), 1)

                    CType(ctlX, ComboBox).Items.Clear()
                    CType(ctlX, ComboBox).Items.Add("")

                    For intCounter = 1 To clsGlobal.RecentCase.GetUpperBound(0)
                        If IsNumeric(clsGlobal.RecentCase(intCounter)) Then
                            CType(ctlX, ComboBox).Items.Add(clsGlobal.RecentCase(intCounter))
                        End If
                    Next
                    CType(ctlX, ComboBox).SelectedIndex = 1
                End If

            Next
        End If
    End Sub
    Public Shared Sub SetCursorHand(ByVal frmX As Form, ByVal ctlX As Control)
        If ctlX.Text <> "" Then
            frmX.Cursor = Cursors.Hand
        End If
    End Sub
    Public Shared Sub SetCursorDefault(ByVal frmX As Form)
        frmX.Cursor = Cursors.Default
    End Sub

    'Public Shared Sub CreateLetterIcon(ByVal frmX As Form, ByVal pnlFormStrip As Panel)
    '    'pbxLetter
    '    Dim pbxLetter As New System.Windows.Forms.PictureBox
    '    'frmMainTabMenu.imlFormStrip.ImageSize = New System.Drawing.Point(33, 28)

    '    pbxLetter.Location = New System.Drawing.Point(477, 2)

    '    pbxLetter.Name = "pbxLetter"
    '    pbxLetter.Size = New System.Drawing.Size(40, 29)
    '    'pbxLetter.BorderStyle = BorderStyle.FixedSingle
    '    pbxLetter.TabIndex = 1
    '    pbxLetter.TabStop = False

    '    pnlFormStrip.Controls.Add(pbxLetter)
    '    pbxLetter.Image = frmMainTabMenu.imlFormStrip1.Images(0)
    '    AddHandler pbxLetter.Click, AddressOf ClickLetter

    'End Sub

    Public Shared Sub CreateLetter(ByVal frmX As Form, ByVal strCaseNo As String, ByVal strPackageId As String, _
                        ByVal strAddrAstType As String, ByVal strClientNo As String)

        'MsgBox(frmX.Name + " " + strCaseNo + " " + strPackageId + " " + strAddrAstType + " " + strBusinessNo)

    End Sub

    Public Shared Sub CreateLetter(ByVal frmX As Form, ByVal strCaseNo As String, ByVal strPackageId As String, _
                        ByVal strAddrAstType As String, ByVal strSupplierNo As String, _
                        ByVal strSupplierWorkRequest As String, ByVal strCapabilityNo As String)

        'MsgBox(frmX.Name + " " + strCaseNo + " " + strPackageId + " " + strAddrAstType + " " + strBusinessNo)

    End Sub

    Public Shared Function FindFocusedControl(ByVal frmX As Control) As Control
        Dim ctlX, ctlY As Control

        For Each ctlX In frmX.Controls
            If TypeOf (ctlX) Is TextBox Then
                If ctlX.Focused Then
                    Return ctlX
                End If
            ElseIf TypeOf (ctlX) Is Panel Then
                ctlY = FindFocusedControl(ctlX)
                If Not ctlY Is Nothing Then Return ctlY
            ElseIf TypeOf (ctlX) Is GroupBox Then
                ctlY = FindFocusedControl(ctlX)
                If Not ctlY Is Nothing Then Return ctlY
            ElseIf TypeOf (ctlX) Is DataGridView Then
                If ctlX.Focused Then
                    Return ctlX
                End If
            End If
        Next
        Return Nothing
    End Function

    Public Shared Sub ZoomClicked(ByVal frmX As Form)
        Dim frmNew As New frmZoom
        Dim ctlX As Control

        ctlX = FindFocusedControl(frmX)
        If Not ctlX Is Nothing Then
            frmNew.CallingForm(ctlX)
            frmNew.ShowDialog()
        End If
    End Sub

    Public Shared Sub ReadOnlyFields(ByVal blnFlag As Boolean, ByVal ctlContainer As Control)

        Dim ctlX As Control
        Dim strFieldNm As String = ""

        For Each ctlX In ctlContainer.Controls
            'If TypeOf (ctlX) Is Panel Then
            '    ReadOnlyFields(blnFlag, ctlX, )
            If TypeOf (ctlX) Is ControlLibrary.SuperComboBox Then
                CType(ctlX, ControlLibrary.SuperComboBox).ReadOnly = blnFlag
                SetBackColor(blnFlag, ctlX)
            ElseIf TypeOf (ctlX) Is TextBox Then
                CType(ctlX, TextBox).ReadOnly = blnFlag
                SetBackColor(blnFlag, ctlX)
                If Not blnFlag Then
                    Try
                        If ctlX.DataBindings.Count > 0 Then
                            ' This finds out the data type of the field bound to the control. If it is integer or double,
                            ' a handler is added to the parse event to ensure that only numeric data is added, a message
                            ' appears if there is a problem, and that the control can be cleared.
                            strFieldNm = ctlX.DataBindings.Item(0).BindingMemberInfo.BindingField
                            If strFieldNm <> "" AndAlso TypeOf CType(ctlX.DataBindings(0).DataSource, BindingSource).List Is DataView Then
                                Select Case CType(CType(ctlX.DataBindings(0).DataSource, BindingSource).List,  _
                                        DataView).Table.Columns(strFieldNm).DataType.ToString
                                    Case "System.Int32"
                                        RemoveHandler ctlX.DataBindings(0).Parse, AddressOf IntegerTextboxValidate
                                    Case "System.Double"
                                        RemoveHandler ctlX.DataBindings(0).Parse, AddressOf DoubleTextboxValidate
                                End Select
                            End If
                        End If

                    Catch ex As Exception
                    Finally
                        If ctlX.DataBindings.Count > 0 Then
                            If strFieldNm <> "" AndAlso TypeOf CType(ctlX.DataBindings(0).DataSource, BindingSource).List Is DataView Then
                                Select Case CType(CType(ctlX.DataBindings(0).DataSource, BindingSource).List,  _
                                        DataView).Table.Columns(strFieldNm).DataType.ToString
                                    Case "System.Int32"
                                        AddHandler ctlX.DataBindings(0).Parse, AddressOf IntegerTextboxValidate
                                    Case "System.Double"
                                        AddHandler ctlX.DataBindings(0).Parse, AddressOf DoubleTextboxValidate
                                End Select
                            End If
                        End If

                    End Try
                End If
            ElseIf TypeOf (ctlX) Is ComboBox Then
                CType(ctlX, ComboBox).Enabled = Not blnFlag
                SetBackColor(blnFlag, ctlX)
            ElseIf TypeOf (ctlX) Is ControlLibrary.SuperCheckBox Then
                CType(ctlX, ControlLibrary.SuperCheckBox).Enabled = Not blnFlag
                SetBackColor(blnFlag, ctlX)
            ElseIf TypeOf (ctlX) Is DataGridView Then
                CType(ctlX, DataGridView).ReadOnly = blnFlag
            ElseIf TypeOf (ctlX) Is MaskedTextBox Then
                CType(ctlX, MaskedTextBox).ReadOnly = blnFlag
                SetBackColor(blnFlag, ctlX)
                If Not blnFlag Then
                    ' A handler is added to the parse event to ensure that only a valid date/time is added, a message
                    ' appears if there is a problem, and that the control can be cleared.
                    Try
                        RemoveHandler ctlX.DataBindings(0).Parse, AddressOf MaskedTextboxValidate
                    Catch ex As Exception
                    Finally
                        AddHandler ctlX.DataBindings(0).Parse, AddressOf MaskedTextboxValidate
                    End Try
                End If
            End If
        Next
    End Sub
    Public Shared Sub SetBackColor(ByVal blnFlag As Boolean, ByVal ctlContainer As Control)
        If blnFlag Then

            ctlContainer.BackColor = Color.FromArgb(conPanelBackColor)
        Else
            ctlContainer.BackColor = Color.White
        End If
    End Sub

    Public Shared Function ExecuteSQL(ByVal strSQL As String, Optional ByVal conSQL As SqlClient.SqlConnection = Nothing) As Boolean
        Try
            Dim commSQL As New SqlClient.SqlCommand

            If conSQL Is Nothing Then conSQL = clsGlobal.Connection
            If conSQL.State = ConnectionState.Closed Then conSQL.Open()

            commSQL.CommandTimeout = 600

            commSQL.Connection = conSQL
            commSQL.CommandType = CommandType.Text
            commSQL.CommandText = strSQL
            commSQL.ExecuteNonQuery()
            ExecuteSQL = True

        Catch ex As Exception
            ExecuteSQL = False
            MsgBox(ex.Message, MsgBoxStyle.Information, "Function ExecuteSQL")
        End Try
    End Function



    Public Shared Function GetCompanyName(ByVal strToken As String) As String
        Try
            Dim strStrippedToken As String
            Dim commSQL As New SqlClient.SqlCommand
            Dim strCompanyName As String
            GetCompanyName = ""

            strStrippedToken = Mid(strToken, 2, Len(strToken) - 2)

            commSQL.Connection = clsGlobal.Connection
            commSQL.CommandType = CommandType.StoredProcedure
            commSQL.CommandText = "maarten.COMPANY_NAME_DETOKEN"

            commSQL.Parameters.Add("@TokenNm", SqlDbType.VarChar).Value = strStrippedToken

            strCompanyName = commSQL.ExecuteScalar()

            If strCompanyName <> "" Then
                GetCompanyName = strCompanyName
            Else
                GetCompanyName = strToken
            End If
        Catch ex As Exception
            GetCompanyName = ""
            MsgBox(ex.Message, MsgBoxStyle.Information, "GetCompanyName")
        End Try
    End Function

    Public Shared Function IsInRole(ByVal intUserId As Integer, ByVal strRoleCd As String) As String
        Try
            Dim commSQL As New SqlClient.SqlCommand

            commSQL.Connection = clsGlobal.Connection
            commSQL.CommandType = CommandType.StoredProcedure
            commSQL.CommandText = "maarten.OMNI_IS_IN_ROLE"

            commSQL.Parameters.Add("@UserId", SqlDbType.Int).Value = intUserId
            commSQL.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = strRoleCd

            IsInRole = commSQL.ExecuteScalar()

        Catch ex As Exception
            IsInRole = "N"
            MsgBox(ex.Message, MsgBoxStyle.Information, "IsInRole")
        End Try
    End Function

    Public Shared Function BillAddress(ByVal strAddress As String) As String
        Dim strHold As String
        strHold = strAddress

        If strHold = "" Then
            BillAddress = ""
        Else
            Do Until InStr(strHold, "!!") = 0
                strHold = Replace(strHold, "!!", "!")
            Loop

            Do Until InStr(strHold, "! !") = 0
                strHold = Replace(strHold, "! !", "!")
            Loop

            If Microsoft.VisualBasic.Left(strHold, 1) = "!" Then strHold = Microsoft.VisualBasic.Mid(strHold, 2)
            strHold = Replace(strHold, "!", vbCrLf)
            BillAddress = strHold
        End If
    End Function

    Public Shared Sub SpellCheck(ByVal txtX As Object, ByVal strCaption As String, Optional ByVal grdX As Object = Nothing)
        Try
            If txtX.Text.Length > 0 Then
                Dim objWord As New Word.Application()
                Dim oNothing As Object = Nothing
                Dim oIgnoreUpperCase As Object = False
                Dim oAlwaysSuggest As Object = True

                objWord.Visible = False
                Dim objTempDoc As Word.Document = objWord.Documents.Add
                Dim rng As Word.Range = objTempDoc.Range

                rng.Text = txtX.Text

                objTempDoc.Activate()

                objWord.WindowState = Word.WdWindowState.wdWindowStateNormal

                objTempDoc.CheckSpelling()

                txtX.Text = objTempDoc.Range().Text

                objTempDoc.Saved = True
                objTempDoc.Close()
                objWord.Quit()

                If TypeOf (txtX) Is TextBox Then
                    CType(txtX, TextBox).Focus()
                ElseIf TypeOf (txtX) Is DataGridView Then
                    CType(txtX, DataGridView).Focus()
                End If

                MessageBox.Show("The spell check is complete.", strCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "SpellCheck")
        End Try
        'Try
        '    If txtX.Text.Length > 0 Then
        '        Dim bSelected As Boolean
        '        Dim objWord As Object
        '        Dim objTempDoc As Object
        '        Dim objSelectable As Object

        '        If grdX Is Nothing Then   ' Textbox
        '            objSelectable = txtX
        '        Else                      ' Grid
        '            objSelectable = grdX
        '        End If

        '        objWord = New Word.Application()
        '        objTempDoc = objWord.Documents.Add
        '        objWord.Visible = False

        '        objWord.WindowState = 0
        '        objWord.Top = -3000

        '        If txtX.SelectionLength > 0 Then
        '            bSelected = True
        '            Clipboard.SetDataObject(txtX.SelectedText)
        '        Else
        '            Clipboard.SetDataObject(txtX.Text)
        '        End If

        '        With objTempDoc
        '            .Content.Paste()
        '            .Activate()
        '            .CheckSpelling()
        '            .Content.Copy()
        '            If Clipboard.ContainsData(DataFormats.Text) Then
        '                If bSelected Then
        '                    txtX.Text = Microsoft.VisualBasic.Left(txtX.Text, (txtX.SelectionStart)) & Clipboard.GetText(1) & _
        '                            Microsoft.VisualBasic.Right(txtX.Text, Len(txtX.Text) - (txtX.SelectionStart + txtX.SelectionLength))
        '                Else
        '                    txtX.Text = Clipboard.GetText(1)
        '                End If
        '            End If
        '            .Saved = True
        '            .Close()
        '        End With

        '        If TypeOf (txtX) Is TextBox Then
        '            CType(txtX, TextBox).Focus()
        '        ElseIf TypeOf (txtX) Is DataGridView Then
        '            CType(txtX, DataGridView).Focus()
        '        End If

        '        objWord.Quit()

        '        MessageBox.Show("The spell check is complete.", strCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End If

        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Information, "SpellCheck")
        'End Try

        'Try
        '    Dim objWord As Object, objSelectable As Object
        '    Dim bSelected As Boolean

        '    If grdX Is Nothing Then   ' Textbox
        '        objSelectable = txtX
        '    Else                      ' Grid
        '        objSelectable = grdX
        '    End If

        '    objWord = CreateObject("Word.Basic")
        '    objWord.FileNew()
        '    objWord.EditClear()

        '    ' spell check only the selected text
        '    If objSelectable.SelectedText <> "" Then
        '        bSelected = True
        '        objWord.Insert(objSelectable.SelectedText)
        '    Else 'spell check all text in the box.
        '        objWord.Insert(txtX.Text)
        '    End If

        '    objWord.StartOfDocument()
        '    objWord.EndOfDocument(1)
        '    objWord.ToolsSpelling()
        '    objWord.StartOfDocument()
        '    objWord.EndOfDocument(1)
        '    objWord.EditCut()

        '    With txtX
        '        If bSelected Then
        '            .Text = Left$(.Text, (objSelectable.SelectionStart - Len(Clipboard.GetText(1)))) & Clipboard.GetText(1) & _
        '                    Right$(.Text, Len(.Text) - (objSelectable.SelectionStart + objSelectable.SelectionLength))
        '        Else
        '            .Text = Clipboard.GetText(1)
        '        End If

        '        ' closes MS Word
        '        objWord.FileClose(2)
        '        If TypeOf (objSelectable) Is TextBox Then
        '            CType(objSelectable, TextBox).Focus()
        '        ElseIf TypeOf (objSelectable) Is DataGridView Then
        '            CType(objSelectable, DataGridView).Focus()
        '        End If
        '    End With

        '    objWord.FileQuit()
        '    objWord = Nothing
        '    Beep()

        '    MsgBox("The spell check is complete.", vbInformation, strCaption)

        '    objSelectable.Focus()
        '    'txtX.Text = txtX.Text

        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Information, "SpellCheck")
        'End Try
    End Sub


    Public Shared Property Database() As String
        Get
            Return mintDatabase
        End Get
        Set(ByVal value As String)
            mintDatabase = value
        End Set
    End Property
    Public Shared Property Server() As String
        Get
            Return mstrServer
        End Get
        Set(ByVal value As String)
            mstrServer = value
        End Set
    End Property
    Public Shared Property PersonNo() As Integer
        Get
            'Dim commSQL As New SqlClient.SqlCommand
            'Dim datRead As SqlClient.SqlDataReader
            'Dim strSQL As String

            'strSQL = "select GENERATED_NO from maarten.PERSON where END_DT is null and PERSON_USER_CD = substring(suser_sname(),charindex('\',suser_sname())+1,len(suser_sname()))"

            'commSQL.Connection = clsGlobal.Connection
            'commSQL.CommandType = CommandType.Text
            'commSQL.CommandText = strSQL

            'datRead = commSQL.ExecuteReader

            'While datRead.Read
            '    PersonNo = CType(datRead.Item("GENERATED_NO").ToString, Integer)
            'End While

            'datRead.Close()
            Return mintPersonNo
        End Get
        Set(ByVal value As Integer)
            mintPersonNo = value
        End Set
    End Property

    Public Shared Property PersonName()
        Get
            'Dim commSQL As New SqlClient.SqlCommand
            'Dim datRead As SqlClient.SqlDataReader
            'Dim strSQL As String
            'PersonName = ""

            'strSQL = "select PERSON_USER_CD from maarten.PERSON where PERSON_USER_CD = substring(suser_sname(),charindex('\',suser_sname())+1,len(suser_sname()))"

            'If clsGlobal.Connection.State = ConnectionState.Closed Then clsGlobal.Connection.Open()

            'commSQL.Connection = clsGlobal.Connection

            'commSQL.CommandType = CommandType.Text
            'commSQL.CommandText = strSQL

            'datRead = commSQL.ExecuteReader

            'While datRead.Read
            '    PersonName = datRead.Item("PERSON_USER_CD").ToString
            'End While

            'datRead.Close()
            Return mstrPersonName
        End Get
        Set(ByVal value)
            mstrPersonName = value
        End Set
    End Property

    Public Shared ReadOnly Property PersonFullName()
        Get
            Dim commSQL As New SqlClient.SqlCommand
            Dim datRead As SqlClient.SqlDataReader
            Dim strSQL As String
            PersonFullName = ""

            strSQL = "select FIRST_NM + ' ' + SURNAME_NM as 'FULL_NM' from maarten.PERSON where PERSON_USER_CD = substring(suser_sname(),charindex('\',suser_sname())+1,len(suser_sname()))"

            If clsGlobal.Connection.State = ConnectionState.Closed Then clsGlobal.Connection.Open()

            commSQL.Connection = clsGlobal.Connection

            commSQL.CommandType = CommandType.Text
            commSQL.CommandText = strSQL

            datRead = commSQL.ExecuteReader

            While datRead.Read
                PersonFullName = datRead.Item("FULL_NM").ToString
            End While

            datRead.Close()
        End Get
    End Property
    Public Shared ReadOnly Property HasManagerPrivilage()
        Get
            Dim commSQL As New SqlClient.SqlCommand
            Dim datRead As SqlClient.SqlDataReader
            'Dim strSQL As String
            HasManagerPrivilage = False

            'strSQL = "select PERSON_USER_CD from maarten.PERSON where PERSON_USER_CD = substring(suser_sname(),charindex('\',suser_sname())+1,len(suser_sname()))"

            If clsGlobal.Connection.State = ConnectionState.Closed Then clsGlobal.Connection.Open()

            commSQL.Connection = clsGlobal.Connection

            commSQL.CommandType = CommandType.StoredProcedure
            commSQL.CommandText = "maarten.OMNI_HAS_MANAGER_PRIVILAGE"

            datRead = commSQL.ExecuteReader

            If datRead.HasRows Then
                HasManagerPrivilage = True
            End If
            'While datRead.Read
            '    HasManagerPrivilage = datRead.Item("PERSON_USER_CD").ToString
            'End While

            datRead.Close()
        End Get
    End Property

    Public Shared Sub PrintForm(ByVal sender As Object, ByVal e As System.EventArgs)

        OutputToWord(sender.getcurrentparent.findform, "")

    End Sub
    Friend Shared Sub OutputToWord(ByVal frmX As Form, ByVal strFilePath As String)

        Dim fclCollection As New clnFormControl, intCompositeTabIndex As Int64 = 1
        Dim frcX As clsFormControl
        Try
            ' Get all form controls regardless of their parents
            FindControls(frmX.Controls, fclCollection, intCompositeTabIndex)
            ' Now find associated label for each control
            For Each frcX In fclCollection
                If frcX.ControlType = clsFormControl.enControlType.TextBox Or _
                        frcX.ControlType = clsFormControl.enControlType.ComboBox Or _
                        frcX.ControlType = clsFormControl.enControlType.CheckBox Then
                    fclCollection.FindTextForControl(frcX)
                End If
            Next

            Dim i, j, k As Integer, strTitle As String = "", srlX As New SortedList
            ' Get Title
            For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsFormControl)
                    If .Name = .Ancestor And .ControlType = clsFormControl.enControlType.Label Then
                        strTitle = CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsFormControl).Text
                        Exit For
                    End If
                End With
            Next

            If strTitle = "" Then strTitle = frmX.Text
            ' Create ordered list of ancestors, i.e. top-level panels
            For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsFormControl)
                    If .IsTitle And .Ancestor <> "pnlFormStrip" AndAlso srlX.Item(frmX.Controls(.Ancestor).Location.Y * 1000 + _
                                frmX.Controls(.Ancestor).Location.X) Is Nothing Then
                        srlX.Add(frmX.Controls(.Ancestor).Location.Y * 1000 + frmX.Controls(.Ancestor).Location.X, _
                                CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsFormControl))
                    End If
                End With
            Next

            For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsFormControl)
                    If (.ControlType = clsFormControl.enControlType.CheckBox Or _
                            .ControlType = clsFormControl.enControlType.ComboBox Or _
                            .ControlType = clsFormControl.enControlType.TextBox Or _
                            .ControlType = clsFormControl.enControlType.DataGridView) And .Ancestor <> "pnlFormStrip" AndAlso srlX.Item(frmX.Controls(.Ancestor).Location.Y * 1000 + _
                                frmX.Controls(.Ancestor).Location.X) IsNot Nothing Then
                        CType(srlX.Item(frmX.Controls(.Ancestor).Location.Y * 1000 + frmX.Controls(.Ancestor).Location.X),  _
                                    clsFormControl).ControlCount += 1
                    End If
                End With
            Next

            ' All required data (except grid contents) stored - now create word document
            Dim appWord As New Word.Application
            Dim newDoc As Word.Document, myRange As Word.Range

            frmX.Cursor = Cursors.WaitCursor
            newDoc = appWord.Documents.Add
            With newDoc
                .Content.Font.Name = "Arial"
                .PageSetup.LeftMargin = 50
                .PageSetup.RightMargin = 50
            End With

            With appWord.Selection
                .TypeText(strTitle)
                .TypeParagraph()
                .TypeParagraph()
            End With

            With newDoc.Sections(1)
                .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers.Add( _
                    PageNumberAlignment:=Word.WdPageNumberAlignment.wdAlignPageNumberCenter, FirstPage:=True)
                .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.InsertAfter(System.Environment.UserName & _
                        " " & Date.Now.ToLongTimeString)
                .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Font.Name = "Arial"
                .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Font.Size = 6
            End With

            myRange = newDoc.Range(newDoc.Paragraphs(1).Range.Start, _
                    newDoc.Paragraphs(1).Range.End)
            myRange.Font.Size = 14
            myRange.Bold = True

            newDoc.PageSetup.Orientation = GetSetting("OMNI", "Autoformat", "Orientation", 0)

            Dim otable, oGridTable As Word.Table, intPageWidthInPoints, intRowCount, intColCount As Integer

            intPageWidthInPoints = newDoc.PageSetup.PageWidth - newDoc.PageSetup.LeftMargin - newDoc.PageSetup.RightMargin

            For j = 0 To srlX.Count - 1
                myRange = newDoc.Range(newDoc.Paragraphs( _
                     newDoc.Paragraphs.Count).Range.Start, newDoc.Paragraphs( _
                     newDoc.Paragraphs.Count).Range.End)
                myRange.InsertBreak(Type:=Word.WdBreakType.wdSectionBreakContinuous)
                myRange.InsertParagraphAfter()
                ' Create a table 2 columns by reccount+1 rows
                otable = newDoc.Tables.Add( _
                    Range:=newDoc.Paragraphs(newDoc.Paragraphs.Count).Range, _
                    NumRows:=CType(srlX.GetByIndex(j), clsFormControl).ControlCount, NumColumns:=2)
                otable.Borders.Enable = False
                otable.Range.Font.Size = 10
                otable.Columns(1).Width = intPageWidthInPoints * 0.25
                otable.Columns(2).Width = intPageWidthInPoints * 0.75

                otable.Rows(1).Cells.Merge()
                otable.Rows(1).Cells(1).Range.InsertAfter(CType(srlX.GetByIndex(j), clsFormControl).Text)
                otable.Rows(1).Cells(1).Range.Font.Size = 12
                otable.Rows(1).Cells(1).Range.Bold = True
                intRowCount = 2

                For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                    With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsFormControl)
                        If .Ancestor = CType(srlX.GetByIndex(j), clsFormControl).Ancestor Then
                            Select Case .ControlType
                                Case clsFormControl.enControlType.CheckBox, clsFormControl.enControlType.ComboBox, _
                                    clsFormControl.enControlType.TextBox
                                    otable.Rows(intRowCount).Cells(1).Range.InsertAfter(.Text)
                                    otable.Rows(intRowCount).Cells(1).Range.Bold = True
                                    otable.Rows(intRowCount).Cells(2).Range.InsertAfter(.Value)

                                    intRowCount += 1
                                Case clsFormControl.enControlType.DataGridView
                                    otable.Rows(intRowCount).Cells.Merge()

                                    ' Get column nfo
                                    Dim colX As DataGridViewColumn, rowX As DataGridViewRow

                                    intColCount = 0
                                    For Each colX In .GridControl.Columns
                                        If colX.Visible And Not (TypeOf colX Is DataGridViewButtonColumn) Then
                                            intColCount += 1
                                        End If
                                    Next

                                    oGridTable = otable.Rows(intRowCount).Cells(1).Tables.Add( _
                                         otable.Rows(intRowCount).Cells(1).Range, _
                                         1, intColCount, Word.WdDefaultTableBehavior.wdWord9TableBehavior, _
                                         Word.WdAutoFitBehavior.wdAutoFitContent)

                                    k = 1
                                    For Each colX In .GridControl.Columns
                                        If colX.Visible And Not (TypeOf colX Is DataGridViewButtonColumn) Then
                                            oGridTable.Rows(1).Cells(k).Range.InsertAfter(colX.HeaderText)
                                            k += 1
                                        End If
                                    Next

                                    For Each rowX In .GridControl.Rows
                                        If .GridControl.NewRowIndex < 0 Or rowX.Index < .GridControl.NewRowIndex Then
                                            k = 1
                                            oGridTable.Rows.Add() ' Can't create table with more than one row - don't know why
                                            For Each colX In .GridControl.Columns
                                                If colX.Visible And Not (TypeOf colX Is DataGridViewButtonColumn) Then
                                                    If Not IsDBNull(rowX.Cells(colX.Name).Value) Then
                                                        If TypeOf colX Is DataGridViewImageColumn Then
                                                            If Not rowX.Cells(colX.Name).GetContentBounds(rowX.Index).IsEmpty Then
                                                                oGridTable.Rows(rowX.Index + 2).Cells(k).Range.InsertAfter("Y")
                                                            End If
                                                        Else
                                                            oGridTable.Rows(rowX.Index + 2).Cells(k).Range.InsertAfter( _
                                                                   rowX.Cells(colX.Name).FormattedValue)
                                                        End If
                                                    End If
                                                    k += 1
                                                End If
                                            Next
                                        End If
                                    Next

                                    oGridTable.AutoFormat(CType(GetSetting("OMNI", "Autoformat", "Type", 23), Integer))
                                    oGridTable.Range.Font.Size = 8
                                    oGridTable.Rows(1).HeadingFormat = True
                                    intRowCount += 1
                            End Select
                        End If
                    End With
                Next i
                otable.Range.MoveEnd()
                otable.Range.InsertParagraphAfter()
            Next j

            frmX.Cursor = Cursors.Default

            If strFilePath <> "" Then
                newDoc.SaveAs(strFilePath, Word.WdSaveFormat.wdFormatHTML, , , , , True)
                newDoc.Close()
                appWord.Quit(False)
                appWord = Nothing
            Else
                appWord.Visible = True
            End If

        Catch ex As Exception
            If frmX IsNot Nothing Then frmX.Cursor = Cursors.Default
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Output to Word")
        End Try

    End Sub
    Public Shared Sub ExportToExcel(ByVal sender As Object, ByVal e As System.EventArgs)

        OutputToExcel(sender.getcurrentparent.findform, "")

    End Sub
    Friend Shared Sub OutputToExcel(ByVal frmX As Form, ByVal strFilePath As String)

        Dim fclCollection As New clsLibraryInstance.clnFormControl
        Dim intCompositeTabIndex As Int64 = 1
        Dim frcX As clsLibraryInstance.clsFormControl

        frmX.Cursor = Cursors.WaitCursor

        Try
            ' Get all form controls regardless of their parents
            FindControls(frmX.Controls, fclCollection, intCompositeTabIndex)
            ' Now find associated label for each control
            For Each frcX In fclCollection
                If frcX.ControlType = clsLibraryInstance.clsFormControl.enControlType.TextBox Or _
                        frcX.ControlType = clsLibraryInstance.clsFormControl.enControlType.ComboBox Or _
                        frcX.ControlType = clsLibraryInstance.clsFormControl.enControlType.CheckBox Then
                    fclCollection.FindTextForControl(frcX)
                End If
            Next

            Dim i, j, k As Integer
            Dim strTitle As String = ""
            Dim srlX As New SortedList

            ' Get Title
            For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsLibraryInstance.clsFormControl)
                    If .Name = .Ancestor And .ControlType = clsLibraryInstance.clsFormControl.enControlType.Label Then
                        strTitle = CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsLibraryInstance.clsFormControl).Text
                        Exit For
                    End If
                End With
            Next

            If strTitle = "" Then strTitle = frmX.Text
            ' Create ordered list of ancestors, i.e. top-level panels
            For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsLibraryInstance.clsFormControl)

                    If .IsTitle And .Ancestor <> "pnlFormStrip" AndAlso srlX.Item(frmX.Controls(.Ancestor).Location.Y * 1000 + _
                                frmX.Controls(.Ancestor).Location.X) Is Nothing Then
                        srlX.Add(frmX.Controls(.Ancestor).Location.Y * 1000 + frmX.Controls(.Ancestor).Location.X, _
                                CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsLibraryInstance.clsFormControl))
                    End If
                End With
            Next

            For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsLibraryInstance.clsFormControl)
                    If (.ControlType = clsLibraryInstance.clsFormControl.enControlType.CheckBox Or _
                            .ControlType = clsLibraryInstance.clsFormControl.enControlType.ComboBox Or _
                            .ControlType = clsLibraryInstance.clsFormControl.enControlType.TextBox Or _
                            .ControlType = clsLibraryInstance.clsFormControl.enControlType.DataGridView) And .Ancestor <> "pnlFormStrip" AndAlso srlX.Item(frmX.Controls(.Ancestor).Location.Y * 1000 + _
                                frmX.Controls(.Ancestor).Location.X) IsNot Nothing Then
                        CType(srlX.Item(frmX.Controls(.Ancestor).Location.Y * 1000 + frmX.Controls(.Ancestor).Location.X),  _
                                    clsLibraryInstance.clsFormControl).ControlCount += 1
                    End If
                End With
            Next

            Dim excelApp As New Excel.Application()
            Dim excelBook As Excel.Workbook = excelApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)

            excelApp.Visible = True

            With excelWorksheet
                Dim intRowCount As Integer = 1

                For j = 0 To srlX.Count - 1
                    excelWorksheet.Cells(intRowCount, 1).value = CType(srlX.GetByIndex(j), clsLibraryInstance.clsFormControl).Text
                    excelWorksheet.Cells(intRowCount, 1).Font.Size = 12
                    excelWorksheet.Cells(intRowCount, 1).Font.Bold = True
                    intRowCount += 1

                    k = 1
                    For i = 0 To fclCollection.ControlsByTabIndex.Count - 1
                        With CType(fclCollection.ControlsByTabIndex.GetByIndex(i), clsLibraryInstance.clsFormControl)
                            If .Ancestor = CType(srlX.GetByIndex(j), clsLibraryInstance.clsFormControl).Ancestor Then
                                Select Case .ControlType
                                    Case clsLibraryInstance.clsFormControl.enControlType.CheckBox, clsLibraryInstance.clsFormControl.enControlType.ComboBox, _
                                        clsLibraryInstance.clsFormControl.enControlType.TextBox

                                        excelWorksheet.Cells(intRowCount, k).Font.Bold = True
                                        excelWorksheet.Cells(intRowCount, k).value = .Text
                                        excelWorksheet.Cells(intRowCount, k + 1).value = .Value

                                        If Microsoft.VisualBasic.Len(.Value).ToString > 8 Then
                                            Dim h As Integer = Microsoft.VisualBasic.Int(Microsoft.VisualBasic.Len(.Value) / 8)
                                            excelWorksheet.Range(excelWorksheet.Cells(intRowCount, k + 1), excelWorksheet.Cells(intRowCount, k + 1 + h)).Merge()
                                        End If

                                        If .Text.GetType.ToString = "System.Double" Then
                                            excelWorksheet.Cells(intRowCount, k).NumberFormat = "#0.00"
                                        End If

                                        intRowCount += 1
                                    Case clsLibraryInstance.clsFormControl.enControlType.DataGridView
                                        Dim colX As DataGridViewColumn
                                        Dim rowX As DataGridViewRow

                                        k = 1
                                        For Each colX In .GridControl.Columns
                                            If colX.Visible And Not (TypeOf colX Is DataGridViewButtonColumn) Then
                                                excelWorksheet.Cells(intRowCount, k).value = colX.HeaderText
                                                excelWorksheet.Rows(intRowCount).font.bold = True
                                                k += 1
                                            End If
                                        Next
                                        intRowCount += 1
                                        For Each rowX In .GridControl.Rows
                                            If .GridControl.NewRowIndex < 0 Or rowX.Index < .GridControl.NewRowIndex Then
                                                k = 1
                                                For Each colX In .GridControl.Columns
                                                    If colX.Visible And Not (TypeOf colX Is DataGridViewButtonColumn) Then
                                                        If Not IsDBNull(rowX.Cells(colX.Name).Value) Then
                                                            excelWorksheet.Cells(intRowCount, k).value = rowX.Cells(colX.Name).Value
                                                            If rowX.Cells(colX.Name).ValueType.ToString = "System.Double" Then
                                                                excelWorksheet.Cells(intRowCount, k).NumberFormat = "#0.00"
                                                            End If
                                                        End If
                                                        k += 1
                                                    End If
                                                Next
                                            End If
                                            intRowCount += 1
                                        Next
                                End Select
                            End If
                        End With
                    Next i
                    intRowCount += 1
                Next j

            End With

            excelWorksheet.Columns.AutoFit()

            frmX.Cursor = Cursors.Default

        Catch ex As Exception
            If frmX IsNot Nothing Then frmX.Cursor = Cursors.Default
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Output to Excel")
        End Try

    End Sub
    Friend Shared Sub FindControls(ByVal ctlCollection As Control.ControlCollection, ByVal fclCollection As clnFormControl, ByRef intCompositeTabIndex As Int64)
        Dim ctlX As Control

        intCompositeTabIndex *= 1000
        For Each ctlX In ctlCollection
            If ctlX.Visible Then
                If TypeOf ctlX Is Panel Then
                    intCompositeTabIndex += 1000
                    FindControls(ctlX.Controls, fclCollection, intCompositeTabIndex)
                ElseIf TypeOf ctlX Is ControlLibrary.SuperTextBox Or TypeOf ctlX Is TextBox Or TypeOf ctlX Is MaskedTextBox Then
                    Dim frcX As New clsFormControl ', bndX As Binding
                    frcX.ControlType = clsFormControl.enControlType.TextBox
                    frcX.Name = ctlX.Name
                    frcX.Value = ctlX.Text
                    frcX.Size = ctlX.Size
                    frcX.Location = ctlX.Location
                    frcX.Parent = ctlX.Parent.Name
                    frcX.Ancestor = GetAncestor(ctlX).Name
                    frcX.TabIndex = intCompositeTabIndex + ctlX.TabIndex * 10
                    fclCollection.Add(frcX)
                ElseIf TypeOf ctlX Is LinkLabel AndAlso CType(ctlX, LinkLabel).LinkColor = Color.White Then

                    Dim frcX As New clsFormControl
                    frcX.ControlType = clsFormControl.enControlType.LinkLabel
                    frcX.IsTitle = True
                    frcX.Name = ctlX.Name
                    frcX.Text = ctlX.Text
                    frcX.Size = ctlX.Size
                    frcX.Location = ctlX.Location
                    frcX.Parent = ctlX.Parent.Name
                    frcX.Ancestor = GetAncestor(ctlX).Name
                    frcX.TabIndex = intCompositeTabIndex + ctlX.TabIndex * 10
                    fclCollection.Add(frcX)
                ElseIf TypeOf ctlX Is ControlLibrary.SuperComboBox Or TypeOf ctlX Is ComboBox Then
                    Dim frcX As New clsFormControl
                    frcX.ControlType = clsFormControl.enControlType.ComboBox
                    frcX.Name = ctlX.Name
                    If CType(ctlX, ComboBox).SelectedIndex > -1 Then
                        frcX.Value = CType(ctlX, ComboBox).Text
                    Else
                        frcX.Value = ""
                    End If
                    frcX.Size = ctlX.Size
                    frcX.Location = ctlX.Location
                    frcX.Parent = ctlX.Parent.Name
                    frcX.Ancestor = GetAncestor(ctlX).Name
                    frcX.TabIndex = intCompositeTabIndex + ctlX.TabIndex * 10
                    fclCollection.Add(frcX)
                ElseIf TypeOf ctlX Is ControlLibrary.SuperCheckBox Then
                    Dim frcX As New clsFormControl
                    frcX.ControlType = clsFormControl.enControlType.CheckBox
                    frcX.Name = ctlX.Name
                    frcX.Value = IIf(CType(ctlX, CheckBox).Checked, "Yes", "No")
                    frcX.Size = ctlX.Size
                    frcX.Location = ctlX.Location
                    frcX.Parent = ctlX.Parent.Name
                    frcX.Ancestor = GetAncestor(ctlX).Name
                    frcX.TabIndex = intCompositeTabIndex + ctlX.TabIndex * 10
                    fclCollection.Add(frcX)
                ElseIf TypeOf ctlX Is ControlLibrary.SuperLabel Or TypeOf ctlX Is Label Then
                    Dim frcX As New clsFormControl
                    frcX.ControlType = clsFormControl.enControlType.Label
                    frcX.Name = ctlX.Name
                    frcX.Text = ctlX.Text
                    frcX.Size = ctlX.Size
                    frcX.Location = ctlX.Location
                    frcX.Parent = ctlX.Parent.Name
                    frcX.Ancestor = GetAncestor(ctlX).Name
                    frcX.TabIndex = intCompositeTabIndex + ctlX.TabIndex * 10
                    frcX.IsTitle = (ctlX.ForeColor = Color.White)
                    fclCollection.Add(frcX)
                ElseIf TypeOf ctlX Is DataGridView Then
                    Dim frcX As New clsFormControl
                    frcX.ControlType = clsFormControl.enControlType.DataGridView
                    frcX.Name = ctlX.Name
                    frcX.Size = ctlX.Size
                    frcX.Location = ctlX.Location
                    frcX.Parent = ctlX.Parent.Name
                    frcX.Ancestor = GetAncestor(ctlX).Name
                    frcX.GridControl = CType(ctlX, DataGridView)
                    frcX.TabIndex = intCompositeTabIndex + ctlX.TabIndex * 10
                    fclCollection.Add(frcX)
                End If
            End If
        Next
        intCompositeTabIndex /= 1000
    End Sub

    Private Shared Function GetAncestor(ByVal ctlX As Control) As Control
        Dim ctlAncestor As Control
        ctlAncestor = ctlX
        Do Until Not (TypeOf ctlAncestor.Parent Is Panel)
            ctlAncestor = ctlAncestor.Parent
        Loop
        Return ctlAncestor
    End Function

    Friend Shared Sub MaskedTextboxValidate(ByVal sender As Object, ByVal e As ConvertEventArgs)
        Dim txbMaskedTextBox As MaskedTextBox = Nothing
        Try
            txbMaskedTextBox = CType(CType(sender, Binding).Control, MaskedTextBox)
            DateTime.Parse(txbMaskedTextBox.Text)
        Catch ex As FormatException
            If txbMaskedTextBox.Text <> "  /  /" And txbMaskedTextBox.Text <> "  :" Then
                MsgBox("Date/time entered is not valid. Reverting to previous entry. ", MsgBoxStyle.Exclamation, "Date/time Incorrect")
                Dim rowX As Object
                rowX = CType(txbMaskedTextBox.DataBindings.Item(0).DataSource, BindingSource).Current
                e.Value = rowX(txbMaskedTextBox.DataBindings.Item(0).BindingMemberInfo.BindingField)
            Else
                e.Value = DBNull.Value
            End If
        End Try
    End Sub

    Friend Shared Sub IntegerTextboxValidate(ByVal sender As Object, ByVal e As ConvertEventArgs)
        Dim txbTextBox As TextBox = Nothing
        Try
            txbTextBox = CType(CType(sender, Binding).Control, TextBox)
            Integer.Parse(txbTextBox.Text)
        Catch ex As FormatException
            If txbTextBox.Text <> "" Then
                MsgBox("Numeric field entry entered is not valid. Reverting to previous entry. ", _
                        MsgBoxStyle.Exclamation, "Value Incorrect")
                Dim rowX As DataRowView
                rowX = CType(txbTextBox.DataBindings.Item(0).DataSource, BindingSource).Current
                If rowX IsNot Nothing Then e.Value = rowX(txbTextBox.DataBindings.Item(0).BindingMemberInfo.BindingField)
            Else
                e.Value = DBNull.Value
            End If
        End Try
    End Sub

    Friend Shared Sub DoubleTextboxValidate(ByVal sender As Object, ByVal e As ConvertEventArgs)
        Dim txbTextBox As TextBox = Nothing
        Try
            txbTextBox = CType(CType(sender, Binding).Control, TextBox)
            Double.Parse(txbTextBox.Text)
        Catch ex As FormatException
            If txbTextBox.Text <> "" Then
                MsgBox("Numeric field entry entered is not valid. Reverting to previous entry. ", _
                        MsgBoxStyle.Exclamation, "Value Incorrect")
                Dim rowX As DataRowView
                rowX = CType(txbTextBox.DataBindings.Item(0).DataSource, BindingSource).Current
                If rowX IsNot Nothing Then e.Value = rowX(txbTextBox.DataBindings.Item(0).BindingMemberInfo.BindingField)
            Else
                e.Value = DBNull.Value
            End If
        End Try
    End Sub

    Public Class clsFormProperties
        Public blnFormReadOnly As Boolean
        Public blnBenchmarkOn As Boolean
    End Class

    Public Shared Function SetupForm1(ByVal frmX As Form, ByVal blnNoQuery As Boolean, _
            Optional ByVal blnReadOnly As Boolean = False) As clsFormProperties

        Dim mclsFormProperties As New clsFormProperties

        frmX.Icon = My.Resources.Atlas___UK

        CreateRightToolStripItems(frmX)

        frmX.BackColor = Color.FromArgb(FormBackColor)

        CurrentForm = frmX

        Dim viewHeader As New DataView(FormView.Tables(0))
        Dim rowHeaderView As DataRowView
        Dim strFormAccess As String = "ACRO"
        Dim strBenchmarkOn As String = Nothing

        viewHeader.RowFilter = " OMF_FORM_NM_OMF = '" + frmX.GetType.Name + "'"
        For Each rowHeaderView In viewHeader
            strFormAccess = rowHeaderView("ACCESS_CD").ToString()
            strBenchmarkOn = rowHeaderView("BENCHMARK_ON_IN").ToString()
        Next

        Dim commSQL As New SqlClient.SqlCommand
        commSQL.Connection = clsGlobal.Connection
        commSQL.CommandType = CommandType.StoredProcedure
        commSQL.CommandText = "maarten.OMNI_INSERT_WINDOW_AUDIT"
        commSQL.Parameters.Add("@FormNm", SqlDbType.VarChar, 300).Value = frmX.Name
        commSQL.Parameters.Add("@EventCd", SqlDbType.VarChar, 4).Value = "OP"
        commSQL.Parameters.Add("@ApplicationNm", SqlDbType.VarChar, 10).Value = "Atlas - UK"

        commSQL.ExecuteNonQuery()
        commSQL.Dispose()

        If strBenchmarkOn = "Y" Then
            mclsFormProperties.blnBenchmarkOn = True
        Else
            mclsFormProperties.blnBenchmarkOn = False
        End If

        If strFormAccess = "ACRW" Then
            mclsFormProperties.blnFormReadOnly = False
        Else
            mclsFormProperties.blnFormReadOnly = True
        End If

        CreateMainToolStripItems(frmX, False, blnNoQuery, blnReadOnly Or mclsFormProperties.blnFormReadOnly)



        'For Each ctl In frmX.Controls
        '    If TypeOf (ctl) Is Panel Then
        '        If ctl.name = "pnlMainToolStrip" Then
        '            Dim lblX As New Label
        '            lblX.Size = New Size(100, 20)
        '            lblX.Location = New Size(10, 10)
        '            lblX.ForeColor = Color.White
        '            lblX.Text = FormatNumber((ci.TotalPhysicalMemory - ci.AvailablePhysicalMemory) / 1024, 0, , , True)
        '            ctl.Controls.Add(lblX)
        '        End If

        '    End If

        'Next

        Return mclsFormProperties
    End Function
    Public Shared Function SetupForm(ByVal frmX As Form, ByVal blnNoQuery As Boolean, _
            Optional ByVal blnReadOnly As Boolean = False) As Boolean

        frmX.Icon = My.Resources.Atlas___UK

        CreateRightToolStripItems(frmX)

        frmX.BackColor = Color.FromArgb(FormBackColor)

        CurrentForm = frmX

        Dim viewHeader As New DataView(FormView.Tables(0))
        Dim rowHeaderView As DataRowView
        Dim strFormAccess As String = "ACRO"
        'Dim blnBenchmarkOn As Boolean

        viewHeader.RowFilter = " OMF_FORM_NM_OMF = '" + frmX.GetType.Name + "'"
        For Each rowHeaderView In viewHeader
            strFormAccess = rowHeaderView("ACCESS_CD").ToString()
            'blnBenchmarkOn = rowHeaderView("BENCHMARK_ON_IN").ToString()
        Next

        Dim commSQL As New SqlClient.SqlCommand
        commSQL.Connection = clsGlobal.Connection
        commSQL.CommandType = CommandType.StoredProcedure
        commSQL.CommandText = "maarten.OMNI_INSERT_WINDOW_AUDIT"
        commSQL.Parameters.Add("@FormNm", SqlDbType.VarChar, 300).Value = frmX.Name
        commSQL.Parameters.Add("@EventCd", SqlDbType.VarChar, 4).Value = "OP"
        commSQL.Parameters.Add("@ApplicationNm", SqlDbType.VarChar, 10).Value = "Atlas - UK"

        commSQL.ExecuteNonQuery()

        If strFormAccess = "ACRW" Then
            SetupForm = False
        Else
            SetupForm = True
        End If

        CreateMainToolStripItems(frmX, False, blnNoQuery, blnReadOnly Or SetupForm)

    End Function
    Public Shared Function UserCanOpenForm(ByVal strFormName As String) As Boolean

        Dim viewHeader As New DataView(FormView.Tables(0))
        Dim rowHeaderView As DataRowView
        Dim strFormAccess As String = "NOAC"

        viewHeader.RowFilter = " MENU_NM = '" + strFormName + "'"
        For Each rowHeaderView In viewHeader
            strFormAccess = rowHeaderView("ACCESS_CD").ToString()
        Next

        If strFormAccess = "NOAC" Then MsgBox("You do not have sufficient rights to open this form.", MsgBoxStyle.Exclamation, "Cannot Open Form")
        Return (strFormAccess <> "NOAC")

    End Function
    'Public Shared Sub OpenAddressSearch()
    '    Dim psInfo As New System.Diagnostics.ProcessStartInfo("C:\omni2\qas\pro.301\Qapro3.exe")

    '    psInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized

    '    psInfo.WorkingDirectory = "C:\omni2\qas\pro.301"

    '    Dim myProcess As Process = System.Diagnostics.Process.Start(psInfo)
    'End Sub
    Public Shared Sub BuildMaskedDateCell(ByVal dgvX As DataGridView, ByVal intColNo As Integer, _
        ByVal strBoundColNm As String, ByVal strHeaderText As String, ByVal blnShowDatePickerButton As Boolean, _
                Optional ByVal blnImplementCellClick As Boolean = True)
        Dim col As New MaskedDateColumn, intCol As Integer, btnX As New DataGridViewButtonColumn()
        intCol = dgvX.Columns.Add(col)

        dgvX.Columns(intCol).DataPropertyName = strBoundColNm

        dgvX.Columns(intCol).Name = strBoundColNm

        dgvX.Columns(intCol).Width = 70

        dgvX.Columns(intCol).HeaderText = strHeaderText
        dgvX.Columns(intCol).DisplayIndex = intColNo
        If blnShowDatePickerButton Then
            btnX.FlatStyle = FlatStyle.Standard
            btnX.Text = "..."
            btnX.UseColumnTextForButtonValue = True
            intCol = dgvX.Columns.Add(btnX)
            dgvX.Columns(intCol).Name = "btnDatePicker" & (intColNo + 1).ToString
            dgvX.Columns(intCol).ValueType = GetType(Date)
            dgvX.Columns(intCol).Width = 20
            dgvX.Columns(intCol).ReadOnly = True
            dgvX.Columns(intCol).HeaderText = ""
            dgvX.Columns(intCol).DisplayIndex = intColNo + 1
            If blnImplementCellClick Then AddHandler dgvX.CellClick, AddressOf dgvX_CellClick
            AddHandler dgvX.CellMouseMove, AddressOf dgvX_CellMouseMove
            AddHandler dgvX.CellMouseLeave, AddressOf dgvX_CellMouseLeave
        End If
    End Sub

    Public Shared Sub dgvX_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        If e.ColumnIndex > -1 Then
            If e.RowIndex > -1 AndAlso InStr(sender.Columns(e.ColumnIndex).Name, "btnDatePicker", CompareMethod.Text) > 0 Then
                If Not sender.Columns(e.ColumnIndex - 1).ReadOnly Then
                    Dim frmX As New frmDatePicker
                    frmX.CallingForm(sender.currentrow, e.ColumnIndex - 1)
                    frmX.ShowDialog()
                End If
            End If
        End If
    End Sub

    Public Shared Sub dgvX_CellMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs)
        If e.RowIndex > -1 And e.ColumnIndex > -1 AndAlso InStr(sender.Columns(e.ColumnIndex).Name, "btnDatePicker", CompareMethod.Text) > 0 Then
            sender.cursor = Cursors.Hand
        End If
    End Sub

    Public Shared Sub dgvX_CellMouseLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        If e.RowIndex > -1 And e.ColumnIndex > -1 AndAlso InStr(sender.Columns(e.ColumnIndex).Name, "btnDatePicker", CompareMethod.Text) > 0 Then
            sender.cursor = Cursors.Default
        End If
    End Sub
    Public Shared Function GetBatchNumber()

        If mintBatchNo = 0 Then
            Dim cmdX As SqlClient.SqlCommand
            cmdX = New SqlClient.SqlCommand("maarten.BILL_GET_BATCH_NO", clsGlobal.Connection)
            cmdX.CommandType = CommandType.StoredProcedure
            mintBatchNo = cmdX.ExecuteScalar
        End If

        GetBatchNumber = mintBatchNo

    End Function
    Public Shared Sub SendFormByEmail(ByVal frmX As Form, ByVal strTitle As String)

        Dim strBody As String = "", strFilename As String
        Dim smrX As StreamReader

        strFilename = "c:\text.rtf"
        OutputToWord(frmX, strFilename)

        smrX = File.OpenText(strFilename)
        strBody = smrX.ReadToEnd
        smrX.Close()

        File.Delete(strFilename)

        Dim mailOutlook As Outlook.MailItem
        Dim appOutlook As New Outlook.Application()
        mailOutlook = appOutlook.CreateItem(Outlook.OlItemType.olMailItem)
        mailOutlook.Subject = strTitle
        mailOutlook.BodyFormat = Outlook.OlBodyFormat.olFormatHTML
        mailOutlook.HTMLBody = strBody
        mailOutlook.Display()

    End Sub
    Public Shared Function SendEmail(ByVal EmailTypeCd As String, ByVal strTitle As String, _
            ByVal strRecipient As String, ByVal strBody As String, _
            ByVal strAttachmentFilename As String, _
            ByVal blnShowMessage As Boolean, ByVal strSensitivity As String, _
            ByVal strImportance As String, ByVal blnDeliveryReceipt As Boolean, _
            ByVal blnReadReceipt As Boolean, ByVal strLetterLocation As String) As Boolean

        Dim intPos As Integer = 1, intEndPos As Integer = 1, i As Integer = 0
        Dim cmdX As SqlClient.SqlCommand, strEmailAddress As String
        Dim atcX As Attachment

        Try
            Dim strCidCd(1, -1) As String
            Dim lnrX As LinkedResource
            Dim av1 As AlternateView

            cmdX = New SqlClient.SqlCommand("exec maarten.OMNI_GET_EMAIL_ADDRESS", clsGlobal.Connection)
            strEmailAddress = cmdX.ExecuteScalar

            Dim mlmX As New MailMessage(strEmailAddress, strRecipient)
            mlmX.Subject = strTitle

            Select Case strImportance
                Case "Low"
                    mlmX.Priority = MailPriority.Low
                Case "Medium"
                    mlmX.Priority = MailPriority.Normal
                Case "High"
                    mlmX.Priority = MailPriority.High
            End Select

            If blnDeliveryReceipt Then
                mlmX.Headers.Add("Return-Receipt-To", strEmailAddress)
            End If

            If blnReadReceipt Then
                mlmX.Headers.Add("Disposition-Notification-To", strEmailAddress)
            End If

            mlmX.Headers.Add("Sensitivity", strSensitivity)

            Select Case EmailTypeCd
                Case "HTML"
                    Do While InStr(intPos, strBody, "src=""", CompareMethod.Text) > 0
                        intPos = InStr(intPos, strBody, "src=""", CompareMethod.Text)
                        If Mid(strBody, intPos - 10, 10) = "imagedata " Then
                            intPos += 10
                        Else
                            intEndPos = InStr(intPos + 5, strBody, """")
                            ReDim Preserve strCidCd(1, i)
                            strCidCd(0, i) = strLetterLocation & Mid(strBody, intPos + 5, intEndPos - (intPos + 5))
                            strCidCd(1, i) = "image" & i
                            strBody = Microsoft.VisualBasic.Left(strBody, intPos + 4) & _
                                    "cid:" & strCidCd(1, i) & Mid(strBody, intEndPos)
                            intPos = intEndPos
                            i += 1
                        End If
                    Loop
                    av1 = AlternateView.CreateAlternateViewFromString(strBody, Nothing, MediaTypeNames.Text.Html)
                    If strCidCd.GetUpperBound(1) >= 0 Then
                        For i = 0 To strCidCd.GetUpperBound(1)
                            lnrX = New LinkedResource(strCidCd(0, i))
                            lnrX.ContentId = strCidCd(1, i)
                            lnrX.ContentType.Name = strCidCd(0, i)
                            av1.LinkedResources.Add(lnrX)
                        Next
                    End If
                    mlmX.AlternateViews.Add(av1)
                    mlmX.IsBodyHtml = True
                Case "TEXT"
                    mlmX.Body = strBody
                    mlmX.IsBodyHtml = False
                Case "ATCH"
                    mlmX.Body = strBody
                    atcX = New Attachment(strAttachmentFilename)
                    mlmX.Attachments.Add(atcX)
            End Select

            'Dim mailSender As New SmtpClient("44exmpr001")
            'Dim mailSender As New SmtpClient("01excpr001")
            Dim mailSender As New SmtpClient("smtp.cartus.com")
            ' Following line needed so that mail server can accept you are a member of the domain
            ' and can therefore relay emails to external sources.
            mailSender.Credentials = CredentialCache.DefaultNetworkCredentials

            mailSender.Send(mlmX)

            SendEmail = True

        Catch ex As Exception
            MsgBox("Emailing has been abandoned: " & ex.Message, _
                   MsgBoxStyle.Exclamation, "Process aborted")
            SendEmail = False
        End Try
    End Function
    Public Shared Sub SetFormProperties(ByVal frmX As Form, ByVal frmCallingForm As Form)
        SubMenuPackageId = 0
        SubMenuOrgNo = 0
        SubMenuCaseId = 0
        frmX.MdiParent = frmCallingForm.MdiParent
        frmX.Location = New Point(mintFormLocationX, mintFormLocationY)
        frmX.FormBorderStyle = FormBorderStyle.None
        clsGlobal.CurrentForm = frmX
        clsGlobal.CurrentNonModalForm = frmX
        frmX.Show()
    End Sub
    Public Shared Sub SetFormProperties(ByVal frmX As Form)
        frmX.StartPosition = FormStartPosition.CenterScreen
        frmX.FormBorderStyle = FormBorderStyle.FixedSingle
        frmX.MinimizeBox = False
        frmX.MaximizeBox = False
        frmX.ShowDialog()
    End Sub
    'Public Shared Function StartMessagingAndLogon() As MAPI.Session
    '    Dim sDefaultUserProfile As String
    '    Dim objSession As New MAPI.Session

    '    Try
    '        'Try to logon.  If it fails, the most likely reason is that you do
    '        'not have an open session.  Error -2147221231  MAPI_E_LOGON_FAILED
    '        'will return.  Trap the error in the ErrorHandler
    '        objSession.Logon(ShowDialog:=False, NewSession:=False)
    '        Return objSession
    '    Catch ex As System.Runtime.InteropServices.COMException
    '        Select Case ex.ErrorCode
    '            Case -2147221231  'MAPI_E_LOGON_FAILED
    '                sDefaultUserProfile = _
    '                   Registry.GetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows NT\" & _
    '                   "CurrentVersion\" & _
    '                   "Windows Messaging Subsystem\Profiles", "DefaultProfile", Nothing)
    '                objSession.Logon(ProfileName:=sDefaultUserProfile, ShowDialog:=False)

    '                Return objSession
    '            Case Else
    '                MsgBox("An error has occured while attempting" & Chr(10) & _
    '                "To create and logon to a new CDO (1.x) session." & _
    '                Chr(10) & "Please report the following error to your " & _
    '                "System Administrator." & Chr(10) & Chr(10) & _
    '                "Error Location: frmMain.StartMessagingAndLogon" & _
    '                Chr(10) & "Error Number: " & ex.ErrorCode & Chr(10) & _
    '                "Description: " & ex.Message)
    '                Return Nothing
    '        End Select
    '    End Try
    'End Function

    Public Shared Sub GetDefaultForm()
        Try
            Dim commSQL As New SqlClient.SqlCommand
            Dim datRead As SqlClient.SqlDataReader
            Dim strSQL As String

            strSQL = "select TAB_NM, DEFAULT_FORM_NM, HEADER_1_NM from maarten.V_OMNI_Default_Form order by PERSON_USER_CD"

            commSQL.Connection = clsGlobal.Connection
            commSQL.CommandType = CommandType.Text
            commSQL.CommandText = strSQL

            datRead = commSQL.ExecuteReader()

            If Not datRead.HasRows Then
                clsGlobal.CurrentTab = "Case"
                clsGlobal.CurrentFormName = "Case File"
                clsGlobal.CurrentMenuItem = "Case File"
                clsGlobal.CurrentHeader = ""
            End If

            Do While datRead.Read
                clsGlobal.CurrentTab = datRead("TAB_NM").ToString
                clsGlobal.CurrentFormName = datRead("DEFAULT_FORM_NM").ToString
                clsGlobal.CurrentMenuItem = datRead("DEFAULT_FORM_NM").ToString
                clsGlobal.CurrentHeader = datRead("HEADER_1_NM").ToString
            Loop
            datRead.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "GetDefaultForm")
        End Try

    End Sub
    Public Shared Function UpperFirst(ByVal strLineOfText As String) As String
        Dim strFinishedString As String = ""
        Dim intCounter As Integer
        Dim strLastChar As String = "99"
        Dim strCurrentChar As String = ""

        strLineOfText = LCase(strLineOfText)
        For intCounter = 1 To Len(strLineOfText)
            strCurrentChar = Mid(strLineOfText, intCounter, 1)
            If intCounter = 1 Then strCurrentChar = UCase(strCurrentChar)
            If strLastChar = " " Then strCurrentChar = UCase(strCurrentChar)
            strFinishedString = strFinishedString & strCurrentChar
            strLastChar = strCurrentChar
        Next intCounter

        Return strFinishedString
    End Function
    Public Shared Sub SetFileReadOnly(ByVal strFileName As String)
        Try
            If Not ((File.GetAttributes(strFileName) And FileAttributes.ReadOnly) = FileAttributes.ReadOnly) Then
                File.SetAttributes(strFileName, FileAttributes.ReadOnly)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Process Failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Public Shared Function TidyExceptionMessage(ByVal strExceptionMessage As String) As String

        Return Left(strExceptionMessage, InStr(strExceptionMessage, ".", CompareMethod.Text) - 1) & "."

    End Function



#Region " Enable\Disable a Form's Close Button "
    Private Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Integer, ByVal revert As Integer) As Integer
    Private Declare Function EnableMenuItem Lib "user32" (ByVal menu As Integer, ByVal ideEnableItem As Integer, ByVal enable As Integer) As Integer
    Private Const SC_CLOSE As Integer = &HF060
    Private Const MF_BYCOMMAND As Integer = &H0
    Private Const MF_GRAYED As Integer = &H1
    Private Const MF_ENABLED As Integer = &H0

    Public Shared Sub DisableFormCloseButton(ByVal form As System.Windows.Forms.Form)
        ' The return value specifies the previous state of the menu item (it is either
        ' MF_ENABLED or MF_GRAYED). 0xFFFFFFFF indicates that the menu item does not exist.
        Select Case EnableMenuItem(GetSystemMenu(form.Handle.ToInt32, 0), SC_CLOSE, MF_BYCOMMAND Or MF_GRAYED)
            Case MF_ENABLED
            Case MF_GRAYED
            Case &HFFFFFFFF
                Throw New Exception("The Close menu item does not exist.")
            Case Else
        End Select
    End Sub

    Public Shared Sub EnableFormCloseButton(ByVal form As System.Windows.Forms.Form)
        EnableMenuItem(GetSystemMenu(form.Handle.ToInt32, 0), SC_CLOSE, MF_BYCOMMAND)
    End Sub

#End Region

    Public Sub CalculateRemainingText(ByVal lblCharactersRemaining As Label, ByVal txtNotes As TextBox)
        'lblCharactersRemaining.Text = "Characters Remaining: " & txtNotes.MaxLength - Microsoft.VisualBasic.Len(txtNotes.Text)
        lblCharactersRemaining.Text = "CR: " & txtNotes.MaxLength - Microsoft.VisualBasic.Len(txtNotes.Text)
    End Sub
    Public Sub CreateEmail(ByVal strEmailRecipient As String, ByVal strEmailBody As String,
                           ByVal strSubject As String, Optional ByVal strCCRecipient As String = "")
        Try
            Dim appOutlook As New Outlook.Application()
            Dim mailOutlook As Outlook.MailItem

            mailOutlook = appOutlook.CreateItem(Outlook.OlItemType.olMailItem)
            mailOutlook.To = strEmailRecipient
            mailOutlook.Subject = strSubject

            If strCCRecipient <> "" Then mailOutlook.CC = strCCRecipient

            If clsGlobal.Server.ToString = "=omnidv.corp.db" Or clsGlobal.Server.ToString = "=omniqa.corp.db" Or clsGlobal.Server.ToString = "=44SQLdvC02\prmaint" Then
                mailOutlook.CC = ""

                If Microsoft.VisualBasic.Right(strEmailRecipient, 11) = "@cartus.com" Then
                    mailOutlook.Subject = "***TEST*** " & strSubject
                Else
                    Dim comSQL As New SqlClient.SqlCommand
                    comSQL.CommandType = CommandType.StoredProcedure
                    comSQL.Connection = clsGlobal.Connection
                    comSQL.CommandText = "maarten.OMNI_GET_EMAIL_ADDRESS"
                    comSQL.Parameters.Add("@UserNm", SqlDbType.VarChar, 15).Value = clsGlobal.PersonName

                    Dim strEmail As String = comSQL.ExecuteScalar

                    mailOutlook.To = strEmail
                    mailOutlook.Subject = "***TEST*** " & strSubject
                    mailOutlook.Subject = mailOutlook.Subject & " (email meant for " & strEmailRecipient & ")"
                End If
            End If

            mailOutlook.BodyFormat = Outlook.OlBodyFormat.olFormatHTML
            mailOutlook.HTMLBody = strEmailBody
            mailOutlook.Send()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "CreateEmail", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Shared Sub SaveBenchmark(ByRef strFormNm As String, strProcedureNm As String, dteStartDt As Date, dteEndDt As Date, strVariables As String)
        Try
            Dim commSQL As New SqlClient.SqlCommand
            commSQL.CommandType = CommandType.StoredProcedure
            commSQL.CommandText = "maarten.OMNI_SAVE_BENCHMARK"
            commSQL.Connection = clsGlobal.Connection

            commSQL.Parameters.Add("@FormNm", SqlDbType.VarChar, 255).Value = strFormNm
            commSQL.Parameters.Add("@ProcedureNm", SqlDbType.VarChar, 255).Value = strProcedureNm
            commSQL.Parameters.Add("@StartDt", SqlDbType.VarChar, 20).Value = dteStartDt.ToString("yyyyMMdd HH:mm:ss")
            commSQL.Parameters.Add("@EndDt", SqlDbType.VarChar, 20).Value = dteEndDt.ToString("yyyyMMdd HH:mm:ss")
            commSQL.Parameters.Add("@Variables", SqlDbType.VarChar, 2000).Value = strVariables

            commSQL.ExecuteNonQuery()
            commSQL.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "SaveBenchmark", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


End Class

