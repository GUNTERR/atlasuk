Public Class clsEnvironment
    Private Shared mstrApplicationLocationUNC As String
    Private Shared mstrDocumentCentreLocationUNC As String
    Private Shared mstrMODSpreadsheetTestUNC As String
    Private Shared mstrMODSpreadsheetUNC As String
    Private Shared mstrHSNBacsFileTestUNC As String
    Private Shared mstrHSNBacsFileUNC As String
    Private Shared mstrClientPolicyDefaultUNC As String


    Public Shared Property ApplicationLocationUNC() As String
        Get
            Return mstrApplicationLocationUNC
        End Get
        Set(ByVal value As String)
            mstrApplicationLocationUNC = value
        End Set
    End Property
    Public Shared Property DocumentCentreLocationUNC() As String
        Get
            Return mstrDocumentCentreLocationUNC
        End Get
        Set(ByVal value As String)
            mstrDocumentCentreLocationUNC = value
        End Set
    End Property
    Public Shared Property MODSpreadsheetUNC() As String
        Get
            Return mstrMODSpreadsheetUNC
        End Get
        Set(ByVal value As String)
            mstrMODSpreadsheetUNC = value
        End Set
    End Property
    Public Shared Property MODSpreadsheetTestUNC() As String
        Get
            Return mstrMODSpreadsheetTestUNC
        End Get
        Set(ByVal value As String)
            mstrMODSpreadsheetTestUNC = value
        End Set
    End Property
    Public Shared Property HSNBacsFileUNC() As String
        Get
            Return mstrHSNBacsFileUNC
        End Get
        Set(ByVal value As String)
            mstrHSNBacsFileUNC = value
        End Set
    End Property
    Public Shared Property HSNBacsFileTestUNC() As String
        Get
            Return mstrHSNBacsFileTestUNC
        End Get
        Set(ByVal value As String)
            mstrHSNBacsFileTestUNC = value
        End Set
    End Property
    Public Shared Property ClientPolicyDefaultUNC() As String
        Get
            Return mstrClientPolicyDefaultUNC
        End Get
        Set(ByVal value As String)
            mstrClientPolicyDefaultUNC = value
        End Set
    End Property
End Class
