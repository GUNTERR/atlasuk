Public Class clsSanctionedCountry
    Public Function CheckSanctionedCountry(ByVal txtCountry As String)

        Dim strSQL As New SqlClient.SqlCommand
        Dim strReturn As String

        strSQL.Connection = clsGlobal.Connection
        strSQL.CommandType = CommandType.StoredProcedure
        strSQL.CommandText = "maarten.OMNI_CHECK_SANCTIONED_COUNTRY"

        strSQL.Parameters.Add("@Country", SqlDbType.VarChar, 50).Value = txtCountry

        strReturn = strSQL.ExecuteScalar()

        If strReturn = "Y" Then
            MessageBox.Show("Legal approval required before authorizing any moves or services in the country selected.", "Sanctioned Country", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        Return True

    End Function
End Class
