﻿Imports System.IO
Public Class clsDocumentCentre

    'Public Function AddDocumentCentreScanned(ByVal intCaseNo As Integer, ByVal intPackageid As Integer, _
    '    ByVal strAddrAstType As String, ByVal intCCPGeneratedNo As Integer, ByVal intWorkRequestNo As Integer, _
    '    ByVal strTabCd As String) As Integer

    '    Dim strFileName As String
    '    Dim strSavePath As String

    '    If clsGlobal.Database.ToString = "=topwin_prod" Then
    '        strSavePath = clsEnvironment.DocumentCentreLocationUNC & "\Case\"
    '    Else
    '        strSavePath = clsEnvironment.DocumentCentreLocationUNC & "\Test\Case\"
    '    End If

    '    strSavePath += intCaseNo.ToString
    '    strSavePath += "\" & strTabCd & "\Document"
    '    Directory.CreateDirectory(strSavePath)





    'End Function
   
    Public Function AddDocumentCentreDocument(ByVal intCaseNo As Integer, ByVal strTabCd As String) As Integer
        'AddDocumentCentreDocument(ByVal intCaseNo As Integer, ByVal strTabCd As String, ByVal strDocumentName As String)
        Dim strFileName As String
        Dim strSourceFileName As String
        Dim OpenFileDialog1 As New OpenFileDialog

        OpenFileDialog1.InitialDirectory = "F:\Domestic"
        OpenFileDialog1.FileName = Nothing
        OpenFileDialog1.Filter = "(*.doc;*.txt;*.pdf;*.xls;*.ppt;*.docx;*.xlsx;*.pptx)|*.doc;*.txt;*.pdf;*.xls;*.ppt;*.docx;*.xlsx;*.pptx"
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()

        If (result = DialogResult.OK) Then
            strSourceFileName = OpenFileDialog1.FileName
            strFileName = Microsoft.VisualBasic.Mid(strSourceFileName, Microsoft.VisualBasic.InStrRev(strSourceFileName, "\") + 1)
            Dim strSavePath As String

            If intCaseNo > 0 Then

                If clsGlobal.Database.ToString = "=topwin_prod" Then
                    strSavePath = clsEnvironment.DocumentCentreLocationUNC & "\Case\"
                Else
                    strSavePath = clsEnvironment.DocumentCentreLocationUNC & "\Test\Case\"
                End If

                strSavePath += intCaseNo.ToString
                strSavePath += "\" & strTabCd & "\Document"
                Directory.CreateDirectory(strSavePath)

                If My.Computer.FileSystem.DirectoryExists(strSavePath) Then
                    strSavePath += "\"
                    strSavePath += strFileName
                End If

                If My.Computer.FileSystem.FileExists(strSavePath) Then
                    MessageBox.Show("A document with this name already exists. Please enter a unique name for the document.", "Process failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return 0
                Else
                    Dim strSQL As String
                    Dim commSQL As New SqlClient.SqlCommand
                    Dim intGeneratedNo As Integer

                    strSQL = "maarten.OMNI_FILE_CASE_DOCUMENT"

                    commSQL.Connection = clsGlobal.Connection
                    commSQL.CommandType = CommandType.StoredProcedure

                    commSQL.CommandText = strSQL
                    commSQL.Parameters.Add("@CaseNo", SqlDbType.Int).Value = intCaseNo
                    commSQL.Parameters.Add("@DocumentNm", SqlDbType.VarChar).Value = strFileName
                    commSQL.Parameters.Add("@TabCd", SqlDbType.Char).Value = strTabCd

                    intGeneratedNo = commSQL.ExecuteScalar
                    If intGeneratedNo > 0 Then
                        My.Computer.FileSystem.CopyFile(strSourceFileName, strSavePath)
                        clsGlobal.SetFileReadOnly(strSavePath)
                        Return intGeneratedNo
                    End If
                End If
            End If

        ElseIf (result = DialogResult.Cancel) Then
            Return 0
        End If
    End Function
    Public Sub OpenDocumentCentreDocument(ByVal intCaseNo As Integer, ByVal strTabCd As String, ByVal strDocumentName As String)
        Dim txtRootFolderPath As String
        Dim newProc As New Process

        If clsGlobal.Database.ToString = "=topwin_prod" Then
            txtRootFolderPath = clsEnvironment.DocumentCentreLocationUNC & "\Case\"
        Else
            txtRootFolderPath = clsEnvironment.DocumentCentreLocationUNC & "\Test\Case\"
        End If

        txtRootFolderPath += intCaseNo.ToString
        txtRootFolderPath += "\" & strTabCd
        txtRootFolderPath += "\Document"
        txtRootFolderPath += "\"
        txtRootFolderPath += strDocumentName

        newProc.StartInfo.FileName = txtRootFolderPath
        newProc.Start()

    End Sub

End Class
