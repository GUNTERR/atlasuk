Public Class clsBudgetInstruction
    Private Shared dblBudgetMilestoneAmount As Double
    Private Shared dblBudgetAmount As Double
    Private Shared dblTotalSpentAmount As Double

    Public Shared Property BudgetMilestoneAmount() As Double
        Get
            Return dblBudgetMilestoneAmount
        End Get
        Set(ByVal value As Double)
            dblBudgetMilestoneAmount = value
        End Set
    End Property
    Public Shared Property BudgetAmount() As Double
        Get
            Return dblBudgetAmount
        End Get
        Set(ByVal value As Double)
            dblBudgetAmount = value
        End Set
    End Property
    Public Shared Property TotalSpentAmount() As Double
        Get
            Return dblTotalSpentAmount
        End Get
        Set(ByVal value As Double)
            dblTotalSpentAmount = value
        End Set
    End Property
    Public Shared Sub GetTotalSpent(ByVal intCaseNo As Integer)
        Dim commSQL As New SqlClient.SqlCommand
        Dim datRead As SqlClient.SqlDataReader

        commSQL.Connection = clsGlobal.Connection
        commSQL.CommandType = CommandType.StoredProcedure
        commSQL.CommandText = "maarten.OMNI_GET_TOTAL_SPENT"
        commSQL.Parameters.Add("@CaseNo", SqlDbType.Int).Value = intCaseNo

        datRead = commSQL.ExecuteReader
        While datRead.Read
            TotalSpentAmount = CType(datRead("TOTAL_NET_AM").ToString(), Double)
            BudgetAmount = CType(datRead("BUDGET_AM").ToString(), Double)
            BudgetMilestoneAmount = CType(datRead("BUDGET_MILESTONE_AM").ToString(), Double)
        End While

        datRead.Close()
    End Sub
End Class
