Imports System.Windows.Forms
Imports System.Drawing

Public Class MaskedDateColumn
    Inherits DataGridViewColumn

    Public Sub New()
        MyBase.New(New DateCell())
    End Sub

    Public Overrides Property CellTemplate() As DataGridViewCell
        Get
            Return MyBase.CellTemplate
        End Get
        Set(ByVal value As DataGridViewCell)

            ' Ensure that the cell used for the template is a DateCell.
            If (value IsNot Nothing) AndAlso _
                Not value.GetType().IsAssignableFrom(GetType(DateCell)) _
                Then
                Throw New InvalidCastException("Must be a DateCell")
            End If
            MyBase.CellTemplate = value

        End Set
    End Property

End Class

Public Class clsBindingSourceItem
    Inherits System.Collections.CollectionBase

    Public Sub Add(ByVal rpiX As BindingSource)
        List.Add(rpiX)
    End Sub

    Default Public ReadOnly Property Item(ByVal i As Integer) As clsBindingSourceItem
        Get
            Return CType(List.Item(i), clsBindingSourceItem)
        End Get
    End Property
End Class

Public Class clnComboBoxSource
    Inherits System.Collections.CollectionBase

    Public Sub Add(ByVal rpiX As clsComboBoxSource)
        List.Add(rpiX)
    End Sub

    Default Public ReadOnly Property Item(ByVal i As Integer) As clsComboBoxSource
        Get
            Return CType(List.Item(i), clsComboBoxSource)
        End Get
    End Property

End Class
Public Class clsComboBoxSource

    Shared i As Integer = 0
    Dim mcboX As Object, mstrDomain As String, mstrLookupListName As String

    Public Sub New(ByVal cboX As Object, ByVal strDomain As String, Optional ByVal blnResetCount As Boolean = False)

        If blnResetCount Then i = 0
        mcboX = cboX
        mstrDomain = strDomain
        mstrLookupListName = strDomain & i.ToString
        i += 1
    End Sub
    Public Property ComboBoxControl() As Object
        Get
            Return mcboX
        End Get
        Set(ByVal value As Object)
            mcboX = value
        End Set
    End Property

    Public Property Domain() As String
        Get
            Return mstrDomain
        End Get
        Set(ByVal value As String)
            mstrDomain = value
        End Set
    End Property

    Public ReadOnly Property LookupListName() As String
        Get
            Return mstrLookupListName
        End Get
    End Property

End Class

Public Class clnFormControl
    Inherits System.Collections.CollectionBase

    Dim msrlX As New SortedList

    Public Sub Add(ByVal fclX As clsFormControl)
        List.Add(fclX)
        Do Until msrlX.IndexOfKey(fclX.TabIndex) < 0
            fclX.TabIndex += 1
        Loop
        msrlX.Add(fclX.TabIndex, fclX)
    End Sub

    Default Public ReadOnly Property Item(ByVal i As Integer) As clsFormControl
        Get
            Return CType(List.Item(i), clsFormControl)
        End Get
    End Property

    Default Public ReadOnly Property Item(ByVal strName As String) As clsFormControl
        Get
            Dim i As Integer

            For i = 0 To List.Count - 1
                If CType(List.Item(i), clsFormControl).Name = strName Then
                    Return CType(List.Item(i), clsFormControl)
                End If
            Next
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property ControlsByTabIndex() As SortedList
        Get
            Return msrlX
        End Get
    End Property

    Public Sub FindTextForControl(ByRef fclX As clsFormControl)

        Dim i As Integer, fclLabel As clsFormControl, fclNearest As clsFormControl = Nothing
        Dim lngTabIndex As Int64, intMidPointY, intMidPointX, intMidPointLabelX, intMidPointLabelY As Integer

        intMidPointY = fclX.Location.Y + fclX.Size.Height / 2
        intMidPointX = fclX.Location.X + fclX.Size.Width / 2

        'If fclX.Name = "txbVerbalEFAm" Or fclX.Name = "cboServiceCd" Or fclX.Name = "txbCaseNo" Then Stop

        For i = 0 To msrlX.Count - 1
            fclLabel = CType(msrlX.GetByIndex(i), clsFormControl)
            intMidPointLabelY = fclLabel.Location.Y + fclLabel.Size.Height / 2

            If fclLabel.Parent = fclX.Parent And ((fclLabel.Location.Y <= intMidPointY And _
                    fclLabel.Location.Y + fclLabel.Size.Height >= intMidPointY) Or _
                        (fclX.Location.Y <= intMidPointLabelY And _
                        fclX.Location.Y + fclX.Size.Height >= intMidPointLabelY)) And _
                        fclLabel.Location.X < fclX.Location.X Then
                ' On 'same line'
                If fclNearest Is Nothing Then
                    fclNearest = fclLabel
                    lngTabIndex = fclLabel.TabIndex
                Else
                    If fclLabel.Location.X > fclNearest.Location.X Then
                        fclNearest = fclLabel
                        lngTabIndex = fclLabel.TabIndex
                    End If
                End If
            End If
        Next
        If fclNearest Is Nothing OrElse fclNearest.ControlType <> clsFormControl.enControlType.Label Then ' See if label is above control
            fclNearest = Nothing
            For i = 0 To msrlX.Count - 1
                fclLabel = CType(msrlX.GetByIndex(i), clsFormControl)
                intMidPointLabelX = fclLabel.Location.X + fclLabel.Size.Width / 2

                If fclLabel.Parent = fclX.Parent And ((fclLabel.Location.X <= intMidPointX And _
                        fclLabel.Location.X + fclLabel.Size.Width >= intMidPointX) Or _
                        (fclX.Location.X <= intMidPointLabelX And _
                        fclX.Location.X + fclX.Size.Width >= intMidPointLabelX)) And _
                        fclLabel.Location.Y < fclX.Location.Y Then
                    ' In 'same column'
                    If fclNearest Is Nothing Then
                        fclNearest = fclLabel
                        lngTabIndex = fclLabel.TabIndex
                    Else
                        If fclLabel.Location.Y > fclNearest.Location.Y Then
                            fclNearest = fclLabel
                            lngTabIndex = fclLabel.TabIndex
                        End If
                    End If
                End If
            Next
        End If
        If fclNearest IsNot Nothing Then fclX.Text = fclNearest.Text

    End Sub
End Class
Public Class clsFormControl

    Dim mectX As enControlType, mszeX As Size, mpntLocation As Point, mintTabIndex As Int64 = 0, mstrValue As String
    Dim mstrName, mstrParentName, mstrText As String, mstrAncestor As String, mblnIsTitle As Boolean = False
    Dim intCount As Integer = 1, mdgvX As DataGridView

    Public Enum enControlType
        TextBox
        ComboBox
        CheckBox
        Label
        DataGridView
        LinkLabel
    End Enum

    Public Property ControlType() As enControlType
        Get
            Return mectX
        End Get
        Set(ByVal value As enControlType)
            mectX = value
        End Set
    End Property

    Public Property Size() As Size
        Get
            Return mszeX
        End Get
        Set(ByVal value As Size)
            mszeX = value
        End Set
    End Property

    Public Property Location() As Point
        Get
            Return mpntLocation
        End Get
        Set(ByVal value As Point)
            mpntLocation = value
        End Set
    End Property
    Public Property TabIndex() As Int64
        Get
            Return mintTabIndex
        End Get
        Set(ByVal value As Int64)
            mintTabIndex = value
        End Set
    End Property
    Public Property Ancestor() As String
        Get
            Return mstrAncestor
        End Get
        Set(ByVal value As String)
            mstrAncestor = value
        End Set
    End Property

    Public Property Value() As String
        Get
            Return mstrValue
        End Get
        Set(ByVal strValue As String)
            mstrValue = strValue
        End Set
    End Property

    Public Property Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal strValue As String)
            mstrName = strValue
        End Set
    End Property
    Public Property Text() As String
        Get
            Return mstrText
        End Get
        Set(ByVal strValue As String)
            mstrText = strValue
        End Set
    End Property
    Public Property Parent() As String
        Get
            Return mstrParentName
        End Get
        Set(ByVal strValue As String)
            mstrParentName = strValue
        End Set
    End Property

    Public Property IsTitle() As Boolean
        Get
            Return mblnIsTitle
        End Get
        Set(ByVal Value As Boolean)
            mblnIsTitle = Value
        End Set
    End Property

    Public Property ControlCount() As Integer
        Get
            Return intCount
        End Get
        Set(ByVal value As Integer)
            intCount = value
        End Set
    End Property

    Public Property GridControl() As DataGridView
        Get
            Return mdgvX
        End Get
        Set(ByVal value As DataGridView)
            mdgvX = value
        End Set
    End Property
End Class

Public Class clnFormDatatable
    Inherits System.Collections.CollectionBase

    Public Sub Add(ByVal rpiX As clsFormDatatable)
        List.Add(rpiX)
    End Sub

    Default Public ReadOnly Property Item(ByVal i As Integer) As clsFormDatatable
        Get
            Return CType(List.Item(i), clsFormDatatable)
        End Get
    End Property

End Class
Public Class clsFormDatatable

    Dim mdttX As New DataTable, mblnAllowAdd, mblnAllowUpdate, mblnAllowDelete, mblnQueryingDatatable As Boolean
    Dim mbsX As New BindingSource, mtaX As New Object, mdgvX As DataGridView, mpnlX As Panel

    Public Sub New(ByVal dttx As DataTable, ByVal bsX As BindingSource, ByVal taX As Object, ByVal dgvX As DataGridView, _
    ByVal pnlHostPanel As Panel, ByVal blnAllowAdd As Boolean, ByVal blnAllowUpdate As Boolean, _
            ByVal blnAllowDelete As Boolean, ByVal blnQueryingDatatable As Boolean)

        mdttX = dttx
        mbsX = bsX
        mtaX = taX
        mdgvX = dgvX
        mpnlX = pnlHostPanel
        mblnAllowAdd = blnAllowAdd
        mblnAllowUpdate = blnAllowUpdate
        mblnAllowDelete = blnAllowDelete
        mblnQueryingDatatable = blnQueryingDatatable

    End Sub

    Public Property Datatable() As DataTable
        Get
            Return mdttX
        End Get
        Set(ByVal value As DataTable)
            mdttX = value
        End Set
    End Property

    Public Property AllowAdd() As Boolean
        Get
            Return mblnAllowAdd
        End Get
        Set(ByVal value As Boolean)
            mblnAllowAdd = value
        End Set
    End Property

    Public Property AllowUpdate() As Boolean
        Get
            Return mblnAllowUpdate
        End Get
        Set(ByVal value As Boolean)
            mblnAllowUpdate = value
        End Set
    End Property

    Public Property AllowDelete() As Boolean
        Get
            Return mblnAllowDelete
        End Get
        Set(ByVal value As Boolean)
            mblnAllowDelete = value
        End Set
    End Property

    Public Property QueryingDatatable() As Boolean
        Get
            Return mblnQueryingDatatable
        End Get
        Set(ByVal value As Boolean)
            mblnQueryingDatatable = value
        End Set
    End Property

    Public Property BindingSource() As BindingSource
        Get
            Return mbsX
        End Get
        Set(ByVal value As BindingSource)
            mbsX = value
        End Set
    End Property

    Public Property TableAdapter() As Object
        Get
            Return mtaX
        End Get
        Set(ByVal value As Object)
            mtaX = value
        End Set
    End Property
    Public Property DataGridView() As DataGridView
        Get
            Return mdgvX
        End Get
        Set(ByVal value As DataGridView)
            mdgvX = value
        End Set
    End Property

    Public Property HostPanel() As Panel
        Get
            Return mpnlX
        End Get
        Set(ByVal value As Panel)
            mpnlX = value
        End Set
    End Property

End Class



Public Class DateCell
    Inherits DataGridViewTextBoxCell

    Public Sub New()
        ' Use the short date format.
        Me.Style.Format = "d"
    End Sub

    Public Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, _
        ByVal initialFormattedValue As Object, _
        ByVal dataGridViewCellStyle As DataGridViewCellStyle)

        ' Set the value of the editing control to the current cell value.
        MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, _
            dataGridViewCellStyle)

        Dim ctl As MaskedDateEditingControl = _
            CType(DataGridView.EditingControl, MaskedDateEditingControl)
        ctl.Text = Me.Value.ToString

    End Sub

    Public Overrides ReadOnly Property EditType() As Type
        Get
            ' Return the type of the editing contol that DateCell uses.
            Return GetType(MaskedDateEditingControl)
        End Get
    End Property

    Public Overrides ReadOnly Property ValueType() As Type
        Get
            ' Return the type of the value that DateCell contains.
            Return GetType(DateTime)
        End Get
    End Property

    'Public Overrides ReadOnly Property DefaultNewRowValue() As Object
    '    Get
    '        ' Use the current date and time as the default value.
    '        Return DateTime.Now
    '    End Get
    'End Property

End Class

Class MaskedDateEditingControl
    Inherits MaskedTextBox
    Implements IDataGridViewEditingControl

    Private dataGridViewControl As DataGridView
    Private valueIsChanged As Boolean = False
    Private rowIndexNum As Integer

    Public Sub New()
        Me.Mask = "00/00/0000"
    End Sub

    Public Property EditingControlFormattedValue() As Object _
        Implements IDataGridViewEditingControl.EditingControlFormattedValue

        Get
            Return Me.Text
        End Get

        Set(ByVal value As Object)
            If TypeOf value Is String Then
                Me.Text = value
            End If
        End Set

    End Property

    Public Function GetEditingControlFormattedValue(ByVal context _
        As DataGridViewDataErrorContexts) As Object _
        Implements IDataGridViewEditingControl.GetEditingControlFormattedValue

        Dim strRetVal As String
        If IsDate(Me.Text) Then
            strRetVal = Me.Text
        Else
            strRetVal = ""
        End If
        Return strRetVal

    End Function

    Public Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As _
        DataGridViewCellStyle) _
        Implements IDataGridViewEditingControl.ApplyCellStyleToEditingControl

        Me.Font = dataGridViewCellStyle.Font

    End Sub

    Public Property EditingControlRowIndex() As Integer _
        Implements IDataGridViewEditingControl.EditingControlRowIndex

        Get
            Return rowIndexNum
        End Get
        Set(ByVal value As Integer)
            rowIndexNum = value
        End Set

    End Property

    Public Function EditingControlWantsInputKey(ByVal key As Keys, _
        ByVal dataGridViewWantsInputKey As Boolean) As Boolean _
        Implements IDataGridViewEditingControl.EditingControlWantsInputKey

        ' Let the DateTimePicker handle the keys listed.
        Select Case key And Keys.KeyCode
            Case Keys.Left, Keys.Up, Keys.Down, Keys.Right, _
                Keys.Home, Keys.End, Keys.PageDown, Keys.PageUp

                Return True

            Case Else
                Return False
        End Select

    End Function

    Public Sub PrepareEditingControlForEdit(ByVal selectAll As Boolean) _
        Implements IDataGridViewEditingControl.PrepareEditingControlForEdit

        ' No preparation needs to be done.

    End Sub

    Public ReadOnly Property RepositionEditingControlOnValueChange() _
        As Boolean Implements _
        IDataGridViewEditingControl.RepositionEditingControlOnValueChange

        Get
            Return False
        End Get

    End Property

    Public Property EditingControlDataGridView() As DataGridView _
        Implements IDataGridViewEditingControl.EditingControlDataGridView

        Get
            Return dataGridViewControl
        End Get
        Set(ByVal value As DataGridView)
            dataGridViewControl = value
        End Set

    End Property

    Public Property EditingControlValueChanged() As Boolean _
        Implements IDataGridViewEditingControl.EditingControlValueChanged

        Get
            Return valueIsChanged
        End Get
        Set(ByVal value As Boolean)
            valueIsChanged = value
        End Set

    End Property

    Public ReadOnly Property EditingControlCursor() As Cursor _
        Implements IDataGridViewEditingControl.EditingPanelCursor

        Get
            Return MyBase.Cursor
        End Get

    End Property

    Protected Overrides Sub OnTextChanged(ByVal eventargs As EventArgs)

        ' Notify the DataGridView that the contents of the cell have changed.
        valueIsChanged = True
        Try
            Me.EditingControlDataGridView.NotifyCurrentCellDirty(True)
        Catch ex As Exception

        End Try
        MyBase.OnTextChanged(eventargs)

    End Sub

End Class


